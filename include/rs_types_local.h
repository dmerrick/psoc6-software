﻿/*
 * Copyright (c) 2018 Socionext Inc.
 * All rights reserved.
 */

/*
 * rs_types_local.h
 */

#ifndef __RS_TYPES_LOCAL_H__
#define __RS_TYPES_LOCAL_H__

#include "rs_types.h"
#include "rs_retcode_local.h"

#ifdef __cplusplus
static inline bool rs_bool_conv(enum RS_BOOL val)
{
	return val == RS_TRUE;
}
static inline enum RS_BOOL rs_bool_conv(bool val)
{
	return val ? RS_TRUE : RS_FALSE;
}
#endif  /* __cplusplus */

#endif	/* __RS_TYPES_LOCAL_H__ */
