﻿/*
 * Copyright (c) 2017 Socionext Inc.
 * All rights reserved.
 */

/*
 * ConvRet.h
 */

#ifndef __ConvRet_H__
#define __ConvRet_H__

#include <stdio.h>
#include <stdint.h>
#include "common.h"

//#define RETMACRO_printf

#ifdef RETMACRO_printf
#define retprintf(fmt, ...)      fprintf(stdout, "%s:%d: " fmt, __FUNCTION__, __LINE__, __VA_ARGS__); fflush(stdout)
#else
#define retprintf(fmt, ...)
#endif

#define RETMACRO_refret(api, refval, retval)										\
{																					\
	const auto ret = (api);															\
	if(ret != refval){																\
		retprintf(																	\
			"%s:%d [error] ref=%x ret=%x\n",										\
			__FUNCTION__, __LINE__, (uint32_t) refval, (uint32_t) ret				\
		);																			\
		return retval;																\
	}																				\
}

#define RETMACRO_refretorg(api, refval)												\
{																					\
	const auto ret = (api);															\
	if(ret != refval){																\
		retprintf(																	\
			"%s:%d [error] ref=%x ret=%x\n",										\
			__FUNCTION__, __LINE__, (uint32_t) refval, (uint32_t) ret				\
		);																			\
		return ret;																	\
	}																				\
}

#define RETMACRO_refretorg_drv(api, refval)											\
{																					\
	const auto ret = (api);															\
	if(ret != refval){																\
		retprintf(																	\
			"%s:%d [error] ref=%x ret=%x\n",										\
			__FUNCTION__, __LINE__, (uint32_t) refval, (uint32_t) ret				\
		);																			\
		return retconv_drv2lib((enum SNI_RET)ret);												\
	}																				\
}

#define RETMACRO_reflog(api, refval, str)											\
{																					\
	const auto ret = (int) (api);													\
	if(ret != refval){																\
		retprintf(																	\
			"%s:%d [error] %s\n",													\
			__FUNCTION__, __LINE__, str												\
		);																			\
	}																				\
}

#endif	/* __ConvRet_H__ */
