﻿/*
 * Copyright (c) 2018 Socionext Inc.
 * All rights reserved.
 */

/*
 * rs_retcode.h
 */

#ifndef __RS_RETCODE_H__
#define __RS_RETCODE_H__

#define RS_OK			0
#define RS_EINV			-1
#define RS_ETOUT		-2
#define RS_EDEV			-3
#define RS_ENOSUPPORT	-4
#define RS_ESTATE		-5
#define RS_ECOM			-6
#define RS_ENOMEM		-7
#define RS_EFAULT		-8
#define RS_ELIB			-9
#define RS_EFORM		-10

#endif	/* __RS_RETCODE_H__ */
