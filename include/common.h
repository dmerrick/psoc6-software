﻿/*
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

/*
 * common.h
 */

#ifndef __common_H__
#define __common_H__

#ifdef WIN32
#include <windows.h>
#endif
#include <cstdint>

#include "rs_retcode.h"
#include "sni_rs_common.h"

// convert uint8_t [] -> uint32_t [] (Little Endian)
inline void array2uint32(const uint8_t *in, size_t in_size, uint32_t *out, size_t width = 4)
{
	uint8_t *lout = reinterpret_cast<uint8_t *>(out);

	for(size_t i=0; i<in_size; i++){
		const size_t s = i % width;
		const size_t is = width - 1 - s;
		lout[is] = in[s];
		if(is == 0){
			in += width;
			lout += width;
		}
	}
}

inline void conv_8to16_be(uint16_t *out, uint8_t *d)
{
	*out = 0;
	*out |= (uint16_t)((d[0] << 8) & 0xff00);
	*out |= (uint16_t)((d[1] << 0) & 0x00ff);
}

inline void conv_8to32_be(uint32_t *out, uint8_t *d)
{
	*out = 0;
	*out |= (uint32_t)((d[0] << 24) & 0xff000000);
	*out |= (uint32_t)((d[1] << 16) & 0x00ff0000);
	*out |= (uint32_t)((d[2] <<  8) & 0x0000ff00);
	*out |= (uint32_t)((d[3] <<  0) & 0x000000ff);
}

inline void conv_16to8_be(uint8_t *out, uint16_t *d)
{
	out[0] = (uint8_t)(*d >>  8);
	out[1] = (uint8_t)(*d >>  0);
}

inline void conv_32to8_be(uint8_t *out, uint32_t *d)
{
	out[0] = (uint8_t)(*d >> 24);
	out[1] = (uint8_t)(*d >> 16);
	out[2] = (uint8_t)(*d >>  8);
	out[3] = (uint8_t)(*d >>  0);
}

inline void conv_8to16_le(uint16_t *out, uint8_t *d)
{
	*out = 0;
	*out |= (uint16_t)((d[0] << 0) & 0x00ff);
	*out |= (uint16_t)((d[1] << 8) & 0xff00);
}

inline void conv_8to32_le(uint32_t *out, uint8_t *d)
{
	*out = 0;
	*out |= (uint32_t)((d[0] <<  0) & 0x000000ff);
	*out |= (uint32_t)((d[1] <<  8) & 0x0000ff00);
	*out |= (uint32_t)((d[2] << 16) & 0x00ff0000);
	*out |= (uint32_t)((d[3] << 24) & 0xff000000);
}

inline void conv_16to8_le(uint8_t *out, uint16_t *d)
{
	out[0] = (uint8_t)(*d >>  0);
	out[1] = (uint8_t)(*d >>  8);
}
inline void conv_32to8_le(uint8_t *out, uint32_t *d)
{
	out[0] = (uint8_t)(*d >>  0);
	out[1] = (uint8_t)(*d >>  8);
	out[2] = (uint8_t)(*d >> 16);
	out[3] = (uint8_t)(*d >> 24);
}

inline enum SNI_RET retconv_lib2drv(int ret)
{
	switch(ret) {
		case RS_OK:				return SNI_RET_OK;
		case RS_EINV:			return SNI_RET_EINV;
		case RS_ETOUT:			return SNI_RET_EBUSY;
		case RS_EDEV:			return SNI_RET_EDEV;
		case RS_ENOSUPPORT:		return SNI_RET_ECMD;
		case RS_ESTATE:			return SNI_RET_ESTATE;
		case RS_ECOM:			return SNI_RET_EDEV;
		case RS_ENOMEM:			return SNI_RET_ENOMEM;
		case RS_EFAULT:			return SNI_RET_ECMD;
		case RS_ELIB:			return SNI_RET_ECMD;
		case RS_EFORM:			return SNI_RET_ECMD;
		default:				return (enum SNI_RET)(-(ret));
	}
}

inline int retconv_drv2lib(enum SNI_RET ret)
{
	switch(ret) {
		case SNI_RET_OK:		return RS_OK;				
		case SNI_RET_EINV:		return RS_EINV;
		case SNI_RET_EDEV:		return RS_EDEV;
		case SNI_RET_ECMD:		return RS_ENOSUPPORT;
		case SNI_RET_ESTATE:	return RS_ESTATE;
		case SNI_RET_ENOMEM:	return RS_ENOMEM;
		case SNI_RET_EBUSY:		return RS_ETOUT;
		case SNI_RET_EDMA12:	return RS_EDEV;
		case SNI_RET_EDMA13:	return RS_EDEV;
		default:				return (int)(-(ret));
	}
}

#endif	/* __common_H__ */
