﻿/*
 * Copyright (c) 2018 Socionext Inc.
 * All rights reserved.
 */

/*
 * rs_types.h
 */

#ifndef __RS_TYPES_H__
#define __RS_TYPES_H__

#include "rs_retcode.h"

enum RS_BOOL {
	RS_TRUE = 0,
	RS_FALSE = -1,
};

#endif	/* __RS_TYPES_H__ */
