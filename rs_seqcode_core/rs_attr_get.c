﻿/*
 * Copyright (c) 2018 Socionext Inc.
 * All rights reserved.
 *
 */

/*
 * rs_attr.c
 */

#ifdef WIN32
#include <windows.h>
#endif
#include <stdint.h>
#include <stdlib.h>

#include "rs_attr.h"
#include "rs_attr_core.h"

DLLAPI int rs_attr_get_modulation_mode(rs_attr_t attr, uint32_t *modu_mode)
{
	if(attr == NULL) {
		return RS_ATTR_EINV;
	}

	*modu_mode = p_rs_attr_t(attr)->modu_mode;

	return 0;
}

DLLAPI int rs_attr_get_txpower(rs_attr_t attr, enum rs_tx_power *tx_power)
{
	if(attr == NULL) {
		return RS_ATTR_EINV;
	}

	*tx_power = (enum rs_tx_power)p_rs_attr_t(attr)->tx_power;

	return 0;
}

DLLAPI int rs_attr_get_rxgain(rs_attr_t attr, enum rs_rx_gain *rx_gain)
{
	if(attr == NULL) {
		return RS_ATTR_EINV;
	}

	*rx_gain = (enum rs_rx_gain)p_rs_attr_t(attr)->rx_gain;

	return 0;
}

DLLAPI int rs_attr_get_center_freq(rs_attr_t attr, uint32_t *center_freq)
{
	if(attr == NULL) {
		return RS_ATTR_EINV;
	}

	*center_freq = p_rs_attr_t(attr)->center_freq;

	return 0;
}

DLLAPI int rs_attr_get_frame_rate(rs_attr_t attr, uint32_t *frame_rate)
{
	if(attr == NULL) {
		return RS_ATTR_EINV;
	}

	*frame_rate = p_rs_attr_t(attr)->frame_rate;

	return 0;
}

DLLAPI int rs_attr_get_chirp_rate_constant(rs_attr_t attr, uint32_t *chirp_rate_constant)
{
	if(attr == NULL) {
		return RS_ATTR_EINV;
	}

	*chirp_rate_constant = p_rs_attr_t(attr)->chirp_rate_constant;

	return 0;
}

DLLAPI int rs_attr_get_data_format(rs_attr_t attr, enum rs_bit_width *bit_width, enum rs_header_mode *header_mode, enum rs_ex_status_header_mode *ex_status_header_mode)
{
	if(attr == NULL) {
		return RS_ATTR_EINV;
	}

	*bit_width = p_rs_attr_t(attr)->bit_width;
	*header_mode = p_rs_attr_t(attr)->header_mode;
	*ex_status_header_mode = p_rs_attr_t(attr)->status_mode;

	return 0;
}

DLLAPI int rs_attr_get_xtal_freq(rs_attr_t attr, enum rs_xtal_freq *xtal_freq)
{
	if(attr == NULL) {
		return RS_ATTR_EINV;
	}

	*xtal_freq = p_rs_attr_t(attr)->xtal_freq;

	return 0;
}

DLLAPI int rs_attr_get_constant_cal(rs_attr_t attr, uint32_t *cal_constant)
{
	if(attr == NULL) {
		return RS_ATTR_EINV;
	}

	*cal_constant = p_rs_attr_t(attr)->calc;

	return 0;
}

DLLAPI int rs_attr_get_init_cal_mode(rs_attr_t attr, enum rs_init_cal_mode *init_cal_mode)
{
	if(attr == NULL) {
		return RS_ATTR_EINV;
	}

	*init_cal_mode = p_rs_attr_t(attr)->init_cal_mode;

	return 0;
}

DLLAPI int rs_attr_get_fft_winfunc(rs_attr_t attr, enum rs_fft_winfunc *fft_winfunc)
{
	if(attr == NULL) {
		return RS_ATTR_EINV;
	}

	*fft_winfunc = p_rs_attr_t(attr)->fft_winfunc;

	return 0;
}

DLLAPI int rs_attr_get_fft_en(rs_attr_t attr, enum rs_fft_en *fft_en)
{
	if(attr == NULL) {
		return RS_ATTR_EINV;
	}

	*fft_en = p_rs_attr_t(attr)->fft_en;

	return 0;
}

DLLAPI int rs_attr_get_fmcw(rs_attr_t attr, struct rs_chirp_fmcw_r *chirp)
{
	if(attr == NULL) {
		return RS_ATTR_EINV;
	}

	if(p_rs_attr_t(attr)->modu_mode != RS_MODU_MODE_FMCW_R) {
		return RS_ATTR_EINV;
	}
	
	chirp->repeat_num				= p_rs_attr_t(attr)->fmcw_r.repeat_num;
	chirp->chirp_fmcw.rxch			= p_rs_attr_t(attr)->fmcw_r.chirp_fmcw.rxch;
	chirp->chirp_fmcw.txch			= p_rs_attr_t(attr)->fmcw_r.chirp_fmcw.txch;
                                                                          
	chirp->chirp_fmcw.iq_mode		= p_rs_attr_t(attr)->fmcw_r.chirp_fmcw.iq_mode;
	chirp->chirp_fmcw.chirp_mode	= p_rs_attr_t(attr)->fmcw_r.chirp_fmcw.chirp_mode;
	chirp->chirp_fmcw.band_width	= p_rs_attr_t(attr)->fmcw_r.chirp_fmcw.band_width;
	chirp->chirp_fmcw.sweep_time	= p_rs_attr_t(attr)->fmcw_r.chirp_fmcw.sweep_time;
	chirp->chirp_fmcw.point_num		= p_rs_attr_t(attr)->fmcw_r.chirp_fmcw.point_num;
                                                                          
	chirp->chirp_fmcw.lpf_fc		= p_rs_attr_t(attr)->fmcw_r.chirp_fmcw.lpf_fc;
	chirp->chirp_fmcw.hpf_fc		= p_rs_attr_t(attr)->fmcw_r.chirp_fmcw.hpf_fc;			     

	return 0;
}

DLLAPI int rs_attr_get_fmcw_multi(rs_attr_t attr, struct rs_chirp_fmcw_multi *chirp)
{
	int i;

	if(attr == NULL) {
		return RS_ATTR_EINV;
	}

	if(p_rs_attr_t(attr)->modu_mode != RS_MODU_MODE_FMCW_M) {
		return RS_ATTR_EINV;
	}

	chirp->repeat_num      = p_rs_attr_t(attr)->fmcw_m.repeat_num;
	chirp->multi_chirp_num = p_rs_attr_t(attr)->fmcw_m.multi_chirp_num;

	for(i = 0; i < chirp->multi_chirp_num; i++) {
		chirp->chirp_fmcw[i].rx_gain    = p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].rx_gain;
		chirp->chirp_fmcw[i].rxch       = p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].rxch;
		chirp->chirp_fmcw[i].txch       = p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].txch;
		chirp->chirp_fmcw[i].iq_mode    = p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].iq_mode;
		chirp->chirp_fmcw[i].chirp_mode = p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].chirp_mode;
		chirp->chirp_fmcw[i].band_width = p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].band_width;
		chirp->chirp_fmcw[i].sweep_time = p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].sweep_time;
		chirp->chirp_fmcw[i].point_num  = p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].point_num;
		chirp->chirp_fmcw[i].lpf_fc     = p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].lpf_fc;
		chirp->chirp_fmcw[i].hpf_fc     = p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].hpf_fc;
	}

	return 0;
}

DLLAPI int rs_attr_get_cw_h(rs_attr_t attr, struct rs_chirp_cw_h *chirp)
{
	if(attr == NULL) {
		return RS_ATTR_EINV;
	}

	if(p_rs_attr_t(attr)->modu_mode != RS_MODU_MODE_CW_H) {
		return RS_ATTR_EINV;
	}

	chirp->rx_gain      = p_rs_attr_t(attr)->cw_h.rx_gain;
	chirp->rxch         = p_rs_attr_t(attr)->cw_h.rxch;
	chirp->txch         = p_rs_attr_t(attr)->cw_h.txch;
	chirp->iq_mode      = p_rs_attr_t(attr)->cw_h.iq_mode;
	chirp->sweep_time   = p_rs_attr_t(attr)->cw_h.sweep_time;
	chirp->point_num    = p_rs_attr_t(attr)->cw_h.point_num;
	chirp->lpf_fc       = p_rs_attr_t(attr)->cw_h.lpf_fc;

	return 0;
}

DLLAPI int rs_attr_get_cw_l(rs_attr_t attr, struct rs_chirp_cw_l *chirp)
{
	if(attr == NULL) {
		return RS_ATTR_EINV;
	}

	if(p_rs_attr_t(attr)->modu_mode != RS_MODU_MODE_CW_L) {
		return RS_ATTR_EINV;
	}

	chirp->repeat_num  = p_rs_attr_t(attr)->cw_l.repeat_num;
	chirp->rx_gain     = p_rs_attr_t(attr)->cw_l.rx_gain;
	chirp->rxch        = p_rs_attr_t(attr)->cw_l.rxch;
	chirp->txch        = p_rs_attr_t(attr)->cw_l.txch;
	chirp->iq_mode     = p_rs_attr_t(attr)->cw_l.iq_mode;
	chirp->sweep_time  = p_rs_attr_t(attr)->cw_l.sweep_time;
	chirp->lpf_fc      = p_rs_attr_t(attr)->cw_l.lpf_fc;

	return 0;
}

DLLAPI int rs_attr_get_fskcw(rs_attr_t attr, struct rs_chirp_fskcw *chirp)
{
	if(attr == NULL) {
		return RS_ATTR_EINV;
	}

	if(p_rs_attr_t(attr)->modu_mode != RS_MODU_MODE_FSKCW) {
		return RS_ATTR_EINV;
	}

	chirp->repeat_num   = p_rs_attr_t(attr)->fskcw.repeat_num;
	chirp->rx_gain      = p_rs_attr_t(attr)->fskcw.rx_gain;
	chirp->rxch         = p_rs_attr_t(attr)->fskcw.rxch;
	chirp->txch         = p_rs_attr_t(attr)->fskcw.txch;
	chirp->iq_mode      = p_rs_attr_t(attr)->fskcw.iq_mode;
	chirp->band_width   = p_rs_attr_t(attr)->fskcw.band_width;
	chirp->sweep_time   = p_rs_attr_t(attr)->fskcw.sweep_time;
	chirp->lpf_fc       = p_rs_attr_t(attr)->fskcw.lpf_fc;

	return 0;
}

DLLAPI int rs_attr_get_fw_ver(rs_attr_t attr, uint8_t *fw_major_ver, uint8_t *fw_minor_ver, uint8_t *fw_revision, uint8_t *fw_hotfix)
{
	if(attr == NULL) {
		return RS_ATTR_EINV;
	}

	*fw_major_ver = p_rs_attr_t(attr)->fw_major_ver;
	*fw_minor_ver = p_rs_attr_t(attr)->fw_minor_ver;
	*fw_revision  = p_rs_attr_t(attr)->fw_revision;
	*fw_hotfix    = p_rs_attr_t(attr)->fw_hotfix;

	return 0;
}

DLLAPI int rs_attr_get_hw_ver(rs_attr_t attr, uint16_t *hw_major_ver, uint16_t *hw_minor_ver)
{
	if(attr == NULL) {
		return RS_ATTR_EINV;
	}

	*hw_major_ver = p_rs_attr_t(attr)->hw_major_ver;
	*hw_minor_ver = p_rs_attr_t(attr)->hw_minor_ver;

	return 0;
}

DLLAPI int rs_attr_get_hw_type(rs_attr_t attr, uint16_t *hw_type)
{
	if(attr == NULL) {
		return RS_ATTR_EINV;
	}

	*hw_type = p_rs_attr_t(attr)->hw_type;

	return 0;
}

DLLAPI int rs_attr_get_fft(rs_attr_t attr, struct rs_chirp_fft *fft)
{
	if(attr == NULL) {
		return RS_ATTR_EINV;
	}

	fft->fft_dc_cut					= p_rs_attr_t(attr)->fft.fft_dc_cut;
	fft->fft_point					= p_rs_attr_t(attr)->fft.fft_point;
	fft->fft_upper_range			= p_rs_attr_t(attr)->fft.fft_upper_range;

	return 0;
}

DLLAPI int rs_attr_get_fft_multi(rs_attr_t attr, struct rs_chirp_fft_multi *fft_multi)
{
	int i;

	if(attr == NULL) {
		return RS_ATTR_EINV;
	}

	switch(p_rs_attr_t(attr)->modu_mode) {
		case RS_MODU_MODE_FMCW_R:
		case RS_MODU_MODE_CW_H:
			fft_multi->multi_chirp_num = 1;
			fft_multi->fft[0].fft_dc_cut            = p_rs_attr_t(attr)->fft.fft_dc_cut;
			fft_multi->fft[0].fft_point             = p_rs_attr_t(attr)->fft.fft_point;
			fft_multi->fft[0].fft_upper_range       = p_rs_attr_t(attr)->fft.fft_upper_range;
		break;
		case RS_MODU_MODE_FMCW_M:
			fft_multi->multi_chirp_num = p_rs_attr_t(attr)->fft_multi.multi_chirp_num;
			for(i = 0; i < fft_multi->multi_chirp_num; i++) {
				fft_multi->fft[i].fft_dc_cut            = p_rs_attr_t(attr)->fft_multi.fft[i].fft_dc_cut;
				fft_multi->fft[i].fft_point             = p_rs_attr_t(attr)->fft_multi.fft[i].fft_point;
				fft_multi->fft[i].fft_upper_range       = p_rs_attr_t(attr)->fft_multi.fft[i].fft_upper_range;
			}
		break;
		default:
		break;

	}

	return 0;
}

