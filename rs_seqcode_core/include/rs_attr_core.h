﻿/*
 * Copyright (c) 2017 Socionext Inc.
 * All rights reserved.
 *
 */

#ifndef	__RS_ATTR_CORE_H__
#define	__RS_ATTR_CORE_H__

#ifdef WIN32
#include <windows.h>
#endif
#include <stdint.h>

#include "rs_attr.h"

#ifndef EXTERN_C
#ifdef __cplusplus
#define EXTERN_C extern "C"
#else	/* __cplusplus */
#define EXTERN_C
#endif	/* __cplusplus */
#endif	/* EXTERN_C */

#ifdef WIN32
#ifndef DLLAPI
#define DLLAPI EXTERN_C __declspec(dllimport)
#endif	/* DLLAPI */
#else	/* WIN32 */
#define DLLAPI EXTERN_C
#endif	/* WIN32 */

/*
 * Enum Definition
 */


/*
 * Constant Definition
 */

/*
 * Macro Definition
 */


/*
 * Struct Definition
 */

struct rs_test_ct {
	uint8_t                 txch;
};

struct rs_test_cr {
	enum rs_sweep_time     sweep_time;
	enum rs_lpf_fc         lpf_fc;
};

struct _rs_attr_t {
    /* generator version */
	uint8_t							fw_major_ver;
	uint8_t							fw_minor_ver;
	uint8_t							fw_revision;
	uint8_t							fw_hotfix;

    /* common params */
	uint32_t						modu_mode;
	int32_t							tx_power;
	uint32_t						rx_gain;
	uint32_t						frame_rate;
	uint32_t						chirp_rate_constant;
	uint32_t						center_freq;
	uint32_t						xtal_freq;
	int32_t							calc;
	uint32_t						eval_mode;
	enum rs_init_cal_mode			init_cal_mode;
	enum rs_bit_width				bit_width;
	enum rs_header_mode				header_mode;
	enum rs_ex_status_header_mode	status_mode;
	enum rs_fft_winfunc				fft_winfunc;
	enum rs_fft_en					fft_en;

    /* modulation params */
    struct rs_chirp_fmcw_r          fmcw_r;
    struct rs_chirp_fmcw_multi      fmcw_m;
    struct rs_chirp_cw_l            cw_l;
    struct rs_chirp_cw_h            cw_h;
    struct rs_chirp_fskcw           fskcw;
    struct rs_test_ct               test_ct;
    struct rs_test_cr               test_cr;
	struct rs_chirp_fft             fft;
	struct rs_chirp_fft_multi       fft_multi;

    /* HW major/minor/type */
	uint16_t						hw_major_ver;
	uint16_t						hw_minor_ver;
	uint16_t						hw_type;
};

/*
 * Constant Definition
 */

#define p_rs_attr_t(a) ((struct _rs_attr_t *) (a))

/* proto type */

#endif	/* __RS_ATTR_CORE_H__ */
