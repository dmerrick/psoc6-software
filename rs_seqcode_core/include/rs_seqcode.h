﻿/*
 * Copyright (c) 2018 Socionext Inc.
 * All rights reserved.
 */

/*
 * rs_seqcode.h
 */

#ifndef __RS_SEQCODE_H__
#define __RS_SEQCODE_H__

#ifdef WIN32
#include <windows.h>
#endif
#include <stdint.h>

#include "rs_retcode.h"

#ifndef EXTERN_C
#ifdef __cplusplus
#define EXTERN_C extern "C"
#else	/* __cplusplus */
#define EXTERN_C
#endif	/* __cplusplus */
#endif	/* EXTERN_C */

#ifdef WIN32
#ifndef DLLAPI
#define DLLAPI EXTERN_C __declspec(dllimport)
#endif	/* DLLAPI */
#else	/* WIN32 */
#define DLLAPI EXTERN_C
#endif	/* WIN32 */

/*
 * Type Definition
 */
typedef void *rs_seqcode_t;

/*
 * Enumerated Type Definition
 */

/*
 * Constant Definition
 */

/*
 * Struct Definition
 */

/*
 * Function Definition
 */
// common function
#ifndef __NOT_USE_IMPORT_LIBRARY__
DLLAPI int rs_seqcode_init(rs_seqcode_t *bin);
DLLAPI int rs_seqcode_free(rs_seqcode_t bin);
DLLAPI int rs_seqcode_save(rs_seqcode_t bin, void *buf, uint32_t bufsize);
DLLAPI int rs_seqcode_load(rs_seqcode_t bin, const void *buf, uint32_t bufsize);
DLLAPI int rs_seqcode_get_size(rs_seqcode_t bin, uint32_t *size);
DLLAPI int rs_seqcode_get_ver(uint16_t *major_ver, uint16_t *minor_ver);

#endif	// __NOT_USE_IMPORT_LIBRARY__

#endif	/* __RS_SEQCODE_H__ */
