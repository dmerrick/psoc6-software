﻿/*
 * Copyright (c) 2017 Socionext Inc.
 * All rights reserved.
 *
 */

/*
 * rs_code.h
 */

#ifndef	__RS_CODE_H__
#define	__RS_CODE_H__

#include <stdint.h>

#include "rs_attr.h"

#ifndef EXTERN_C
#ifdef __cplusplus
#define EXTERN_C extern "C"
#else	/* __cplusplus */
#define EXTERN_C
#endif	/* __cplusplus */
#endif	/* EXTERN_C */

#ifdef WIN32
#ifndef DLLAPI
#define DLLAPI EXTERN_C __declspec(dllimport)
#endif	/* DLLAPI */
#else	/* WIN32 */
#define DLLAPI EXTERN_C
#endif	/* WIN32 */

/*
 * Type Definition
 */

/*
 * Enumerated Type Definition
 */

/*
 * Constant Definition
 */

/*
 * Struct Definition
 */
struct rs_code_t {
	struct {
		uint32_t size;
		uint8_t *data;
	} param[4];
	uint16_t frame_size;
	enum rs_bit_width				bit_width;
	enum rs_header_mode				header_mode;
	enum rs_ex_status_header_mode	status_mode;
	int32_t tx_power;

};

/*
 * Function Definition
 */
DLLAPI int rs_code_init(struct rs_code_t **code);
DLLAPI int rs_code_resize_param(struct rs_code_t *code, uint32_t id, uint32_t size);
DLLAPI int rs_code_free(struct rs_code_t **code);

#endif	/* __RS_CODE_H__ */
