﻿/*
 * Copyright (c) 2018 Socionext Inc.
 * All rights reserved.
 */

/*
 * rs_seqcode_local.h
 */

#ifndef __RS_SEQCODE_LOCAL_H__
#define __RS_SEQCODE_LOCAL_H__

#ifdef WIN32
#include <windows.h>
#endif
#include <stdint.h>

#include "rs_seqcode.h"

#ifndef EXTERN_C
#ifdef __cplusplus
#define EXTERN_C extern "C"
#else	/* __cplusplus */
#define EXTERN_C
#endif	/* __cplusplus */
#endif	/* EXTERN_C */

#ifdef WIN32
#ifndef DLLAPI
#define DLLAPI EXTERN_C __declspec(dllimport)
#endif	/* DLLAPI */
#else	/* WIN32 */
#define DLLAPI EXTERN_C
#endif	/* WIN32 */

/*
 * Type Definition
 */

/*
 * Enumerated Type Definition
 */

/*
 * Constant Definition
 */
const static uint8_t header_magic = 0xa5;
const static uint8_t header_reserved1 = 1;		/* tentative: attr always exists  */
const static uint8_t header_reserved2 = 0;

/*
 * Struct Definition
 */
class bindata
{
public:
	struct header_t {
		uint8_t magic;
		uint8_t reserved1;
		uint8_t major_ver;
		uint8_t minor_ver;
		uint32_t size;
		uint8_t gen_major_ver;
		uint8_t gen_minor_ver;
		uint8_t gen_revision_ver;
		uint8_t gen_hotfix_ver;
		uint32_t reserved2;
	} header;

	uint16_t hw_type;
	uint16_t hw_major_ver;
	uint16_t hw_minor_ver;

	uint32_t code_size;
	uint32_t attr_size;
	uint8_t* code;
	uint8_t* attr;

	const static uint32_t header_size = sizeof(struct header_t);

public:
	bindata(void) : header({}), code(0), attr(0) {}
	~bindata(void);
	int code_resize(uint32_t size);
	int attr_resize(uint32_t size);
};

/*
 * Function Definition
 */
#ifndef __NOT_USE_IMPORT_LIBRARY__
DLLAPI int rs_seqcode_check(rs_seqcode_t bin);
#endif	// __NOT_USE_IMPORT_LIBRARY__

static inline class bindata *p_bin(rs_seqcode_t bin)
{
	return static_cast<class bindata *>(bin);
}

#endif	/* __RS_SEQCODE_LOCAL_H__ */
