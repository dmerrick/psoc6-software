/*
 * Copyright (c) 2018 Socionext Inc.
 * All rights reserved.
 */

/*
 * rs_seqcode_version.h
 */

#ifndef __RS_SEQCODE_VERSION_H__
#define __RS_SEQCODE_VERSION_H__

#ifdef WIN32
#include <windows.h>
#endif
#include <stdint.h>

const static uint8_t rs_seqcode_data_major_ver = 4;
const static uint8_t rs_seqcode_data_minor_ver = 1;

#endif	/* __RS_SEQCODE_VERSION_H__ */
