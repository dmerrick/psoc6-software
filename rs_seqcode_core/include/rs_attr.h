﻿/*
 * Copyright (c) 2017 Socionext Inc.
 * All rights reserved.
 *
 */

#ifndef	__RS_ATTR_H__
#define	__RS_ATTR_H__

#ifdef WIN32
#include <windows.h>
#endif
#include <stdint.h>

#include "rs_seqcode.h"

#ifndef EXTERN_C
#ifdef __cplusplus
#define EXTERN_C extern "C"
#else	/* __cplusplus */
#define EXTERN_C
#endif	/* __cplusplus */
#endif	/* EXTERN_C */

#ifdef WIN32
#ifndef DLLAPI
#define DLLAPI EXTERN_C __declspec(dllimport)
#endif	/* DLLAPI */
#else	/* WIN32 */
#define DLLAPI EXTERN_C
#endif	/* WIN32 */

/*
 * define macro
 */

/* modulation mode macro */
#define RS_MODU_MODE_FMCW_R		(0x00) /* FMCW Repeat        */
#define RS_MODU_MODE_FMCW_M		(0x10) /* FMCW Multi(Repeat) */
#define RS_MODU_MODE_CW_H		(0x01) /* CW High            */
#define RS_MODU_MODE_CW_L		(0x11) /* CW low             */
#define RS_MODU_MODE_FSKCW		(0x02) /* FSKCW              */
#define RS_MODU_MODE_NO_MODE	(0xFF) /* not yet set mode   */

/* error code */
#define RS_ATTR_OK								0		/* OK                            */
#define RS_ATTR_EINV							-128	/* Invalid argument              */
#define RS_ATTR_ETOUT							-129	/* Time out                      */
#define RS_ATTR_EDEV							-130	/* Device error(reserved)        */
#define RS_ATTR_ENOSUPPORT						-131	/* Not supported                 */
#define RS_ATTR_ESTATE							-132	/* State error                   */
#define RS_ATTR_ECOM							-133	/* UART/SPI open error(reserved) */
#define RS_ATTR_ENOMEM							-134	/* not enough space              */
#define RS_ATTR_EFAULT							-138	/* Bad address or internal error */

#define RS_ATTR_NO_MODU_MODE					-139	/* no set modulation mode        */

#define RS_ATTR_ERANGE_MODU_MODE				-256	/* Range error Modulation mode     */
#define RS_ATTR_ERANGE_TX_POWER					-257	/* Range error TX power            */
#define RS_ATTR_ERANGE_RX_GAIN					-258	/* Range error RX gain             */
#define RS_ATTR_ERANGE_FRAME_RATE				-259	/* Range error Frame rate          */
#define RS_ATTR_ERANGE_CHIRP_RATE_CONSTANT		-260	/* Range error chirp rate constant */
#define RS_ATTR_ERANGE_CENTER_FREQ				-261	/* Range error Center freq         */
#define RS_ATTR_ERANGE_XTAL_FREQ				-262	/* Range error XTAL freq           */
#define RS_ATTR_ERANGE_CAL_PERIOD				-263	/* Range error CAL period          */
#define RS_ATTR_ERANGE_CAL_CONSTANT				-264	/* Range error CAL constant        */
#define RS_ATTR_ERANGE_BIT_WIDTH				-265	/* Range error bit width           */
#define RS_ATTR_ERANGE_HEADER_MODE				-266	/* Range error header mode         */
#define RS_ATTR_ERANGE_EX_STATUS_HEADER_MODE	-267	/* Range error ex_status header    */

#define RS_ATTR_ERANGE_R_REPEAT_NUM				-268	/* Range error repreat num         */
#define RS_ATTR_ERANGE_R_CHIRP_RXCH				-269	/* Range error chirp RXch          */
#define RS_ATTR_ERANGE_R_CHIRP_TXCH				-270	/* Range error chirp TXch          */
#define RS_ATTR_ERANGE_R_CHIRP_IQ_PERM			-271	/* Range error chirp IQ perm       */
#define RS_ATTR_ERANGE_R_CHIRP_IQ_MODE			-272	/* Range error chirp IQ mode       */
#define RS_ATTR_ERANGE_R_CHIRP_CHIRP_MODE		-273	/* Range error chirp chirp mode    */
#define RS_ATTR_ERANGE_R_CHIRP_BAND_WIDTH		-274	/* Range error chirp band width    */
#define RS_ATTR_ERANGE_R_CHIRP_SWEEP_TIME		-275	/* Range error chirp sweep time    */
#define RS_ATTR_ERANGE_R_CHIRP_POINT_NUM		-276	/* Range error chirp point num     */
#define RS_ATTR_ERANGE_R_CHIRP_WAIT_TIME		-277	/* Range error chirp wait time     */
#define RS_ATTR_ERANGE_R_CHIRP_LPF_ORDER		-278	/* Range error chirp LPF order     */
#define RS_ATTR_ERANGE_R_CHIRP_LPF_FC			-279	/* Range error chirp LPF fc        */
#define RS_ATTR_ERANGE_R_CHIRP_HPF_ORDER		-280	/* Range error chirp HPF order     */
#define RS_ATTR_ERANGE_R_CHIRP_HPF_FC			-281	/* Range error chirp HPF fc        */
#define RS_ATTR_ERANGE_MULTI_CHIRP_NUM          -282	/* Range error multi chirp num     */
#define RS_ATTR_ERANGE_INIT_CAL_MODE			-283	/* Range error init CAL mode       */
#define RS_ATTR_ERANGE_FFT_WINFUNC				-284	/* Range error fft window function */
#define RS_ATTR_ERANGE_FFT_EN                   -285    /* Range error fft enable              */
#define RS_ATTR_ERANGE_FFT_INVERT_FFT_SPEC      -286    /* Range error fft data reverse enable */
#define RS_ATTR_ERANGE_FFT_DC_CUT               -287    /* Range error fft dc cut enable       */
#define RS_ATTR_ERANGE_FFT_POINT                -288    /* Range error fft point               */
#define RS_ATTR_ERANGE_FFT_UPPER_RANGE          -289    /* Range error fft output range upper  */
#define RS_ATTR_ERANGE_R_CHIRP_POINT_NUM_FFT	-290	/* Range error chirp point num     */
#define RS_ATTR_ERANGE_R_CHIRP_CHIRP_MODE_FFT	-291	/* Range error chirp chirp mode    */

#define RS_ATTR_EVALID_CHIRP_RATE_CONSTANT		-512	/* Invalid param chirp rate constant */
#define RS_ATTR_EVALID_CHIRP_BAND_WIDTH			-513	/* Invalid param band width          */
#define RS_ATTR_EVALID_FRAME_RATE				-514	/* Invalid param frame rate          */
#define RS_ATTR_EVALID_FRAME_SIZE				-515	/* Invalid param frame size          */
#define RS_ATTR_EVALID_POINT_NUM				-516	/* Invalid param point num           */
#define RS_ATTR_EVALID_IQ_MODE					-517	/* Invalid param IQ mode             */
#define RS_ATTR_EVALID_TXCH						-518	/* Invalid param TXch                */
#define RS_ATTR_EVALID_SWEEP_TIME				-519	/* Invalid param sweep time          */
#define RS_ATTR_EVALID_RXCH						-520	/* Invalid param RXch                */
#define RS_ATTR_EVALID_TX_POWER					-521	/* Invalid param TX power            */

/*
 * Constant Definition
 */

#define RS_MAX_CHIRP_PATTERN	(4)
#define RS_FIFO_SIZE_LSI		(8192)

/*
 * Default paramters
 */

#define RS_DEFAULT_MODU_MODE			RS_MODU_MODE_FMCW_R		/* FMCW Repeat            */
#define	RS_DEFAULT_FMCW_TX_POWER		-10						/* -10 dBm                */
#define RS_DEFAULT_FMCW_CENTER_FREQ		(60500)					/* 60500 MHz(60.5GHz)     */
#define RS_DEFAULT_FMCW_XTAL_FREQ		RS_XTAL_FREQ_40MHZ		/* 40 MHz                 */
#define RS_DEFAULT_FMCW_PERIODIC_CAL	(0)						/* PeriodicCal 0(T.B.D)   */
#define RS_DEFAULT_FMCW_CONSTANT_CAL	(0)						/* ConstantCal disable    */
#define RS_DEFAULT_FMCW_INIT_CAL_MODE	RS_INIT_CAL_DISABLE		/* init CAL mode disable  */

#define RS_DEFAULT_FFT_WIN_NO						RS_FFT_WIN_NO						/* FFT window func no */
#define RS_DEFAULT_FFT_DISABLE						RS_FFT_DISABLE						/* FFT disable        */
#define RS_DEFAULT_FFT_INVERT_FFT_SPEC_DISABLE		RS_FFT_INVERT_FFT_SPEC_DISABLE		/* FFT inverter fft disable */
#define RS_DEFAULT_FFT_DC_CUT_DISABLE				RS_FFT_DC_CUT_DISABLE				/* FFT dc cut disable       */
#define RS_DEFAULT_FFT_POINT_512					RS_FFT_POINT_512					/* FFT fft point 128        */

#define RS_INIT_TX_POWER				(-256)					/* initial value TX Power  */
#define RS_INIT_RX_GAIN					(0)						/* initial value RX gain   */

/*
 * Type Definition
 */
typedef void *rs_attr_t;

/*
 * Enumerated Type Definition
 */

enum rs_tx_power {
	RS_TX_POWER_10 =  -10, /* -10dB */
	RS_TX_POWER_0  =    0, /*   0dB */
};

enum rs_rx_gain {
	RS_RX_GAIN_60  =  60, /* 60dB */
	RS_RX_GAIN_50  =  50, /* 50dB */
	RS_RX_GAIN_40  =  40, /* 40dB */
};

enum rs_xtal_freq {
	RS_XTAL_FREQ_40MHZ       = 40000000, /* 40MHz   */
	RS_XTAL_FREQ_38_4MHZ     = 38400000, /* 38.4MHz */
};

enum rs_bit_width {
	RS_BIT_WIDTH_16BIT       = 0, /* 16bit (no truncation) */
	RS_BIT_WIDTH_12BIT_MODE1 = 1, /* 12bit                 */
};

enum rs_header_mode {
	RS_HEADER_MODE_DISABLE	= 0, /* Not adding any header to the frame */
	RS_HEADER_MODE_ENABLE	= 1, /* Adding a header to the frame       */
};

enum rs_ex_status_header_mode {
	RS_EX_STATUS_HEADER_MODE_DISABLE = 0, /* Not adding any status information to the frame */
	RS_EX_STATUS_HEADER_MODE_ENABLE  = 1, /* Adding status information to the frame         */
};


enum rs_IQ_mode {
	RS_IQ_MODE_I	= 0, /* Output date I only  */
	RS_IQ_MODE_IQ	= 1, /* Output date I and Q */
};

enum rs_chirp_mode {
	RS_CHIRP_MODE_UP		= 0, /* upward only */
	RS_CHIRP_MODE_UPDOWN	= 1, /* upward and downward */
};

enum rs_sweep_time {
	/* fXTAL=40MHz */
	/* common */
	RS_SWEEP_T_22		= 22,		/*  22 us */
	RS_SWEEP_T_33		= 33,		/*  33 us */
	RS_SWEEP_T_55		= 55,		/*  55 us */
	RS_SWEEP_T_110		= 110,		/* 110 us */
	RS_SWEEP_T_220		= 220,		/* 220 us */
	RS_SWEEP_T_440		= 440,		/* 440 us */
	/* CW_L, FSKCW */
	RS_SWEEP_T_20		= 20,		/*  20 us */
	/* FMCW, CW_L, FSKCW, CW_H */
	RS_SWEEP_T_1100		= 1100,     /* 1.1 ms */
	RS_SWEEP_T_2200		= 2200,     /* 2.2 ms */
	RS_SWEEP_T_4400		= 4400,     /* 4.4 ms */
	/* CW_H */
	RS_SWEEP_T_11000	= 11000,    /* 11  ms */
	RS_SWEEP_T_22000	= 22000,    /* 22  ms */
	RS_SWEEP_T_44000	= 44000,    /* 44  ms */
	/* fXTAL=38.4MHz x1.04 */
	/* common */
	RS_SWEEP_T_23		= 23,		/*  23 us */
	RS_SWEEP_T_34		= 34,		/*  34 us */
	RS_SWEEP_T_57		= 57,		/*  57 us */
	RS_SWEEP_T_115		= 115,		/* 115 us */
	RS_SWEEP_T_230		= 230,		/* 230 us */
	RS_SWEEP_T_460		= 460,		/* 460 us */
	/* CW_L, FSKCW */
	RS_SWEEP_T_21		= 21,		/*  21 us */
	/* FMCW, CW_L, FSKCW, CW_H */
	RS_SWEEP_T_1150		= 1150,     /* 1.15 ms */
	RS_SWEEP_T_2300		= 2300,     /* 2.3  ms */
	RS_SWEEP_T_4600		= 4600,     /* 4.6  ms */
	/* CW_H */
	RS_SWEEP_T_11500	= 11500,    /* 11.5 ms */
	RS_SWEEP_T_23000	= 23000,    /* 23   ms */
	RS_SWEEP_T_46000	= 46000,    /* 46   ms */
};

enum rs_wait_time {
	RS_WAIT_T_30	= 30,	/* wait time 30 us      */
	RS_WAIT_T_110	= 110,	/* wait time 110 us      */
};

enum rs_lpf_fc {
	RS_LPF_FC_500	= 500,	/*  500 kHz */
	RS_LPF_FC_1000	= 1000,	/* 1000 kHz */
};

enum rs_hpf_fc {
	RS_HPF_FC_5		= 5, 	/*  5kHz  */
	RS_HPF_FC_10	= 10, 	/*  10kHz */
	RS_HPF_FC_20	= 20, 	/*  20kHz */
	RS_HPF_FC_50	= 50, 	/*  50kHz */
	RS_HPF_FC_100	= 100, 	/* 100kHz */
	RS_HPF_FC_200	= 200, 	/* 200kHz */
	RS_HPF_FC_400	= 400, 	/* 400kHz */
};

enum rs_init_cal_mode {
	RS_INIT_CAL_DISABLE = 0, /* init CAL disable */
	RS_INIT_CAL_ENABLE  = 1, /* init CAL enable */
};

enum rs_fft_winfunc {
	RS_FFT_WIN_NO                = 0, /* FFT window no       */
	RS_FFT_WIN_HANN              = 1, /* FFT window hann     */
	RS_FFT_WIN_HAMMING           = 2, /* FFT window hamming  */
	RS_FFT_WIN_BLACKMAN          = 3, /* FFT window blackman */
};

enum rs_fft_en {
	RS_FFT_DISABLE                   = 0, /* FFT mode disable */
	RS_FFT_ENABLE                    = 1, /* FFT mode enable */
};

enum rs_fft_dc_cut {
	RS_FFT_DC_CUT_DISABLE            = 0, /* FFT dccut disable */
	RS_FFT_DC_CUT_ENABLE             = 1, /* FFT dccut enable */
};

enum rs_fft_point {
	RS_FFT_POINT_128             = 0, /* FFT point 128 */
	RS_FFT_POINT_256             = 1, /* FFT point 256 */
	RS_FFT_POINT_512             = 2, /* FFT point 512 */
};

/*
 * Struct Definition
 */

struct rs_chirp_fmcw {
	uint8_t					rxch;
	uint8_t					txch;
	enum rs_IQ_mode			iq_mode;
	enum rs_chirp_mode		chirp_mode;
	uint32_t				band_width;
	enum rs_sweep_time		sweep_time;
	uint16_t				point_num;
	enum rs_lpf_fc			lpf_fc;
	enum rs_hpf_fc			hpf_fc;
};

struct rs_chirp_fmcw_r {
	uint8_t					repeat_num;	/* num of chirp repeat  */
	struct rs_chirp_fmcw	chirp_fmcw;	/* chirp struct         */
};

struct rs_chirp_fmcw_m {
	enum rs_rx_gain			rx_gain; /* add */
	uint8_t					rxch;
	uint8_t					txch;
	enum rs_IQ_mode			iq_mode;
	enum rs_chirp_mode		chirp_mode;
	uint32_t				band_width;
	enum rs_sweep_time		sweep_time;
	uint16_t				point_num;
	enum rs_lpf_fc			lpf_fc;
	enum rs_hpf_fc			hpf_fc;
};

struct rs_chirp_fmcw_multi {
	uint8_t					repeat_num;							/* num of chirp repeat  */
	uint8_t					multi_chirp_num;					/* num of chirp pattern */
	struct rs_chirp_fmcw_m	chirp_fmcw[RS_MAX_CHIRP_PATTERN];	/* chirp struct         */
};

struct rs_chirp_cw_h {
	enum rs_rx_gain			rx_gain;
	uint8_t					rxch;
	uint8_t					txch;
	enum rs_IQ_mode			iq_mode;
	enum rs_sweep_time		sweep_time;
	uint16_t				point_num;
	enum rs_lpf_fc			lpf_fc;
};

struct rs_chirp_cw_l {
	uint8_t					repeat_num;
	enum rs_rx_gain			rx_gain;
	uint8_t					rxch;
	uint8_t					txch;
	enum rs_IQ_mode			iq_mode;
	enum rs_sweep_time		sweep_time;
	enum rs_lpf_fc			lpf_fc;
};

struct rs_chirp_fskcw {
	uint8_t					repeat_num;
	enum rs_rx_gain			rx_gain;
	uint8_t					rxch;
	uint8_t					txch;
	enum rs_IQ_mode			iq_mode;
	uint32_t				band_width; /* 1MHz to 100MHz */
	enum rs_sweep_time		sweep_time;
	enum rs_lpf_fc			lpf_fc;
};

struct rs_chirp_fft  {
	enum rs_fft_dc_cut				fft_dc_cut;
	enum rs_fft_point				fft_point;
	uint16_t						fft_upper_range;
};

struct rs_chirp_fft_multi {
	uint8_t					multi_chirp_num;					/* num of chirp pattern */
	struct rs_chirp_fft		fft[RS_MAX_CHIRP_PATTERN];			/* chirp struct         */
};

/*
 * Function Definition
 */
#ifndef __NOT_USE_IMPORT_LIBRARY__
DLLAPI int rs_attr_init(rs_attr_t *attr);
DLLAPI int rs_attr_free(rs_attr_t attr);

DLLAPI int rs_attr_get_modulation_mode(rs_attr_t attr, uint32_t *modu_mode);
DLLAPI int rs_attr_get_txpower(rs_attr_t attr, enum rs_tx_power *tx_power);
DLLAPI int rs_attr_get_rxgain(rs_attr_t attr, enum rs_rx_gain *rx_gain);
DLLAPI int rs_attr_get_frame_rate(rs_attr_t attr, uint32_t *frame_rate);
DLLAPI int rs_attr_get_center_freq(rs_attr_t attr, uint32_t *center_freq);
DLLAPI int rs_attr_get_chirp_rate_constant(rs_attr_t attr, uint32_t *chirp_rate_constant);
DLLAPI int rs_attr_get_data_format(rs_attr_t attr, enum rs_bit_width *bit_width, enum rs_header_mode *header_mode, enum rs_ex_status_header_mode *ex_status_header_mode);
DLLAPI int rs_attr_get_xtal_freq(rs_attr_t attr, enum rs_xtal_freq *xtalfreq);
DLLAPI int rs_attr_get_constant_cal(rs_attr_t attr, uint32_t *cal_constant);
DLLAPI int rs_attr_get_init_cal_mode(rs_attr_t attr, enum rs_init_cal_mode *init_cal_mode);
DLLAPI int rs_attr_get_fft_winfunc(rs_attr_t attr, enum rs_fft_winfunc *fft_winfunc);
DLLAPI int rs_attr_get_fft_en(rs_attr_t attr, enum rs_fft_en *fft_en);

DLLAPI int rs_attr_get_fmcw(rs_attr_t attr, struct rs_chirp_fmcw_r *chirp);
DLLAPI int rs_attr_get_fmcw_multi(rs_attr_t attr, struct rs_chirp_fmcw_multi *chirp);
DLLAPI int rs_attr_get_cw_h(rs_attr_t attr, struct rs_chirp_cw_h *chirp);
DLLAPI int rs_attr_get_cw_l(rs_attr_t attr, struct rs_chirp_cw_l *chirp);
DLLAPI int rs_attr_get_fskcw(rs_attr_t attr, struct rs_chirp_fskcw *chirp);

DLLAPI int rs_attr_get_fft_multi(rs_attr_t attr, struct rs_chirp_fft_multi *fft_multi);

DLLAPI int rs_seqcode_get_attr(rs_seqcode_t bin, rs_attr_t attr);

#endif	// __NOT_USE_IMPORT_LIBRARY__

#endif	/* __RS_ATTR_H__ */
