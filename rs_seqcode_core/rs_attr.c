﻿/*
 * Copyright (c) 2017 Socionext Inc.
 * All rights reserved.
 *
 */

/*
 * rs_attr.c
 */

#ifdef WIN32
#include <windows.h>
#endif
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "rs_attr.h"
#include "rs_attr_core.h"

DLLAPI int rs_attr_init(rs_attr_t *attr)
{
	if(attr == NULL) {
		return RS_ATTR_EINV;
	}

	*attr = malloc(sizeof(struct _rs_attr_t));
	if(*attr == NULL) {
		return RS_ATTR_ENOMEM;
	}

	/* 0 clear */
	memset(*attr, 0, sizeof(struct _rs_attr_t));

	/* set generator version 0 */
	p_rs_attr_t(*attr)->fw_major_ver = 0;
	p_rs_attr_t(*attr)->fw_minor_ver = 0;
	p_rs_attr_t(*attr)->fw_revision  = 0;
	p_rs_attr_t(*attr)->fw_hotfix    = 0;

	p_rs_attr_t(*attr)->modu_mode = RS_DEFAULT_MODU_MODE;

	/* for version 1.x backward compatibility */
	p_rs_attr_t(*attr)->center_freq = RS_DEFAULT_FMCW_CENTER_FREQ;
	p_rs_attr_t(*attr)->xtal_freq = RS_DEFAULT_FMCW_XTAL_FREQ;

	p_rs_attr_t(*attr)->calc = RS_DEFAULT_FMCW_CONSTANT_CAL;
	p_rs_attr_t(*attr)->init_cal_mode = RS_DEFAULT_FMCW_INIT_CAL_MODE; /* FMCW: changed, other mode: fixed */

	/* flag for not set yet */
	p_rs_attr_t(*attr)->tx_power = RS_INIT_TX_POWER;
	p_rs_attr_t(*attr)->rx_gain = RS_INIT_RX_GAIN;

	/* for temporary parameter */
	p_rs_attr_t(*attr)->hw_major_ver = 1;
	p_rs_attr_t(*attr)->hw_minor_ver = 0x0103;
	p_rs_attr_t(*attr)->hw_type = 0x0000;
	p_rs_attr_t(*attr)->eval_mode = 0;
	
	p_rs_attr_t(*attr)->fft_winfunc          = RS_DEFAULT_FFT_WIN_NO;
	p_rs_attr_t(*attr)->fft_en               = RS_DEFAULT_FFT_DISABLE;
	
	p_rs_attr_t(*attr)->fft.fft_dc_cut       = RS_DEFAULT_FFT_DC_CUT_DISABLE;
	p_rs_attr_t(*attr)->fft.fft_point        = RS_DEFAULT_FFT_POINT_512;
	p_rs_attr_t(*attr)->fft.fft_upper_range  = 128;

	for (uint8_t i=0; i<4; i++) {
		p_rs_attr_t(*attr)->fft_multi.fft[i].fft_dc_cut       = RS_DEFAULT_FFT_DC_CUT_DISABLE;
		p_rs_attr_t(*attr)->fft_multi.fft[i].fft_point        = RS_DEFAULT_FFT_POINT_512;
		p_rs_attr_t(*attr)->fft_multi.fft[i].fft_upper_range  = 128;
	}
	
	return 0;
}

DLLAPI int rs_attr_free(rs_attr_t attr)
{
	if(attr == NULL) {
		return RS_ATTR_EINV;
	}

	free(p_rs_attr_t(attr));

	return 0;
}


