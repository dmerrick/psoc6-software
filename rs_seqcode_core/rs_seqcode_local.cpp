﻿/*
 * Copyright (c) 2018 Socionext Inc.
 * All rights reserved.
 */

/*
 * rs_seqcode_local.cpp
 */

#ifdef WIN32
#include <windows.h>
#endif
#include <stdint.h>
#include <stdlib.h>

#include "ConvRet.h"
#include "rs_seqcode.h"
#include "rs_seqcode_local.h"

bindata::~bindata(void)
{
	if(code != NULL){
		free(code);
	}
	if(attr != NULL){
		free(attr);
	}
}

int bindata::code_resize(uint32_t size)
{
	uint8_t* code_new = NULL;
	code_new = (uint8_t*)calloc((size_t)size, sizeof(uint8_t));
	if(code_new == NULL){
		return -1;
	}
	
	if(code != NULL){
		free(code);
	}
	code = code_new;
	code_size = size;
	
	return 0;
}

int bindata::attr_resize(uint32_t size)
{
	uint8_t* attr_new = NULL;
	attr_new = (uint8_t*)calloc((size_t)size, sizeof(uint8_t));
	if(attr_new == NULL){
		return -1;
	}
	
	if(attr != NULL){
		free(attr);
	}
	attr = attr_new;
	attr_size = size;
	
	return 0;
}

DLLAPI int rs_seqcode_check(rs_seqcode_t bin)
{
	uint16_t major_ver = 0;
	uint16_t minor_ver = 0;

	RETMACRO_refretorg(rs_seqcode_get_ver(&major_ver, &minor_ver), RS_OK);

	if(p_bin(bin)->header.magic != header_magic) {
		return RS_EINV;
	}
	if(p_bin(bin)->header.reserved1 != header_reserved1) {
		return RS_EINV;
	}
	if(p_bin(bin)->header.major_ver != major_ver) {
		return RS_EINV;
	}
	if(p_bin(bin)->header.minor_ver != minor_ver) {
		return RS_EINV;
	}
	if(p_bin(bin)->header.reserved2 != header_reserved2) {
		return RS_EINV;
	}
	if(p_bin(bin)->header.size != 2 + 2 + 2 + p_bin(bin)->code_size + p_bin(bin)->attr_size + 4 + 4) {
		return RS_EINV;
	}

	return RS_OK;
}
