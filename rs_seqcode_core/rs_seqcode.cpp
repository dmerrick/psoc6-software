/*
 * Copyright (c) 2018 Socionext Inc.
 * All rights reserved.
 */

/*
 * rs_seqcode.cpp
 */

#ifdef WIN32
#include <windows.h>
#endif
#include <stdint.h>

#include "ConvRet.h"
#include "convbin.h"
#include "rs_seqcode.h"
#include "rs_seqcode_local.h"
#include "rs_seqcode_version.h"


DLLAPI int rs_seqcode_init(rs_seqcode_t *bin)
{
	*bin = new class bindata;

	if(*bin == nullptr) {
		return RS_ENOMEM;
	}

	return RS_OK;
}

DLLAPI int rs_seqcode_free(rs_seqcode_t bin)
{
	if(bin == nullptr) {
		return RS_EINV;
	}

	delete(p_bin(bin));

	return RS_OK;
}

DLLAPI int rs_seqcode_save(rs_seqcode_t bin, void *buf, uint32_t bufsize)
{
	if(bin == nullptr) {
		return RS_EINV;
	}
	if(buf == nullptr) {
		return RS_EINV;
	}
	{
		uint32_t size;
		RETMACRO_refretorg(rs_seqcode_get_size(bin, &size), RS_OK);
		if(size < bufsize) {
			return RS_EINV;
		}
	}

	{
		auto pbuf = buf;
		size_t res = bufsize;

//		printf("%s:%d: res=%d\n", __FUNCTION__, __LINE__, res);

		{
			const auto &header = p_bin(bin)->header;
			RETMACRO_refret(bsave8(&pbuf, &res, header.magic), 0, RS_EINV);
			RETMACRO_refret(bsave8(&pbuf, &res, header.reserved1), 0, RS_EINV);
			RETMACRO_refret(bsave8(&pbuf, &res, header.major_ver), 0, RS_EINV);
			RETMACRO_refret(bsave8(&pbuf, &res, header.minor_ver), 0, RS_EINV);
			RETMACRO_refret(bsave32(&pbuf, &res, header.size), 0, RS_EINV);
			RETMACRO_refret(bsave8(&pbuf, &res, header.gen_major_ver), 0, RS_EINV);
			RETMACRO_refret(bsave8(&pbuf, &res, header.gen_minor_ver), 0, RS_EINV);
			RETMACRO_refret(bsave8(&pbuf, &res, header.gen_revision_ver), 0, RS_EINV);
			RETMACRO_refret(bsave8(&pbuf, &res, header.gen_hotfix_ver), 0, RS_EINV);
			RETMACRO_refret(bsave32(&pbuf, &res, header.reserved2), 0, RS_EINV);
		}
		{
			RETMACRO_refret(bsave16(&pbuf, &res, p_bin(bin)->hw_type), 0, RS_EINV);
			RETMACRO_refret(bsave16(&pbuf, &res, p_bin(bin)->hw_major_ver), 0, RS_EINV);
			RETMACRO_refret(bsave16(&pbuf, &res, p_bin(bin)->hw_minor_ver), 0, RS_EINV);
		}
		{
			RETMACRO_refret(bsave32(&pbuf, &res, p_bin(bin)->code_size), 0, RS_EINV);
			RETMACRO_refret(bsave8a(&pbuf, &res, p_bin(bin)->code, p_bin(bin)->code_size), 0, RS_EINV);
		}
		{
			RETMACRO_refret(bsave32(&pbuf, &res, p_bin(bin)->attr_size), 0, RS_EINV);
			RETMACRO_refret(bsave8a(&pbuf, &res, p_bin(bin)->attr, p_bin(bin)->attr_size), 0, RS_EINV);
		}

//		printf("%s:%d: res=%d\n", __FUNCTION__, __LINE__, res);
	}

	return RS_OK;
}

DLLAPI int rs_seqcode_load(rs_seqcode_t bin, const void *buf, uint32_t bufsize)
{
	if(bin == nullptr) {
		return RS_EINV;
	}
	if(buf == nullptr) {
		return RS_EINV;
	}

	{
		auto pbuf = buf;
		size_t res = bufsize;

		{
			auto &header = p_bin(bin)->header;
			RETMACRO_refret(bload8(&pbuf, &res, &(header.magic)), 0, RS_EINV);
			RETMACRO_refret(bload8(&pbuf, &res, &(header.reserved1)), 0, RS_EINV);
			RETMACRO_refret(bload8(&pbuf, &res, &(header.major_ver)), 0, RS_EINV);
			RETMACRO_refret(bload8(&pbuf, &res, &(header.minor_ver)), 0, RS_EINV);
			RETMACRO_refret(bload32(&pbuf, &res, &(header.size)), 0, RS_EINV);
			RETMACRO_refret(bload8(&pbuf, &res, &(header.gen_major_ver)), 0, RS_EINV);
			RETMACRO_refret(bload8(&pbuf, &res, &(header.gen_minor_ver)), 0, RS_EINV);
			RETMACRO_refret(bload8(&pbuf, &res, &(header.gen_revision_ver)), 0, RS_EINV);
			RETMACRO_refret(bload8(&pbuf, &res, &(header.gen_hotfix_ver)), 0, RS_EINV);
			RETMACRO_refret(bload32(&pbuf, &res, &(header.reserved2)), 0, RS_EINV);
		}
		{
			RETMACRO_refret(bload16(&pbuf, &res, &(p_bin(bin)->hw_type)), 0, RS_EINV);
			RETMACRO_refret(bload16(&pbuf, &res, &(p_bin(bin)->hw_major_ver)), 0, RS_EINV);
			RETMACRO_refret(bload16(&pbuf, &res, &(p_bin(bin)->hw_minor_ver)), 0, RS_EINV);
		}
		{
			uint32_t size = 0;
			RETMACRO_refret(bload32(&pbuf, &res, &size), 0, RS_EINV);
			p_bin(bin)->code_resize(size);
			RETMACRO_refret(bload8a(&pbuf, &res, p_bin(bin)->code, p_bin(bin)->code_size), 0, RS_EINV);
		}
		{
			uint32_t size = 0;
			RETMACRO_refret(bload32(&pbuf, &res, &size), 0, RS_EINV);
			p_bin(bin)->attr_resize(size);
			RETMACRO_refret(bload8a(&pbuf, &res, p_bin(bin)->attr, p_bin(bin)->attr_size), 0, RS_EINV);
		}

	}

	RETMACRO_refretorg(rs_seqcode_check(bin), RS_OK);

	return RS_OK;
}

DLLAPI int rs_seqcode_get_size(rs_seqcode_t bin, uint32_t *size)
{
	if(bin == nullptr) {
		return RS_EINV;
	}
	if(size == nullptr) {
		return RS_EINV;
	}

	RETMACRO_refretorg(rs_seqcode_check(bin), RS_OK);

	*size = bindata::header_size + p_bin(bin)->header.size;

	return RS_OK;
}

DLLAPI int rs_seqcode_get_ver(uint16_t *major_ver, uint16_t *minor_ver)
{
	*major_ver = rs_seqcode_data_major_ver;
	*minor_ver = rs_seqcode_data_minor_ver;

	return RS_OK;
}

