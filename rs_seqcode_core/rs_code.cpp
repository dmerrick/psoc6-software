﻿/*
 * Copyright (c) 2017 Socionext Inc.
 * All rights reserved.
 *
 */

/*
 * rs_code.c
 */

#ifdef WIN32
#include <windows.h>
#endif
#include <stdint.h>
#include <stdlib.h>

#include "rs_attr.h"
#include "rs_code.h"


DLLAPI int rs_code_init(struct rs_code_t **code)
{
	*code = new struct rs_code_t();

	(*code)->param[0].size = 0;
	(*code)->param[0].data = nullptr;
	(*code)->param[1].size = 0;
	(*code)->param[1].data = nullptr;
	(*code)->param[2].size = 0;
	(*code)->param[2].data = nullptr;
	(*code)->param[3].size = 0;
	(*code)->param[3].data = nullptr;
	(*code)->frame_size  = 0;
	(*code)->tx_power  = 0;
	(*code)->bit_width   = RS_BIT_WIDTH_16BIT;
	(*code)->header_mode = RS_HEADER_MODE_DISABLE;
	(*code)->status_mode = RS_EX_STATUS_HEADER_MODE_DISABLE;

	return 0;
}

DLLAPI int rs_code_resize_param(struct rs_code_t *code, uint32_t id, uint32_t size)
{
	if(id > 4) {
		return -1;
	}

	if(code->param[id].data == nullptr) {
		delete []code->param[id].data;
	}
	code->param[id].size = size;
	code->param[id].data = new uint8_t[code->param[id].size]();

	return 0;
}

DLLAPI int rs_code_free(struct rs_code_t **code)
{

	if(*code == NULL) {
		return RS_ATTR_EINV;
	}

	for(size_t i = 0; i < (sizeof((*code)->param)/sizeof(*((*code)->param))); i++) {
		if((*code)->param[i].size != 0) {
			delete [](*code)->param[i].data;
		}
	}

	delete *code;

	return 0;
}
