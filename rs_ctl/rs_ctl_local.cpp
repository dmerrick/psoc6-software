﻿/*
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

/*
 * rs_ctl_local.cpp
 * Debug API
 */

#ifdef WIN32
#include <windows.h>
#endif
#include <cstdint>
#include <cstdio>
#include <cstring>
#include <memory>

#include "rs_ctl_local.h"
#include "rs_handle_t.h"
#include "rs_ctl_version.h"

#include "ConvRet.h"
#include "DeviceControl.h"


/* Debug API */
DLLAPI int rs_read_reg(rs_handle_t handle, uint16_t address, uint32_t *value)
{
	if(handle == nullptr){
		return RS_EINV;
	}

	// REG_R
	RETMACRO_refretorg(p_hndl(handle)->get_dev()->read_reg(address, value), RS_OK);

	return RS_OK;
}

DLLAPI int rs_write_reg(rs_handle_t handle, uint16_t address, uint32_t value)
{
	if(handle == nullptr){
		return RS_EINV;
	}

	// REG_W
	RETMACRO_refretorg(p_hndl(handle)->get_dev()->write_reg(address, value), RS_OK);

	return RS_OK;
}

DLLAPI int rs_get_hw_type(rs_handle_t handle, uint16_t *hw_type)
{
	if(handle == nullptr){
		return RS_EINV;
	}
	if(hw_type == nullptr){
		return RS_EINV;
	}

	RETMACRO_refretorg(p_hndl(handle)->get_dev()->get_lsi_type(hw_type), RS_OK);

	return RS_OK;
}
