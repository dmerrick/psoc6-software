/*
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

/*
 * DeviceControl.cpp
 */

#ifdef WIN32
#include <windows.h>
#endif
#include <cstdint>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <memory>

#include "ConvRet.h"
#include "FrameData.h"
#include "DeviceControl.h"
#include "rs_code.h"
#include "common.h"

DeviceControl::DeviceControl() :
	opened(false),
	readstat()
{
	clear_readstat();
}

DeviceControl::~DeviceControl(void)
{
	close();
}

int DeviceControl::open(struct sni_rs_ctrl *_ctrl)
{
	RETMACRO_refret(isopened(), false, RS_ESTATE);

	opened = true;
	ctrl    = _ctrl;

	return RS_OK;
}

int DeviceControl::close(void)
{
	RETMACRO_refret(isopened(), true, RS_ESTATE);

	opened  = false;
	ctrl    = nullptr;

	return RS_OK;
}

int DeviceControl::boot(void)
{
	RETMACRO_refret(isopened(), true, RS_ESTATE);

	// BOOT
	RETMACRO_refretorg_drv(sni_rs_cmd_boot(ctrl), SNI_RET_OK);

	return RS_OK;
}

int DeviceControl::shutdown(void)
{
	RETMACRO_refret(isopened(), true, RS_ESTATE);

	// SHUTDOWN
	RETMACRO_refretorg_drv(sni_rs_cmd_shutdown(ctrl), SNI_RET_OK);

	return RS_OK;
}

int DeviceControl::reset(void)
{
	RETMACRO_refret(isopened(), true, RS_ESTATE);

	// RESET
	RETMACRO_refretorg_drv(sni_rs_cmd_reset(ctrl), SNI_RET_OK);

	return RS_OK;
}

int DeviceControl::get_state(enum DEV_STATE *state)
{
	RETMACRO_refret(isopened(), true, RS_ESTATE);

	// GET_STATUS
	{
		enum SNI_STATE drv_state = sni_rs_ctrl_get_state(ctrl);
		RETMACRO_refret(conv_state(drv_state, state), true, RS_EDEV);
	}

	return RS_OK;
}

bool DeviceControl::conv_state(enum SNI_STATE in_state, enum DEV_STATE *out_state)
{
	switch(in_state){
	case SNI_STATE_INIT:
	case SNI_STATE_BOOT:
	case SNI_STATE_READY:
	case SNI_STATE_SENSING:
	case SNI_STATE_INIT2:
	case SNI_STATE_READY_SD:
	case SNI_STATE_READY_DS:
		*out_state = static_cast<enum DEV_STATE>(in_state);
		break;
	default:
		return false;
	}

	return true;
}

int DeviceControl::clear_readstat(void)
{
	readstat.frame_cnt = 0;
	readstat.frame_err_cnt = 0;

	return RS_OK;
}

int DeviceControl::get_readstat(unsigned int *frame_cnt, unsigned int *frame_err_cnt) const
{
	*frame_cnt     = (unsigned int)readstat.frame_cnt;
	*frame_err_cnt = (unsigned int)readstat.frame_err_cnt;

	return RS_OK;
}

int DeviceControl::get_watermark(uint16_t *val)
{
	RETMACRO_refret(isopened(), true, RS_ESTATE);

	// GET_WATERMARK
	if (sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY)
	|| sni_rs_ctrl_check_state(ctrl, SNI_STATE_SENSING)
	|| sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_SD)
	|| sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_DS)) {
		*val = sni_rs_startinfo_get_watermark_word(&(ctrl->startinfo));
		return RS_OK;
	}
	else {
		return RS_ESTATE;
	}
}

int DeviceControl::set_watermark(uint16_t val)
{
	RETMACRO_refret(isopened(), true, RS_ESTATE);

	// SET_WATERMARK
	RETMACRO_refretorg_drv(sni_rs_cmd_set_watermark(ctrl, val), SNI_RET_OK);

	return RS_OK;
}

int DeviceControl::get_framesize(uint16_t *size)
{
	RETMACRO_refret(isopened(), true, RS_ESTATE);

	// GET_FRAME_SIZE
	if (sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY)
	|| sni_rs_ctrl_check_state(ctrl, SNI_STATE_SENSING)
	|| sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_SD)
	|| sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_DS)) {
		*size = (uint16_t) ctrl->startinfo.frameinfo.word.device_iqdata;
		return RS_OK;
	}
	else {
		return RS_ESTATE;
	}

}

int DeviceControl::get_header_mode(uint8_t *val)
{
	RETMACRO_refret(isopened(), true, RS_ESTATE);

	// GET_HEADER_MODE
	*val = sni_rs_startinfo_get_headheadmode(&(ctrl->startinfo));

	return RS_OK;
}

int DeviceControl::get_status_mode(uint8_t *val)
{
	RETMACRO_refret(isopened(), true, RS_ESTATE);

	// GET_STATUS_MODE
	*val = sni_rs_startinfo_get_statheadmode(&(ctrl->startinfo));

	return RS_OK;
}

int DeviceControl::get_bitwidth(uint8_t *val)
{
	RETMACRO_refret(isopened(), true, RS_ESTATE);

	// GET_BITWIDTH
	*val = sni_rs_startinfo_get_bitwidth(&(ctrl->startinfo));

	return RS_OK;
}

int DeviceControl::set_frame_num(uint8_t val)
{
	RETMACRO_refret(isopened(), true, RS_ESTATE);

	// TX_SET_FRAME_NUM
	if (sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY)
	|| sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_SD)
	|| sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_DS)) {
		if (sni_rs_startinfo_set_framenum(&(ctrl->startinfo), val)) {
			sni_rs_fifo_set_framenum(&(ctrl->fifo), &(ctrl->startinfo));
			return RS_OK;
		} else {
			return RS_EINV;
		}
	} else {
			return RS_ESTATE;
	}
}

int DeviceControl::get_frame_num(uint8_t *val)
{
	RETMACRO_refret(isopened(), true, RS_ESTATE);

	// TX_GET_FRAME_NUM
	if (sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY)
	|| sni_rs_ctrl_check_state(ctrl, SNI_STATE_SENSING)
	|| sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_SD)
	|| sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_DS)) {
		*val = sni_rs_startinfo_get_framenum(&(ctrl->startinfo));
		return RS_OK;
	} else {
		return RS_ESTATE;
	}
}

int DeviceControl::start(bool flag_continue, uint32_t framenum)
{
	RETMACRO_refret(isopened(), true, RS_ESTATE);

	uint8_t fnum = 1;

	if(flag_continue == true){
		RETMACRO_refretorg(update_frameinfo(), RS_OK);

		if(framenum > 0){
			fnum = framenum;
		}
	}

	RETMACRO_refretorg(set_frame_num(fnum), RS_OK);

	RETMACRO_refretorg(update_frameinfo(), RS_OK);

	// START
	if(flag_continue == true){
		RETMACRO_refretorg_drv(sni_rs_cmd_start(ctrl, 1), SNI_RET_OK);
	} else {
		RETMACRO_refretorg_drv(sni_rs_cmd_start(ctrl, 0), SNI_RET_OK);
	}

	return RS_OK;
}

int DeviceControl::stop(void)
{
	RETMACRO_refret(isopened(), true, RS_ESTATE);

	// STOP
	RETMACRO_refretorg_drv(sni_rs_cmd_stop(ctrl), SNI_RET_OK);

	return RS_OK;
}

int DeviceControl::cal(void)
{
	RETMACRO_refret(isopened(), true, RS_ESTATE);

	// CAL
	RETMACRO_refretorg_drv(sni_rs_cmd_cal(ctrl), SNI_RET_OK);

	return RS_OK;
}

int DeviceControl::change_mode(enum DEV_STATE new_state)
{
	RETMACRO_refret(isopened(), true, RS_ESTATE);

	// MODE_CHANGE
	RETMACRO_refretorg_drv(sni_rs_cmd_change_sensemode(ctrl, (uint8_t)new_state), SNI_RET_OK);
//	sni_rs_dmesg_clear();

	return RS_OK;
}

int DeviceControl::write_reg(uint16_t addr, uint32_t val)
{
	RETMACRO_refret(isopened(), true, RS_ESTATE);

	// REG_W
	RETMACRO_refretorg_drv(sni_rs_cmd_reg_w(ctrl, addr, val), SNI_RET_OK);

	return RS_OK;
}

int DeviceControl::read_reg(uint16_t addr, uint32_t *val)
{
	RETMACRO_refret(isopened(), true, RS_ESTATE);

	// REG_R
	RETMACRO_refretorg_drv(sni_rs_cmd_reg_r(ctrl, addr, val), SNI_RET_OK);

	return RS_OK;
}

int DeviceControl::mask_write_reg(uint16_t addr, uint32_t val, uint32_t mask)
{
	uint32_t rval;
	RETMACRO_refretorg(read_reg(addr, &rval), RS_OK);
	const uint32_t wval = (rval & ~mask) | (val & mask);
	RETMACRO_refretorg(write_reg(addr, wval), RS_OK);

	return RS_OK;
}

int DeviceControl::read_status(uint8_t *val)
{
	RETMACRO_refret(isopened(), true, RS_ESTATE);

	// READ_STATUS
	RETMACRO_refretorg_drv(sni_rs_cmd_read_status(ctrl, val), SNI_RET_OK);

	return RS_OK;
}

bool DeviceControl::is_data_exist(void)
{
	// TX_RAW
	{
		uint8_t ret = (uint8_t)sni_rs_cmd_tx_raw(ctrl);
		switch(ret){
		case 0x00:
			break;
		case 0x0c:	// [TX_RAW] warning: detect DMA error
//            printf("warning: detect DMA error\n");
			return true;
		case 0x05:	// [TX_RAW] error: state error
//            printf("state error\n");
			return false;
		case 0x0d:	// [TX_RAW] waiting: no data
//            printf("no data\n");
			return false;
		case 0x03:	// [TX_RAW] error: fatal error
//            printf("fatal error\n");
			return false;
		default:	// [TX_RAW] error: unknown error core
//            printf("unknown error core\n");
			return false;
		}
	}

	return true;
}

int DeviceControl::data_wait(uint32_t timeout_msec)
{
	uint32_t time_start = sni_rs_systimer_get_usec();

	for(;;){
		if(is_data_exist()){
			break;
		}
		uint32_t time_cur = sni_rs_systimer_get_usec();
		uint32_t time_diff = (time_cur - time_start)/1000;
		if(time_diff >= timeout_msec){
			return RS_ETOUT;
		}

		sni_rs_sleep_msec(10);
	}

	return RS_OK;
}

int DeviceControl::read_frame_core(uint8_t *data, size_t size)
{
	size_t dsize = 0;
	void *buf = NULL;

	sni_rs_fifo_get_pagedata(&(ctrl->fifo), &buf, &dsize);
	if(dsize == size) {
		memcpy(data, buf, size);
		sni_rs_fifo_purge_pagedata(&(ctrl->fifo));
	}

	return RS_OK;
}

int DeviceControl::read_frame(class FrameData *frame, uint32_t *err_num)
{
	const size_t fsize = get_frame_bufsize();
	uint8_t *readbuf = new uint8_t[fsize * framenum];	// framenum is 1

	*err_num = 0;

	RETMACRO_refretorg(read_frame_core(readbuf, fsize * framenum), RS_OK);

	frame->init(framesize, h_mode, s_mode, bitmode);
	const bool ret = frame->insert_data(readbuf, fsize);
	if(ret == true){
		readstat.frame_cnt++;
	} else {
		(*err_num)++;
		readstat.frame_err_cnt++;
	}
	// delete readbuf after deconstruct frame

	return RS_OK;
}

int DeviceControl::update_frameinfo(void)
{
	RETMACRO_refretorg(get_framesize(&framesize), RS_OK);
	RETMACRO_refretorg(get_header_mode(&h_mode), RS_OK);
	RETMACRO_refretorg(get_status_mode(&s_mode), RS_OK);
	RETMACRO_refretorg(get_bitwidth(&bitwidth), RS_OK);
	RETMACRO_refretorg(get_frame_num(&framenum), RS_OK);

	bitmode = bitwidth == 0 ? 4 : 3;

	return RS_OK;
}

int DeviceControl::soft_reset(void)
{
	RETMACRO_refret(isopened(), true, RS_ESTATE);

	// SOFT_RESET
	RETMACRO_refretorg_drv(sni_rs_cmd_soft_reset(ctrl), SNI_RET_OK);

	return RS_OK;
}

int DeviceControl::hard_reset(void)
{
	RETMACRO_refret(isopened(), true, RS_ESTATE);

	// HARD_RESET
	RETMACRO_refretorg_drv(sni_rs_cmd_hard_reset(ctrl), SNI_RET_OK);

	return RS_OK;
}

int DeviceControl::deep_sleep(void)
{
	RETMACRO_refret(isopened(), true, RS_ESTATE);

	// DEEPSLEEP
	RETMACRO_refretorg_drv(sni_rs_cmd_deep_sleep(ctrl), SNI_RET_OK);

	return RS_OK;
}

int DeviceControl::setup_code(struct rs_code_t *lcode)
{
	RETMACRO_refret(isopened(), true, RS_ESTATE);

	// SET_REG_S
	RETMACRO_refretorg_drv(sni_rs_cmd_set_reg_s(ctrl, lcode->param[0].data, lcode->param[0].size), SNI_RET_OK);

	// SET_SEQ
	RETMACRO_refretorg_drv(sni_rs_cmd_set_seq(ctrl, lcode->param[1].data, lcode->param[1].size), SNI_RET_OK);

	// SET_SEQ_ADR
	RETMACRO_refretorg_drv(sni_rs_cmd_set_seq_addr(ctrl, lcode->param[2].data, lcode->param[2].size), SNI_RET_OK);

	// SET_FFTWIN
	RETMACRO_refretorg_drv(sni_rs_cmd_set_fftwin(ctrl, lcode->param[3].data, lcode->param[3].size), SNI_RET_OK);

	// SET_FRAME_SIZE
	if (sni_rs_ctrl_check_state(ctrl, SNI_STATE_BOOT)
	|| sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY)) {
		if (!sni_rs_setupinfo_set_framesize(&(ctrl->setupinfo), lcode->frame_size)) {
			return RS_EINV;
		}
	}
	else {
		return RS_ESTATE;
	}

	// SET_TX_POWER
	{
		uint8_t	value = static_cast<uint8_t>(std::abs(lcode->tx_power));
		uint8_t	sign  = static_cast<uint8_t>(lcode->tx_power < 0 ? 1 : 0);
		RETMACRO_refretorg_drv(sni_rs_cmd_set_txpower(ctrl, value, sign), SNI_RET_OK);
	}

	// SET_HEADER_MODE
	{
		if (sni_rs_ctrl_check_state(ctrl, SNI_STATE_BOOT)
		|| sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY)) {
			if (!sni_rs_setupinfo_set_headheadmode(&(ctrl->setupinfo), lcode->header_mode)) {
				return RS_EINV;
			}
		} else {
			return RS_ESTATE;
		}
	}

	// SET_STATUS_MODE
	{
		if (sni_rs_ctrl_check_state(ctrl, SNI_STATE_BOOT)
		|| sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY)) {
			if (!sni_rs_setupinfo_set_statheadmode(&(ctrl->setupinfo), lcode->status_mode)) {
				return RS_EINV;
			}
		} else {
			return RS_ESTATE;
		}
	}

	// SET_BITWIDTH
	{
		if (sni_rs_ctrl_check_state(ctrl, SNI_STATE_BOOT)
		|| sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY)) {
			if (!sni_rs_setupinfo_set_bitwidth(&(ctrl->setupinfo), lcode->bit_width)) {
				return RS_EINV;
			}
		} else {
			return RS_ESTATE;
		}
	}

	// SETUP
	RETMACRO_refretorg_drv(sni_rs_cmd_setup(ctrl), SNI_RET_OK);

	return RS_OK;
}

int DeviceControl::get_drv_version(uint16_t *major_ver, uint16_t *minor_ver, uint16_t *revision, uint16_t *hotfix)
{
	RETMACRO_refret(isopened(), true, RS_ESTATE);

	*major_ver = sni_rs_version_major;
	*minor_ver = sni_rs_version_minor;
	*revision  = sni_rs_version_variation;
	*hotfix    = sni_rs_version_update;

	return RS_OK;
}

int DeviceControl::get_lsi_version(uint16_t *major, uint16_t *minor)
{
	// major
	{
		uint32_t val0;
		RETMACRO_refretorg(read_reg(0xcd, &val0), RS_OK);
		*major = (val0 & 0x000000f0) >> 4;
	}

	// minor
	{
		uint32_t val1;
		RETMACRO_refretorg(read_reg(0xf6, &val1), RS_OK);
		*minor = (val1 & 0x0000ffff) >> 0;
	}

	return RS_OK;
}

int DeviceControl::get_lsi_type(uint16_t *type)
{
	// type
	{
		uint32_t val0;
		RETMACRO_refretorg(read_reg(0xf6, &val0), RS_OK);
		*type = (val0 & 0xffff0000) >> 16;
	}

	return RS_OK;
}

int DeviceControl::get_frameinfo(uint32_t *time, uint32_t *fcount, uint32_t *fcount_err, uint32_t *fcount_ovf, uint8_t *stored_size, uint8_t *queue_size)
{
	RETMACRO_refret(isopened(), true, RS_ESTATE);

	// GET_STATISTICS
	{
		const uint8_t *data;
		size_t size;

		RETMACRO_refretorg_drv(sni_rs_cmd_get_statistics(ctrl, &data, &size), SNI_RET_OK);

		if(size != STATISTICS_SIZE) {
			return RS_EINV;
		}

		conv_8to32_le(time,       (uint8_t *)&data[0]);
		conv_8to32_le(fcount,     (uint8_t *)&data[4]);
		conv_8to32_le(fcount_err, (uint8_t *)&data[8]);
		conv_8to32_le(fcount_ovf, (uint8_t *)&data[12]);
    
		*stored_size = data[16];
		*queue_size  = data[17];
	}

	return RS_OK;
}

