﻿/*
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

/*
 * FrameData.cpp
 */

#include <cstdint>
#include <cstdio>
#include <cstring>

#include "FrameData.h"
#include "ConvRet.h"
#include "common.h"

//#define d_printf printf
#define d_printf(...)

static uint32_t pre_time_info = 0;
static uint32_t pre_counter = 0;

static inline bool check_header(const uint8_t **in, size_t *in_size, struct FrameDataHeader *header)
{
	RETMACRO_refret(*in_size >= 20, true, false);
	RETMACRO_refret(*in != nullptr, true, false);

	uint32_t header_buf[5];
	array2uint32(*in, 20, header_buf, 4);
	*in_size -= 20;
	*in += 20;
	const uint16_t syncword = (header_buf[0] & 0xffff0000) >> 16;

	header->header_version = (header_buf[0] & 0x0000ff00) >>  8;
	header->header_size    = (header_buf[0] & 0x000000ff) >>  0;
	header->time_info      = header_buf[1];
	header->counter        = header_buf[2];
	header->framesize      = (header_buf[3] & 0xffff0000) >> 16;
	header->h_mode         = (header_buf[3] & 0x00008000) >> 15;
	header->s_mode         = (header_buf[3] & 0x00004000) >> 14;
	header->bitwidth       = (header_buf[3] & 0x00003000) >> 12;
	header->framenum       = (header_buf[4] & 0xff000000) >> 24;
	header->frame_no       = (header_buf[4] & 0x00ff0000) >> 16;

	pre_time_info = header->time_info;
	pre_counter = header->counter;

	if(syncword != 0x55aa){
		return false;
	}
	if(header->header_version != 0x0){
		return false;
	}
	if(header->header_size != 20){
		return false;
	}

	return true;
}

bool FrameData::insert_data(const uint8_t *in, size_t in_size)
{
	RETMACRO_refret(in != nullptr, true, false);

	RETMACRO_refret(data_exist, false, false);

	frame.data_w_header = (uint8_t *) in;

	if(check_header(&in, &in_size, &header) != true) {
		return false;
	}

	if(header.framesize != framesize){
		return false;
	}
	if(header.h_mode != h_mode){
		return false;
	}
	if(header.s_mode != s_mode){
		return false;
	}
	if((header.bitwidth == 0 ? 4 : 3) != bitmode){
		return false;
	}

	if((in_size % bitmode) != 0){
		return false;
	}

	// check end-code
	{
		RETMACRO_refret(in_size >= bitmode, true, false);

		uint32_t endcode = 0;
		array2uint32(in + in_size - bitmode, bitmode, &endcode, bitmode);

		if((bitmode == 4) && (endcode != 0x80018001)){
			return false;
		}
		if((bitmode == 3) && (endcode != 0x801801)){
			return false;
		}

		in_size -= bitmode;
	}

	// get status
	if(s_mode != 0){
		RETMACRO_refret(in_size >= bitmode, true, false);

		uint32_t data = 0;
		array2uint32(in, bitmode, &data, bitmode);

		frame.status = data & 0x000000ff;

		in += bitmode;
		in_size -= bitmode;
	}

	// get frame
	if(h_mode != 0){
		RETMACRO_refret(in_size >= bitmode, true, false);

		uint32_t data = 0;
		array2uint32(in, bitmode, &data, bitmode);

		const uint32_t check = (data & 0x40000000) >> 29;
		if((bitmode == 4) && (check != 0x2)){
			return false;
		}

		frame.counter = (data & 0x00ff0000) >> 16;
		frame.size = data & 0x00001fff;

		in += bitmode;
		in_size -= bitmode;
	}

	// get data
	{
		RETMACRO_refret(in_size, framesize * bitmode, false);

		frame.data = (uint8_t *)in;
//		memcpy(frame.data, in, in_size);
	}

	data_exist = true;

	return true;
}

bool FrameData::get_rawdata(uint32_t *size, uint32_t *counter, uint32_t *status, uint32_t *time, void *data) const
{
	RETMACRO_refret(data_exist, true, false);

	RETMACRO_refret(size != nullptr, true, false);
	RETMACRO_refret(counter != nullptr, true, false);
	RETMACRO_refret(status != nullptr, true, false);
	RETMACRO_refret(time != nullptr, true, false);
	RETMACRO_refret(data != nullptr, true, false);

	*size = frame.size;
	*counter = frame.counter;
	*status = frame.status;
	*time = header.time_info;
	memcpy(data, frame.data, framesize * bitmode);

	return true;
}

bool FrameData::get_framesize(uint32_t *size) const
{
	RETMACRO_refret(data_exist, true, false);

	*size = framesize;

	return true;
}
