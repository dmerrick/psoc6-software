﻿/*
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

/*
 * rs_ctl.h
 */

#ifndef __RS_CTL_H__
#define __RS_CTL_H__

#include <stdint.h>

#include "rs_retcode.h"
#include "sni_rs_ctrl.h"
#include "sni_rs_termctl.h"

#ifndef EXTERN_C
#ifdef __cplusplus
#define EXTERN_C extern "C"
#else	/* __cplusplus */
#define EXTERN_C
#endif	/* __cplusplus */
#endif	/* EXTERN_C */

#define DLLAPI EXTERN_C

/*
 * Type Definition
 */
typedef void *rs_handle_t;

/*
 * Enumerated Type Definition
 */

/*
 * Constant Definition
 */
#define RS_SETUP_TYPE_SEQCODE		1

#define RS_CONTINUOUS_RUN			1
#define RS_SINGLE_RUN				0

/*
 * Struct Definition
 */

/*
 * Function Definition
 */
DLLAPI int rs_init(void);
DLLAPI int rs_open(rs_handle_t *handle, struct sni_rs_ctrl *ctrl);
DLLAPI int rs_close(rs_handle_t handle);
DLLAPI int rs_reset(rs_handle_t handle);
DLLAPI int rs_boot(rs_handle_t handle);
DLLAPI int rs_shutdown(rs_handle_t handle);
DLLAPI int rs_soft_reset(rs_handle_t handle);
DLLAPI int rs_hard_reset(rs_handle_t handle);
DLLAPI int rs_deep_sleep(rs_handle_t handle);

DLLAPI int rs_setup(rs_handle_t handle, int setup_type, void *setup);
DLLAPI int rs_start(rs_handle_t handle, int type);
DLLAPI int rs_stop(rs_handle_t handle);
DLLAPI int rs_cal(rs_handle_t handle);
DLLAPI int rs_change_sensemode(rs_handle_t handle, int new_status);

DLLAPI int rs_read_status(rs_handle_t handle, uint8_t *status);
DLLAPI int rs_get_watermark(rs_handle_t handle, int *watermark);
DLLAPI int rs_change_watermark(rs_handle_t handle, int watermark);

DLLAPI int rs_get_framesize(rs_handle_t handle, int *framesize);
DLLAPI int rs_get_api_ver(uint16_t *api_major_ver, uint16_t *api_minor_ver);
DLLAPI int rs_get_hw_ver(rs_handle_t handle, uint16_t *hw_major_ver, uint16_t *hw_minor_ver);
DLLAPI int rs_get_lib_ver(rs_handle_t handle, uint16_t *lib_major_ver, uint16_t *lib_minor_ver, uint16_t *lib_revision, uint16_t *lib_hotfix);
DLLAPI int rs_get_drv_ver(rs_handle_t handle, uint16_t *drv_major_ver, uint16_t *drv_minor_ver, uint16_t *drv_revision, uint16_t *drv_hotfix);
DLLAPI int rs_get_rawdata(rs_handle_t handle, unsigned int timeout, unsigned int readsize, unsigned int *size, unsigned int *counter, unsigned int *status, unsigned int *time, unsigned char *framedata);
DLLAPI int rs_get_status(rs_handle_t handle, int *status);
DLLAPI int rs_get_frame_statistics_drv(rs_handle_t handle, unsigned int *time, unsigned int *frame_count, unsigned int *frame_error_count, unsigned int *overflow_count, unsigned int *stored_size, unsigned int *queue_size);

#endif	/* __RS_CTL_H__ */
