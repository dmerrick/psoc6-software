﻿/*
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

/*
 * FrameData.h
 */

#ifndef __FrameData_H__
#define __FrameData_H__

#include <cstdint>
#include <vector>

struct FrameDataCore
{
	uint32_t size;
	uint32_t counter;
	uint32_t status;
	uint8_t *data_w_header;
	uint8_t *data;
};

struct FrameDataHeader
{
	uint8_t header_version;
	uint8_t header_size;
	uint32_t time_info;
	uint32_t counter;
	uint16_t framesize;
	uint8_t h_mode;
	uint8_t s_mode;
	uint8_t bitwidth;
	uint8_t framenum;
	uint8_t frame_no;
};

class FrameData
{
private:
	uint16_t framesize;
	uint8_t h_mode;
	uint8_t s_mode;
	uint8_t bitmode;
	struct FrameDataCore frame;
	struct FrameDataHeader header;
	bool data_exist;

public:
	FrameData() {
		frame.data_w_header = nullptr;
		frame.data = nullptr;
	}
	FrameData(uint16_t _framesize, uint8_t _h_mode, uint8_t _s_mode, uint8_t _bitmode) :
		framesize(_framesize),
		h_mode(_h_mode),
		s_mode(_s_mode),
		bitmode(_bitmode),
		frame({}),
		header({}),
		data_exist(false)
		{
		}

	~FrameData(void)
		{
			if(frame.data_w_header != nullptr) {
				delete[] frame.data_w_header;
				frame.data_w_header = nullptr;
				frame.data = nullptr;
			}
		}

	void init(uint16_t _framesize, uint8_t _h_mode, uint8_t _s_mode, uint8_t _bitmode)
	{
		framesize =_framesize;
		h_mode =_h_mode;
		s_mode =_s_mode;
		bitmode =_bitmode;
		frame = {0, 0, 0, nullptr, nullptr};
		header = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}; 
		data_exist =false;
	}

	bool insert_data(const uint8_t *in, size_t in_size);
	bool get_rawdata(uint32_t *size, uint32_t *counter, uint32_t *status, uint32_t *time, void *data) const;
	bool get_framesize(uint32_t *size) const;
};

#endif	/* __FrameData_H__ */
