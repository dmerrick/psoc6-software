﻿/*
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

/*
 * rs_ctl_local.h
 */

#ifndef __RS_CTL_LOCAL_H__
#define __RS_CTL_LOCAL_H__

#ifdef WIN32
#include <windows.h>
#endif
#include <stdint.h>

#include "rs_ctl.h"

#ifndef EXTERN_C
#ifdef __cplusplus
#define EXTERN_C extern "C"
#else	/* __cplusplus */
#define EXTERN_C
#endif	/* __cplusplus */
#endif	/* EXTERN_C */

#ifdef WIN32
#ifndef DLLAPI
#define DLLAPI EXTERN_C __declspec(dllimport)
#endif	/* DLLAPI */
#else	/* WIN32 */
#define DLLAPI EXTERN_C
#endif	/* WIN32 */

/*
 * Type Definition
 */

/*
 * Enumerated Type Definition
 */

/*
 * Constant Definition
 */

/*
 * Struct Definition
 */

/*
 * Function Definition
 */

/* Debug API */
DLLAPI int rs_read_reg(rs_handle_t handle, uint16_t address, uint32_t *value);
DLLAPI int rs_write_reg(rs_handle_t handle, uint16_t address, uint32_t value);
DLLAPI int rs_get_hw_type(rs_handle_t handle, uint16_t *hw_type);

#endif	/* __RS_CTL_LOCAL_H__ */
