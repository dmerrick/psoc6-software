﻿/*
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

/*
 * rs_handle_t.h
 */

#ifndef __rs_handle_t_H__
#define __rs_handle_t_H__

#include "sni_rs_ctrl.h"
#include "sni_rs_termctl.h"
#include "DeviceControl.h"

class _rs_handle_t
{
private:
	_rs_handle_t(_rs_handle_t const&) = delete;
	_rs_handle_t(_rs_handle_t&&) = delete;
	_rs_handle_t& operator =(_rs_handle_t const&) = delete;
	_rs_handle_t& operator =(_rs_handle_t&&) = delete;

	class DeviceControl dev_ctl;

public:
	uint32_t debug_framenum;

public:
	_rs_handle_t() :
		dev_ctl(),
		debug_framenum(0) {}
	~_rs_handle_t(void) = default;
	inline class DeviceControl *get_dev(void) {return &dev_ctl;}
};

static inline class _rs_handle_t *p_hndl(rs_handle_t handle)
{
	return static_cast<class _rs_handle_t *>(handle);
}

#endif	/* __rs_handle_t_H__ */
