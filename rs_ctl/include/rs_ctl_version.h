﻿/*
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

/*
 * rs_ctl_version.h
 */


#ifndef	__RS_CTL_VERSION_H__
#define	__RS_CTL_VERSION_H__

#include <stdint.h>

/*
 * Version of API
 */
const uint16_t api_version_major = 0;
const uint16_t api_version_minor = 9;

/*
 * Library (DLL/so) Version
 */
const uint16_t library_version_major = api_version_major;
const uint16_t library_version_minor = api_version_minor;
const uint16_t library_version_revision = 0;
const uint16_t library_version_hotfix = 0;

#endif	/* __RS_CTL_VERSION_H__ */
