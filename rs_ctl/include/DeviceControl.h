﻿/*
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

/*
 * DeviceControl.h
 */

#ifndef __DeviceControl_H__
#define __DeviceControl_H__

#include <cstdint>
#include <chrono>
#include <list>
#include <memory>

#include "rs_ctl.h"
//#include "rs_ctl_local.h"
#include "rs_code.h"

#include "sni_rs_ctrl.h"
#include "sni_rs_termctl.h"
#include "sni_rs_cmd.h"
//#include "sni_rs_dmesg.h"
#include "sni_rs_version.h"

#include "ConvRet.h"
#include "FrameData.h"

class DeviceControl
{
public:
	enum DEV_STATE
	{
		DEV_STATE_ERROR = -1,
		DEV_STATE_INIT = 0,
		DEV_STATE_BOOT = 1,
		DEV_STATE_READY = 2,
		DEV_STATE_SENSING = 3,
		DEV_STATE_INIT2 = 4,
		DEV_STATE_READY_SD = 5,
		DEV_STATE_READY_DS = 6,
	};

	// opcode
	static const uint8_t SPI_OP_WRITE_DATA		= 0x02;
	static const uint8_t SPI_OP_READ_DATA		= 0x0B;

	// reg
//	static const uint16_t REG_DIG_1F			= 0x009F;
//	static const uint16_t REG_BBSET40			= 0x00A8;

	// addr
	static const uint32_t SPI_ADDR_FFT 			= 0x030000;

private:

//	std::unique_ptr<DeviceControlTarget> target;

	bool opened;

	uint16_t framesize;
	uint8_t h_mode;
	uint8_t s_mode;
	uint8_t bitwidth;
	uint8_t bitmode;
	uint8_t framenum;

	struct {
		uint32_t frame_cnt;
		uint32_t frame_err_cnt;
	} readstat;

	int update_frameinfo(void);

	inline size_t get_frame_bufsize(void) const {
		return (((h_mode == 1) ? 1 : 0) + ((s_mode == 1) ? 1 : 0) + framesize + 1) * bitmode + 20;
	}

public:
	DeviceControl(void);
	~DeviceControl(void);

	inline bool isopened(void) {
		return opened;
	}

	struct sni_rs_ctrl      *ctrl;

	int open(struct sni_rs_ctrl *_ctrl);
	int close(void);

	int boot(void);
	int shutdown(void);

	int reset(void);

	int start(bool flag_continue, uint32_t framenum = 0);
	int stop(void);
	int cal(void);
	int change_mode(enum DEV_STATE new_state);

	int write_reg(uint16_t addr, uint32_t val);
	int read_reg(uint16_t addr, uint32_t *val);
	int mask_write_reg(uint16_t addr, uint32_t val, uint32_t mask);
	int read_status(uint8_t *val);

	int get_state(enum DEV_STATE *state);
	bool conv_state(enum SNI_STATE in_state, enum DEV_STATE *out_state);

private:

public:
	inline int check_state(enum DEV_STATE state) {
		enum DEV_STATE cur_state;
		RETMACRO_refretorg(get_state(&cur_state), RS_OK);
		return cur_state == state ? RS_OK : RS_ESTATE;
	}

	int clear_readstat(void);
	int get_readstat(unsigned int *frame_cnt, unsigned int *frame_err_cnt) const;

public:
	int get_watermark(uint16_t *val);
	int set_watermark(uint16_t val);

	int get_framesize(uint16_t *size);
	int get_header_mode(uint8_t *val);
	int get_status_mode(uint8_t *val);
	int get_bitwidth(uint8_t *val);

	int set_frame_num(uint8_t val);
	int get_frame_num(uint8_t *val);

	bool is_data_exist(void);
	int data_wait(uint32_t timeout_msec);
	int read_frame_core(uint8_t *data, size_t size);
	int read_frame(class FrameData *frame, uint32_t *err_num);

	int soft_reset(void);
	int hard_reset(void);
	int deep_sleep(void);

	int setup_code(struct rs_code_t *lcode);

	int get_drv_version(uint16_t *major_ver, uint16_t *minor_ver, uint16_t *revision, uint16_t *hotfix);
	int get_lsi_version(uint16_t *major, uint16_t *minor);
	int get_lsi_type(uint16_t *type);
	int get_frameinfo(uint32_t *time, uint32_t *fcount, uint32_t *fcount_err, uint32_t *fcount_ovf, uint8_t *stored_size, uint8_t *queue_size);
};

#endif	/* __DeviceControl_H__ */
