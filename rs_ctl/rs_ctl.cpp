/*
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

/*
 * rs_ctl.cpp
 */

#include <cstdint>
#include <cstdio>
#include <cstring>
#include <memory>

#include "rs_ctl.h"
#include "rs_ctl_local.h"
#include "rs_handle_t.h"
#include "rs_ctl_version.h"
#include "ConvRet.h"
#include "rs_code.h"
#include "rs_seqcode.h"
#include "rs_seqcode_conv.h"
#include "common.h"

#include "FrameData.h"

DLLAPI int rs_init(void)
{
	return RS_OK;
}

DLLAPI int rs_open(rs_handle_t *handle, struct sni_rs_ctrl *ctrl)
{
	if(handle == nullptr){
		return RS_EFAULT;
	}

	*handle = new class _rs_handle_t;

	if(*handle == nullptr){
		return RS_ENOMEM;
	}

	// device open
	int ret;
	if((ret = p_hndl(*handle)->get_dev()->open(ctrl)) != RS_OK) {
		delete p_hndl(*handle);
		return  ret;
	}

	return RS_OK;
}

DLLAPI int rs_close(rs_handle_t handle)
{
	if(handle == nullptr){
		return RS_EINV;
	}

	delete p_hndl(handle);

	return RS_OK;
}

DLLAPI int rs_reset(rs_handle_t handle)
{
	if(handle == nullptr){
		return RS_EINV;
	}

	// RESET
	RETMACRO_refretorg(p_hndl(handle)->get_dev()->reset(), RS_OK);

	return RS_OK;
}

DLLAPI int rs_boot(rs_handle_t handle)
{
	if(handle == nullptr){
        printf("handle == NULL in rs_boot\n");
		return RS_EINV;
	}

	// BOOT
	RETMACRO_refretorg(p_hndl(handle)->get_dev()->boot(), RS_OK);

	return RS_OK;
}

DLLAPI int rs_shutdown(rs_handle_t handle)
{
	if(handle == nullptr){
		return RS_EINV;
	}

	// SHUTDOWN
	RETMACRO_refretorg(p_hndl(handle)->get_dev()->shutdown(), RS_OK);

	return RS_OK;
}

DLLAPI int rs_soft_reset(rs_handle_t handle)
{
	if(handle == nullptr){
		return RS_EINV;
	}

	// SOFT_RESET
	 RETMACRO_refretorg(p_hndl(handle)->get_dev()->soft_reset(), RS_OK);

	return RS_OK;
}

DLLAPI int rs_hard_reset(rs_handle_t handle)
{
	if(handle == nullptr){
		return RS_EINV;
	}

	// HARD_RESET
	RETMACRO_refretorg(p_hndl(handle)->get_dev()->hard_reset(), RS_OK);

	return RS_OK;
}

DLLAPI int rs_deep_sleep(rs_handle_t handle)
{
	if(handle == nullptr){
		return RS_EINV;
	}

	// DEEP_SLEEP
	RETMACRO_refretorg(p_hndl(handle)->get_dev()->deep_sleep(), RS_OK);

	return RS_OK;
}

static inline int rs_setup_seqcode(rs_handle_t handle, rs_seqcode_t code_bin)
{
	if(handle == nullptr){
        printf("handle is null\n");
		return RS_EINV;
	}

	{
		uint16_t hw_major_ver = -1;
		uint16_t hw_minor_ver = -1;
		uint16_t bin_hw_major_ver = 0;
		uint16_t bin_hw_minor_ver = 0;
		RETMACRO_refretorg(rs_get_hw_ver(handle, &hw_major_ver, &hw_minor_ver), RS_OK);
        printf("rs_get_hw_ver: hw_major_ver: %d, hw_minor_ver: %d\n", hw_major_ver, hw_minor_ver);
		RETMACRO_refretorg(rs_seqcode_get_hw_ver(code_bin, &bin_hw_major_ver, &bin_hw_minor_ver), 0);
        printf("rs_seqcode_get_hw_ver: bin_hw_major_ver: %d, bin_hw_minor_ver: %d\n", bin_hw_major_ver, bin_hw_minor_ver);
		RETMACRO_refret(hw_major_ver == bin_hw_major_ver, true, RS_EFORM);
		RETMACRO_refret(hw_minor_ver == bin_hw_minor_ver, true, RS_EFORM);
	}
	{
		uint16_t hw_type = 0;
		uint16_t bin_hw_type = 0;
		RETMACRO_refretorg(rs_get_hw_type(handle, &hw_type), RS_OK);
		RETMACRO_refretorg(rs_seqcode_get_hw_type(code_bin, &bin_hw_type), 0);
		RETMACRO_refret(hw_type == bin_hw_type, true, RS_EFORM);
	}
	{
		struct rs_code_t *code = nullptr;
		int ret;

		RETMACRO_refretorg(rs_code_init(&code), RS_OK);
		if((ret = rs_seqcode_get_code(code_bin, code)) != 0) {
            printf("rs_seqcode_get_code failed\n");
			RETMACRO_refretorg(rs_code_free(&code), RS_OK);
			return ret;
		}
		if((ret = p_hndl(handle)->get_dev()->setup_code(code)) != RS_OK) { // here is where we flash the sequencer code to radar
            printf("setupcode failed\n");
			RETMACRO_refretorg(rs_code_free(&code), RS_OK);
			return ret;
		}
		RETMACRO_refretorg(rs_code_free(&code), RS_OK);
	}
    // printf("returning RS_OK");
	return RS_OK;
}

DLLAPI int rs_setup(rs_handle_t handle, int setup_type, void *setup)
{
	if(handle == nullptr){
		return RS_EINV;
	}

	RETMACRO_refretorg(p_hndl(handle)->get_dev()->clear_readstat(), RS_OK);

	switch(setup_type) {
	    case RS_SETUP_TYPE_SEQCODE:
		    return rs_setup_seqcode(handle, setup);
        default:
            break;
	}

	return RS_EINV;
}

DLLAPI int rs_start(rs_handle_t handle, int type)
{
	if(handle == nullptr){
		return RS_EINV;
	}

	RETMACRO_refretorg(p_hndl(handle)->get_dev()->clear_readstat(), RS_OK);

	// START
	switch(type) {
		case RS_CONTINUOUS_RUN:
			RETMACRO_refretorg(p_hndl(handle)->get_dev()->start(true, p_hndl(handle)->debug_framenum), RS_OK);
			break;
		case RS_SINGLE_RUN:
			RETMACRO_refretorg(p_hndl(handle)->get_dev()->start(false), RS_OK);
			break;
		default:
			return RS_EINV;
	}

	return RS_OK;
}

DLLAPI int rs_stop(rs_handle_t handle)
{
	if(handle == nullptr){
		return RS_EINV;
	}

	// STOP
	RETMACRO_refretorg(p_hndl(handle)->get_dev()->stop(), RS_OK);

	return RS_OK;
}

DLLAPI int rs_cal(rs_handle_t handle)
{
	if(handle == nullptr){
		return RS_EINV;
	}
	// CAL
	RETMACRO_refretorg(p_hndl(handle)->get_dev()->cal(), RS_OK);

	return RS_OK;
}

DLLAPI int rs_change_sensemode(rs_handle_t handle, int new_state)
{
	if(handle == nullptr){
		return RS_EINV;
	}
	{
		enum DeviceControl::DEV_STATE state = (enum DeviceControl::DEV_STATE)new_state;
		// MODECHANGE
		RETMACRO_refretorg(p_hndl(handle)->get_dev()->change_mode(state), RS_OK);
	}

	return RS_OK;
}

DLLAPI int rs_read_status(rs_handle_t handle, uint8_t *status)
{
	if(handle == nullptr){
		return RS_EINV;
	}
	// READ_STATUS
	RETMACRO_refretorg(p_hndl(handle)->get_dev()->read_status(status), RS_OK);

	return RS_OK;
}

DLLAPI int rs_get_watermark(rs_handle_t handle, int *watermark)
{
	if(handle == nullptr){
		return RS_EINV;
	}

	uint16_t val;
	RETMACRO_refretorg(p_hndl(handle)->get_dev()->get_watermark(&val), RS_OK);
	*watermark = val;

	return RS_OK;
}

DLLAPI int rs_change_watermark(rs_handle_t handle, int watermark)
{
	if(handle == nullptr){
		return RS_EINV;
	}

	if((watermark < 1) || (watermark > 0xffff)){
		return RS_EINV;
	}

	RETMACRO_refretorg(p_hndl(handle)->get_dev()->set_watermark(static_cast<uint16_t>(watermark)), RS_OK);

	return RS_OK;
}

DLLAPI int rs_get_framesize(rs_handle_t handle, int *framesize)
{
	if(handle == nullptr){
		return RS_EINV;
	}

	uint16_t _framesize;
	RETMACRO_refretorg(p_hndl(handle)->get_dev()->get_framesize(&_framesize), RS_OK);

	*framesize = _framesize;

	return RS_OK;
}

DLLAPI int rs_get_api_ver(uint16_t *api_major_ver, uint16_t *api_minor_ver)
{
	if(api_major_ver == nullptr){
		return RS_EINV;
	}
	if(api_minor_ver == nullptr){
		return RS_EINV;
	}

	*api_major_ver = api_version_major;
	*api_minor_ver = api_version_minor;

	return RS_OK;
}

DLLAPI int rs_get_hw_ver(rs_handle_t handle, uint16_t *hw_major_ver, uint16_t *hw_minor_ver)
{
	if(handle == nullptr){
		return RS_EINV;
	}
	if(hw_major_ver == nullptr){
		return RS_EINV;
	}
	if(hw_minor_ver == nullptr){
		return RS_EINV;
	}

	RETMACRO_refretorg(p_hndl(handle)->get_dev()->get_lsi_version(hw_major_ver, hw_minor_ver), RS_OK);

	return RS_OK;
}

DLLAPI int rs_get_lib_ver(rs_handle_t handle, uint16_t *lib_major_ver, uint16_t *lib_minor_ver, uint16_t *lib_revision, uint16_t *lib_hotfix)
{
	if(handle == nullptr){
		return RS_EINV;
	}
	if(lib_major_ver == nullptr){
		return RS_EINV;
	}
	if(lib_minor_ver == nullptr){
		return RS_EINV;
	}
	if(lib_revision == nullptr){
		return RS_EINV;
	}
	if(lib_hotfix == nullptr){
		return RS_EINV;
	}

	*lib_major_ver = library_version_major;
	*lib_minor_ver = library_version_minor;
	*lib_revision = library_version_revision;
	*lib_hotfix = library_version_hotfix;

	return RS_OK;
}

DLLAPI int rs_get_drv_ver(rs_handle_t handle, uint16_t *drv_major_ver, uint16_t *drv_minor_ver, uint16_t *drv_revision, uint16_t *drv_hotfix)
{
	if(handle == nullptr){
		return RS_EINV;
	}
	if(drv_major_ver == nullptr){
		return RS_EINV;
	}
	if(drv_minor_ver == nullptr){
		return RS_EINV;
	}
	if(drv_revision == nullptr){
		return RS_EINV;
	}
	if(drv_hotfix == nullptr){
		return RS_EINV;
	}

	RETMACRO_refretorg(p_hndl(handle)->get_dev()->get_drv_version(drv_major_ver, drv_minor_ver, drv_revision, drv_hotfix), RS_OK);

	return RS_OK;
}

DLLAPI int rs_get_rawdata(rs_handle_t handle, unsigned int timeout, unsigned int readsize, unsigned int *size, unsigned int *counter, unsigned int *status, unsigned int *time, unsigned char *framedata)
{
	if(handle == nullptr){
		return RS_EINV;
	}
	if(size == nullptr){
		return RS_EFAULT;
	}
	if(counter == nullptr){
		return RS_EFAULT;
	}
	if(status == nullptr){
		return RS_EFAULT;
	}
	if(time == nullptr){
		return RS_EFAULT;
	}
	if(framedata == nullptr){
		return RS_EFAULT;
	}
	if(readsize > 1){ // no support
		return RS_EINV;
	}

	RETMACRO_refretorg(p_hndl(handle)->get_dev()->data_wait(timeout), RS_OK);
	{
		class FrameData frame;
		uint32_t err_num;
		RETMACRO_refretorg(p_hndl(handle)->get_dev()->read_frame(&frame, &err_num), RS_OK);
		RETMACRO_refret(err_num, 0, RS_EDEV);

		uint32_t lsize = 0;
		uint32_t lcounter = 0;
		uint32_t lstatus = 0;
		uint32_t ltime = 0;

		RETMACRO_refret(frame.get_rawdata(&lsize, &lcounter, &lstatus, &ltime, framedata), true, RS_EDEV);

		*size = lsize;
		*counter = lcounter;
		*status = lstatus;
		*time = ltime;
	}

	return RS_OK;
}

DLLAPI int rs_get_status(rs_handle_t handle, int *status)
{
	if(handle == nullptr){
		return RS_EINV;
	}

	enum DeviceControl::DEV_STATE state;
	RETMACRO_refretorg(p_hndl(handle)->get_dev()->get_state(&state), RS_OK);
	*status = (int)state; 

	return RS_OK;
}

DLLAPI int rs_get_frame_statistics_drv(rs_handle_t handle, unsigned int *time, unsigned int *frame_count, unsigned int *frame_error_count, unsigned int *overflow_count, unsigned int *stored_size, unsigned int *queue_size)
{
	if(handle == nullptr){
		return RS_EINV;
	}

	uint32_t _time;
	uint32_t _frame_count;
	uint32_t _frame_error_count;
	uint32_t _overflow_count;
	uint8_t _stored_size;
	uint8_t _queue_size;
	RETMACRO_refretorg(p_hndl(handle)->get_dev()->get_frameinfo(&_time, &_frame_count, &_frame_error_count, &_overflow_count, &_stored_size, &_queue_size), RS_OK);
	*time              = _time;               
	*frame_count       = _frame_count;
	*frame_error_count = _frame_error_count;
	*overflow_count    = _overflow_count;    
	*stored_size       = _stored_size;
	*queue_size        = _queue_size;

	return RS_OK;
}
