/******************************************************************************
* File Name:   main.c
*
* Description: This example project demonstrates the basic operation of SPI
* resource as Master using HAL. The SPI master sends command packets
* to the SPI slave to control an user LED.
*
* Related Document: See README.md
*
*
*******************************************************************************
* Copyright 2019-2021, Cypress Semiconductor Corporation (an Infineon company) or
* an affiliate of Cypress Semiconductor Corporation.  All rights reserved.
*
* This software, including source code, documentation and related
* materials ("Software") is owned by Cypress Semiconductor Corporation
* or one of its affiliates ("Cypress") and is protected by and subject to
* worldwide patent protection (United States and foreign),
* United States copyright laws and international treaty provisions.
* Therefore, you may use this Software only as provided in the license
* agreement accompanying the software package from which you
* obtained this Software ("EULA").
* If no EULA applies, Cypress hereby grants you a personal, non-exclusive,
* non-transferable license to copy, modify, and compile the Software
* source code solely for use in connection with Cypress's
* integrated circuit products.  Any reproduction, modification, translation,
* compilation, or representation of this Software except as specified
* above is prohibited without the express written permission of Cypress.
*
* Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. Cypress
* reserves the right to make changes to the Software without notice. Cypress
* does not assume any liability arising out of the application or use of the
* Software or any product or circuit described in the Software. Cypress does
* not authorize its products for use in any products where a malfunction or
* failure of the Cypress product may reasonably be expected to result in
* significant property damage, injury or death ("High Risk Product"). By
* including Cypress's product in a High Risk Product, the manufacturer
* of such system or application assumes all risk of such use and in doing
* so agrees to indemnify Cypress against all liability.
*******************************************************************************/

#include "cy_pdl.h"
#include "cyhal.h"
#include "cybsp.h"
#include "cy_retarget_io.h"
#include "app_config.h"
#include "sni_rs_spi.h"

/***************************************
*            Constants
****************************************/
#define SPI_FREQ_HZ                (1000000UL)
#define CMD_TO_CMD_DELAY_MS        (1000UL)

/*******************************************************************************
* Function Name: handle_error
********************************************************************************
* Summary:
* User defined error handling function
*
* Parameters:
*  void
*
* Return:
*  void
*
*******************************************************************************/
void handle_error(void)
{
     /* Disable all interrupts. */
    __disable_irq();

    CY_ASSERT(0);
}

/*******************************************************************************
* Function Name: main
********************************************************************************
* Summary:
* This is the main function for CM4 CPU.
*   1. SPI Master sends command packet to the slave
*   2. Slave reads the packet and executes the instructions
*
* Parameters:
*  void
*
* Return:
*  int
*
*******************************************************************************/
int main(void)
{
    Cy_SysLib_Delay(1000u); // Add delay else debugger may force some code to be executed twice

    cy_rslt_t result;

    /* Initialize the device and board peripherals */
    result = cybsp_init();

    if (result != CY_RSLT_SUCCESS)
    {
        handle_error();
    }

    result = cy_retarget_io_init( CYBSP_DEBUG_UART_TX, CYBSP_DEBUG_UART_RX,
                                  CY_RETARGET_IO_BAUDRATE);

    if (result != CY_RSLT_SUCCESS)
    {
        handle_error();
    }


    /* Initialize the SPI Master Interface */
	printf("Configuring SPI master...\r\n");
	cyhal_spi_t mSPI;

    memset(&mSPI, 0, sizeof(mSPI)); // bug in some board support packages

	result = cyhal_spi_init( &mSPI, mSPI_MOSI, mSPI_MISO, mSPI_SCLK,
							 mSPI_SS, NULL, 8, CYHAL_SPI_MODE_00_MSB, false);

	if (result != CY_RSLT_SUCCESS)
	{
		handle_error();
	}

	result = cyhal_spi_set_frequency( &mSPI, SPI_FREQ_HZ);

	if (result != CY_RSLT_SUCCESS)
	{
		handle_error();
	}


	// Initialize pin NRST GPIO as an output with strong drive mode (Digital Output)
	// and initial value = false (low) - Low NRST == RADAR Reset
    result = cyhal_gpio_init(NRST, CYHAL_GPIO_DIR_OUTPUT, CYHAL_GPIO_DRIVE_STRONG, false);
	if (result != CY_RSLT_SUCCESS)
	{
		handle_error();
	}

	// Initialize pin CE GPIO as an output with strong drive mode (Digital Output)
	// and initial value = false (low) - Low CE == RADAR OFF
	result = cyhal_gpio_init(CE, CYHAL_GPIO_DIR_OUTPUT, CYHAL_GPIO_DRIVE_STRONG, false);
	if (result != CY_RSLT_SUCCESS)
	{
		handle_error();
	}

    /* Enable interrupts */
    __enable_irq();

    { // Shutdown RADAR
		cyhal_gpio_write(NRST, false);
		Cy_SysLib_Delay(50U);  // 50 ms delay
		cyhal_gpio_write(CE, false);
    }

    { // Turn on RADAR
		cyhal_gpio_write(CE, true);
		Cy_SysLib_Delay(50U);  // 5 ms delay
		cyhal_gpio_write(NRST, true);
    }

    { // 1. send soft reset command to RADAR
        // sni_rs_spi_resetctl(ctrl->_rf_spi1, OP_SOFT_RESET);
        cyhal_spi_send(&mSPI, OP_SOFT_RESET);
		Cy_SysLib_Delay(50U);  // 5 ms delay
    }

    { // 2. Initial register setting for water mark
		uint8_t rcv_data[8] = {0};
		uint8_t wdata[8]    = {0};

		wdata[0] = OP_WRITE_DATA;
		wdata[1] = (REG_WATERMARK >> 16) & 0xff;
		wdata[2] = (REG_WATERMARK >>  8) & 0xff;
		wdata[3] = (REG_WATERMARK >>  0) & 0xff;

		wdata[4] = (0x00001FFF >> 24) & 0xff;
		wdata[5] = (0x00001FFF >> 16) & 0xff;
		wdata[6] = (0x00001FFF >>  8) & 0xff;
		wdata[7] = (0x00001FFF >>  0) & 0xff;

		cyhal_spi_transfer(&mSPI, wdata, 8u, rcv_data, 8u, 0xFF);
		Cy_SysLib_Delay(50U);  // 50 ms delay

    }

    { // 3. e-fuse - Set efuse read enable trigger
		// sni_rs_spi_reg_write32(ctrl->_rf_spi1, REG_DIG_18, 0x00000001);
		uint8_t wdata[8]    = {0};
		uint8_t rcv_data[8] = {0};

		wdata[0] = OP_WRITE_DATA;
		wdata[1] = (REG_DIG_18 >> 16) & 0xff;
		wdata[2] = (REG_DIG_18 >>  8) & 0xff;
		wdata[3] = (REG_DIG_18 >>  0) & 0xff;

		wdata[4] = (0x00000001 >> 24) & 0xff;
		wdata[5] = (0x00000001 >> 16) & 0xff;
		wdata[6] = (0x00000001 >>  8) & 0xff;
		wdata[7] = (0x00000001 >>  0) & 0xff;

		cyhal_spi_transfer(&mSPI, wdata, 8u, rcv_data, 8u, 0xFF);
		Cy_SysLib_Delay(50U);  // 50 ms delay

    }
    { // Set internal clock enable
    	// sni_rs_spi_reg_write32(ctrl->_rf_spi1, REG_DIG_2F, 0x00000F0F);
		uint8_t wdata[8]    = {0};
		uint8_t rcv_data[8] = {0};

    	wdata[0] = OP_WRITE_DATA;
		wdata[1] = (REG_DIG_2F >> 16) & 0xff;
		wdata[2] = (REG_DIG_2F >>  8) & 0xff;
		wdata[3] = (REG_DIG_2F >>  0) & 0xff;

		wdata[4] = (0x00000F0F >> 24) & 0xff;
		wdata[5] = (0x00000F0F >> 16) & 0xff;
		wdata[6] = (0x00000F0F >>  8) & 0xff;
		wdata[7] = (0x00000F0F >>  0) & 0xff;

		cyhal_spi_transfer(&mSPI, wdata, 8u, rcv_data, 8u, 0xFF);
		Cy_SysLib_Delay(50U);  // 50 ms delay

    }

    { // 5. Set Sel13Register
    	//	sni_rs_spi_reg_write32(ctrl->_rf_spi1, REG_SELSET13, 0x00000000);
		uint8_t wdata[8]    = {0};
		uint8_t rcv_data[8] = {0};

    	wdata[0] = OP_WRITE_DATA;
    	wdata[1] = (REG_SELSET13 >> 16) & 0xff;
    	wdata[2] = (REG_SELSET13 >>  8) & 0xff;
    	wdata[3] = (REG_SELSET13 >>  0) & 0xff;

    	wdata[4] = (0x00000000 >> 24) & 0xff;
    	wdata[5] = (0x00000000 >> 16) & 0xff;
    	wdata[6] = (0x00000000 >>  8) & 0xff;
    	wdata[7] = (0x00000000 >>  0) & 0xff;


    	cyhal_spi_transfer(&mSPI, wdata, 8u, rcv_data, 8u, 0xFF);
		Cy_SysLib_Delay(50U);  // 50 ms delay

    }
    { // deep sleep
        cyhal_spi_send(&mSPI, OP_DEEP_SLEEP);
		Cy_SysLib_Delay(50U);  // 5 ms delay
    }
	//	/* 4. Read EFUSE registers */
	//	// uint32_t rdata;
	//	// sni_rs_spi_reg_read32(ctrl->_rf_spi1, REG_EFUSE36, &rdata);
	//	// sni_rs_spi_reg_read32(ctrl->_rf_spi1, REG_EFUSE16, &rdata);
	//
	//	// Read EFUSE32
	//	uint32_t rdata;
	//
	//	uint8_t wdata2[5] = {0};
	//
	//	wdata2[0] = OP_READ_DATA;
	//	wdata2[1] = (REG_EFUSE36 >> 16) & 0xff;
	//	wdata2[2] = (REG_EFUSE36 >>  8) & 0xff;
	//	wdata2[3] = (REG_EFUSE36 >>  0) & 0xff;
	//	wdata2[4] = 0x00; // dummy byte
	//
	//	uint8_t rcv_data2[9] = {0};
	//
	//	cyhal_spi_transfer(&mSPI, wdata2, 5u, rcv_data2, 9u, 0xFF);
	//
	//	rdata = rcv_data2[5];
	//	rdata <<= 8;
	//	rdata |= rcv_data2[6];
	//	rdata <<= 8;
	//	rdata |= rcv_data2[7];
	//	rdata <<= 8;
	//	rdata |= rcv_data2[8];
	//
	//	printf("EFUSE32 Register Value: %ld", rdata);
	//
	//	// Read EFUSE16
	//	uint32_t rdata2;
	//
	//	uint8_t wdata3[5] = {0};
	//	wdata3[0] = OP_READ_DATA;
	//	wdata3[1] = (REG_EFUSE16 >> 16) & 0xff;
	//	wdata3[2] = (REG_EFUSE16 >>  8) & 0xff;
	//	wdata3[3] = (REG_EFUSE16 >>  0) & 0xff;
	//	wdata3[4] = 0x00; // dummy byte
	//
	//	uint8_t rcv_data3[9] = {0};
	//
	//	cyhal_spi_transfer(&mSPI, wdata3, 5u, rcv_data3, 9u, 0xFF);
	//
	//	rdata2 = 0;
	//	rdata2 = rcv_data3[5];
	//	rdata2 <<= 8;
	//	rdata2 |= rcv_data3[6];
	//	rdata2 <<= 8;
	//	rdata2 |= rcv_data3[7];
	//	rdata2 <<= 8;
	//	rdata2 |= rcv_data3[8];
	//
	//	printf("EFUSE16 Register Value: %ld", rdata2);


    { // Read Hardware Major Version
		uint32_t major_version;
		uint8_t wdata[5]    = {0};
		uint8_t rcv_data[9] = {0};

		wdata[0] = OP_READ_DATA;
		wdata[1] = (0x0000CD >> 16) & 0xff;
		wdata[2] = (0x0000CD >>  8) & 0xff;
		wdata[3] = (0x0000CD >>  0) & 0xff;
		wdata[4] = 0x00; // dummy byte

		cyhal_spi_transfer(&mSPI, wdata, 5u, rcv_data, 9u, 0xFF);
		Cy_SysLib_Delay(50U);  // 50 ms delay

		major_version = 0;
		major_version = rcv_data[5];
		major_version <<= 8;
		major_version |= rcv_data[6];
		major_version <<= 8;
		major_version |= rcv_data[7];
		major_version <<= 8;
		major_version |= rcv_data[8];
		major_version = (major_version & 0x000000f0) >> 4;

		printf("Hardware Major Version: %ld\r\n", major_version);
    }

    { // Read Hardware Minor Version
		uint32_t minor_version;
		uint8_t wdata[5]    = {0};
		uint8_t rcv_data[9] = {0};

		wdata[0] = OP_READ_DATA;
		wdata[1] = (REG_EFUSE37 >> 16) & 0xff;
		wdata[2] = (REG_EFUSE37 >>  8) & 0xff;
		wdata[3] = (REG_EFUSE37 >>  0) & 0xff;
		wdata[4] = 0x00; // dummy byte

		cyhal_spi_transfer(&mSPI, wdata, 5u, rcv_data, 9u, 0xFF);
		Cy_SysLib_Delay(50U);  // 50 ms delay

		minor_version = 0;
		minor_version = rcv_data[5];
		minor_version <<= 8;
		minor_version |= rcv_data[6];
		minor_version <<= 8;
		minor_version |= rcv_data[7];
		minor_version <<= 8;
		minor_version |= rcv_data[8];

		minor_version = (minor_version & 0x0000ffff) >> 0;

		printf("Hardware Minor Version: %ld\r\n", minor_version);
    }
}
