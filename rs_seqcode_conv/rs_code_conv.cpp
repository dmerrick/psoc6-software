﻿/*
 * Copyright (c) 2018 Socionext Inc.
 * All rights reserved.
 *
 */

/*
 * rs_code_conv.c
 */

#ifdef WIN32
#include <windows.h>
#endif
#include <stdint.h>

#include "rs_retcode.h"
#include "rs_code.h"
#include "ConvRet.h"
#include "convbin.h"
#include "rs_code_conv.h"


DLLAPI int rs_code_conv_save(const struct rs_code_t *code, void *buf, uint32_t bufsize)
{
	if(code == nullptr) {
		return RS_EINV;
	}
	if(buf == nullptr) {
		return RS_EINV;
	}

	{
		auto pbuf = buf;
		size_t res = bufsize;

//		printf("%s:%d: res=%d\n", __FUNCTION__, __LINE__, res);

		for(int i=0; i<4; i++) {
			RETMACRO_refret(bsave32(&pbuf, &res, code->param[i].size), 0, RS_EINV);
			RETMACRO_refret(bsave8a(&pbuf, &res, code->param[i].data, code->param[i].size), 0, RS_EINV);
		}

		RETMACRO_refret(bsave16(&pbuf, &res, code->frame_size), 0, RS_EINV);
		RETMACRO_refret(bsave32(&pbuf, &res, static_cast<uint32_t>(code->tx_power)), 0, RS_EINV);
		RETMACRO_refret(bsave32(&pbuf, &res, code->bit_width), 0, RS_EINV);
		RETMACRO_refret(bsave32(&pbuf, &res, code->header_mode), 0, RS_EINV);
		RETMACRO_refret(bsave32(&pbuf, &res, code->status_mode), 0, RS_EINV);

//		printf("%s:%d: res=%d\n", __FUNCTION__, __LINE__, res);
	}

	return RS_OK;
}

DLLAPI int rs_code_conv_load(struct rs_code_t *code, const void *buf, uint32_t bufsize)
{
	if(code == nullptr) {
		return RS_EINV;
	}
	if(buf == nullptr) {
		return RS_EINV;
	}

	{
		auto pbuf = buf;
		size_t res = bufsize;

//		printf("%s:%d: res=%d\n", __FUNCTION__, __LINE__, res);

		for(int i=0; i<4; i++) {
			RETMACRO_refret(bload32(&pbuf, &res, &(code->param[i].size)), 0, RS_EINV);

			code->param[i].data = new uint8_t[code->param[i].size]();
			if(code->param[i].data == nullptr) {
				return RS_ENOMEM;
			}
			RETMACRO_refret(bload8a(&pbuf, &res, code->param[i].data, code->param[i].size), 0, RS_EINV);
		}

		RETMACRO_refret(bload16(&pbuf, &res, &(code->frame_size)), 0, RS_EINV);
		RETMACRO_refret(bload32(&pbuf, &res, reinterpret_cast<uint32_t *>(&(code->tx_power))), 0, RS_EINV);
		{
			uint32_t d;
			RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
			code->bit_width = static_cast<decltype(code->bit_width)>(d);
		}
		{
			uint32_t d;
			RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
			code->header_mode = static_cast<decltype(code->header_mode)>(d);
		}
		{
			uint32_t d;
			RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
			code->status_mode = static_cast<decltype(code->status_mode)>(d);
		}

//		printf("%s:%d: res=%d\n", __FUNCTION__, __LINE__, res);
	}

	return RS_OK;
}

DLLAPI int rs_code_conv_get_size(const struct rs_code_t *code, uint32_t *size)
{
	if(code == nullptr) {
		return RS_EINV;
	}
	if(size == nullptr) {
		return RS_EINV;
	}

	*size = 0;
	for(int i=0; i<4; i++) {
		*size += 4 + code->param[i].size;
	}
	*size += 2 + 4 + 4 + 4 + 4;

	return RS_OK;
}
