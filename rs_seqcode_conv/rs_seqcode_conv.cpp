﻿/*
 * Copyright (c) 2018 Socionext Inc.
 * All rights reserved.
 */

/*
 * rs_seqcode_conv.cpp
 */

#ifdef WIN32
#include <windows.h>
#endif
#include <stdint.h>

#include "ConvRet.h"
#include "rs_retcode.h"
#include "rs_attr.h"
#include "rs_attr_conv.h"
#include "rs_code.h"
#include "rs_code_conv.h"

#include "rs_seqcode.h"
#include "rs_seqcode_local.h"
#include "rs_seqcode_conv.h"

int rs_seqcode_get_hw_ver(rs_seqcode_t bin, uint16_t *hw_major_ver, uint16_t *hw_minor_ver)
{
	if(bin == nullptr) {
		return RS_EINV;
	}
	if(hw_major_ver == nullptr) {
		return RS_EINV;
	}
	if(hw_minor_ver == nullptr) {
		return RS_EINV;
	}

	RETMACRO_refretorg(rs_seqcode_check(bin), RS_OK);

	*hw_major_ver = p_bin(bin)->hw_major_ver;
	*hw_minor_ver = p_bin(bin)->hw_minor_ver;

	return RS_OK;
}

int rs_seqcode_get_hw_type(rs_seqcode_t bin, uint16_t *hw_type)
{
	if(bin == nullptr) {
		return RS_EINV;
	}
	if(hw_type == nullptr) {
		return RS_EINV;
	}

	RETMACRO_refretorg(rs_seqcode_check(bin), RS_OK);

	*hw_type = p_bin(bin)->hw_type;

	return RS_OK;
}
int rs_seqcode_get_code(rs_seqcode_t bin, struct rs_code_t *code)
{
	if(bin == nullptr) {
		return RS_EINV;
	}
	if(code == nullptr) {
		return RS_EINV;
	}

	RETMACRO_refretorg(rs_seqcode_check(bin), RS_OK);

	RETMACRO_refretorg(rs_code_conv_load(code, p_bin(bin)->code, p_bin(bin)->code_size), RS_OK);
	
	return RS_OK;
}

int rs_seqcode_get_attr(rs_seqcode_t bin, rs_attr_t attr)
{
	if(bin == nullptr) {
		return RS_EINV;
	}
	if(attr == nullptr) {
		return RS_EINV;
	}

	RETMACRO_refretorg(rs_seqcode_check(bin), RS_OK);

	RETMACRO_refretorg(rs_attr_conv_load(attr, p_bin(bin)->attr, p_bin(bin)->attr_size), RS_OK);
	
	return RS_OK;
}
