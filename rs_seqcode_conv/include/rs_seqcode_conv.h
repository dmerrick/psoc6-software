﻿/*
 * Copyright (c) 2018 Socionext Inc.
 * All rights reserved.
 */

/*
 * rs_seqcode_conv.h
 */

#ifndef __RS_SEQCODE_CONV_H__
#define __RS_SEQCODE_CONV_H__

#ifdef WIN32
#include <windows.h>
#endif
#include <stdint.h>

#include "rs_attr.h"
#include "rs_seqcode.h"
#include "rs_code.h"

/*
 * Type Definition
 */

/*
 * Enumerated Type Definition
 */

/*
 * Constant Definition
 */

/*
 * Struct Definition
 */

/*
 * Function Definition
 */
#ifdef __cplusplus
extern "C" {
#endif  /* __cplusplus */

	extern int rs_seqcode_get_hw_ver(rs_seqcode_t bin, uint16_t *hw_major_ver, uint16_t *hw_minor_ver);
	extern int rs_seqcode_get_hw_type(rs_seqcode_t bin, uint16_t *hw_type);
	extern int rs_seqcode_get_code(rs_seqcode_t bin, struct rs_code_t *code);

#ifdef __cplusplus
}
#endif  /* __cplusplus */

#endif	/* __RS_SEQCODE_CONV_H__ */
