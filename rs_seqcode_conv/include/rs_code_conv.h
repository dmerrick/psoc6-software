﻿/*
 * Copyright (c) 2018 Socionext Inc.
 * All rights reserved.
 *
 */

#ifndef	__RS_CODE_CONV_H__
#define	__RS_CODE_CONV_H__

#ifdef WIN32
#include <windows.h>
#endif
#include <stdint.h>

#include "rs_code.h"

#ifndef EXTERN_C
#ifdef __cplusplus
#define EXTERN_C extern "C"
#else	/* __cplusplus */
#define EXTERN_C
#endif	/* __cplusplus */
#endif	/* EXTERN_C */

#ifdef WIN32
#ifndef DLLAPI
#define DLLAPI EXTERN_C __declspec(dllimport)
#endif	/* DLLAPI */
#else	/* WIN32 */
#define DLLAPI EXTERN_C
#endif	/* WIN32 */

/*
 * define macro
 */

/*
 * Constant Definition
 */

/*
 * Default paramters
 */

/*
 * Type Definition
 */

/*
 * Enumerated Type Definition
 */

/*
 * Function Definition
 */
#ifndef __NOT_USE_IMPORT_LIBRARY__
DLLAPI int rs_code_conv_save(const struct rs_code_t *code, void *buf, uint32_t bufsize);
DLLAPI int rs_code_conv_load(struct rs_code_t *code, const void *buf, uint32_t bufsize);
DLLAPI int rs_code_conv_get_size(const struct rs_code_t *code, uint32_t *size);
#endif	// __NOT_USE_IMPORT_LIBRARY__

#endif	/* __RS_CODE_CONV_H__ */
