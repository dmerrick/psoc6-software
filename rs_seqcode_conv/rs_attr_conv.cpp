﻿/*
 * Copyright (c) 2018 Socionext Inc.
 * All rights reserved.
 *
 */

/*
 * rs_attr_conv.cpp
 */

#ifdef WIN32
#include <windows.h>
#endif
#include <stdint.h>

#include "rs_retcode.h"
#include "rs_attr.h"
#include "rs_attr_core.h"
#include "ConvRet.h"
#include "convbin.h"
#include "rs_attr_conv.h"

DLLAPI int rs_attr_conv_save(const rs_attr_t attr, void *buf, uint32_t bufsize)
{
	if(attr == NULL) {
		return RS_EINV;
	}
	if(buf == NULL) {
		return RS_EINV;
	}

	{
		auto pbuf = buf;
		size_t res = bufsize;
		uint32_t i;

//		printf("%s:%d: res=%ld\n", __FUNCTION__, __LINE__, res);

		// set generator version uint8_t x 4
		RETMACRO_refret(bsave8(&pbuf, &res, p_rs_attr_t(attr)->fw_major_ver), 0, RS_EINV);
		RETMACRO_refret(bsave8(&pbuf, &res, p_rs_attr_t(attr)->fw_minor_ver), 0, RS_EINV);
		RETMACRO_refret(bsave8(&pbuf, &res, p_rs_attr_t(attr)->fw_revision), 0, RS_EINV);
		RETMACRO_refret(bsave8(&pbuf, &res, p_rs_attr_t(attr)->fw_hotfix), 0, RS_EINV);

		// enum rs_modulation_mode	modu_mode;
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->modu_mode), 0, RS_EINV);
		// int32_t	tx_power;
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->tx_power), 0, RS_EINV);
		// uint32_t	rx_gain;
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->rx_gain), 0, RS_EINV);
		// uint32_t	frame_rate;
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->frame_rate), 0, RS_EINV);
		// uint32_t	chirp_rate_constant;
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->chirp_rate_constant), 0, RS_EINV);
		// uint32_t	center_freq;
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->center_freq), 0, RS_EINV);
		// uint32_t	xtal_freq;
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->xtal_freq), 0, RS_EINV);
		// int32_t	calc;
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->calc), 0, RS_EINV);

		// enum rs_init_cal_mode	init_cal_mode;
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->init_cal_mode), 0, RS_EINV);
		// enum rs_bit_width		bit_width;
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->bit_width), 0, RS_EINV);
		// enum rs_header_mode		header_mode;
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->header_mode), 0, RS_EINV);
		// enum rs_ex_status_header_mode	status_mode;
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->status_mode), 0, RS_EINV);
		// enum rs_fft_winfunc	fft_winfunc
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->fft_winfunc), 0, RS_EINV);
		// enum rs_fft_en	fft_en
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->fft_en), 0, RS_EINV);

		// struct rs_fmcw_r	fmcw_r;
		// uint8_t				repeat_num;
		RETMACRO_refret(bsave8(&pbuf, &res, p_rs_attr_t(attr)->fmcw_r.repeat_num), 0, RS_EINV);
		// uint8_t				rxch;
		RETMACRO_refret(bsave8(&pbuf, &res,  p_rs_attr_t(attr)->fmcw_r.chirp_fmcw.rxch), 0, RS_EINV);
		// uint8_t				txch;
		RETMACRO_refret(bsave8(&pbuf, &res,  p_rs_attr_t(attr)->fmcw_r.chirp_fmcw.txch), 0, RS_EINV);
		// enum rs_IQ_mode		iq_mode;
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->fmcw_r.chirp_fmcw.iq_mode), 0, RS_EINV);
		// enum rs_chirp_mode	chirp_mode;
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->fmcw_r.chirp_fmcw.chirp_mode), 0, RS_EINV);
		// uint32_t				band_width;
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->fmcw_r.chirp_fmcw.band_width), 0, RS_EINV);
		// uint32_t				sweep_time;
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->fmcw_r.chirp_fmcw.sweep_time), 0, RS_EINV);
		// uint16				point_num;
		RETMACRO_refret(bsave16(&pbuf, &res, p_rs_attr_t(attr)->fmcw_r.chirp_fmcw.point_num), 0, RS_EINV);
		// uint32_t				lpf_fc
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->fmcw_r.chirp_fmcw.lpf_fc), 0, RS_EINV);
		// uint32_t				hpf_fc
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->fmcw_r.chirp_fmcw.hpf_fc), 0, RS_EINV);

		// enum rs_fft_dc_cut	fft_dc_cut
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->fft.fft_dc_cut), 0, RS_EINV);
		// enum rs_fft_point	fft_point
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->fft.fft_point), 0, RS_EINV);
		// uint16_t	fft_upper_range
		RETMACRO_refret(bsave16(&pbuf, &res, p_rs_attr_t(attr)->fft.fft_upper_range), 0, RS_EINV);
		
		// struct rs_fmcw_m	fmcw_m;
		// uint8_t				repeat_num;
		RETMACRO_refret(bsave8(&pbuf, &res, p_rs_attr_t(attr)->fmcw_m.repeat_num), 0, RS_EINV);
		// uint8_t				multi_chirp_num;
		RETMACRO_refret(bsave8(&pbuf, &res, p_rs_attr_t(attr)->fmcw_m.multi_chirp_num), 0, RS_EINV);

		// struct rs_fmcw_multi	chirp[RS_MAX_CHIRP_PATTERN];
		for(i = 0; i < RS_MAX_CHIRP_PATTERN; i++) {
			// uint32_t				rx_gain;
			RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].rx_gain), 0, RS_EINV);
			// uint8_t				rxch;
			RETMACRO_refret(bsave8(&pbuf, &res,  p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].rxch), 0, RS_EINV);
			// uint8_t				txch;
			RETMACRO_refret(bsave8(&pbuf, &res,  p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].txch), 0, RS_EINV);
			// enum rs_IQ_mode		iq_mode;
			RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].iq_mode), 0, RS_EINV);
			// enum rs_chirp_mode	chirp_mode;
			RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].chirp_mode), 0, RS_EINV);
			// uint32_t				band_width;
			RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].band_width), 0, RS_EINV);
			// enum rs_sweep_time	sweep_time;
			RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].sweep_time), 0, RS_EINV);
			// uint16_t				point_num;
			RETMACRO_refret(bsave16(&pbuf, &res, p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].point_num), 0, RS_EINV);
			// enum rs_lpf_fc		lpf_fc
			RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].lpf_fc), 0, RS_EINV);
			// enum rs_hpf_fc		hpf_fc
			RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].hpf_fc), 0, RS_EINV);
		}

		// struct rs_chirp_fft_multi	fft_multi;
		// uint8_t				multi_chirp_num;
		RETMACRO_refret(bsave8(&pbuf, &res, p_rs_attr_t(attr)->fft_multi.multi_chirp_num), 0, RS_EINV);

		for(i = 0; i < RS_MAX_CHIRP_PATTERN; i++) {
			// enum rs_fft_dc_cut	fft_dc_cut
			RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->fft_multi.fft[i].fft_dc_cut), 0, RS_EINV);
			// enum rs_fft_point	fft_point
			RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->fft_multi.fft[i].fft_point), 0, RS_EINV);
			// uint16_t	fft_upper_range
			RETMACRO_refret(bsave16(&pbuf, &res, p_rs_attr_t(attr)->fft_multi.fft[i].fft_upper_range), 0, RS_EINV);
		}
			
		// struct rs_chirp_cw_h		cw_h;
		// uint32_t				rx_gain;
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->cw_h.rx_gain), 0, RS_EINV);
		// uint8_t              rxch;
		RETMACRO_refret(bsave8(&pbuf, &res,  p_rs_attr_t(attr)->cw_h.rxch), 0, RS_EINV);
		// uint8_t              txch;
		RETMACRO_refret(bsave8(&pbuf, &res,  p_rs_attr_t(attr)->cw_h.txch), 0, RS_EINV);
		// enum rs_IQ_mode      iq_mode;
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->cw_h.iq_mode), 0, RS_EINV);
		// enum rs_sweep_time	sweep_time;
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->cw_h.sweep_time), 0, RS_EINV);
		// enum rs_lpf_fc		point_num;
		RETMACRO_refret(bsave16(&pbuf, &res, p_rs_attr_t(attr)->cw_h.point_num), 0, RS_EINV);
		// uint32_t             lpf_fc;
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->cw_h.lpf_fc), 0, RS_EINV);

		// struct rs_chirp_cw_l		cw_l;
		// uint8_t				repeat_num;
		RETMACRO_refret(bsave8(&pbuf, &res, p_rs_attr_t(attr)->cw_l.repeat_num), 0, RS_EINV);
		// enum rs_rx_gain		rx_gain;
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->cw_l.rx_gain), 0, RS_EINV);
		// uint8_t              rxch;
		RETMACRO_refret(bsave8(&pbuf, &res,  p_rs_attr_t(attr)->cw_l.rxch), 0, RS_EINV);
		// uint8_t              txch;
		RETMACRO_refret(bsave8(&pbuf, &res,  p_rs_attr_t(attr)->cw_l.txch), 0, RS_EINV);
		// enum rs_IQ_mode      iq_mode;
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->cw_l.iq_mode), 0, RS_EINV);
		// enum rs_sweep_time	sweep_time;
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->cw_l.sweep_time), 0, RS_EINV);
		// enum rs_lpf_fc		lpf_fc;
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->cw_l.lpf_fc), 0, RS_EINV);

		// struct rs_chirp_fskcw	fskcw;
		// uint8_t				repeat_num;
		RETMACRO_refret(bsave8(&pbuf, &res, p_rs_attr_t(attr)->fskcw.repeat_num), 0, RS_EINV);
		// enum rs_rx_gain		rx_gain;
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->fskcw.rx_gain), 0, RS_EINV);
		// uint8_t				rxch;
		RETMACRO_refret(bsave8(&pbuf, &res,  p_rs_attr_t(attr)->fskcw.rxch), 0, RS_EINV);
		// uint8_t				txch;
		RETMACRO_refret(bsave8(&pbuf, &res,  p_rs_attr_t(attr)->fskcw.txch), 0, RS_EINV);
		// enum rs_IQ_mode		iq_mode;
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->fskcw.iq_mode), 0, RS_EINV);
		// uint32_t				band_width;
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->fskcw.band_width), 0, RS_EINV);
		// uint32_t				sweep_time;
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->fskcw.sweep_time), 0, RS_EINV);
		// uint32_tlpf_fc;
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->fskcw.lpf_fc), 0, RS_EINV);

		// struct rs_test_ct test_ct;
		// uint8_t                 txch;
		RETMACRO_refret(bsave8(&pbuf, &res, p_rs_attr_t(attr)->test_ct.txch), 0, RS_EINV);

		// struct rs_test_cr test_cr;
		// uint32_t                sweep_time;
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->test_cr.sweep_time), 0, RS_EINV);
		// uint32_t                lpf_fc;
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->test_cr.lpf_fc), 0, RS_EINV);

		// uint16_t				hw_major_ver;
		RETMACRO_refret(bsave16(&pbuf, &res, p_rs_attr_t(attr)->hw_major_ver), 0, RS_EINV);
		// uint16_t				hw_minor_ver;
		RETMACRO_refret(bsave16(&pbuf, &res, p_rs_attr_t(attr)->hw_minor_ver), 0, RS_EINV);
		// uint16_t				hw_type;
		RETMACRO_refret(bsave16(&pbuf, &res, p_rs_attr_t(attr)->hw_type), 0, RS_EINV);
		// uint32_t				eval_mode;
		RETMACRO_refret(bsave32(&pbuf, &res, p_rs_attr_t(attr)->eval_mode), 0, RS_EINV);

//		printf("%s:%d: res=%ld\n", __FUNCTION__, __LINE__, res);
	}

	return RS_OK;
}

DLLAPI int rs_attr_conv_load(rs_attr_t attr, const void *buf, uint32_t bufsize)
{
	if(attr == NULL) {
		return RS_EINV;
	}
	if(buf == NULL) {
		return RS_EINV;
	}

	{
		auto pbuf = buf;
		size_t res = bufsize;
		uint32_t i;

//		printf("%s:%d: res=%ld\n", __FUNCTION__, __LINE__, res);

		// set generator version uint8_t x 4
		RETMACRO_refret(bload8(&pbuf, &res, &(p_rs_attr_t(attr)->fw_major_ver)), 0, RS_EINV);
		RETMACRO_refret(bload8(&pbuf, &res, &(p_rs_attr_t(attr)->fw_minor_ver)), 0, RS_EINV);
		RETMACRO_refret(bload8(&pbuf, &res, &(p_rs_attr_t(attr)->fw_revision)), 0, RS_EINV);
		RETMACRO_refret(bload8(&pbuf, &res, &(p_rs_attr_t(attr)->fw_hotfix)), 0, RS_EINV);

		// enum rs_modulation_mode		modu_mode;
		{
			uint32_t d;
			RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
			p_rs_attr_t(attr)->modu_mode = static_cast<decltype(p_rs_attr_t(attr)->modu_mode)>(d);
		}
		// int32_t			tx_power;
		{
			uint32_t d;
			RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
			p_rs_attr_t(attr)->tx_power = static_cast<decltype(p_rs_attr_t(attr)->tx_power)>(d);
		}
		// uint32_t			rx_gain;
		RETMACRO_refret(bload32(&pbuf, &res, &(p_rs_attr_t(attr)->rx_gain)), 0, RS_EINV);
		// uint32_t			frame_rate;
		RETMACRO_refret(bload32(&pbuf, &res, &(p_rs_attr_t(attr)->frame_rate)), 0, RS_EINV);
		// uint32_t			chirp_rate_constant;
		RETMACRO_refret(bload32(&pbuf, &res, &(p_rs_attr_t(attr)->chirp_rate_constant)), 0, RS_EINV);
		// uint32_t			center_freq;
		RETMACRO_refret(bload32(&pbuf, &res, &(p_rs_attr_t(attr)->center_freq)), 0, RS_EINV);
		// uint32_t			xtal_freq;
		RETMACRO_refret(bload32(&pbuf, &res, &(p_rs_attr_t(attr)->xtal_freq)), 0, RS_EINV);

		// int32_t			calc;
		{
			uint32_t d;
			RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
			p_rs_attr_t(attr)->calc = static_cast<decltype(p_rs_attr_t(attr)->calc)>(d);
		}
		// enum rs_init_cal_mode	init_cal_mode;
		{
			uint32_t d;
			RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
			p_rs_attr_t(attr)->init_cal_mode = static_cast<decltype(p_rs_attr_t(attr)->init_cal_mode)>(d);
		}
		// enum rs_bit_width		bit_width;
		{
			uint32_t d;
			RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
			p_rs_attr_t(attr)->bit_width = static_cast<decltype(p_rs_attr_t(attr)->bit_width)>(d);
		}
		// enum rs_header_mode		header_mode;
		{
			uint32_t d;
			RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
			p_rs_attr_t(attr)->header_mode = static_cast<decltype(p_rs_attr_t(attr)->header_mode)>(d);
		}
		// enum rs_ex_status_header_mode	status_mode;
		{
			uint32_t d;
			RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
			p_rs_attr_t(attr)->status_mode = static_cast<decltype(p_rs_attr_t(attr)->status_mode)>(d);
		}
		// enum rs_fft_winfunc fft_winfunc;
		{
			uint32_t d;
			RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
			p_rs_attr_t(attr)->fft_winfunc = static_cast<decltype(p_rs_attr_t(attr)->fft_winfunc)>(d);
		}
		// rs_fft_en fft_en
		{
			uint32_t d;
			RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
			p_rs_attr_t(attr)->fft_en = static_cast<decltype(p_rs_attr_t(attr)->fft_en)>(d);
		}
		
		// struct rs_fmcw_r	fmcw_r;
		// uint32_t			repeat_num;
		RETMACRO_refret(bload8(&pbuf, &res, &(p_rs_attr_t(attr)->fmcw_r.repeat_num)), 0, RS_EINV);

		// uint8_t					rxch;
		RETMACRO_refret(bload8(&pbuf, &res, &(p_rs_attr_t(attr)->fmcw_r.chirp_fmcw.rxch)), 0, RS_EINV);
		// uint8_t					txch;
		RETMACRO_refret(bload8(&pbuf, &res, &(p_rs_attr_t(attr)->fmcw_r.chirp_fmcw.txch)), 0, RS_EINV);
		// enum rs_IQ_mode			iq_mode;
		{
			uint32_t d;
			RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
			p_rs_attr_t(attr)->fmcw_r.chirp_fmcw.iq_mode
				= static_cast<decltype(p_rs_attr_t(attr)->fmcw_r.chirp_fmcw.iq_mode)>(d);
		}
		// enum rs_chirp_mode		chirp_mode;
		{
			uint32_t d;
			RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
			p_rs_attr_t(attr)->fmcw_r.chirp_fmcw.chirp_mode
				= static_cast<decltype(p_rs_attr_t(attr)->fmcw_r.chirp_fmcw.chirp_mode)>(d);
		}
		// uint32_t				band_width;
		RETMACRO_refret(bload32(&pbuf, &res, &(p_rs_attr_t(attr)->fmcw_r.chirp_fmcw.band_width)), 0, RS_EINV);
		// uint32_t				sweep_time;
		{
			uint32_t d;
			RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
			p_rs_attr_t(attr)->fmcw_r.chirp_fmcw.sweep_time
				= static_cast<decltype(p_rs_attr_t(attr)->fmcw_r.chirp_fmcw.sweep_time)>(d);
		}
		// uint32_t				point_num;
		RETMACRO_refret(bload16(&pbuf, &res, &(p_rs_attr_t(attr)->fmcw_r.chirp_fmcw.point_num)), 0, RS_EINV);

		// uint32_t				lpf_fc
		{
			uint32_t d;
			RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
			p_rs_attr_t(attr)->fmcw_r.chirp_fmcw.lpf_fc
				= static_cast<decltype(p_rs_attr_t(attr)->fmcw_r.chirp_fmcw.lpf_fc)>(d);
		}
		// uint32_t				hpf_fc
		{
			uint32_t d;
			RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
			p_rs_attr_t(attr)->fmcw_r.chirp_fmcw.hpf_fc
				= static_cast<decltype(p_rs_attr_t(attr)->fmcw_r.chirp_fmcw.hpf_fc)>(d);
		}
		// enum rs_fft_dc_cut fft_dc_cut
		{
			uint32_t d;
			RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
			p_rs_attr_t(attr)->fft.fft_dc_cut = static_cast<decltype(p_rs_attr_t(attr)->fft.fft_dc_cut)>(d);
		}
		// enum rs_fft_point fft_point
		{
			uint32_t d;
			RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
			p_rs_attr_t(attr)->fft.fft_point = static_cast<decltype(p_rs_attr_t(attr)->fft.fft_point)>(d);
		}
		// uint16_t				fft_upper_range;
		RETMACRO_refret(bload16(&pbuf, &res, &(p_rs_attr_t(attr)->fft.fft_upper_range)), 0, RS_EINV);

		// struct rs_fmcw_m	fmcw_m;
		// uint8_t			repeat_num;
		RETMACRO_refret(bload8(&pbuf, &res, &(p_rs_attr_t(attr)->fmcw_m.repeat_num)), 0, RS_EINV);
		// uin8_t			multi_chirp_num;
		RETMACRO_refret(bload8(&pbuf, &res, &(p_rs_attr_t(attr)->fmcw_m.multi_chirp_num)), 0, RS_EINV);

		// struct rs_fmcw	chirp[RS_MAX_CHIRP_PATTERN];
		for(i = 0; i < RS_MAX_CHIRP_PATTERN; i++) {
			// uint32_t		rx_gain;
			{
				uint32_t d;
				RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
				p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].rx_gain
					= static_cast<decltype(p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].rx_gain)>(d);
			}

			// uint8_t		rxch;
			RETMACRO_refret(bload8(&pbuf, &res,  &(p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].rxch)), 0, RS_EINV);
			// uint8_t		txch;
			RETMACRO_refret(bload8(&pbuf, &res,  &(p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].txch)), 0, RS_EINV);
			// enum rs_IQ_mode			iq_mode;
			{
				uint32_t d;
				RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
				p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].iq_mode
					= static_cast<decltype(p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].iq_mode)>(d);
			}
			// enum rs_chirp_mode		chirp_mode;
			{
				uint32_t d;
				RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
				p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].chirp_mode
					= static_cast<decltype(p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].chirp_mode)>(d);
			}
			// uint32_t				band_width;
			RETMACRO_refret(bload32(&pbuf, &res, &(p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].band_width)), 0, RS_EINV);
			// uint32_t				sweep_time;
			{
				uint32_t d;
				RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
				p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].sweep_time
					= static_cast<decltype(p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].sweep_time)>(d);
			}

			// uint32_t				point_num;
			RETMACRO_refret(bload16(&pbuf, &res, &(p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].point_num)), 0, RS_EINV);
			// uint32_t				lpf_fc
			{
				uint32_t d;
				RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
				p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].lpf_fc
					= static_cast<decltype(p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].lpf_fc)>(d);
			}
			// uint32_t				hpf_fc
			{
				uint32_t d;
				RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
				p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].hpf_fc
					= static_cast<decltype(p_rs_attr_t(attr)->fmcw_m.chirp_fmcw[i].hpf_fc)>(d);
			}
		}

		// struct rs_chirp_fft_multi	fft_multi;
		// uin8_t			multi_chirp_num;
		RETMACRO_refret(bload8(&pbuf, &res, &(p_rs_attr_t(attr)->fft_multi.multi_chirp_num)), 0, RS_EINV);
		
		for(i = 0; i < RS_MAX_CHIRP_PATTERN; i++) {
			// enum rs_fft_dc_cut fft_dc_cut
			{
				uint32_t d;
				RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
				p_rs_attr_t(attr)->fft_multi.fft[i].fft_dc_cut = static_cast<decltype(p_rs_attr_t(attr)->fft_multi.fft[i].fft_dc_cut)>(d);
			}
			// enum rs_fft_point fft_point
			{
				uint32_t d;
				RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
				p_rs_attr_t(attr)->fft_multi.fft[i].fft_point = static_cast<decltype(p_rs_attr_t(attr)->fft_multi.fft[i].fft_point)>(d);
			}
			// uint16_t				fft_upper_range;
			RETMACRO_refret(bload16(&pbuf, &res, &(p_rs_attr_t(attr)->fft_multi.fft[i].fft_upper_range)), 0, RS_EINV);
		}
		
		// struct rs_cw_h		cw_h
		// uint32_t				rx_gain;
		{
			uint32_t d;
			RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
			p_rs_attr_t(attr)->cw_h.rx_gain
				= static_cast<decltype(p_rs_attr_t(attr)->cw_h.rx_gain)>(d);
		}
		// uint8_t				rxch;
		RETMACRO_refret(bload8(&pbuf, &res, &(p_rs_attr_t(attr)->cw_h.rxch)), 0, RS_EINV);
		// uint8_t				txch;
		RETMACRO_refret(bload8(&pbuf, &res, &(p_rs_attr_t(attr)->cw_h.txch)), 0, RS_EINV);
		// enum rs_IQ_mode		iq_mode;
		{
			uint32_t d;
			RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
			p_rs_attr_t(attr)->cw_h.iq_mode
				= static_cast<decltype(p_rs_attr_t(attr)->cw_h.iq_mode)>(d);
		}
		// enum rs_sweep_time	sweep_time;
		{
			uint32_t d;
			RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
			p_rs_attr_t(attr)->cw_h.sweep_time
				= static_cast<decltype(p_rs_attr_t(attr)->cw_h.sweep_time)>(d);
		}
		// uint16_t				point_num;
		RETMACRO_refret(bload16(&pbuf, &res, &(p_rs_attr_t(attr)->cw_h.point_num)), 0, RS_EINV);
		// enum rs_lpf_fc 		lpf_fc;
		{
			uint32_t d;
			RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
			p_rs_attr_t(attr)->cw_h.lpf_fc
				= static_cast<decltype(p_rs_attr_t(attr)->cw_h.lpf_fc)>(d);
		}

		// struct rs_cw_l		cw_l
		// uint8_t				repeat_num;
		RETMACRO_refret(bload8(&pbuf, &res, &(p_rs_attr_t(attr)->cw_l.repeat_num)), 0, RS_EINV);
		// enum rs_rx_gain		rx_gain;
		{
			uint32_t d;
			RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
			p_rs_attr_t(attr)->cw_l.rx_gain
				= static_cast<decltype(p_rs_attr_t(attr)->cw_l.rx_gain)>(d);
		}

		// uint8_t				rxch;
		RETMACRO_refret(bload8(&pbuf, &res, &(p_rs_attr_t(attr)->cw_l.rxch)), 0, RS_EINV);
		// uint8_t				txch;
		RETMACRO_refret(bload8(&pbuf, &res, &(p_rs_attr_t(attr)->cw_l.txch)), 0, RS_EINV);
		// enum rs_IQ_mode		iq_mode;
		{
			uint32_t d;
			RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
			p_rs_attr_t(attr)->cw_l.iq_mode = static_cast<decltype(p_rs_attr_t(attr)->cw_l.iq_mode)>(d);
		}
		// enum rs_sweep_time	sweep_time;
		{
			uint32_t d;
			RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
			p_rs_attr_t(attr)->cw_l.sweep_time
				= static_cast<decltype(p_rs_attr_t(attr)->cw_l.sweep_time)>(d);
		}
		// enum rs_lpf_fc 		lpf_fc;
		{
			uint32_t d;
			RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
			p_rs_attr_t(attr)->cw_l.lpf_fc
				= static_cast<decltype(p_rs_attr_t(attr)->cw_l.lpf_fc)>(d);
		}

		// struct rs_fskcw		fskcw;
		// uint8_t				repeat_num;
		RETMACRO_refret(bload8(&pbuf, &res, &(p_rs_attr_t(attr)->fskcw.repeat_num)), 0, RS_EINV);
		// enum rs_rx_gain		rx_gain;
		{
			uint32_t d;
			RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
			p_rs_attr_t(attr)->fskcw.rx_gain
				= static_cast<decltype(p_rs_attr_t(attr)->fskcw.rx_gain)>(d);
		}

		// uint8_t				rxch;
		RETMACRO_refret(bload8(&pbuf, &res, &(p_rs_attr_t(attr)->fskcw.rxch)), 0, RS_EINV);
		// uint8_t				txch;
		RETMACRO_refret(bload8(&pbuf, &res, &(p_rs_attr_t(attr)->fskcw.txch)), 0, RS_EINV);
		// enum rs_IQ_mode		iq_mode;
		{
			uint32_t d;
			RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
			p_rs_attr_t(attr)->fskcw.iq_mode = static_cast<decltype(p_rs_attr_t(attr)->fskcw.iq_mode)>(d);
		}
		// uint32_t            band_width;
		RETMACRO_refret(bload32(&pbuf, &res, &(p_rs_attr_t(attr)->fskcw.band_width)), 0, RS_EINV);
		// enum rs_sweep_time	sweep_time;
		{
			uint32_t d;
			RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
			p_rs_attr_t(attr)->fskcw.sweep_time
				= static_cast<decltype(p_rs_attr_t(attr)->fskcw.sweep_time)>(d);
		}
		// enum rs_lpf_fc 		lpf_fc;
		{
			uint32_t d;
			RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
			p_rs_attr_t(attr)->fskcw.lpf_fc
				= static_cast<decltype(p_rs_attr_t(attr)->fskcw.lpf_fc)>(d);
		}

		// struct rs_test_ct	test_ct;
		// uint8_t				txch;
		RETMACRO_refret(bload8(&pbuf, &res, &(p_rs_attr_t(attr)->test_ct.txch)), 0, RS_EINV);


		// struct rs_test_cr	test_cr;
		// enum rs_sweep_time	sweep_time;
		{
			uint32_t d;
			RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
			p_rs_attr_t(attr)->test_cr.sweep_time
				= static_cast<decltype(p_rs_attr_t(attr)->test_cr.sweep_time)>(d);
		}
		// enum rs_lpf_fc 		lpf_fc;
		{
			uint32_t d;
			RETMACRO_refret(bload32(&pbuf, &res, &d), 0, RS_EINV);
			p_rs_attr_t(attr)->test_cr.lpf_fc
				= static_cast<decltype(p_rs_attr_t(attr)->test_cr.lpf_fc)>(d);
		}


		// uint16_t				hw_major_ver;
		RETMACRO_refret(bload16(&pbuf, &res, &(p_rs_attr_t(attr)->hw_major_ver)), 0, RS_EINV);
		// uint16_t				hw_minor_ver;
		RETMACRO_refret(bload16(&pbuf, &res, &(p_rs_attr_t(attr)->hw_minor_ver)), 0, RS_EINV);
		// uint16_t				hw_type;
		RETMACRO_refret(bload16(&pbuf, &res, &(p_rs_attr_t(attr)->hw_type)), 0, RS_EINV);
		// uint32_t				eval_mode;
		RETMACRO_refret(bload32(&pbuf, &res, &(p_rs_attr_t(attr)->eval_mode)), 0, RS_EINV);

//		printf("%s:%d: res=%ld\n", __FUNCTION__, __LINE__, res);

	}

	return RS_OK;
}

DLLAPI int rs_attr_conv_get_size(const rs_attr_t attr, uint32_t *size)
{
	if(attr == NULL) {
		return RS_EINV;
	}
	if(size == NULL) {
		return RS_EINV;
	}

	*size = 4;	/* set generator version uint8_t x 4 */
				// uint8_t					fw_major_ver;
				// uint8_t					fw_minor_ver;
				// uint8_t					fw_revision;
				// uint8_t					fw_hotfix;

    /* common params */
	*size += 4;	// enum rs_modulation_mode	modu_mode;
	*size += 4;	// int32_t					tx_power;
	*size += 4;	// uint32_t					rx_gain;
	*size += 4;	// uint32_t					frame_rate;
	*size += 4;	// uint32_t					chirp_rate_constant;
	*size += 4;	// uint32_t					center_freq;
	*size += 4;	// uint32_t					xtal_freq;
	*size += 4;	// int32_t					calc;
	*size += 4;	// enum rs_init_cal_mode	init_cal_mode;
	*size += 4;	// enum rs_bit_width		bit_width;
	*size += 4;	// enum rs_header_mode		header_mode;
	*size += 4;	// enum rs_ex_status_header_mode	status_mode;
	*size += 4;	// enum rs_fft_winfunc fft_winfunc;
	*size += 4;	// enum rs_fft_en fft_en;

//	printf("%s:%d: size=%d status_mode \n", __FUNCTION__, __LINE__, *size);

    /* modulation params */
	// struct rs_chrip_fmcw_r	fmcw_r;
	*size += 1;	// uint8_t		repeat_num;
	// struct rs_chirp_fmcw		chirp_fmcw;
	*size += 1;	// uint8_t				rxch;
	*size += 1;	// uint8_t				txch;
	*size += 4;	// enum rs_IQ_mode		iq_mode;
	*size += 4;	// enum rs_chirp_mode	chirp_mode;
	*size += 4;	// uint32_t				band_width;
	*size += 4;	// enum rs_sweep_time	sweep_time;
	*size += 2;	// uint16_t				point_num;
	*size += 4;	// enum rs_lpf_fc		lpf_fc
	*size += 4;	// enum rs_hpf_fc		hpf_fc

	*size += 4;	// enum rs_fft_dc_cut fft_dc_cut;
	*size += 4;	// enum rs_fft_point fft_point;
	*size += 2;	// uint16_t fft_upper_range;
	
	// struct rs_chirp_fmcw_multi	fmcw_m;
	*size += 1;	// uint8_t				repeat_num;
	*size += 1;	// uint8_t				multi_chirp_num;
	// struct rs_fmcw_param				chirp[RS_MAX_CHIRP_PATTERN];
	for(int i = 0; i < RS_MAX_CHIRP_PATTERN; i++) {
		*size += 4;	// enum rs_rx_gain		rx_gain;
		*size += 1;	// uint8_t				rxch;
		*size += 1;	// uint8_t				txch;
		*size += 4;	// enum rs_IQ_mode		iq_mode;
		*size += 4;	// enum rs_chirp_mode	chirp_mode;
		*size += 4;	// uint32_t				band_width;
		*size += 4;	// enum rs_sweep_time	sweep_time;
		*size += 2;	// uint16_t				point_num;
		*size += 4;	// enum rs_lpf_fc		lpf_fc
		*size += 4;	// enum rs_hpf_fc		hpf_fc
	}

	// struct rs_chirp_fft_multi	fft_multi;
	*size += 1;	// uint8_t				multi_chirp_num;
	for(int i = 0; i < RS_MAX_CHIRP_PATTERN; i++) {
		*size += 4;	// enum rs_fft_dc_cut fft_dc_cut;
		*size += 4;	// enum rs_fft_point fft_point;
		*size += 2;	// uint16_t fft_upper_range;
	}
	
	// struct rs_chirp_cw_h            cw_h;
	*size += 4; // enum rs_rx_gain			rx_gain;
	*size += 1; // uint8_t					rxch;
	*size += 1; // uint8_t					txch;
	*size += 4; // enum rs_IQ_mode			iq_mode;
	*size += 4; // enum rs_sweep_time		sweep_time;
	*size += 2; // uint16_t					point_num;
	*size += 4; // enum rs_lpf_fc			lpf_fc;

	// struct rs_chirp_cw_l            cw_l;
	*size += 1; // uint8_t					repeat_num;
	*size += 4; // enum rs_rx_gain			rx_gain;
	*size += 1; // uint8_t					rxch;
	*size += 1; // uint8_t					txch;
	*size += 4; // enum rs_IQ_mode			iq_mode;
	*size += 4; // enum rs_sweep_time		sweep_time;
	*size += 4; // enum rs_lpf_fc			lpf_fc;

	// struct rs_chirp_fskcw           fskcw;
	*size += 1; // uint8_t					repeat_num;
	*size += 4; // enum rs_rx_gain			rx_gain;
	*size += 1; // uint8_t					rxch;
	*size += 1; // uint8_t					txch;
	*size += 4; // enum rs_IQ_mode			iq_mode;
	*size += 4; // uint32_t					band_width; /* 1MHz to 100MHz */
	*size += 4; // enum rs_sweep_time		sweep_time;
	*size += 4; // enum rs_lpf_fc			lpf_fc;


	// struct rs_test_ct               test_ct;
	*size += 1; // uint8_t                 txch;

	// struct rs_test_cr               test_cr;
	*size += 4; // enum rs_sweep_time     sweep_time;
	*size += 4; // enum rs_lpf_fc         lpf_fc;

	*size += 2;	// uint16_t				hw_major_ver;
	*size += 2;	// uint16_t				hw_minor_ver;
	*size += 2;	// uint16_t				hw_type;
	*size += 4;	// uint32_t				eval_mode;

//	printf("%s:%d: size=%d all\n", __FUNCTION__, __LINE__, *size);

	return RS_OK;
}
