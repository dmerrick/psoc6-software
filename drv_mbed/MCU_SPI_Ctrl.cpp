/*
 * Copyright (c) 2017 Socionext Inc.
 * All rights reserved.
 */

/*
 * MCU_SPI_Ctrl.cpp
 */

//#include <mbed.h>

#include "MCU_SPI_Ctrl.h"

static MCU_SPI_Ctrl *pspi1 = nullptr;
static TIM_HandleTypeDef *phtim17 = nullptr;
static SPI_HandleTypeDef *phspi = nullptr;

void MCU_SPI_Ctrl::start_to_receive(MCU_SPI_Ctrl *p, SPI_HandleTypeDef *hspi, TIM_HandleTypeDef *htim17)
{
    // printf("Start to Receive\n");
	if (hspi->Instance == SPI1) {
		pspi1 = p;
        phtim17 = htim17;
        phspi = hspi;
	}
}

#if defined(__USE_HAL__) && defined(__USE_DMA__)
    extern "C" {
        void HAL_SPI_RxCpltCallback(SPI_HandleTypeDef *hspi) //HAL_SPI_TxRxCpltCallback
        {
            // printf("HAL_SPI_RxCpltCallback\n");
            if (hspi->Instance == SPI1) {
                pspi1->done2nd();
                HAL_SPI_DMAStop(hspi);
            }
        }
    }

    // extern "C" {
    //     void HAL_SPI_ErrorCallback(SPI_HandleTypeDef *hspi)
    //     {
    //         if(hspi->Instance == SPI1)
    //         {
    //             printf("SPI1 Error\n");
    //         }
    //     }
    // }

    extern "C" {
        void SPI1_IRQHandler(void){
            // printf("SPI1_IRQHandler invoked\n");
            HAL_SPI_IRQHandler(phspi);
        }
    }
    

    /* IRQ Handler */
    // void DMA2_Stream0_IRQHandler(void)
    // {
    //         // pspi1->irq_handler();
    // }
    extern "C" {
        void GPDMA1_Channel6_IRQHandler(void){
            // printf("GPDMA1_Channel6_IRQHandler Invoked\n");
            pspi1->irq_handler();
        }
    }
    extern "C" {
        void GPDMA1_Channel7_IRQHandler(void){
            // printf("GPDMA1_Channel7_IRQHandler Invoked\n");
            pspi1->irq_handler_tx();
        }
    }
#endif
