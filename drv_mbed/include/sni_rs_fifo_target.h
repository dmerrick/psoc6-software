/*
 * Copyright (c) 2017 Socionext Inc.
 * All rights reserved.
 */

/*
 * sni_rs_fifo_target.h
 */

#ifndef _sni_rs_fifo_target_H
#define _sni_rs_fifo_target_H

#include "sni_rs_setupinfo.h"
#include "sni_rs_mem.h"

#define FIFO_PAGENUM_MAX					(3)
#define FIFO_NEEDBUF_SIZE(pagenum)			((PAGE_CAPACITY) * (pagenum))

struct sni_rs_fifo;
struct sni_rs_fifo_attr;


struct sni_rs_fifo_attr_target
{
	struct sni_rs_mem_attr bufattr;
};

struct sni_rs_fifo_target								// FIFO management information
{
	uint8_t *_buf[FIFO_PAGENUM_MAX];					// FIFO
	int _wpage;											// write page pointer
	int _rpage;											// read page pointer
	int _wptr;											// write pointer in a page
#ifdef OVER_WRITE
	int _dma_set;										// Host DMA setting flag
	uint8_t _use_page_num;								// current number of pages of FIFO
	uint8_t _sort_use[FIFO_PAGENUM_MAX];				// order of page number in use
	uint8_t _sort_emp[FIFO_PAGENUM_MAX];				// order of page number not in use
#endif /* OVER_WRITE */
	int _or_flag;										// OR interrupt flag

	struct {
		struct sni_rs_mem buf;
	} local;
};

#ifdef __cplusplus
extern "C" {
#endif	/* __cplusplus */

	/* sni_rs_fifo functions (local) */
	extern void sni_rs_fifo_clear_irq_or(struct sni_rs_fifo *fifo);
	extern void sni_rs_fifo_set_irq_or(struct sni_rs_fifo *fifo);
	extern int sni_rs_fifo_isexistdata(struct sni_rs_fifo *fifo);

#ifdef __cplusplus
}
#endif	/* __cplusplus */

#endif /* _sni_rs_fifo_target_H */
