/*
 * Copyright (c) 2017 Socionext Inc.
 * All rights reserved.
 */

/*
 * MCU_SPI1_Ctrl.h
 */

#ifndef _MCU_SPI1_CTRL_H
#define _MCU_SPI1_CTRL_H

#include <mbed.h>
#include <stdint.h>
#include <memory>

#include "MCU_SPI_Ctrl.h"
#include "configuration.h"


class MCU_SPI1_Ctrl : public MCU_SPI_Ctrl {
public:
    #ifdef __USE_HAL__
        SPI_HandleTypeDef _hspi;
        SPI_AutonomousModeConfTypeDef HAL_SPI_AutonomousMode_Cfg_Struct = {0};
        TIM_HandleTypeDef htim17;

        #ifdef __USE_DMA__
            DMA_HandleTypeDef handle_GPDMA1_rx;
            DMA_HandleTypeDef handle_GPDMA1_tx;
        #endif
    #else
        SPI* _SPI1B;
        DigitalOut* _SPI1B_CS;
    #endif
protected:
	virtual HAL_StatusTypeDef init_spi(void) {
		HAL_StatusTypeDef status = HAL_OK;
        
        #ifdef __USE_HAL__
            RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};
            PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_SPI1;
            PeriphClkInit.Spi1ClockSelection = RCC_SPI1CLKSOURCE_SYSCLK;
            if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
            {
                // Error_Handler();
            }
            __HAL_RCC_SPI1_CLK_ENABLE();

            _hspi.Instance = SPI1;
            _hspi.Init.Mode = SPI_MODE_MASTER;
            _hspi.Init.Direction = SPI_DIRECTION_2LINES;
            _hspi.Init.DataSize = SPI_DATASIZE_8BIT;
            _hspi.Init.CLKPolarity = SPI_POLARITY_LOW;
            _hspi.Init.CLKPhase = SPI_PHASE_1EDGE;
            _hspi.Init.NSS = SPI_NSS_SOFT;
            _hspi.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_32;
            _hspi.Init.FirstBit = SPI_FIRSTBIT_MSB;
            _hspi.Init.TIMode = SPI_TIMODE_DISABLE;
            _hspi.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
            _hspi.Init.CRCPolynomial = 0;
            _hspi.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
            _hspi.Init.NSSPolarity = SPI_NSS_POLARITY_LOW;
            _hspi.Init.FifoThreshold = SPI_FIFO_THRESHOLD_01DATA;
            _hspi.Init.MasterSSIdleness = SPI_MASTER_SS_IDLENESS_00CYCLE;
            _hspi.Init.MasterInterDataIdleness = SPI_MASTER_INTERDATA_IDLENESS_00CYCLE;
            _hspi.Init.MasterReceiverAutoSusp = SPI_MASTER_RX_AUTOSUSP_DISABLE;
            _hspi.Init.MasterKeepIOState = SPI_MASTER_KEEP_IO_STATE_DISABLE;
            _hspi.Init.IOSwap = SPI_IO_SWAP_DISABLE;
            _hspi.Init.ReadyMasterManagement = SPI_RDY_MASTER_MANAGEMENT_INTERNALLY;
            _hspi.Init.ReadyPolarity = SPI_RDY_POLARITY_HIGH;
            
            if (HAL_SPI_Init(&_hspi) != HAL_OK)
            {
                // Error_Handler();
            }
            HAL_SPI_AutonomousMode_Cfg_Struct.TriggerState = SPI_AUTO_MODE_DISABLE;
            HAL_SPI_AutonomousMode_Cfg_Struct.TriggerSelection = SPI_GRP1_GPDMA_CH0_TCF_TRG;
            HAL_SPI_AutonomousMode_Cfg_Struct.TriggerPolarity = SPI_TRIG_POLARITY_RISING;
            if (HAL_SPIEx_SetConfigAutonomousMode(&_hspi, &HAL_SPI_AutonomousMode_Cfg_Struct) != HAL_OK)
            {
            // Error_Handler();
            }
            #ifdef __USE_DMA__
                printf("USING DMA\n\n\n\n\n\n");

                __HAL_LINKDMA(&_hspi, hdmarx, handle_GPDMA1_rx);

                if (HAL_DMA_ConfigChannelAttributes(&handle_GPDMA1_rx, DMA_CHANNEL_NPRIV) != HAL_OK)
                {
                    // Error_Handler();
                }

                __HAL_LINKDMA(&_hspi, hdmatx, handle_GPDMA1_tx);

                if (HAL_DMA_ConfigChannelAttributes(&handle_GPDMA1_tx, DMA_CHANNEL_NPRIV) != HAL_OK)
                {
                    // Error_Handler();
                }
            #endif
        #else
            (*_SPI1B_CS) = 1;
            _SPI1B->format(8, 0); // clock polarity 0, rising edge
            _SPI1B->frequency(5000000); // 12.5 MHz
            _SPI1B->set_default_write_value(0x00);
        #endif

		return status;
	}
	virtual HAL_StatusTypeDef deinit_spi(void) {
		HAL_StatusTypeDef status;
        #ifdef __USE_HAL__
            status = HAL_SPI_DeInit(&_hspi);
            __HAL_RCC_SPI1_CLK_DISABLE();
        #else

        #endif
		return status;
	}

	virtual HAL_StatusTypeDef init_gpio(void) {
        #ifdef __USE_HAL__
            /* Start clock for GPIOA Pins */
            __HAL_RCC_GPIOA_CLK_ENABLE();
            
            /* Prepare pins for MOSI, MISO, and SCK SPI1 Operation */
            GPIO_InitTypeDef init_SPI1;
            init_SPI1.Pin = SPI1_SCK_GPIO_PIN | SPI1_MISO_GPIO_PIN | SPI1_MOSI_GPIO_PIN;
            init_SPI1.Mode = GPIO_MODE_AF_PP;   // Push Pull Alternate Function Mode
            init_SPI1.Pull = GPIO_NOPULL; 
            init_SPI1.Speed = GPIO_SPEED_HIGH;
            init_SPI1.Alternate = GPIO_AF5_SPI1;

            /* Prepare pin PA_4 for Chip Select for SPI1 */
            GPIO_InitTypeDef init_SPI1_CS;
            init_SPI1_CS.Pin = SPI1_CS_GPIO_PIN; 
            init_SPI1_CS.Mode = GPIO_MODE_OUTPUT_PP;
            init_SPI1_CS.Pull = GPIO_NOPULL;
            init_SPI1_CS.Speed = GPIO_SPEED_HIGH;
            init_SPI1_CS.Alternate = 0;

            /* Start clock to all pins we prepared */
            HAL_GPIO_Init(SPI1_GPIO_ADDRESS, &init_SPI1);
            HAL_GPIO_Init(SPI1_GPIO_ADDRESS, &init_SPI1_CS);

            /* Set Chip Select */
            GPIO_PinState state;
            state = GPIO_PIN_SET; // Turn On - GPIO_PIN_RESET is Turn Off
            HAL_GPIO_WritePin(SPI1_GPIO_ADDRESS, SPI1_CS_GPIO_PIN, state);
        #else
            (*_SPI1B_CS) = 1; // Set high (off)
        #endif

		return HAL_OK;
	}
	virtual HAL_StatusTypeDef deinit_gpio(void) {
        #ifdef __USE_HAL__
    		uint32_t SPI1_A_pins;
 		    SPI1_A_pins = SPI1_CS_GPIO_PIN | SPI1_SCK_GPIO_PIN | SPI1_MISO_GPIO_PIN | SPI1_MOSI_GPIO_PIN;
	    	HAL_GPIO_DeInit(SPI1_GPIO_ADDRESS, SPI1_A_pins);
	    	__HAL_RCC_GPIOA_CLK_DISABLE();
        #else
        #endif
		return HAL_OK;
	}
	
	virtual HAL_StatusTypeDef init_dma(bool circular) {
		HAL_StatusTypeDef status;
        #if defined(__USE_HAL__) && defined(__USE_DMA__)
            __HAL_RCC_GPDMA1_CLK_ENABLE();

            /* GPDMA1_REQUEST_SPI1_RX Init */
            handle_GPDMA1_rx.Instance = GPDMA1_Channel6;
            handle_GPDMA1_rx.Init.Request = GPDMA1_REQUEST_SPI1_RX;
            handle_GPDMA1_rx.Init.BlkHWRequest = DMA_BREQ_SINGLE_BURST;
            handle_GPDMA1_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
            handle_GPDMA1_rx.Init.SrcInc = DMA_SINC_FIXED;
            handle_GPDMA1_rx.Init.DestInc = DMA_DINC_INCREMENTED; // DMA_DINC_FIXED = ba
            handle_GPDMA1_rx.Init.SrcDataWidth = DMA_SRC_DATAWIDTH_BYTE;
            handle_GPDMA1_rx.Init.DestDataWidth = DMA_DEST_DATAWIDTH_BYTE;
            handle_GPDMA1_rx.Init.Priority = DMA_LOW_PRIORITY_MID_WEIGHT;
            handle_GPDMA1_rx.Init.SrcBurstLength = 1;
            handle_GPDMA1_rx.Init.DestBurstLength = 1;
            handle_GPDMA1_rx.Init.TransferAllocatedPort = DMA_SRC_ALLOCATED_PORT0 | DMA_DEST_ALLOCATED_PORT0;
            handle_GPDMA1_rx.Init.TransferEventMode = DMA_TCEM_BLOCK_TRANSFER;
            handle_GPDMA1_rx.Init.Mode = DMA_NORMAL;

            // handle_GPDMA1_rx.Lock = HAL_UNLOCKED;
            // handle_GPDMA1_rx.State = HAL_DMA_STATE_RESET;
            // handle_GPDMA1_rx.Parent = (void *)0;
            // handle_GPDMA1_rx.XferCpltCallback = (void (*)(struct __DMA_HandleTypeDef * hdma))0;
            // handle_GPDMA1_rx.XferHalfCpltCallback = (void (*)(struct __DMA_HandleTypeDef * hdma))0;
            // handle_GPDMA1_rx.XferErrorCallback = (void (*)(struct __DMA_HandleTypeDef * hdma))0;
            // handle_GPDMA1_rx.ErrorCode = 0;

            if (HAL_DMA_Init(&handle_GPDMA1_rx) != HAL_OK)
            {
                // Error_Handler();
            }
        #endif
		return status;
	}
	virtual HAL_StatusTypeDef init_dma_tx(void) {
		HAL_StatusTypeDef status;
        #if defined(__USE_HAL__) && defined(__USE_DMA__)
            __HAL_RCC_GPDMA1_CLK_ENABLE();

            /* GPDMA1_REQUEST_SPI1_TX Init */
            handle_GPDMA1_tx.Instance = GPDMA1_Channel7;
            handle_GPDMA1_tx.Init.Request = GPDMA1_REQUEST_SPI1_TX;
            handle_GPDMA1_tx.Init.BlkHWRequest = DMA_BREQ_SINGLE_BURST;
            handle_GPDMA1_tx.Init.Direction = DMA_MEMORY_TO_PERIPH;
            handle_GPDMA1_tx.Init.SrcInc = DMA_SINC_INCREMENTED; // DMA_SINC_FIXED DMA_SINC_INCREMENTED
            handle_GPDMA1_tx.Init.DestInc = DMA_DINC_FIXED; // DMA_DINC_FIXED DMA_SINC_INCREMENTED
            handle_GPDMA1_tx.Init.SrcDataWidth = DMA_SRC_DATAWIDTH_BYTE;
            handle_GPDMA1_tx.Init.DestDataWidth = DMA_DEST_DATAWIDTH_BYTE;
            handle_GPDMA1_tx.Init.Priority = DMA_LOW_PRIORITY_MID_WEIGHT;
            handle_GPDMA1_tx.Init.SrcBurstLength = 1;
            handle_GPDMA1_tx.Init.DestBurstLength = 1;
            handle_GPDMA1_tx.Init.TransferAllocatedPort = DMA_SRC_ALLOCATED_PORT0 | DMA_DEST_ALLOCATED_PORT0;
            handle_GPDMA1_tx.Init.TransferEventMode = DMA_TCEM_BLOCK_TRANSFER;
            handle_GPDMA1_tx.Init.Mode = DMA_NORMAL;

            // handle_GPDMA1_tx.Lock = HAL_UNLOCKED;
            // handle_GPDMA1_tx.State = HAL_DMA_STATE_RESET;
            // handle_GPDMA1_tx.Parent = (void *)0;
            // handle_GPDMA1_tx.XferCpltCallback = (void (*)(struct __DMA_HandleTypeDef * hdma))0;
            // handle_GPDMA1_tx.XferHalfCpltCallback = (void (*)(struct __DMA_HandleTypeDef * hdma))0;
            // handle_GPDMA1_tx.XferErrorCallback = (void (*)(struct __DMA_HandleTypeDef * hdma))0;
            // handle_GPDMA1_tx.ErrorCode = 0;

            if (HAL_DMA_Init(&handle_GPDMA1_tx) != HAL_OK)
            {
                // Error_Handler();
            }
        #endif
		return status;
	}
	virtual HAL_StatusTypeDef deinit_dma(void) {
		HAL_StatusTypeDef status;
        #if defined(__USE_HAL__) && defined(__USE_DMA__)

        #else
        #endif
		return status;
	}
	virtual HAL_StatusTypeDef deinit_dma_tx(void) {
		HAL_StatusTypeDef status;
        #if defined(__USE_HAL__) && defined(__USE_DMA__)
        #else
        #endif
		return status;
	}
	
	virtual HAL_StatusTypeDef init_nvic(void) {
        #if defined(__USE_HAL__) && defined(__USE_DMA__)
            /* initialize the nested vector interrupt controller */

            HAL_NVIC_SetPriority(GPDMA1_Channel6_IRQn, 0, 0);
            HAL_NVIC_EnableIRQ(GPDMA1_Channel6_IRQn);
            
            HAL_NVIC_SetPriority(GPDMA1_Channel7_IRQn, 0, 0);
            HAL_NVIC_EnableIRQ(GPDMA1_Channel7_IRQn);

            HAL_NVIC_SetPriority(SPI1_IRQn, 0, 0);
            HAL_NVIC_EnableIRQ(SPI1_IRQn);
        #else

        #endif
		return HAL_OK;
	}
	virtual HAL_StatusTypeDef deinit_nvic(void) {
        #if defined(__USE_HAL__) && defined(__USE_DMA__)
            /* De initialize the nested vector interrupt controller */
            HAL_NVIC_DisableIRQ(GPDMA1_Channel6_IRQn);
        #else

        #endif
		return HAL_OK;
	}
	
	virtual HAL_StatusTypeDef transmit(uint8_t *pTxData, uint8_t *pRxData, uint16_t rsize) {
        HAL_StatusTypeDef ret = HAL_OK;
        #ifdef __USE_HAL__
            ret = HAL_SPI_TransmitReceive(&_hspi, pTxData, pRxData, rsize, 100);
        #else
            _SPI1B->write((char *) pTxData, rsize, (char *) pRxData, rsize);
        #endif

        
		return ret;
	}
	virtual HAL_StatusTypeDef transmit_only(uint8_t *pTxData, uint16_t Size) {
        HAL_StatusTypeDef ret = HAL_OK;
        #ifdef __USE_HAL__
            ret = HAL_SPI_Transmit(&_hspi, pTxData, Size, 100);
        #else
            _SPI1B->write((char *) pTxData, Size, NULL, 0);
        #endif
        
		return ret;
	}
	virtual HAL_StatusTypeDef receive_only(uint8_t *pRxData, uint16_t Size) {
        HAL_StatusTypeDef ret = HAL_OK;
        #ifdef __USE_HAL__
            ret = HAL_SPI_Receive(&_hspi, pRxData, Size, 100);
        #else
            _SPI1B->write(NULL, 0, (char *) pRxData, Size);
        #endif
		
		return ret;
	}
	virtual HAL_StatusTypeDef receive_only_dma(uint8_t *pRxData, uint16_t Size) {

        HAL_StatusTypeDef ret = HAL_OK;
        #if defined(__USE_HAL__) && defined(__USE_DMA__)
            ret = HAL_SPI_Receive_DMA(&_hspi, pRxData, Size);
        #elif defined(__USE_HAL__) && !defined(__USE_DMA__)
            ret = HAL_SPI_Receive(&_hspi, pRxData, Size, 100);
        #else
            _SPI1B->write(NULL, 0, (char *) pRxData, Size);
        #endif

		return ret;
	}



public:
	virtual void start(void){
        #if defined(__USE_HAL__)
            start_to_receive(this, &_hspi, &htim17);

        #endif
    }
    
    virtual void set_cs(int value) {
        #ifdef __USE_HAL__
            GPIO_PinState state;

            if (value != 0) {
                state = GPIO_PIN_SET;
            }
            else {
                state = GPIO_PIN_RESET;
            }
            HAL_GPIO_WritePin(SPI1_GPIO_ADDRESS, SPI1_CS_GPIO_PIN, state);
        #else
            if (value != 0){
                (*_SPI1B_CS) = 1;
            }
            else{
                (*_SPI1B_CS) = 0;
            }
        #endif


    }

	virtual void handle_irq(void) {
        #if defined(__USE_HAL__) && defined(__USE_DMA__)
            HAL_DMA_IRQHandler(&handle_GPDMA1_rx);
            return;
        #else
            return;
        #endif
	}

	virtual void handle_irq_tx(void) {
        #if defined(__USE_HAL__) && defined(__USE_DMA__)
            HAL_DMA_IRQHandler(&handle_GPDMA1_tx);
            return;
        #else
            return;
        #endif
	}
public:
	MCU_SPI1_Ctrl() : MCU_SPI_Ctrl() {
        #ifdef __USE_HAL__
        #else
            _SPI1B = (new SPI(SPI1_MOSI_GPIO_PIN, SPI1_MISO_GPIO_PIN, SPI1_SCK_GPIO_PIN));
            _SPI1B_CS = (new DigitalOut(SPI1_CS_GPIO_PIN));
        #endif

    }
	virtual ~MCU_SPI1_Ctrl() {
        #ifdef __USE_HAL__
        #else
            delete _SPI1B;
            delete _SPI1B_CS;
        #endif

    }

};

#endif /* _MCU_SPI1_CTRL_H */
