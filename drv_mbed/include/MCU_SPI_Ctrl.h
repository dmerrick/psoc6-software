/*
 * Copyright (c) 2017 Socionext Inc.
 * All rights reserved.
 */

/*
 * MCU_SPI_Ctrl.h
 */

#ifndef _MCU_SPI_CTRL_H
#define _MCU_SPI_CTRL_H

#include <mbed.h>

class MCU_SPI_Ctrl {
public:
	virtual HAL_StatusTypeDef init_spi(void) = 0;
	virtual HAL_StatusTypeDef deinit_spi(void) = 0;
	virtual HAL_StatusTypeDef init_dma(bool circular) = 0;
	virtual HAL_StatusTypeDef deinit_dma(void) = 0;
	virtual HAL_StatusTypeDef init_dma_tx(void) = 0;
	virtual HAL_StatusTypeDef deinit_dma_tx(void) = 0;
	virtual HAL_StatusTypeDef init_nvic(void) = 0;
	virtual HAL_StatusTypeDef deinit_nvic(void) = 0;
	virtual HAL_StatusTypeDef init_gpio(void) = 0;
	virtual HAL_StatusTypeDef deinit_gpio(void) = 0;
	virtual HAL_StatusTypeDef transmit(uint8_t *pTxData, uint8_t *pRxData, uint16_t Size) = 0;
	virtual HAL_StatusTypeDef transmit_only(uint8_t *pTxData, uint16_t Size) = 0;
	virtual HAL_StatusTypeDef receive_only(uint8_t *pRxData, uint16_t Size) = 0;
	virtual HAL_StatusTypeDef receive_only_dma(uint8_t *pRxData, uint16_t Size) = 0;
	virtual void set_cs(int value) = 0;
	virtual void handle_irq(void) = 0;
    virtual void handle_irq_tx(void) = 0;
	virtual void start(void) = 0;

private:
	volatile bool _dma_end_flag;
	
public:
	MCU_SPI_Ctrl() {}
	virtual ~MCU_SPI_Ctrl() {}
	bool init(void) {
		HAL_StatusTypeDef status;

		status = init_gpio();
		if (status != HAL_OK) {
            printf("init_gpio failed\n");
			return false;
		}else{
            printf("init_gpio success\n");
        }
		status = init_nvic();
		if (status != HAL_OK) {
            printf("init_nvic failed\n");
			return false;
		}else{
            printf("init_nvic success\n");
        }
		status = init_dma(true);
		if (status != HAL_OK) {
            printf("init_dma failed\n");
			return false;
		}else{
            printf("init_dma success\n");
        }
		status = init_dma_tx();
		if (status != HAL_OK) {
            printf("init_dma_tx failed\n");
			return false;
		}else{
            printf("init_dma_tx success\n");
        }
		status = init_spi();
		if (status != HAL_OK) {
            printf("init_spi failed\n");
			return false;
		}else{
            printf("init_spi success\n");
        }

		set_cs(1);
		clear_dma_end_flag();

		return true;
	}
	bool deinit(void) {
		HAL_StatusTypeDef status;

		status = deinit_spi();
		if (status != HAL_OK) {
			return false;
		}
		status = deinit_dma_tx();
		if (status != HAL_OK) {
			return false;
		}
		status = deinit_dma();
		if (status != HAL_OK) {
			return false;
		}
		status = deinit_nvic();
		if (status != HAL_OK) {
			return false;
		}
		status = deinit_gpio();
		if (status != HAL_OK) {
			return false;
		}

		return true;
	}
	void begin(void) {
		set_cs(0);
	}
	void end(void) {
		set_cs(1);
	}

	void done2nd(void) {
		set_dma_end_flag();
	}
	void irq_handler(void) {
		handle_irq();
	}
    void irq_handler_tx(void){
        handle_irq_tx();
    }
	inline bool get_dma_end_flag(void) const {
		return _dma_end_flag;
	}
	inline void set_dma_end_flag(void) {
		_dma_end_flag = true;
	}
	inline void clear_dma_end_flag(void) {
		_dma_end_flag = false;
	}
	void wait_dma(void){
		while(_dma_end_flag == false) {
			sni_rs_sleep_usec(10);
		}
	}

public:
	static void start_to_receive(MCU_SPI_Ctrl *p, SPI_HandleTypeDef *hspi, TIM_HandleTypeDef *htim17);
};

#endif /* _MCU_SPI_CTRL_H */
