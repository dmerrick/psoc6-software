/*
 * Copyright (c) 2017 Socionext Inc.
 * All rights reserved.
 */

/*
 * RF_SPI1.h
 */

#ifndef _RF_SPI1_H
#define _RF_SPI1_H

//#include <mbed.h>
#include <stdint.h>

#include "MCU_SPI_Ctrl.h"
#include "sni_rs_target.h"
#include "sni_rs_spi.h"

class RF_SPI1 {
public:
	MCU_SPI_Ctrl *_spi;

private:
	enum SNI_RET p_data_transmit_only(uint8_t *data, const size_t size){
		enum SNI_RET ret = SNI_RET_OK;
		_spi->begin();
		_spi->transmit_only(data, size);
		_spi->end();
		return ret;
	}
	enum SNI_RET p_data_transmit(uint8_t *wdata, uint8_t *rdata, const size_t size){
        // printf("p_data_transmit\n");
		enum SNI_RET ret = SNI_RET_OK;
		_spi->begin();
        _spi->transmit(wdata, rdata, size);
		_spi->end();
		return SNI_RET_OK;
	}
	enum SNI_RET p_reg_write8(const uint32_t addr, uint8_t *data, const size_t size) {

        enum SNI_RET ret = SNI_RET_OK;
		const size_t header_size = 4;
		uint8_t wdata[header_size] = {0};

		wdata[0] = OP_WRITE_DATA;
		wdata[1] = (addr >> 16) & 0xff;
		wdata[2] = (addr >>  8) & 0xff;
		wdata[3] = (addr >>  0) & 0xff;

		_spi->begin();
		if(_spi->transmit_only(wdata, sizeof(wdata))) {
			ret = SNI_RET_ESPI;
		}
		if(_spi->transmit_only(data, size)) {
			ret = SNI_RET_ESPI;
		}
		_spi->end();
		return SNI_RET_OK;
	}
	enum SNI_RET p_reg_read8(const uint32_t addr, uint8_t *data, const size_t size) {
		enum SNI_RET ret = SNI_RET_OK;
		const size_t header_size = 5;
		uint8_t wdata[header_size] = {0};
		uint8_t rdata[header_size] = {0};

		wdata[0] = OP_READ_DATA;
		wdata[1] = (addr >> 16) & 0xff;
		wdata[2] = (addr >>  8) & 0xff;
		wdata[3] = (addr >>  0) & 0xff;
		wdata[4] = 0x00; // dummy byte

		_spi->begin();
		if(_spi->transmit(wdata, rdata, sizeof(wdata))) {
			ret = SNI_RET_ESPI;
		}
		if(_spi->receive_only(data, size)) {
			ret = SNI_RET_ESPI;
		}
		_spi->end();

		return SNI_RET_OK;
	}
	enum SNI_RET p_reg_read8_iq(uint8_t *data, const size_t size, const size_t ssize) {
		enum SNI_RET ret = SNI_RET_OK;
		ret = p_reg_read8_iq_start(data, size, ssize);
		p_reg_read8_iq_stop();
		return ret;
	}
	enum SNI_RET p_reg_read8_iq_dma(uint8_t *data, const size_t size, const size_t ssize) {
		enum SNI_RET ret;


        #if defined(__USE_HAL__) && defined(__USE_DMA__)
            _spi->clear_dma_end_flag();
            _spi->start();
            ret = p_reg_read8_iq_start(data, size, ssize);
            _spi->wait_dma();
            _spi->clear_dma_end_flag();
            p_reg_read8_iq_stop();
        #else
            // _spi->clear_dma_end_flag();
            // _spi->start();
            ret = p_reg_read8_iq_start(data, size, ssize);
            // _spi->wait_dma();
            // _spi->clear_dma_end_flag();
            p_reg_read8_iq_stop();
        #endif
        
		return ret;
	}
	enum SNI_RET p_reg_read8_iq_start(uint8_t *data, const size_t size, const size_t ssize) {
		enum SNI_RET ret = SNI_RET_OK;
		const size_t header_size = 5 + ssize;
		uint8_t wdata[header_size] = {0};

		wdata[0] = OP_READ_DATA;
		wdata[1] = 0x01; //addr 
        wdata[2] = 0x00; //addr
        wdata[3] = 0x00; //addr
        wdata[4] = 0x00; //dummy									// SRAM_IQ_FIFO address 1st byte

		_spi->begin();
		if(_spi->transmit_only(wdata, sizeof(wdata))) {
			ret = SNI_RET_ESPI;
		}
		if(_spi->receive_only_dma(data, size)) {
			ret = SNI_RET_ESPI;
		}

		return ret;
	}
	void p_reg_read8_iq_stop(void) {
		_spi->end();
	}

public:
	RF_SPI1(MCU_SPI_Ctrl *pspi) : _spi(pspi) {}
	virtual ~RF_SPI1() {}
	enum SNI_RET data_transmit_only(uint8_t *data, const size_t size){
		enum SNI_RET ret;
		ret = p_data_transmit_only(data, size);
		return ret;
	}
	enum SNI_RET data_transmit(uint8_t *wdata, uint8_t *rdata, const size_t size){
		enum SNI_RET ret;
		ret = p_data_transmit(wdata, rdata, size);
		return ret;
	}
	enum SNI_RET reg_write8(const uint32_t addr, uint8_t *data, const size_t size) {
		enum SNI_RET ret;
		ret = p_reg_write8(addr, data, size);
		return ret;
	}
	enum SNI_RET reg_read8(const uint32_t addr, uint8_t *data, const size_t size) {
		enum SNI_RET ret;
		ret = p_reg_read8(addr, data, size);
		return ret;
	}
	enum SNI_RET reg_read8_iq(uint8_t *data, const size_t size, const size_t ssize) {
		enum SNI_RET ret;
		ret = p_reg_read8_iq(data, size, ssize);
		return ret;
	}
	enum SNI_RET reg_read8_iq_dma(uint8_t *data, const size_t size, const size_t ssize) {
		enum SNI_RET ret;
		ret = p_reg_read8_iq_dma(data, size, ssize);
		return ret;
	}

	int init(void) {
		_spi->init();
		return 0;
	}

};

#endif /* _RF_SPI1_H */
