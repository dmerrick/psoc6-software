/*
 * Copyright (c) 2017 Socionext Inc.
 * All rights reserved.
 */

/*
 * sni_rs_termctl.cpp
 */

//#include <mbed.h>

#include "sni_rs_termctl.h"
#include "configuration.h"

// // for test (fixed signal)
// static DigitalOut digtest0(PB_2);
// static DigitalOut digtest1(PB_6);

// // fixed signal
// static DigitalIn SPIMODE(PB_4);
// static DigitalIn SENS(PA_3);

/* Dan: OR: GPIO, Output Ready signal of FIFO data stored in sensor LSI FIFO memory */
static InterruptIn m_or(RS1_OR_PIN);

/* Dan: NRST: GPIO, reset signal for sensor LSI */
static DigitalOut nrst(RS1_NRST_PIN);

/* Dan: CE (Chip Enable): GPIO, enabling signal for sensor LSI */
static DigitalOut ce(RS1_CE_PIN);

/* Green Laser Diode on U575ZI-Q*/
static DigitalOut ld1(GLED_PIN);

/* Blue Laser Diode on U575ZI-Q*/
static DigitalOut ld3(BLED_PIN);

/* Red Laser Diode on U575ZI-Q*/
static DigitalOut led(RLED_PIN);

/* What the ?? */
// static DigitalOut digtest0(RS1_DIGTEST0_PIN);
// static DigitalOut digtest1(RS1_DIGTEST1_PIN);

/* What the ?? */
// static DigitalIn SPIMODE(RS1_SPIMODE_PIN);
// static DigitalIn SENS(RS1_SENS_PIN);


int sni_rs_termctl_init(struct sni_rs_termctl_t *term, void *_spi)
{
	(void) _spi;

	term->nrst = &nrst;
	term->ce = &ce;
	term->ld1 = &ld1;
	term->ld3 = &ld3;
	term->led = &led;
	// term->digtest0 = &digtest0;
	// term->digtest1 = &digtest1;
	// term->sens = &SENS;

	term->m_or = &m_or;

	// HAL_NVIC_SetPriority(EXTI2_IRQn, 4, 0);
	HAL_NVIC_SetPriority(EXTI0_IRQn, 4, 0);

	return 0;
}

int sni_rs_termctl_free(struct sni_rs_termctl_t *term)
{
	(void) term;
	return 0;
}

int sni_rs_term_write(sni_rs_term_t *term, int val)
{
	((DigitalOut *) *term)->write(val);

	return 0;
}

int sni_rs_intr_enable(sni_rs_intr_t *intr)
{
	((InterruptIn *) *intr)->enable_irq();

	return 0;
}

int sni_rs_intr_disable(sni_rs_intr_t *intr)
{
	((InterruptIn *) *intr)->disable_irq();

	return 0;
}


int sni_rs_intr_setisr(sni_rs_intr_t *intr, sni_rs_intr_isr_t func, void *arg)
{
	((InterruptIn *) *intr)->rise(callback(func, arg));
// #if MBED_VERSION >= MBED_ENCODE_VERSION(5, 1, 0)
// 	((InterruptIn *) *intr)->rise(callback(func, arg));
// #else
// 	((InterruptIn *) *intr)->rise(func, arg);
// #endif

	return 0;
}
