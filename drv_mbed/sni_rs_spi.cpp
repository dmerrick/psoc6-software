/*
 * Copyright (c) 2017 Socionext Inc.
 * All rights reserved.
 */

/*
 * sni_rs_spi.cpp
 */

#include "RF_SPI1.h"
#include "sni_rs_spi.h"

enum SNI_RET sni_rs_spi_data_transmit_only(void *_spi, uint8_t *data, const size_t size)
{
	enum SNI_RET ret;
	ret = ((class RF_SPI1 *) _spi)->data_transmit_only(data, size);

	return ret;
}

enum SNI_RET sni_rs_spi_data_transmit(void *_spi, uint8_t *wdata, uint8_t *rdata, const size_t size)
{
	enum SNI_RET ret;
	ret = ((class RF_SPI1 *) _spi)->data_transmit(wdata, rdata, size);

	return ret;
}

enum SNI_RET sni_rs_spi_reg_write8(void *_spi, const uint32_t addr, uint8_t *data, const size_t size)
{
	enum SNI_RET ret;
	ret = ((class RF_SPI1 *) _spi)->reg_write8(addr, data, size);

	return ret;
}

enum SNI_RET sni_rs_spi_reg_read8(void *_spi, const uint32_t addr, uint8_t *data, const size_t size)
{
	enum SNI_RET ret;
	ret = ((class RF_SPI1 *) _spi)->reg_read8(addr, data, size);

	return ret;
}

enum SNI_RET sni_rs_spi_reg_read8_iq(void *_spi, uint8_t *data, const size_t size, const size_t ssize)
{
	enum SNI_RET ret;
	ret = ((class RF_SPI1 *) _spi)->reg_read8_iq(data, size, ssize);

	return ret;
}

enum SNI_RET sni_rs_spi_reg_read8_iq_dma(void *_spi, uint8_t *data, const size_t size, const size_t ssize)
{
	enum SNI_RET ret;
	ret = ((class RF_SPI1 *) _spi)->reg_read8_iq_dma(data, size, ssize);

	return ret;
}

int sni_rs_spi_init(void *_spi)
{
	return ((class RF_SPI1 *) _spi)->init();
}
