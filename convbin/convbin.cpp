﻿/*
 * Copyright (c) 2018 Socionext Inc.
 * All rights reserved.
 *
 */

/*
 * convbin.cpp
 */

#include <cstdio>
#include <cstdlib>

#include "convbin.h"


int bsavefp(void **buf, size_t *res, float data)
{
	char tmp[CONVBIN_SIZE_FP]{};

#ifdef WIN32
	_snprintf_s(tmp, sizeof(tmp), _TRUNCATE, "%f", data);
#else
	snprintf(tmp, sizeof(tmp), "%f", data);
#endif
//	printf("        %f: <%s>\n", data, tmp);

	return bsave8a(buf, res, (uint8_t *) tmp, sizeof(tmp));
}

int bloadfp(const void **buf, size_t *res, float *data)
{
	char tmp[CONVBIN_SIZE_FP]{};

	if(bload8a(buf, res, (uint8_t *) tmp, CONVBIN_SIZE_FP) != 0) {
		return -1;
	}

	*data = static_cast<float>(strtod(tmp, nullptr));
//	printf("        %f: <%s>\n", *data, tmp);

	return 0;
}
