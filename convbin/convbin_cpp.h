﻿/*
 * Copyright (c) 2018 Socionext Inc.
 * All rights reserved.
 *
 */

#ifndef	__CONVBIN_CPP_H__
#define	__CONVBIN_CPP_H__

#ifdef __cplusplus

#include <cstdint>
#include <vector>

#include "convbin.h"

//
// functions for C++ only
//

template<typename T> static bool bsave(void **buf, size_t *res, T data);
template<typename T> static bool bsave(void **buf, size_t *res, const T *data, size_t size);
template<typename T> static bool bsave_l(void **buf, size_t *res, T data);
template<typename T> static bool bsave_l(void **buf, size_t *res, const T *data, size_t size);

template<typename T> static bool bload(const void **buf, size_t *res, T *data);
template<typename T> static bool bload(const void **buf, size_t *res, T *data, size_t size);
template<typename T> static bool bload_l(const void **buf, size_t *res, T *data);

template<class T, class... Args> static bool bsave(void **buf, size_t *res, T data, const Args&... args);
template<class T, class... Args> static bool bsave_l(void **buf, size_t *res, T data, const Args&... args);

template<typename T> static bool bsave_ap(std::vector<uint8_t> *buf, T data);
template<typename T> static bool bsave_ap(std::vector<uint8_t> *buf, const T *data, size_t size);

template<typename T> static bool bfill(void **buf, size_t *res, T data, size_t size);
template<typename T> static bool bskip(const void **buf, size_t *res, size_t size);

//
// body of functions
//
static inline bool bsave(void **buf, size_t *res, uint8_t data)
{
	return bsave8(buf, res, data) == 0;
}
static inline bool bsave(void **buf, size_t *res, uint16_t data)
{
	return bsave16(buf, res, data) == 0;
}
static inline bool bsave(void **buf, size_t *res, uint32_t data)
{
	return bsave32(buf, res, data) == 0;
}
static inline bool bsave(void **buf, size_t *res, uint64_t data)
{
	return bsave64(buf, res, data) == 0;
}
static inline bool bsave(void **buf, size_t *res, int32_t data)
{
	return bsave32s(buf, res, data) == 0;
}
static inline bool bsave_l(void **buf, size_t *res, uint8_t data)
{
	return bsave8(buf, res, data) == 0;
}
static inline bool bsave_l(void **buf, size_t *res, uint16_t data)
{
	return bsave16l(buf, res, data) == 0;
}
static inline bool bsave_l(void **buf, size_t *res, uint32_t data)
{
	return bsave32l(buf, res, data) == 0;
}
static inline bool bsave(void **buf, size_t *res, float data)
{
	return bsavefp(buf, res, data) == 0;
}

template<typename T>
static inline bool bsave(void **buf, size_t *res, T data)
{
	return bsave(buf, res, data);
}
template<class T, class... Args>
static inline bool bsave(void **buf, size_t *res, T data, const Args&... args)
{
	if(bsave<T>(buf, res, data) == false) {
		return false;
	}
	if(bsave<T>(buf, res, args...) == false) {
		return false;
	}
	return true;
}

static inline bool bload(const void **buf, size_t *res, uint8_t *data)
{
	return bload8(buf, res, data) == 0;
}
static inline bool bload(const void **buf, size_t *res, uint16_t *data)
{
	return bload16(buf, res, data) == 0;
}
static inline bool bload(const void **buf, size_t *res, uint32_t *data)
{
	return bload32(buf, res, data) == 0;
}
static inline bool bload(const void **buf, size_t *res, uint64_t *data)
{
	return bload64(buf, res, data) == 0;
}
static inline bool bload(const void **buf, size_t *res, int32_t *data)
{
	return bload32s(buf, res, data) == 0;
}
static inline bool bload_l(const void **buf, size_t *res, uint8_t *data)
{
	return bload8(buf, res, data) == 0;
}
static inline bool bload_l(const void **buf, size_t *res, uint16_t *data)
{
	return bload16l(buf, res, data) == 0;
}
static inline bool bload_l(const void **buf, size_t *res, uint32_t *data)
{
	return bload32l(buf, res, data) == 0;
}
static inline bool bload(const void **buf, size_t *res, float *data)
{
	return bloadfp(buf, res, data) == 0;
}

template<typename T>
static inline bool bload(const void **buf, size_t *res, T *data)
{
	return bload(buf, res, data);
}

template<typename T>
static inline size_t bsize_local(T *data)
{
	return sizeof(*data);
}
template<typename T>
static inline size_t bsize_local(float *data)
{
	(void) data;
	return CONVBIN_SIZE_FP;
}
template<typename T>
static inline size_t bsize(void)
{
	T *data = nullptr;
	return bsize_local<T>(data);
}

template<typename T>
static inline bool bsave(void **buf, size_t *res, const T *data, size_t size)
{
	for(size_t s=0; s<size; s++) {
		if(bsave(buf, res, data[s]) != true) {
			return false;
		}
	}

	return true;
}

template<typename T>
static inline bool bsave_l(void **buf, size_t *res, const T *data, size_t size)
{
	for(size_t s=0; s<size; s++) {
		if(bsave_l(buf, res, data[s]) != true) {
			return false;
		}
	}

	return true;
}

template<typename T>
static inline bool bload(const void **buf, size_t *res, T *data, size_t size)
{
	for(size_t s=0; s<size; s++) {
		if(bload(buf, res, &(data[s])) != true) {
			return false;
		}
	}

	return true;
}

template<typename T>
static inline bool bsave_ap(std::vector<uint8_t> *buf, T data)
{
	const auto asize = bsize<T>();
	const auto len = buf->size();
	buf->resize(len + asize);

	void *pos = buf->data() + len;
	size_t res = asize;

	if(bsave(&pos, &res, data) == false) {
		return false;
	}

	return true;
}

template<typename T>
static inline bool bsave_ap(std::vector<uint8_t> *buf, const T *data, size_t size)
{
	const auto asize = bsize<T>() * size;
	const auto len = buf->size();
	buf->resize(len + asize);

	void *pos = buf->data() + len;
	size_t res = asize;

	if(bsave(&pos, &res, data, size) == false) {
		return false;
	}

	return true;
}

template<typename T>
static inline bool bsave_l(void **buf, size_t *res, T data)
{
	return bsave_l(buf, res, data);
}

template<class T, class... Args>
static inline bool bsave_l(void **buf, size_t *res, T data, const Args&... args)
{
	if(bsave_l<T>(buf, res, data) == false) {
		return false;
	}
	if(bsave_l<T>(buf, res, args...) == false) {
		return false;
	}
	return true;
}

template<typename T>
static inline bool bload_l(const void **buf, size_t *res, T *data)
{
	return bload_l(buf, res, data);
}

template<typename T>
static inline bool bfill(void **buf, size_t *res, T data, size_t size)
{
	return bfill8(buf, res, data, size) == 0;
}

template<typename T>
static inline bool bskip(const void **buf, size_t *res, size_t size)
{
	return bskip8(buf, res, size) == 0;
}

#endif  /* __cplusplus */

#endif	/* __CONVBIN_CPP_H__ */
