
/*
 * Copyright (c) 2016 Socionext Inc.
 * All rights reserved.
 */

#include <stdio.h>
#include <cmath>

#include "rs_loc3d_top.h"
#include "detect_2d.h"
#include "rs_loc3d_algo_local.h"
#include "init_param.h"
#include "sp_common.h"

#define ALGO_EN

#ifdef ALGO_EN
static C_2D_detect         c2d[MAX_APN];
#endif 

static struct proc_param   sp_param[MAX_APN];
static struct app_param    ap_param[MAX_APN];
static struct bitmap_param bp_param[MAX_APN];
static struct clst_param   cl_param[MAX_APN];
static int                 rxchmap[MAX_APN][MAX_RCH_N];
static int                 valid_apn[MAX_APN];
static snsr_param          *p_sens_param[MAX_CHIRP];
static int                 multi_chirp_num;
struct snsr_param          sens_param[MAX_CHIRP];

uint8_t fdata[8192];
static srsl::complex<short>  in[MAX_RCH_N][MAX_IQIN_SIZE];

uint16_t curr_frame_count;
int frame_loss_count;
int frame_count_exp;

//void printf(const char *fmt, ...);

//---------------------------------------------------------------------------
void setIQ(const int snum,
		   const int nrx,
		   const int bitw,
		   const int (&chmap)[MAX_RCH_N],
		   uint8_t* fdp)
{
	short idata,qdata;
	int chcnt=0;
	int scnt=0;

	for(int i=0;i<snum*bitw*nrx;){

		int ch = chmap[chcnt];

		if( bitw==4 ) {
			idata = *(fdp++)<<8;
			idata |= *(fdp++)&0xff;
			qdata = *(fdp++)<<8;
			qdata |= *(fdp++)&0xff;

			in[ch][scnt].real = idata;
			in[ch][scnt].imag = qdata;

			i+=4;
		}
		else {
			idata = *(fdp)<<4;
			idata |= (*(fdp++)&0xf0)>>4;
			qdata = (*(fdp++)&0x0f)<<8;
			qdata |= *(fdp++)&0xff;

			qdata <<= 4;
			qdata >>= 4;
			idata <<= 4;
			idata >>= 4;

			in[ch][scnt].real = idata;
			in[ch][scnt].imag = qdata;

			i+=3;
		}

		chcnt++;
		chcnt = chcnt%nrx;
		if( chcnt==0 ) {
			scnt++;
		}
	}
}

//---------------------------------------------------------------------------
void setFFT(const int snum,
               const int nrx,
               const int bitw,
               const int (&chmap)[MAX_RCH_N],
               uint8_t* fdp)
{
	short idata,qdata;
	int chcnt=0;
	int scnt=0;

	for(int i=0;i<snum*bitw*nrx;){

		int ch = -1;
		if( chcnt<MAX_RCH_N ) {
			ch = chmap[chcnt];
		}
		
		if( bitw==4 ) {
			idata = *(fdp++)<<8;
			idata |= *(fdp++)&0xff;
			qdata = *(fdp++)<<8;
			qdata |= *(fdp++)&0xff;

			if(ch>=0) {
				in[ch][scnt].real = idata;
				in[ch][scnt].imag = -qdata;
			}

			i+=4;
		}
		else {
			idata = *(fdp)<<4;
			idata |= (*(fdp++)&0xf0)>>4;
			qdata = (*(fdp++)&0x0f)<<8;
			qdata |= *(fdp++)&0xff;

			qdata <<= 4;
			qdata >>= 4;
			idata <<= 4;
			idata >>= 4;

			if(ch>=0) {
				in[ch][scnt].real = idata;
				in[ch][scnt].imag = -qdata;
			}

			i+=3;
		}
		
		scnt++;
		if( scnt>=snum ) {
			scnt = 0;
			chcnt++;
			chcnt = chcnt%nrx;
		}
	}
}

//---------------------------------------------------------------------------
rs_int algo_init(int n_chirp, snsr_param *snsr_param)
{
	multi_chirp_num = n_chirp;
	memset(valid_apn, 0, sizeof(valid_apn));
    
    if (p_sens_param == NULL){
        printf("p_sens_param is NULL\n");
    }

	valid_apn[0] = 1;

	memset(rxchmap, -1, sizeof(rxchmap));
	
	for(int m=0;m<n_chirp;m++) {
		p_sens_param[m] = &snsr_param[m];
	}

	if(p_sens_param[0]->fft_en==0) { 
		printf("fft_en == 0\n");
        set_init_app_param(8, 1, &ap_param[0]);
        set_init_bmp_param(8, 1, &bp_param[0]);
        set_init_clst_param(8, 1, &cl_param[0]);
	}
	else {
        printf("fft_en == 1\n");
		set_init_app_param_hwfft(8, 1, &ap_param[0]);
		set_init_bmp_param_hwfft(8, 1, &bp_param[0]);
		set_init_clst_param_hwfft(8, 1, &cl_param[0]);
	}
	
	for(int apn=0;apn<MAX_APN;apn++) {
		// if( valid_apn[m]==0 ) continue;

		sp_param[apn].app = &ap_param[apn];
		sp_param[apn].clst = &cl_param[apn];
		

		int nrx = 0;
		for(int i=0;i<MAX_RCH_N;i++) {
			if( p_sens_param[apn]->rs_para_m.rxch & (1<<i) ) {
				rxchmap[apn][nrx] = i;
				nrx++;
			}
		}

		sp_param[apn].SweepTime = p_sens_param[apn]->rs_para_m.sweep_time;
		sp_param[apn].SweepFreq = p_sens_param[apn]->rs_para_m.band_width;
		srsl::fp fs = p_sens_param[apn]->rs_para_m.point_num/(sp_param[apn].SweepTime*102.4/110);
		fs *= 1000000;
		sp_param[apn].Fs = fs;

		if( p_sens_param[apn]->rs_para_m.point_num>MAX_IQIN_SIZE ) {
			return RS_LOC_EINV;
		}
		c2d[apn].apn = apn;
		sp_param[apn].mimo_en = 1;
		c2d[apn].SetParam(&sp_param[apn]);
		c2d[apn].bmp_param = &bp_param[apn];
	}

	frame_count_exp = -1;
	frame_loss_count = 0;

	return RS_LOC_OK;
}

//---------------------------------------------------------------------------
rs_int ffs_algo_init(int mode, snsr_param *snsr_param)
{
	return RS_LOC_OK;
}

//---------------------------------------------------------------------------
rs_int algo_run(uint8_t *fdp, int frame_count)
{
	int apn = 0;
	
	if(frame_count_exp>=0) {
		int now_frame_loss_count = frame_count - frame_count_exp;
		if(now_frame_loss_count<0) now_frame_loss_count += 256;
		frame_loss_count += now_frame_loss_count;
	}
	frame_count_exp = frame_count + 1;
	if(frame_count_exp>=256) frame_count_exp = 0;
	
	curr_frame_count = frame_count;

	for(int m=0;m<multi_chirp_num;m++) {
		//if( valid_apn[m]==0 ) continue;
		int snum = p_sens_param[m]->rs_para_m.point_num;
		int fft_en = p_sens_param[m]->fft_en;
		int txch = p_sens_param[m]->rs_para_m.txch;

		int nrx = 0;
		for(int i=0;i<MAX_RCH_N;i++) {
			if( rxchmap[apn][nrx]!=-1 ) {
				nrx++;
			}
		}

		int bitw = (p_sens_param[m]->bit_width == RS_BIT_WIDTH_16BIT) ? 4 : 3;
		if( !fft_en ) {
			setIQ(snum, nrx, bitw, rxchmap[apn], fdp);
		}
		else {
			snum = p_sens_param[m]->fft.fft_upper_range + 1;
			setFFT(snum, 4, bitw, rxchmap[apn], fdp);
		}
		fdp += (snum*nrx*bitw);

		{
#ifdef ALGO_EN
			c2d[apn].sp_run(in, 0, rxchmap[apn], snum, fft_en, txch);

			if (c2d[apn].param->app->PkDetMode_r > 0) {
				if (c2d[apn].r_pk_maxpow_index_en) {
					c2d[apn].r_pk_indexs[0] = c2d[m].r_pk_maxpow_index;
				}
			}
#endif
		}

	}

	return RS_LOC_OK;
}

//------------------------------------------------------------------------------
// getting parameters from sequencer program code API
// setting sequencer program code API
//------------------------------------------------------------------------------
int algo_setup_seqcode(rs_handle_t handle, uint8_t *seqcode, int setup_en, int hwfft_en)
{
	rs_seqcode_t code_bin;

    rs_attr_t                      attr;
	uint32_t                       modu_mode;
	uint32_t                       center_freq;
	enum rs_tx_power               tx_power;
	enum rs_xtal_freq              xtalfreq;
	uint32_t                       frame_rate;
	uint32_t                       chirp_rate_constant;
	enum rs_bit_width              bit_width;
	enum rs_header_mode            header_mode;
	enum rs_ex_status_header_mode  status_header_mode;
	struct rs_chirp_fmcw_multi     fmcw_m;
	enum rs_fft_winfunc            fft_winfunc;
	enum rs_fft_en                 fft_en;
	struct rs_chirp_fft_multi      fft_multi;

	if(rs_seqcode_init(&code_bin)) { // empty bin
		printf("rs_seqcode_init fail\n");
		return __LINE__;
	}
	else {
		printf("rs_seqcode_init done\n");
	}

	int size = 0;
	if(hwfft_en==0) {
		load_seq_code(seqcode, &size); // load mode 8 40 hz bin data into seqcode 
	}
	else {
		load_seq_code_hwfft(seqcode, &size);
	}

	if(rs_seqcode_load(code_bin, seqcode, size)) { // ???
		printf("rs_seqcode_load fail\n");
		return __LINE__;
	}
	else {
		printf("rs_seqcode_load done\n");
	}

	if(setup_en) {
	    if(rs_setup(handle, RS_SETUP_TYPE_SEQCODE, code_bin)) { // good
			printf("rs_setup fail\n");
			return __LINE__;
		}
		else {
			printf("rs_setup done\n");
		}
	}
	else {
		printf("rs_setup Skip\n");
	}
    if(rs_attr_init(&attr)) { // initialize values
		printf("rs_attr_init fail\n");
		return __LINE__;
	}
	else {
		printf("rs_attr_init done\n");
	}

	if(rs_seqcode_get_attr(code_bin, attr)) {
		printf("rs_seqcode_get_attr fail\n");
		return __LINE__;
	}
	else {
		printf("rs_seqcode_get_attr done\n");
	}

    if(rs_attr_get_modulation_mode(attr, &modu_mode)) {
		printf("rs_attr_get_modulation_mode fail\n");
		return __LINE__;
	}
	else {
		printf("rs_attr_get_modulation_mode done\n");
	}

    if(modu_mode == RS_MODU_MODE_FMCW_M) {
		if(rs_attr_get_fmcw_multi(attr, &fmcw_m)) { // set chirp parameters
			printf("rs_attr_get_fmcw_multi fail\n");
			return __LINE__;
		}
		else {
			printf("rs_attr_get_fmcw_multi done\n");
		}
    }
	else {
		printf("the sample code support only RS_MODU_MODE_FMCW_M\n");
	}

    if(rs_attr_get_txpower(attr, &tx_power)) {
		printf("rs_attr_get_txpower fail\n");
		return __LINE__;
	}
	else {
		printf("rs_attr_get_txpower done\n");
	}

    if(rs_attr_get_center_freq(attr, &center_freq)) {
		printf("rs_attr_get_center_freq fail\n");
		return __LINE__;
	}
	else {
		printf("rs_attr_get_center_freq done\n");
	}

    if(rs_attr_get_xtal_freq(attr, &xtalfreq)) {
		printf("rs_attr_get_xtal_freq fail\n");
		return __LINE__;
	}
	else {
		printf("rs_attr_get_xtal_freq done\n");
	}

    if(rs_attr_get_frame_rate(attr, &frame_rate)) {
		printf("rs_attr_get_frame_rate fail\n");
		return __LINE__;
	}
	else {
		printf("rs_attr_get_frame_rate done\n");
	}

	if(rs_attr_get_chirp_rate_constant(attr, &chirp_rate_constant)) {
		printf("rs_attr_get_chirp_rate_constant fail\n");
		return __LINE__;
	}
	else {
		printf("rs_attr_get_chirp_rate_constant done\n");
	}
	
	if(rs_attr_get_data_format( attr, &bit_width, &header_mode, &status_header_mode)) {
		printf("rs_attr_get_data_format fail\n");
		return __LINE__;
	}
	else {
		printf("rs_attr_get_data_format done\n");
	}
	if (rs_attr_get_fft_en(attr, &fft_en)) {
		printf("rs_attr_get_fft_en fail\n");
		return __LINE__;
	}
	else {
		printf("rs_attr_get_fft_en done\n");
	}

	if (fft_en == RS_FFT_ENABLE) {
		if (rs_attr_get_fft_winfunc(attr, &fft_winfunc)) {
			printf("rs_attr_get_fft_winfunc fail\n");
			return __LINE__;
		}
		else {
			printf("rs_attr_get_fft_winfunc done\n");
		}
		if (rs_attr_get_fft_multi(attr, &fft_multi)) {
			printf("rs_attr_get_fft_multi fail\n");
			return __LINE__;
		}
		else {
			printf("rs_attr_get_fft_multi done\n");
		}
	}

	memset(sens_param, 0, sizeof(struct snsr_param));
	for(int i = 0; i < fmcw_m.multi_chirp_num; i++) {
		if(i>=MAX_CHIRP) break;
		sens_param[i].modu_mode             = modu_mode;
		sens_param[i].tx_power              = tx_power;
		sens_param[i].frame_rate            = frame_rate;
		sens_param[i].center_freq           = center_freq;
		sens_param[i].chirp_rate_constant   = chirp_rate_constant;
		sens_param[i].bit_width             = bit_width;
		sens_param[i].header_mode           = header_mode;
		sens_param[i].ex_status_header_mode = status_header_mode;
		sens_param[i].rx_gain               = fmcw_m.chirp_fmcw[i].rx_gain;
		sens_param[i].rs_para_m.rx_gain     = fmcw_m.chirp_fmcw[i].rx_gain;
		sens_param[i].rs_para_m.rxch        = fmcw_m.chirp_fmcw[i].rxch;
		sens_param[i].rs_para_m.txch        = fmcw_m.chirp_fmcw[i].txch;
		sens_param[i].rs_para_m.iq_mode     = fmcw_m.chirp_fmcw[i].iq_mode;
		sens_param[i].rs_para_m.chirp_mode  = fmcw_m.chirp_fmcw[i].chirp_mode;
		sens_param[i].rs_para_m.band_width  = fmcw_m.chirp_fmcw[i].band_width;
		sens_param[i].rs_para_m.sweep_time  = fmcw_m.chirp_fmcw[i].sweep_time;
		sens_param[i].rs_para_m.point_num   = fmcw_m.chirp_fmcw[i].point_num;
		sens_param[i].rs_para_m.lpf_fc      = fmcw_m.chirp_fmcw[i].lpf_fc;
		sens_param[i].rs_para_m.hpf_fc      = fmcw_m.chirp_fmcw[i].hpf_fc;
		sens_param[i].fft_en                = fft_en;
		if (fft_en == RS_FFT_ENABLE) {
			sens_param[i].fft.fft_dc_cut      = fft_multi.fft[i].fft_dc_cut;
			sens_param[i].fft.fft_point       = fft_multi.fft[i].fft_point;
			sens_param[i].fft.fft_upper_range = fft_multi.fft[i].fft_upper_range;
		}
	}

	algo_init(fmcw_m.multi_chirp_num, sens_param); // good



	printf("TX_POWER             %d\n",   tx_power);
	printf("CENTRAL_FREQ         %d\n",   center_freq);
	printf("XTAL_FREQ            %d\n",   xtalfreq);
	printf("FRAME_RATE           %d\n",   frame_rate);
	printf("CHIRP_RATE_CONSTANT  %d\n",   chirp_rate_constant);
	printf("BIT_WIDTH            %d\n",   bit_width);
	printf("HEADER_MODE          %d\n",   header_mode);
	printf("STATUS_MODE          %d\n",   status_header_mode);
	printf("HW_FFT_EN            %d\n",   fft_en);
	if (fft_en == RS_FFT_ENABLE) {
		printf("FFT_WINFUNC          %d\n", fft_winfunc);
	}

	printf("CHIRP_R_NUM          %d\n",   fmcw_m.repeat_num);
	printf("CHIRP_MULTI_NUM      %d\n",   fmcw_m.multi_chirp_num);
	for(int i = 0; i < fmcw_m.multi_chirp_num; i++) {
		printf("RX_GAIN          %d\n",   fmcw_m.chirp_fmcw[i].rx_gain);
		printf("RXCH             %02x\n", fmcw_m.chirp_fmcw[i].rxch);
		printf("TXCH             %02x\n", fmcw_m.chirp_fmcw[i].txch);
		printf("IQ_MODE          %d\n",   fmcw_m.chirp_fmcw[i].iq_mode);
		printf("CHIRP_MODE       %d\n",   fmcw_m.chirp_fmcw[i].chirp_mode);
		printf("CHIRP_BAND_WIDTH %d\n",   fmcw_m.chirp_fmcw[i].band_width);
		printf("SWEEP_TIME       %d\n",   fmcw_m.chirp_fmcw[i].sweep_time);
		printf("CHIRP_POINT      %d\n",   fmcw_m.chirp_fmcw[i].point_num);
		printf("LPF_FC           %d\n",   fmcw_m.chirp_fmcw[i].lpf_fc);
		printf("HPF_FC           %d\n",   fmcw_m.chirp_fmcw[i].hpf_fc);
	}
	if (fft_en == RS_FFT_ENABLE) {
		for (int i = 0; i < fft_multi.multi_chirp_num; i++) {
			printf("DC_CUT           %d\n", fft_multi.fft[i].fft_dc_cut);
			printf("POINT            %d\n", fft_multi.fft[i].fft_point);
			printf("UPPER_RANGE      %d\n", fft_multi.fft[i].fft_upper_range);
		}
	}

    if(rs_attr_free(attr)) {
		printf("rs_attr_free fail\n");
		return __LINE__;
	}
	else {
		printf("rs_attr_free done\n");
	}

	if(rs_seqcode_free(code_bin)) {
		printf("rs_seqcode_free fail\n");
		return __LINE__;
	}
	else {
		printf("rs_seqcode_free done\n");
	}

	return 0;

}


//---------------------------------------------------------------------------
rs_int algo_get_dist_data(rs_int apn, struct rs_dist_data *p_dist_data)
{
	if(apn>=MAX_APN) return RS_LOC_EINV;
	if( valid_apn[apn]==0 ) return RS_LOC_EINV;

#ifdef ALGO_EN
	p_dist_data->num = c2d[apn].r_pk_num;
	for (rs_uint32_t pk = 0; pk < p_dist_data->num; ++pk)
	{
		int r = c2d[apn].r_pk_indexs[pk];
		p_dist_data->r[pk] = (int)(c2d[apn].ix2dist[r] * 10.0);

	}
#else
	p_dist_data->num = 0;
#endif

	return RS_LOC_OK;
}

//---------------------------------------------------------------------------

rs_int algo_get_xyz_data(rs_int apn, struct rs_xyz_data *data)

{
	if(apn>=MAX_APN) return RS_LOC_EINV;
	if( valid_apn[apn]==0 ) return RS_LOC_EINV;

#ifdef ALGO_EN
	C_2D_detect *p_c2d = &c2d[apn];
	int num = p_c2d->xyz_data.num;
	data->num = num;
	for(int i=0;i<num;i++) {
		data->pos[i].x = p_c2d->xyz_data.pos[i].x;
		data->pos[i].y = p_c2d->xyz_data.pos[i].y;
		data->pos[i].z = p_c2d->xyz_data.pos[i].z;
	}
#endif

	return RS_LOC_OK;
}
//---------------------------------------------------------------------------

rs_int algo_get_xyz_data_clst(rs_int apn, struct rs_xyz_data *data)

{
	if(apn>=MAX_APN) return RS_LOC_EINV;
	if( valid_apn[apn]==0 ) return RS_LOC_EINV;

#ifdef ALGO_EN
	C_2D_detect *p_c2d = &c2d[apn];
	int num = p_c2d->xyz_data_clst.num;
	data->num = num;
	for(int i=0;i<num;i++) {
		data->pos[i].x = p_c2d->xyz_data_clst.pos[i].x;
		data->pos[i].y = p_c2d->xyz_data_clst.pos[i].y;
		data->pos[i].z = p_c2d->xyz_data_clst.pos[i].z;
	}
#endif

	return RS_LOC_OK;
}
//---------------------------------------------------------------------------

rs_int rs_loc3d_get_r_spc(rs_int apn, struct rs_r_spc *spc)

{
	if(apn>=MAX_APN) return RS_LOC_EINV;
	if( valid_apn[apn]==0 ) return RS_LOC_EINV;

#ifdef ALGO_EN
	spc->fftsize = c2d[apn].nfft;
	for (int i = 0; i < spc->fftsize; ++i)
	{
		spc->spc[i] = c2d[apn].a_R_spc_tmp[i];
	}
	srsl::fp th = (srsl::fp) c2d[apn].R_spc_threshold/DB_V;
	th = std::pow(10, th);
	spc->threshold = th;
#endif

	return RS_LOC_OK;
}
//---------------------------------------------------------------------------

static int test_counter = 0;
static uint8_t exor_data = 0;
uint16_t proc_time = 0;
rs_int algo_test_write(uint16_t addr, uint8_t *data, size_t size)
{
	int apn = 0;
	int counter = test_counter;
	if(addr==0x0000) {
		// ctrl cmd
		if(data[0]==1) {
			extern uint8_t param_seqcode[SNI_RS_SETUP_SEQCODE_MAXSIZE];
			extern rs_handle_t handle;
			test_counter = 0;
			exor_data = 0;
			return algo_setup_seqcode(handle, param_seqcode, 0, 0);
		}
		if(data[0]==2) {
			printf("exor_data 0x%02x\n", exor_data);
			exor_data = 0;
			uint32_t t1 = sni_rs_systimer_get_usec();
			int ret = algo_run(fdata, test_counter);
			uint32_t t2 = sni_rs_systimer_get_usec();
			proc_time = t2-t1;
			printf("# algo_run proc time %d\n", proc_time);
			if(ret != RS_LOC_OK) {
				return ret;
			}
			test_counter++;
			test_counter &= 0xff;
			//----------------------------------------------------
			rs_xyz_data xyz_raw;
			ret = algo_get_xyz_data(0, &xyz_raw);
			if (ret == RS_LOC_OK) {
				printf("# [XYZ_RAW] (%d): ", counter);
				if( xyz_raw.num>0 ) {
					printf("[num %d] ", xyz_raw.num);
					for(uint32_t d=0;d<xyz_raw.num;d++) {
						printf("%d,%d,%d ", xyz_raw.pos[d].x, xyz_raw.pos[d].y, xyz_raw.pos[d].z);
					}
					printf("\n");
				}
				else {
					printf("none...\n");
				}
			}
			//----------------------------------------------------
			rs_xyz_data xyz_clst;
			ret = algo_get_xyz_data_clst(0, &xyz_clst);
			if (ret == RS_LOC_OK) {
				printf("# [XYZ_CLST] (%d): ", counter);
				if( xyz_clst.num>0 ) {
					printf("[num %d] ", xyz_clst.num);
					for(uint32_t d=0;d<xyz_clst.num;d++) {
						printf("%d,%d,%d ", xyz_clst.pos[d].x, xyz_clst.pos[d].y, xyz_clst.pos[d].z);
					}
					printf("\n");
				}
				else {
					printf("none...\n");
				}
			}
		}
	}
	else if(addr>=0x2000 && addr<0x4000) {
		// frame data
		uint16_t ad = addr-0x2000;
		for(int i=0;i<size;i++) {
			//printf("addr 0x%x wdata 0x%x\n", addr, data[i]);
			fdata[ad++] = data[i];
			exor_data ^= data[i];
			if(ad==0x4000) break;
		}
	}
	return RS_LOC_OK;
}

rs_int algo_test_read(uint16_t addr, uint8_t *data, size_t *size)
{
	int apn=0;
	
	if(addr==0x0000) {
		rs_xyz_data xyz_raw;
		int ret = algo_get_xyz_data(0, &xyz_raw);
		if (ret == RS_LOC_OK) {
			// ctrl cmd
			int p = 0;
			*(uint16_t*)(&data[p]) = xyz_raw.num;
			p+=2;
			for(int i=0;i<xyz_raw.num;i++) {
				*(uint16_t*)(&data[p]) = xyz_raw.pos[i].x;
				p+=2;
				*(uint16_t*)(&data[p]) = xyz_raw.pos[i].y;
				p+=2;
				*(uint16_t*)(&data[p]) = xyz_raw.pos[i].z;
				p+=2;
			}
			*size = p;
		}
	}
	else if(addr==0x0001) {
		rs_xyz_data xyz_clst;
		int ret = algo_get_xyz_data_clst(0, &xyz_clst);
		if (ret == RS_LOC_OK) {
			// ctrl cmd
			int p = 0;
			*(uint16_t*)(&data[p]) = xyz_clst.num;
			p+=2;
			for(int i=0;i<xyz_clst.num;i++) {
				*(uint16_t*)(&data[p]) = xyz_clst.pos[i].x;
				p+=2;
				*(uint16_t*)(&data[p]) = xyz_clst.pos[i].y;
				p+=2;
				*(uint16_t*)(&data[p]) = xyz_clst.pos[i].z;
				p+=2;
			}
			*size = p;
		}
	}
	else if(addr==0x0002) {
		*(uint16_t*)(&data[0]) = proc_time;
		*size = 2;
	}
	else if(addr==0x0003) {
		*(short*)(&data[0]) = frame_loss_count;
		frame_loss_count = 0;
		*size = 2;
	}
	else if(addr==0x0004) {
		*(uint16_t*)(&data[0]) = curr_frame_count;
		*size = 2;
	}
	else if(addr>=0x2000 && addr<0x4000) {
		// frame data
		uint16_t ad = addr-0x2000;
		*size = 1;
		for(int i=0;i<*size;i++) {
			data[i] = fdata[ad++];
			if(ad==0x4000) break;
		}
	}
	else if(addr==0x4000 || addr==0x4001) {
		int nfft = 256;//c2d[apn].nfft;
		int ad=0;
		for(int i=nfft-1;i>=nfft/2;i--) {
			float spc = 0;
			if(addr==0x4000) {
				spc = c2d[apn].a_R_spc_tmp[i];
				if(spc>0) spc = DB_V * SP_LOG10(spc);
			}
			else if(addr==0x4001)
				spc = c2d[apn].cfar_th[i];
			
			spc = spc*1000;
			*(uint32_t*)(&data[ad]) = (uint32_t)spc;
			ad+=4;
		}
		*size = ad;
	}

	return RS_LOC_OK;
}
