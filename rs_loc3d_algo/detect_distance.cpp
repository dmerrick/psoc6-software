/*
 * Copyright (c) 2016-2021 Socionext Inc.
 * All rights reserved.
 */
#include <stdlib.h>
#include <stdio.h>
#include <srsl_math.h>

#include "detect_distance.h"

//---------------------------------------------------------------------------
C_detect_distance::C_detect_distance()
{
	Init();
}

//---------------------------------------------------------------------------
int C_detect_distance::SetParam(const proc_param *param_in)
{
	param = param_in;
	nfft = param->app->FFTNum;
	fftwin = param->app->FFTWin;
	dccuten = param->app->DcCutEn;

	if( nfft>MAX_SIZE_R ) {
		//return -1;
		nfft = MAX_SIZE_R;
	}

	gf = (srsl::fp) param->Fs / nfft;
	gR = (srsl::fp) 299792458 * 2 * param->SweepTime / 1000000 / (8 * param->SweepFreq * 1000000) * gf;
	gR_resolution = (srsl::fp) gR * 2 * 100;
	gR_resolution_en = 1;

	return 0;
}

//---------------------------------------------------------------------------
void C_detect_distance::Init( void )
{
	//!!intmp.resize(MAX_SIZE_R);

	param = nullptr;

	gf = gR = 0;
	gR_resolution_en = 0;
}

//---------------------------------------------------------------------------
void C_detect_distance::FFT_run(const int dataSize, const srsl::complex<short>* input)
{
	int i;


	// DC-Cut
	if( dccuten ) {
		WindowFunction.window1(WindowFunction.RECTANGULAR_WINDOW, dataSize, input, intmp);
		srsl::complexfp ave(0, 0);
		for(i=0;i<dataSize;i++) {
			ave += intmp[i];
		}
		ave = ave / (srsl::fp)dataSize;
		for(i=0;i<dataSize;i++) {
			intmp[i] -= ave;
			intmp[i] -= ave;
		}
		// Window function
		fftwin = param->app->FFTWin;
		WindowFunction.window2(fftwin, dataSize, intmp, intmp);
	}
	else {
		fftwin = param->app->FFTWin;
		WindowFunction.window1(fftwin, dataSize, input, intmp);
	}


	// 0-padding
	if( dataSize != 0 ) {
		if( nfft > dataSize ) {
			for( i = dataSize; i < nfft; i++ ) {
				intmp[i].real = 0;
				intmp[i].imag = 0;
			}
		}
	}

	//
	// range fft
	//
	CFFT.run(nfft,intmp);

}

//---------------------------------------------------------------------------
srsl::fp C_detect_distance::GetDopplerFreq(int iIndex, int nfft, srsl::fp dFs)
{
	srsl::fp dFdelta;
	srsl::fp dFd;

	dFdelta = dFs / (srsl::fp)(nfft);

	if(iIndex > (nfft >> 1) ) {
		dFd = dFdelta * (srsl::fp)(nfft - iIndex);
		dFd *= -1;
	}
	else {
		dFd = dFdelta * (srsl::fp)(iIndex);
	}

	return dFd;

}

//---------------------------------------------------------------------------
int C_detect_distance::GetDist2Index(srsl::fp dist)
{
	srsl::fp index;

	index = dist;

	index = index / 100;

	index += 0.023; // 2.3 cm is the processing delay inside Device

	index = index / 2 / gR;

	// up
	if( index<=0 ) {
		index = nfft-1;
	}
	else if( index<(nfft/2) ) {
		index = nfft-index;
	}
	else {
		index = nfft/2;
	}

	int res = (int)index;
	if( res>=nfft ) {
		res = nfft-1;
	}


	return res;

}

//---------------------------------------------------------------------------
srsl::fp C_detect_distance::GetIndex2Dist(int index)
{
	int index_2 = (index<(nfft/2))? index : nfft - index;
	srsl::fp dLL = (srsl::fp) index_2 * 2 * gR;

	dLL -= 0.023; // 2.3 cm is the processing delay inside Device
	if( dLL<0 )
		dLL = 0;

	dLL *= 100;

	return dLL;

}

//---------------------------------------------------------------------------
srsl::fp C_detect_distance::GetR(void) const
{
	return gR;
}
