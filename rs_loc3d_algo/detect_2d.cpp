
/*
 * Copyright (c) 2016-2021 Socionext Inc.
 * All rights reserved.
 */


#ifdef WIN32
#include <windows.h>
#endif
#include <cstdint>
#include <string>
#include <cstring>
#include <chrono>
#include <memory>
#include <cmath>
#include <srsl_math.h>
#include "detect_2d.h"
#include "AngE_CmpTbl.h"
#include "AngH_CmpTbl.h"
#include "sp_common.h"

#define FFT_H(i)    (i - nfft/2) 

#if TARGET_MCU==0
#define dprintf printf
#endif

//------------------------------------------------------------------------------
C_2D_detect::C_2D_detect()
{
}

//------------------------------------------------------------------------------
void C_2D_detect::angle_axis_init(void)
{
	int i;

	for(i=0;i<=180;i++) {
		angle_axis_c[i] = i-90;
	}

}
//------------------------------------------------------------------------------
srsl::fp C_2D_detect::get_cfar(int cfar_mode, int gd, int m, srsl::fp* fdata, int index, int datasize)
{
	srsl::fp res;
	int st[2];
	int ix;
	int ave_n;
	st[0] = index - gd - m;
	st[1] = index + gd;
	srsl::fp sigma[2] = {0,0};
	
	int size_min = datasize/2;
	int size_max = datasize-1;
	
	for(int i=0;i<2;i++) {
		ave_n=0;
		ix = st[i];
		for(int s=0;s<m;s++) {
			if(ix<size_min) ix = size_min;
			if(ix>size_max) ix = size_max;
			if(fdata[ix]==0) {
				//printf("ix(%d) zero\n", ix);
				sigma[i] = 0;
				break;
			}
			sigma[i] += fdata[ix];
			ix++;
			ave_n++;
		}
		if( ave_n>0 ) {
			sigma[i] = sigma[i]/ave_n;
		}
	}
	
	
	res = sigma[0];
	if( cfar_mode==1 ) {
		// caso-cfar
		if( sigma[1]<sigma[0] && sigma[1]>0 ) {
			res = sigma[1];
		}
	}
	else if( cfar_mode==2 ) {
		// cago-cfar
		if( sigma[1]>sigma[0] && sigma[1]>0 ) {
			res = sigma[1];
		}
	}
	else if( cfar_mode==3 ) {
		// caso-cfar
		res = (sigma[1]+sigma[0])/2;
		if(sigma[0]==0) {
			res = sigma[1];
		}
		else if(sigma[1]==0) {
			res = sigma[0];
		}
	}
	
	res = DB_V * SP_LOG10(res);
	
	return res;
}
//------------------------------------------------------------------------------
srsl::fp C_2D_detect::GetPeakIndexFmcw(int pkdetmode,
                                       int pksortmode,
                                       srsl::fp* fdata,
                                       srsl::fp* cfar_th,
                                       int dataSize,
                                       int *iIndexNum,
                                       int *iIndexs,
                                       int iIndexMaxNum,
                                       int dThMode,
                                       srsl::fp _dThreshold,
                                       srsl::fp _dThreshold_abs,
                                       int iStIndex,
                                       int iEdIndex,
                                       int *ShortIndex,
                                       int *MaxpowIndex,
                                       int *MaxpowIndex_en,
                                       srsl::fp *DistWidth,
                                       srsl::fp *PowDegree,
                                       int *PkIndexNum)
{
	int kn, kp, ko;
	srsl::fp an, ap, ao;
	srsl::vector<srsl::fp> index_fdata(iIndexMaxNum);
	index_fdata.zeros();
	srsl::fp fdata_average = 0;
	srsl::fp max_pow_log10 = 0;

	int st_index = iStIndex;
	int ed_index = iEdIndex;

	int _pksortmode = pksortmode;		// 0:sort in power, 1:sort in distance

	int index_range;
	if( pkdetmode>=1 ) {
		_pksortmode = 1;
		index_range = GetDist2IndexNum(param->app->PkDetDist);
	}
	else {
		*ShortIndex = -1;
		*MaxpowIndex = -1;
	}
	
	if( dThMode&0xf0 ) {
		int N = (dThMode&0xf0)>>4;
		for( int k=(st_index); (ed_index)<k; k-- ) {
			srsl::fp ave = 0;
			srsl::fp ave_n = 0;
			for( int n=-N;n<N;n++) {
				int ix = k-n;
				if(ix>=dataSize)continue;
				if(ix<=(dataSize/2))continue;
				ave += fdata[ix];
				ave_n++;
			}
			if(ave_n>0) ave = (srsl::fp) ave / ave_n;
			fdata[k] = ave;
		}
	}

	
	srsl::fp dThreshold;
	srsl::fp dThreshold_ave = 0;
	int cfar_mode = 0;
	int cfar_mode_p = 0;
	int cfar_gd = 0;
	int cfar_m = 0;
	dThreshold = _dThreshold;
	if( (dThMode&0xf)==1 ) {
		int st = dataSize-1;
		int ed = dataSize/2;
		srsl::fp ave = 0;
		srsl::fp ave_n = 0;
		for(int i=st;i>ed;i--) {
			srsl::fp fdata_log = DB_V * SP_LOG10(fdata[i]);
			ave += fdata_log;
			ave_n++;
			if(max_pow_log10==0||max_pow_log10<fdata_log) {
				max_pow_log10 = fdata_log;
			}
		}
		ave = (srsl::fp) ave / ave_n;
		dThreshold = _dThreshold + ave;
		dThreshold_ave = dThreshold;
		fdata_average = ave;
	}
	else if( (dThMode&0xf)==2 ) {
		fdata_average = CalcNoiseLevel_of_fdata(dataSize, fdata);
		dThreshold = _dThreshold + fdata_average;
		if(max_pow_log10==0||max_pow_log10<fdata_average) {
			max_pow_log10 = fdata_average;
		}
	}
	
	cfar_mode = param->app->cfar&0x7;
	cfar_mode_p = (param->app->cfar>>3)&0x1;
	
	if( dThreshold<_dThreshold_abs ) {
		dThreshold = _dThreshold_abs;
	}
	
	srsl::fp dth_topdiff = max_pow_log10 - param->app->fMagTh_TopDiff;
	if(param->app->fMagTh_TopDiff>0) {
		if( dThreshold<dth_topdiff ) {
			dThreshold = dth_topdiff;
		}
	}

	srsl::fp cfar_th_p = 0;
	if( cfar_mode ) {
		cfar_gd = (param->app->cfar&0xf0) >>4;
		cfar_m  = (param->app->cfar&0xf00)>>8;
		if(cfar_mode_p) {
			cfar_th_p = max_pow_log10 - _dThreshold;
			if(cfar_th_p<dThreshold_ave) {
				cfar_th_p = dThreshold_ave;
			}
			if(cfar_th_p<(_dThreshold_abs+_dThreshold)) {
				cfar_th_p = _dThreshold_abs+_dThreshold;
			}
		}
	}

	*iIndexNum = 0;
	
	for( int k=(st_index); (ed_index)<k; k-- ) {
		//
		// find convex of spectrum
		//

		kn = k + 1; if(kn < 0) {kn += dataSize;} if(kn >= dataSize) {kn -= dataSize;}	// pre index
		ko = k; if(ko < 0) {ko += dataSize;} if(ko >= dataSize) {ko -= dataSize;}		// currnet index
		kp = k - 1; if(kp < 0) {kp += dataSize;} if(kp >= dataSize) {kp -= dataSize;}	// post index

		an = fdata[kn];
		ao = fdata[ko];
		ap = fdata[kp];

		if( cfar_mode ) {
			dThreshold = get_cfar(cfar_mode, cfar_gd, cfar_m, fdata, ko, dataSize);
			
			if( dThreshold>0 ) {
				dThreshold += _dThreshold;
				if( dThreshold<_dThreshold_abs ) {
					dThreshold = _dThreshold_abs;
				}
			}
			if(cfar_mode_p) {
				if(dThreshold>cfar_th_p) {
					dThreshold = cfar_th_p;
				}
			}

			if(param->app->fMagTh_TopDiff>0) {
				if( dThreshold<dth_topdiff ) {
					dThreshold = dth_topdiff;
				}
			}

			
			cfar_th[ko] = dThreshold;
		}
		
		if( (an < ao) && (ap < ao) ) {
			if( (ao > 0) && (ko != 0) ) {
				srsl::fp fdata_log10 = 0;
				if( fdata[ko]>0 ) fdata_log10 = DB_V * SP_LOG10(fdata[ko]);
				
				if( fdata_log10 > dThreshold ) {
					if( (iIndexNum!=NULL) && (iIndexs!=NULL) ) {
						if( *iIndexNum < iIndexMaxNum ) {
							// save index, data
#if 0
							iIndexs[*iIndexNum] = ko;
							index_fdata(*iIndexNum) = fdata_log10;
							*iIndexNum += 1;
#else
							// Save the index before and after the convex index to calculate the angle
							for(int i=-param->app->PkDetAddBin;i<=param->app->PkDetAddBin;i++) {
								int _ko = ko+i;
								if(_ko>st_index || _ko<ed_index) continue;
								if( *iIndexNum >= iIndexMaxNum ) continue;
								if( fdata[_ko]>0 ) fdata_log10 = DB_V * SP_LOG10(fdata[_ko]);
								iIndexs[*iIndexNum] = _ko;
								index_fdata(*iIndexNum) = fdata_log10;
								*iIndexNum += 1;
							}
#endif
						}
						else if(_pksortmode==0) {
							// full filled, so replace min data with now data
							srsl::fp min_fdata = fdata_log10;
							int ino = -1;
							for(int i=0;i<iIndexMaxNum;i++) {
								// search min data
								if( index_fdata(i) < min_fdata ) {
									ino = i;
									min_fdata = index_fdata(i);
								}
							}
							if( ino!=-1 ) {
								// replace data
								iIndexs[ino] = ko;
								index_fdata(ino) = fdata_log10;
							}
						}
					}
				}
			}
		}
	}

	*ShortIndex = iIndexs[0];
	*MaxpowIndex = iIndexs[0];
	*MaxpowIndex_en = *iIndexNum;

	if(_pksortmode==0) {
		// Sort by power
		int p, q;
		for(p = 0; p < *iIndexNum-1; p++){
			for(q = p + 1; q < *iIndexNum; q++){
				if(index_fdata(p) < index_fdata(q)) {

					srsl::fp tmp1 = index_fdata(p);
					index_fdata(p) = index_fdata(q);
					index_fdata(q) = tmp1;

					int tmp2 = iIndexs[p];
					iIndexs[p] = iIndexs[q];
					iIndexs[q] = tmp2;

				}
			}
		}
		*MaxpowIndex = iIndexs[0];
		//max_pow_log10 = index_fdata(0);
	}
	else {
		
		// Sort by distance
		int p, q;
		for(p = 0; p < *iIndexNum-1; p++){
			for(q = p + 1; q < *iIndexNum; q++){
				if(iIndexs[p] < iIndexs[q]) {

					srsl::fp tmp1 = index_fdata(p);
					index_fdata(p) = index_fdata(q);
					index_fdata(q) = tmp1;

					int tmp2 = iIndexs[p];
					iIndexs[p] = iIndexs[q];
					iIndexs[q] = tmp2;

				}
			}
		}
		
		// search max data
		//max_pow_log10 = 0;
		if( *iIndexNum>0 ) {
			srsl::fp max_fdata = index_fdata(0);
			for(int i = 1; i < *iIndexNum; i++){
				if( max_fdata < index_fdata(i) ) {
					max_fdata = index_fdata(i);
					*MaxpowIndex = iIndexs[i];
				}
			}
			//max_pow_log10 = max_fdata;
		}
	}

	{
		srsl::vector<bool> index_en(*iIndexNum+1);
		index_en.zeros();
		for(int i=0;i<*iIndexNum;i++) {
			index_en(i) = 1;
		}

		if( pkdetmode>=1 ) {
			// caluclate valid max-min index range
			int min_index = iIndexs[0];
			int max_index = min_index - index_range;
			int max_index_valid = 0;
			int en_n = 0;
			for(int i=*iIndexNum-1;i>=0;i--) {
				index_en(i) = 0;
				if(pkdetmode==1) {
					if( iIndexs[i]>=max_index ) {
						index_en(i) = 1;
						en_n++;
						max_index_valid = iIndexs[i];
					}
				}
				else {
					if( i==0 && en_n==0 ) {
						index_en(i) = 1;
						en_n++;
						max_index_valid = iIndexs[i];
					}
					else if( iIndexs[i]>=max_index ) {
						if( en_n==0 ) {
							index_en(i) = 1;
							en_n++;
							max_index_valid = iIndexs[i];
						}
					}
				}
			}
			if( *MaxpowIndex<max_index_valid ) {
				*MaxpowIndex = max_index_valid;
			}

			if( pkdetmode==2 ) {
				*PowDegree = 0;
				*DistWidth = 0;
				*PkIndexNum = *iIndexNum;
				int max_index_over_th = max_index_valid;

				if( *PkIndexNum>1 ) {
					srsl::fp ave = 0;
					int ave_n = 0;
					// calculate peak convex
					for( int k=(min_index); (max_index)<k; k-- ) {
						srsl::fp tmp_fdata = DB_V * SP_LOG10(fdata[k]);
						if( tmp_fdata>dThreshold ) {
							max_index_over_th = k;
						}
						ave += tmp_fdata;
						ave_n++;
					}
					if( ave_n ) {
						ave = (srsl::fp) ave / ave_n;
					}

					srsl::fp max_dist = det_r[0].GetIndex2Dist(max_index_over_th);
					srsl::fp min_dist = det_r[0].GetIndex2Dist(min_index);
					*DistWidth = max_dist-min_dist;
					*PowDegree = ave-fdata_average;
					//printf("iIndexNum = %d, dist_diff = %.2f, pow_diff = %.2f\n",*PkIndexNum,*DistWidth,*PowDegree);
					//printf("replace iIndexs[0] %d --> max_index_over_th %d\n" ,iIndexs[0], max_index_over_th);
					iIndexs[0] = max_index_over_th;
					index_en(0) = 1;
					*iIndexNum = 1;
				}

			}

		}
		else {
			for(int i=0;i<*iIndexNum;i++) {
				for(int j=0;j<i;j++) {
					int idiff = abs(iIndexs[i]-iIndexs[j]);
					if( idiff<min_idxnum ) {
						// Remove adjacent peaks
						index_en(i) = 0;
					}
					if( idiff>max_idxnum && max_idxnum>0 ) {
						// Remove adjacent peaks
						index_en(i) = 0;
					}
				}
			}
		}
		
		if(pkdetmode==0) {

			srsl::fp dth_topdiff = max_pow_log10 - param->app->fMagTh_TopDiff;
			for(int i=0;i<*iIndexNum;i++) {
				if( index_fdata(i)<dth_topdiff ) {
					// Remove data below threshold from maximum value
					index_en(i) = 0;
				}
			}
			if(dThreshold<dth_topdiff) {
				dThreshold = dth_topdiff;
			}
		}

		// remove invalid index data
		for(int i=0;i<*iIndexNum;) {

			if( index_en(i)==0 ) {
				for(int j=i;j<*iIndexNum-1;j++) {
					iIndexs[j] = iIndexs[j+1];
					index_en(j) = index_en(j+1);
				}
				*iIndexNum = *iIndexNum - 1;
			}
			else {
				i++;
			}
		}


	}
	
	return dThreshold;
}

//------------------------------------------------------------------------------
#if TARGET_MCU==0
int C_2D_detect::sp_run(srsl::vector< srsl::complex<int> > in[MAX_RCH_N],
#else
int C_2D_detect::sp_run(srsl::complex<short> in[MAX_RCH_N][MAX_IQIN_SIZE],
#endif
                        int objnum,
                        int *rxchmap,
                        int snum,
                        int fft_en,
                        int txch
                        )
{
	//
	// Outline processing step
	//
	// 1. Range FFT per RxCh
	// 2. Remove static object from range spectrum(back-ground substruction)
	// 3. Find peak from Rage fft spectrum
	// 4. Angle estimation with Capon per E/H plane
	//

	if( txch<=0 ) return -1;
	
	int ant_n = 2;
	int nrx = 0;
	for(int i=0;i<MAX_RCH_N;i++) {
		if( rxchmap[nrx]!=-1 ) {
			nrx++;
		}
	}
	int mimo_en = param->mimo_en;
	int ang_skip = 0;
	if( mimo_en ) {
		if( txch==2 ) {
			ang_skip = 1;
			txch2_det = 1;
		}
	}


	{
		int multi_frm = param->app->bg_elim_interval;
		int max_frm_n = MAX_FRM_N;
		if(mimo_en==0) {
			if(multi_frm>max_frm_n) multi_frm = max_frm_n;
		}
		else {
			max_frm_n /= 2;
			max_frm_n *= 2;
			if((multi_frm*2)>max_frm_n) multi_frm = max_frm_n/2;
		}

		{
			
			srsl::fp alpha_r = pre_alpha_r;

			if( param->app->adp_alpha_r_en ) {
				srsl::fp max_alpha = param->app->adp_alpha_r_max;//0.9;//0.95;
				srsl::fp min_alpha = param->app->adp_alpha_r_min;//0.2;
				srsl::fp pk_r_spc = 0;
				for(int j=nfft-1;j>=nfft/2;j--) {
					if( pk_r_spc < a_R_spc_tmp[j] ) {
						pk_r_spc = a_R_spc_tmp[j];
					}
				}
				pk_r_spc = DB_V * SP_LOG10(pk_r_spc);
				srsl::fp spc_diff = pk_r_spc - param->app->adp_alpha_r_th;
				srsl::fp val = param->app->adp_alpha_r_step;
				if(spc_diff<0) {
					alpha_r += val;
					if( alpha_r > max_alpha ) {
						alpha_r = max_alpha;
					}
				}
				else {
					alpha_r -= val;
					if( alpha_r < min_alpha ) {
						alpha_r = min_alpha;
					}
				}
			}
			pre_alpha_r = alpha_r;
			//printf("alpha %.2f\n", alpha_r);

			int add_frm_n = 0;
			for(int ii=0;ii<nrx;ii++) {
				int ch = rxchmap[ii];
				if( ch<MAX_RCH_N ) {
					if( fft_en == 0 ) {
#if TARGET_MCU==0
						det_r[0].FFT_run(snum,&in[ch].get()[0]);
#else
						det_r[0].FFT_run(snum,&in[ch][0]);
#endif
					}
					else {
						int s;
						int size = nfft/2;
						if( size>snum ) {
							size = snum;
						}
#if TARGET_MCU==0
						for(s=0;s<size;s++) {
							det_r[0].CFFT.out[nfft-1-s] = in[ch](s);
						}
						for(;s<nfft/2;s++) {
							det_r[0].CFFT.out[nfft-1-s] = in[ch](s);
						}
#else
						for(s=0;s<size;s++) {
							det_r[0].CFFT.out[nfft-1-s] = in[ch][s];
						}
						for(;s<nfft/2;s++) {
							det_r[0].CFFT.out[nfft-1-s] = in[ch][s];
						}
#endif
					}
					for(int i=nfft-1;i>=nfft/2;i--) {
						//
						// back-graound substruction
						//
						srsl::complexfp dReIm(0, 0);

						int inc = 1;
						if( mimo_en ) {
							inc = 2;
						}
						
						if(frm_counter_i==0) {
							R_out_pre[frm][ch][FFT_H(i)] = det_r[0].CFFT.out[i];
						}
						
						add_frm_n = 0;
						for(int f=0;f<multi_frm;f+=inc) {

							if( f>=frm_counter_i ) {
								// end of valid frame
								break;
							}

							int tfrm = frm-f-inc;
							if( tfrm<0 ) {
								tfrm = max_frm_n + tfrm;
							}
							dReIm += det_r[0].CFFT.out[i] - R_out_pre[tfrm][ch][FFT_H(i)];
							add_frm_n++;
						}
						if(add_frm_n==0) {
							add_frm_n = 1;
						}
						BGNR_data[txch-1][ch][FFT_H(i)].real = (rs_float)dReIm.real;
						BGNR_data[txch-1][ch][FFT_H(i)].imag = (rs_float)dReIm.imag;
						
						srsl::fp alpha_fbgnr = param->app->alpha_r2;
						if( param->app->bg_elim_interval>0 ) {
							//
							// Save current Range spectrum
							//
							if(R_out_pre_valid[frm]) {
								R_out_pre[frm][ch][FFT_H(i)] = R_out_pre[frm][ch][FFT_H(i)] * alpha_fbgnr + det_r[0].CFFT.out[i] * (1-alpha_fbgnr);
							}
							else {
								R_out_pre[frm][ch][FFT_H(i)] = det_r[0].CFFT.out[i];
							}
						}
						else {
							//
							// clear Range spectrum storage
							//
							R_out_pre[frm][ch][FFT_H(i)].real = 0;
							R_out_pre[frm][ch][FFT_H(i)].imag = 0;
						}
						
					}
				}
			}
			R_out_pre_valid[frm] = 1;
		
			if( ang_skip ) {
				frm++;
				if( frm>=max_frm_n ) {
					frm = 0;
				}
				return 0;
			}

			for(int i=nfft-1;i>=nfft/2;i--) {
				srsl::fp means_row = 0;
				for(int ii=0;ii<nrx;ii++) {
					int ch = rxchmap[ii];
					//
					// back-graound substruction
					//
					srsl::complexfp dReIm(0, 0);

					dReIm.real = BGNR_data[txch-1][ch][FFT_H(i)].real;
					dReIm.imag = BGNR_data[txch-1][ch][FFT_H(i)].imag;
					if(add_frm_n>0) {
						BGNR_data[txch-1][ch][FFT_H(i)].real = (rs_float)BGNR_data[txch-1][ch][FFT_H(i)].real/add_frm_n;
						BGNR_data[txch-1][ch][FFT_H(i)].imag = (rs_float)BGNR_data[txch-1][ch][FFT_H(i)].imag/add_frm_n;
					}

					//
					// smoothing processing
					//
					srsl::fp fmag = sqabs(dReIm);


					if( fmag ) {
						srsl::fp sq_d = 0;
						if( fmag>0 ) sq_d = srsl::sqrt(fmag);
						means_row += sq_d;
					}
				}
				means_row = (srsl::fp) means_row / nrx;
				a_R_spc[i] = a_R_spc[i] * alpha_r + means_row * (1-alpha_r);
			}
			
			{
				//
				// test code
				//
				int spc_ave = 0;
				int spc_min = 0;
				int spc_max = 0;
				int ave_n = 0;
				int i=0;
				for(int j=nfft-1;j>=nfft/2;j--) {
					a_R_spc_tmp[j] = a_R_spc[j];
					if (param->app->RWeiLutEn) {
						a_R_spc_tmp[j] *= r_wei_table[i];
					}
					i++;
					a_R_spc_tmp[j] *= r_wei[j];
					ave_n++;
					
					spc_ave += a_R_spc_tmp[j];
					if(ave_n==1) {
						spc_max = a_R_spc_tmp[j];
						spc_min = a_R_spc_tmp[j];
					}
					else {
						if(spc_max < a_R_spc_tmp[j])
							spc_max = a_R_spc_tmp[j];
						if(spc_min > a_R_spc_tmp[j])
							spc_min = a_R_spc_tmp[j];
					}
				}
				spc_ave /= ave_n;
				spc_ave = DB_V * SP_LOG10(spc_ave);
				spc_max = DB_V * SP_LOG10(spc_max);
				spc_min = DB_V * SP_LOG10(spc_min);
				//printf("#frm%d:spc ave/min/max %d %d %d\n", frm_counter_i, spc_ave, spc_min, spc_max);
			}

			int srch_peak_num = PEAK_N_R;
			
			//
			// Range spectrum peak search
			//
			r_pk_mindist_index = 0;
			r_pk_maxpow_index = 0;
			r_pk_maxpow_index_en = 0;
			R_spc_threshold = GetPeakIndexFmcw(
			                 param->app->PkDetMode_r,
			                 param->app->PkSortMode_r,
			                 a_R_spc_tmp,
			                 cfar_th,
			                 nfft,
			                 &r_pk_num,
			                 r_pk_indexs,
			                 srch_peak_num,
			                 param->app->fMagMode,
			                 param->app->fMagTh_r,
			                 param->app->fMagTh_r2,
			                 GetStIndex(),
			                 GetEdIndex(),
			                 &r_pk_mindist_index,
			                 &r_pk_maxpow_index,
			                 &r_pk_maxpow_index_en,
			                 &r_pk_dist_width,
			                 &r_pk_pow_degree,
			                 &r_pk_num_detmode2);

			if( frm_counter_i==0 ) {
				// Peak detection result is invalid if there is no past frame
				r_pk_num = 0;
			}

			for(int i=0;i<xyz_data_hold_req.num;i++) {
				if(xyz_data_hold_req.pos[i].life) {
					int add_req=1;
					xyz_data_hold_req.pos[i].rep_r_index = -1;
					for(int ii=0;ii<r_pk_num;ii++) {
						int diff = r_pk_indexs[ii]-xyz_data_hold_req.pos[i].r_index;
						if(abs(diff)<=5) {
							xyz_data_hold_req.pos[i].rep_r_index = r_pk_indexs[ii];
							xyz_data_hold_req.pos[i].r_index_dummy_en = 0;
							add_req=0;
							//printf("rep range pk\n");
							break;
						}
					}
					if(add_req) {
						xyz_data_hold_req.pos[i].rep_r_index = xyz_data_hold_req.pos[i].r_index;
						r_pk_indexs[r_pk_num] = xyz_data_hold_req.pos[i].r_index;
						xyz_data_hold_req.pos[i].r_index_dummy_en = 1;
						r_pk_num++;
						//printf("add range pk life %d index %d\n", xyz_data_hold_req.pos[i].life, xyz_data_hold_req.pos[i].r_index);
					}
				}
			}
			
			
			r_pk_num_for_ang = r_pk_num;

			int PhaseNum;
			int PairNum;
			PhaseNum = D3D;
			PairNum = 2;

			// init
			for(int eh=0;eh<PhaseNum;eh++) {
				for(int i = 0; i < nang; i++) {
//!!					a_A_spc[eh][i] = 0;
				}
			}
			//
			// Angle estimation
			//
			pki2[ED].num = pki2[HD].num = 0;

//			int angle_calc_done[MAX_SIZE_R];
//			memset(angle_calc_done, 0, sizeof(angle_calc_done));

			int pkcount = 0;
			int InfiniteConcern = 0; // Flag of concern for INFINITE errors at Capon function
			int ang_r_n[D3D] = {0, 0};

			int angle_loop = r_pk_num;
			if( param->app->HeatMapOp==3 ) {
				angle_loop = 0;
			}

			if( angle_loop>(nfft/2) ) {
				angle_loop = (nfft/2);
			}

			for(int ln=0;ln<angle_loop;ln++) {
				int r_index = r_pk_indexs[ ln ];
				{

					if(ln>=r_pk_num) {
						r_index = nfft-ln-1;
						for(int p=0;p<r_pk_num;p++) {
							if( r_index==r_pk_indexs[p] ) {
								r_index = nfft-p-1;
								break;
							}
						}
					}

					int r_pk_diff = INT_MAX;
					for(int r=0;r<r_pk_num;r++) {
						int diff = r_index - r_pk_indexs[r];
						if(diff<0) diff = -diff;
						if(r_pk_diff>diff) {
							r_pk_diff = diff;
						}
					}


					distance[ln] = det_r[0].GetIndex2Dist(r_index);
					if( distance[ln]<=0 ) {
						// 0 cm or less is invalid
						continue;
					}
					if( distance[ln]>MaxDist ) {
						// Over the maximum distance is invalid
						continue;
					}

#if RS_LOC3D_ALGO_USE_BMPMAP == 1
					if( a_A_spc_all ) {
						{ //if( param->app->HeatMapCalMode==0 ) {
							if( r_pk_diff>a_valid_r_index ) {
								for(int j = 0; j < nang; j++) {
									A_spc[ED][j] = a_A_spc_all[ED][r_index][j] * param->app->alpha_a_e;
									a_A_spc_all[ED][r_index][j] = A_spc[ED][j];

									A_spc[HD][j] = a_A_spc_all[HD][r_index][j] * param->app->alpha_a_h;
									a_A_spc_all[HD][r_index][j] = A_spc[HD][j];
									
								}
								continue;
							}
						}
						#if 0
						else if( ln>=r_pk_num ) {
							int deci = param->app->HeatMapCalMode;
							int amari = r_index%deci;
							if(amari) {
								int r_index_pre = deci-amari+r_index; // Closest calculated index
								if( r_index_pre<nfft ) {
									if( angle_calc_done[r_index_pre]==1 ) { // Check if it has been calculated
										for(int j = 0; j < nang; j++) {
											a_A_spc_all[ED][r_index][j] = a_A_spc_all[ED][r_index_pre][j];
											a_A_spc_all[HD][r_index][j] = a_A_spc_all[HD][r_index_pre][j];
										}
										continue;
									}
								}
							}
						}
						#endif
					}
#endif
					pkcount++;
				}

//				angle_calc_done[r_index] = 1;

				Epair = param->app->Epair;
				Hpair = param->app->Hpair;

				for(int eh=0;eh<PhaseNum;eh++) { // Loop of E and H planes (0 = E plane, 1 = H plane)

					if( eh==ED && Epair==0 ) continue;
					if( eh==HD && Hpair==0 ) continue;

					{
						int Ng = param->app->ang_Ng;// 8
						int min_ang = 0;//20;
						int max_ang = 180;//160;

						int ch1 = -1;
						int ch2 = -1;
						int ang_calc_en = 1;
						if( eh==ED ) {
							if( Epair&1 )      { ch1 = 0; ch2 = 3; }
							else if( Epair&2 ) { ch1 = 1; ch2 = 2; }
							else ang_calc_en = 0;
						}
						else {
							if( Hpair&1 )      { ch1 = 1; ch2 = 0; }
							else if( Hpair&2 ) { ch1 = 2; ch2 = 3; }
							else ang_calc_en = 0;
						}
						
						if( ang_calc_en ) {
							
							// Prepare input data of Capon
							int pks_val_st = r_index - Ng;
							int pks_val_ed = r_index + Ng;
							if (pks_val_st < 0) pks_val_st = 0;
							if (pks_val_ed >= nfft) pks_val_ed = nfft - 1;
							int numRpks = pks_val_ed - pks_val_st + 1;
							
							ant_n = 2;
							srsl::fp tx2_scale_fct = param->app->tx2_scale_fct;
							if( mimo_en && eh==ED && txch2_det ) {
								ant_n = 4;
							}
							srsl::vector<srsl::complexfp> pks(ant_n * numRpks);
							pks.zeros();
							
							for (int an = 0; an < ant_n; an++)
							{
								int rch;
								int tch;
								if( !(an&1) ) rch = ch1;
								if(  (an&1) ) rch = ch2;
								if( !(an&2) ) tch = 0;
								if(  (an&2) ) tch = 1;
								
								srsl::fp ang_rot = 0;
								if(an&1) {
									ang_rot = (eh==ED)? param->app->AngInPhaseRotE : param->app->AngInPhaseRotH;
								}
								if( eh==ED && mimo_en && tch==1) {
									ang_rot += 180;
								}
								srsl::fp rad ;
								srsl::fp cosp;
								srsl::fp sinp;
								srsl::fp sinm;
								
								if( ang_rot!=0 ) {
									rad = (srsl::fp) ang_rot*(PI/180);
									cosp = srsl::cos(rad);
									sinp = srsl::sin(rad);
									sinm = - srsl::sin(rad);
								}

								int ann = an * numRpks;
								for (int pn = pks_val_st; pn <= pks_val_ed; pn++)
								{

									srsl::complexfp dReIm = BGNR_data[tch][rch][FFT_H(pn)];
									dReIm /= 2.0f;
									if(tch==1 && tx2_scale_fct>0) {
										srsl::fp ang = (srsl::fp) 90*(PI/180);
										if(std::abs(dReIm.real)>0.0001) {
											ang = srsl::atan(dReIm.imag/dReIm.real);
										}
										srsl::fp mag = srsl::sqabs(dReIm);
										if(mag>0) mag = srsl::sqrt(mag);
										mag *= tx2_scale_fct;
										srsl::fp real = srsl::cos(ang)*mag;
										srsl::fp imag = srsl::sin(ang)*mag;
										if((dReIm.real*real)<0) {
											real = -real;
										}
										if((dReIm.imag*imag)<0) {
											imag = -imag;
										}
										
										dReIm.real = real;
										dReIm.imag = imag;
									}
									
									//if( ang_rot!=0 && tch==1 ) {
									if( ang_rot!=0 ) {
										// Phase rotation processing
										srsl::fp rot_real = dReIm.real*cosp + dReIm.imag*sinm;
										srsl::fp rot_imag = dReIm.real*sinp + dReIm.imag*cosp;
										dReIm.real = rot_real;
										dReIm.imag = rot_imag;
									}
									
									pks(ann).real = dReIm.real;
									pks(ann).imag = dReIm.imag;
									ann++;
								}
							}

							srsl::fp       alpha_a = (eh==ED)? param->app->alpha_a_e : param->app->alpha_a_h;
							int r_index_pair = r_index;
								
#if SMALL_A_RSS==0
							if( alpha_a!=0 ) {
								// Close distance index and pairing for smoothing angle
								int min_diff = nfft/2;
								for(int rn=0;rn<pre_r_pk_num;rn++) {
									int index_diff = abs( pre_r_pk_indexs[rn] - r_index );
									if(min_diff>index_diff) {
										min_diff = index_diff;
										r_index_pair = pre_r_pk_indexs[rn];
									}
								}
								
								// Weaken alpha by difference with paired index
								alpha_a *= DETECT_2D_POW(0.95, min_diff);
								
							}
#else
							short smt_id = -1;
							short min_bin=-1;
							bool  smt_en=0;
							short old_smt_id=0;
							short max_fdiff=0;
							for(int i=0;i<MAX_A_RSS_N;i++) {
								if( a_Rss_inf[eh][i].en ) {
									int fdiff = (frm_counter_i&0xff) - a_Rss_inf[eh][i].frm;
									if(fdiff<0) fdiff = 256 + fdiff;
									if(max_fdiff<fdiff) {
										max_fdiff = fdiff;
										old_smt_id = i;
									}
									//if( fdiff<=param->app->a_rss_life ) {
									if( fdiff<=10 ) {
										short diff = a_Rss_inf[eh][i].bin - r_index;
										if(diff<0) diff = -diff;
										//if( diff<=param->app->a_rss_bin ) {
										if( diff<=3 ) {
											if( min_bin>diff ||min_bin<0 ) {
												min_bin = diff;
												smt_id = i;
												smt_en = 1;
											}
										}
									}
									else {
										a_Rss_inf[eh][i].en = 0;
										if(smt_id<0) smt_id = i;
									}
								}
								else {
									if(smt_id<0) smt_id = i;
								}
							}
							if(smt_id<0) {
								smt_id = old_smt_id;
							}
							if(eh==ED) {
								dprintf("r_index %d en %d smt_id %d min_bin %d\n", r_index, smt_en, smt_id, min_bin);
							}
							if(smt_en) {
								for(int i=0;i<MAX_RCH_N*MAX_RCH_N;i++) {
									sa_Rss[i] = a_Rss_inf[eh][smt_id].Rss[i];
								}
								// Weaken alpha by difference with paired index
								alpha_a *= DETECT_2D_POW(0.95, min_bin);
								//dprintf("# alpha_a %f\n", alpha_a);
							}
#endif

							Capon[eh].apn = apn; // for debug
							Capon[eh].eh = eh;   // for debug
							
							Capon[eh].Run(
							              min_ang, max_ang,
							              Ng, nfft, ant_n,
							              nang,
							              alpha_a,
							              FFT_H(r_index),
							              FFT_H(r_index_pair),

							              pks,
							              numRpks,

#if SMALL_A_RSS==0
							              a_Rss[eh],
#else
							              NULL,
#endif

							              sa_Rss,

							              A_spc[eh]);
							for(int aw=0;aw<MAX_SIZE_A;aw++) {
								A_spc[eh][aw] *= a_wei[aw];
							}
#if SMALL_A_RSS==1
							if(smt_id>=0) {
								for(int i=0;i<MAX_RCH_N*MAX_RCH_N;i++) {
									a_Rss_inf[eh][smt_id].Rss[i] = sa_Rss[i];
								}
								a_Rss_inf[eh][smt_id].en = 1;
								a_Rss_inf[eh][smt_id].bin = r_index;
								a_Rss_inf[eh][smt_id].frm = frm_counter_i&0xff;
							}
							else {
								dprintf("ERROR smt_id is -1, r_index %d\n", r_index);
							}
#endif
						}


#if RS_LOC3D_ALGO_USE_BMPMAP == 1
						if( a_A_spc_all ) {
							for(int j = 0; j < nang; j++) {
								a_A_spc_all[eh][r_index][j] = A_spc[eh][j];
							}
						}
#endif
					} // for(int pair=0;pair<2;pair++) {

#if 1 //RS_LOC3D_ALGO_USE_BMPMAP == 0
					{	// peak detection of angle spectrum

						srsl::fp A_spc_log[MAX_SIZE_A];
						for(int j = 0; j < nang; j++) {
							A_spc_log[j] = DB_V * SP_LOG10(A_spc[eh][j]);
							a_A_spc[eh][ln][j] = A_spc[eh][j];
						}
						int a_pk_list[MAX_RCH_N];
						int min_ang = (eh==ED)? param->app->ang_min_e : param->app->ang_min_h;
						int max_ang = (eh==ED)? param->app->ang_max_e : param->app->ang_max_h;
						int a_pk_n = AspcCovexIndexSearch(eh, A_spc_log, min_ang, max_ang, 0, ant_n-1, a_pk_list);
						int a_pk_n_org = a_pk_n;
						int r_index_dummy_en = 0;
						
						if(eh==ED) {
							for(int i=0;i<xyz_data_hold_req.num;i++) {
								if(xyz_data_hold_req.pos[i].life) {
									if(r_index==xyz_data_hold_req.pos[i].rep_r_index) {
										int add_req=1;
										for(int ii=0;ii<a_pk_n;ii++) {
											int diff = a_pk_list[ii] - xyz_data_hold_req.pos[i].a_e;
											if(abs(diff)<1) {
												add_req = 0;
												//xyz_data_hold_req.pos[i].a_e = a_pk_list[ii];
												//printf("rep ang pk %d\n", a_pk_list[ii]);
												break;
											}
										}
										if(add_req) {
											a_pk_list[a_pk_n] = xyz_data_hold_req.pos[i].a_e;
											a_pk_n++;
											//printf("add ang pk %d\n", xyz_data_hold_req.pos[i].a_e);
										}
										
										if(xyz_data_hold_req.pos[i].r_index_dummy_en) {
											r_index_dummy_en = 1;
										}
									}
								}
							}
						}
						

						int fno = frm_counter_256;
						int a_pk_list_del[MAX_RCH_N];
						memset(a_pk_list_del, 0, sizeof(a_pk_list_del));

						// step1
						for(int an=0;an<a_pk_n;an++) {
							int a = a_pk_list[an];
							//if( a_pk_list_del[an] ) continue;
							if( ang_r_n[eh]<pk_max ) {
								
								ang_r_inf[eh][fno].r[ang_r_n[eh]] = r_index;
								ang_r_inf[eh][fno].a[ang_r_n[eh]] = a;
								ang_r_inf[eh][fno].pow[ang_r_n[eh]] = (int)A_spc_log[a];
								
							}
							ang_r_n[eh]++;
						}
						ang_r_inf[eh][fno].num = ang_r_n[eh];

						// step2
						int ang_r_frm       = (eh==ED)? param->app->ang_r_frm_e:param->app->ang_r_frm_h;
						int ang_r           = (eh==ED)? param->app->ang_r_e:param->app->ang_r_h;
						int ang_r_dist      = (eh==ED)? ang_r_dist_e:ang_r_dist_h;
						for(int an=0;an<a_pk_n;an++) {
							if(ang_r<=0) break;
							int a = a_pk_list[an];
							rs_float ang_r_coeff = (abs(90-a) * param->app->ang_r_coeff) + ang_r;
							//printf("ang_r_coeff ang %d : %d -> %.1f\n", a, ang_r, ang_r_coeff);
							for(int f=0;f<ang_r_frm;f++) {
								if( a_pk_list_del[an] ) break;
								int pf = fno-f;
								if( pf<0 ) pf = 256+pf;
								for(int s=0;s<ang_r_inf[eh][pf].num;s++) {
									if( a_pk_list_del[an] ) break;
									if( (r_index+ang_r_dist)<ang_r_inf[eh][pf].r[s] ) {
										if( a>(ang_r_inf[eh][pf].a[s]-ang_r_coeff) && a<(ang_r_inf[eh][pf].a[s]+ang_r_coeff) ) {
											if(param->app->ang_r_pow_th==0) {
												a_pk_list_del[an] = 1;
											}
											else {
												srsl::fp th = ang_r_inf[eh][pf].pow[s] - param->app->ang_r_pow_th;
												if(A_spc_log[a]<th) {
													a_pk_list_del[an] = 1;
												}
											}
										}
									}
								}
							}
						}
						
						// step3
						for(int an=0;an<a_pk_n;an++) {
							if( a_pk_list_del[an] ) continue;
							int a = a_pk_list[an];
							if( pki2[eh].num<pk_max ) {
								pki2[eh].h[pki2[eh].num] = -1;
								pki2[eh].w[pki2[eh].num] = -1;
								pki2[eh].a[pki2[eh].num] = a;
								pki2[eh].r[pki2[eh].num] = r_index;
								pki2[eh].pow[pki2[eh].num] = (int)A_spc_log[a];//A_spc[eh][a];
								if(an<a_pk_n_org) {
									pki2[eh].a_real[pki2[eh].num] = 1;
								}
								else {
									pki2[eh].a_real[pki2[eh].num] = 0;
								}
								if(r_index_dummy_en) {
									pki2[eh].a_real[pki2[eh].num] = 0;
								}
							}
							pki2[eh].num++;
						}
					}
#endif
				} // for(int eh=0;eh<1;eh++) {

			} // for(int pk=0;pk<r_pk_num;pk++) {


			// Save peak count information
			if(r_pk_num>0) {
				// case of found range peak
				pre_r_pk_num = r_pk_num;
				for(int rn=0;rn<pre_r_pk_num;rn++) {
					pre_r_pk_indexs[rn] = r_pk_indexs[rn];
				}
			}
			else {
				// case of not found range peak
#if SMALL_A_RSS==0
				for(int i=0;i<MAX_HSIZE_R * ant_n * ant_n;i++) {
					a_Rss[ED][i].real *= 0.9;
					a_Rss[ED][i].imag *= 0.9;
					a_Rss[HD][i].real *= 0.9;
					a_Rss[HD][i].imag *= 0.9;
				}
#endif
			}

//			get_xyz_data(&xyz_data);
			get_xyz_data_wpara(param->clst, &xyz_data);
			clst.run(param->clst, frm_counter_i, &xyz_data, &xyz_data_clst);
			clst.run_2nd(param->clst, &xyz_data_clst, &xyz_data_clst);
			clst.run_sort(param->clst, &xyz_data_clst);
				
			for(int i=0;i<xyz_data_hold_req.num;i++) {
				if(xyz_data_hold_req.pos[i].life) {
					xyz_data_hold_req.pos[i].life--;
//					if(xyz_data_hold_req.pos[i].life==0)
//						printf("life end%d %d\n", i, xyz_data_hold_req.pos[i].life);
				}
			}

			frm++;
			if( frm>=max_frm_n ) {
				frm = 0;
			}
			frm_counter_256++;
			if( frm_counter_256>=256 ) frm_counter_256 = 0;
			frm_counter_i++;

		}

	}

	return 0;
}

//------------------------------------------------------------------------------
int C_2D_detect::SetParam( const proc_param *param_in)
{
	param = param_in;
	nfft = param->app->FFTNum;
	if(nfft>MAX_SIZE_R) {
		nfft = MAX_SIZE_R;
	}
	nang = MAX_SIZE_A;
	Epair = param->app->Epair;
	Hpair = param->app->Hpair;
	dccuten = param->app->DcCutEn;
	
#if RS_LOC3D_ALGO_USE_STOPWATCH
	rs_stopwatch_start(&measure->sw[2]);
#endif /* RS_LOC3D_ALGO_USE_STOPWATCH */
	Init();
#if RS_LOC3D_ALGO_USE_STOPWATCH
	rs_stopwatch_stop(&measure->sw[2]);
#endif /* RS_LOC3D_ALGO_USE_STOPWATCH */

#if RS_LOC3D_ALGO_USE_STOPWATCH
	rs_stopwatch_start(&measure->sw[3]);
#endif /* RS_LOC3D_ALGO_USE_STOPWATCH */

	int res = det_r[0].SetParam(param);
	if( res) return res;

	max_idxnum = GetDist2IndexNum(param->app->Pk2PkMaxDist);
	min_idxnum = GetDist2IndexNum(param->app->Pk2PkMinDist);
#if RS_LOC3D_ALGO_USE_STOPWATCH
	rs_stopwatch_stop(&measure->sw[3]);
#endif /* RS_LOC3D_ALGO_USE_STOPWATCH */

#if RS_LOC3D_ALGO_USE_STOPWATCH
	rs_stopwatch_start(&measure->sw[4]);
#endif /* RS_LOC3D_ALGO_USE_STOPWATCH */
	srsl::fp wei = param->app->RWeiVal;
	{
		//!!r_wei.resize(nfft);
		srsl::fp wei_base = DETECT_2D_POW(nfft/2,wei);
		int i;
		for(i=0;i<nfft/2;i++) {
			int diff = nfft-i;
			r_wei[i] = DETECT_2D_POW(diff,wei);
			r_wei[i] = r_wei[i] - wei_base + 1;
		}
		int j=0;
		for(;i<nfft;i++) {
			r_wei[i] = r_wei[j];
			j++;
		}
	}

	srsl::fp wei_a = param->app->AWeiVal;
	{
		srsl::fp wei_base = DETECT_2D_POW(MAX_SIZE_A/2,wei_a);
		int i;
		for(i=0;i<MAX_SIZE_A/2;i++) {
			int diff = MAX_SIZE_A-i;
			a_wei[i] = DETECT_2D_POW(diff,wei_a);
			a_wei[i] = a_wei[i] - wei_base + 1;
		}
		int j=i-1;
		for(;i<MAX_SIZE_A;i++) {
			a_wei[i] = a_wei[j];
			j--;
			if(j<0) j=0;
		}
		
	}

	{
		int antn = 2;
		srsl::fp ant_space_e = param->app->ant_space_e;
		ant_space_e *= 1e-3;
		srsl::fp ant_space_h = param->app->ant_space_h;
		ant_space_h *= 1e-3;
		Capon[HD].Init(antn, ant_space_h);
		int mimo_en = param->mimo_en;
		if( mimo_en ) {
			Capon[ED].Init(4, ant_space_e);
		}
		else {
			Capon[ED].Init(antn, ant_space_e);
		}
	}

	a_valid_r_index = GetDist2IndexNum(param->app->AngValidDist);
	//if( param->app->HeatMapCalMode!=0 ) {
	//	a_valid_r_index = -1;
	//}

	for(int r=nfft-1;r>=nfft/2;r--) {
		ix2dist[r] = det_r[0].GetIndex2Dist(r);
	}
	MinDist = MaxDist = -1;
	GetEdIndex();
	GetStIndex();
	
	pre_alpha_r = param->app->alpha_r;
	
	ang_r_dist_e  = GetDist2IndexNum(param->app->ang_r_dist_e);
	ang_r_dist_h  = GetDist2IndexNum(param->app->ang_r_dist_h);
	
#if RS_LOC3D_ALGO_USE_STOPWATCH
	rs_stopwatch_stop(&measure->sw[4]);
#endif /* RS_LOC3D_ALGO_USE_STOPWATCH */

	return 0;
}

//------------------------------------------------------------------------------
void C_2D_detect::Init( void )
{
	MaxDist = 0;
	MinDist = 0;
	EdIndex = 0;
	StIndex = 0;
	AngR_Cnt = 0;
	frm = 0;
	frm_counter_256 = 0;
	frm_counter_i = 0;
	txch2_det = 0;
	det_r[0].Init();

	r_pk_num = 0;
	r_pk_num_for_ang = 0;
	pre_r_pk_num = 0;
	a_A_spc_max = 0;
	R_spc_threshold = 0;

	for(int ch=0;ch<MAX_RCH_N;ch++) {
		for(int j=0;j<MAX_FRM_N;j++) {
			for(int k=0;k<MAX_HSIZE_R;k++) {
				R_out_pre[j][ch][k].real = 0;
				R_out_pre[j][ch][k].imag = 0;
			}
		}
	}
	memset(R_out_pre_valid, 0, sizeof(R_out_pre_valid));

#if SMALL_A_RSS==0
	for(int i=0;i<MAX_HSIZE_R * MAX_RCH_N * MAX_RCH_N;i++) {
		a_Rss[ED][i].real = 0;
		a_Rss[ED][i].imag = 0;
		a_Rss[HD][i].real = 0;
		a_Rss[HD][i].imag = 0;
	}
#else
	memset(a_Rss_inf, 0, sizeof(a_Rss_inf));
#endif

	for(int i=0;i<MAX_SIZE_R;i++) {
		a_R_spc[i] = 0;
		cfar_th[i] = 0;
	}

	memset(ang_r_inf, 0, sizeof(ang_r_inf));
	angle_axis_init();
	clst.init();
	
	for(int i=0;i<128;i++) {
		r_wei_table[i] = r_wei_lut[i];
	}
	
	hold_count = 0;
	hold_en = 0;
	memset(&xyz_data_hold_req, 0, sizeof(xyz_data_hold_req));

}

//------------------------------------------------------------------------------
int C_2D_detect::GetEdIndex(void)
{
	if( MaxDist != param->app->MaxDist ) {
		MaxDist = param->app->MaxDist;
		EdIndex = nfft/2;
		for(int i=nfft/2;i<nfft;i++) {
			if( ix2dist[i]<=MaxDist ) {
				EdIndex = i;
				if( (i-1)>=(nfft/2) ) {
					EdIndex -= 1;
				}
				break;
			}
		}
	}
	return EdIndex;
}

//------------------------------------------------------------------------------
int C_2D_detect::GetDist2Index(srsl::fp dist)
{
	return det_r[0].GetDist2Index(dist);
}

//------------------------------------------------------------------------------
int C_2D_detect::GetDist2IndexNum(srsl::fp dist)
{
	int st = det_r[0].GetDist2Index(5);
	int ed = det_r[0].GetDist2Index(dist+5);
	return (st-ed);
}

//------------------------------------------------------------------------------
int C_2D_detect::GetStIndex(void)
{
	if( MinDist != param->app->MinDist ) {
		MinDist = param->app->MinDist;
		StIndex = nfft-1;
		for(int i=nfft-1;i>=nfft/2;i--) {
			if( ix2dist[i]>=MinDist ) {
				StIndex = i;
				if( (i+1)<=(nfft-1) ) {
					StIndex += 1;
				}
				break;
			}
		}
	}
	return StIndex;
}

//------------------------------------------------------------------------------
#ifndef xy_pk_n_max
#define xy_pk_n_max (32)
#endif
int C_2D_detect::AspcCovexIndexSearch(int phase, srsl::fp *indat, int st, int ed, int deci, int max_pk_n, int *res)
{
	static int dbg_cnt=0;
	dbg_cnt++;

	srsl::fp max = 0;
	srsl::fp min = 0;
	srsl::fp th = 0;
	srsl::fp ang_spc_th = param->app->fMagTh_a;
	int max_ang;

	int pk_n = 0;
	int _res[xy_pk_n_max]={};
	srsl::fp spc[xy_pk_n_max]={};

//mode//	for(int a=st+1;a<ed;a+=1) {
	for(int a=0;a<MAX_SIZE_A;a++) {
		if( (a==st+1) || indat[a]>max ) {
			max = indat[a];
			max_ang = a;
		}
		if( (a==st+1) || indat[a]<min ) {
			min = indat[a];
		}
	}
	th = (max-min)*(ang_spc_th/100);
	th = max-th;

	int p=0;
	int m=0;
	int _st = st;
	int _ed = ed;
	if(_st<=0) {
		_st = 0;
		p = 1;
	}
	if(_ed>=180) {
		_ed = 180;
		m = 1;
	}
	for(int a=_st+p;a<_ed-m;a++) {
		int a0 = a-1;
		int a1 = a;
		int a2 = a+1;

		srsl::fp d0 = indat[a0];
		srsl::fp d1 = indat[a1];
		srsl::fp d2 = indat[a2];

		if( (d1>d0) && (d1>d2) ) {
//			if( (d1-min)>=ang_spc_th ) {
			if( d1>=th ) {

				int en = 0;
				if( param->app->PkDetMode_r==2 && phase==ED ) {
					{
						if( a>=param->app->outof_min_ang_restriction && a<=param->app->outof_max_ang_restriction ) {
							if( r_pk_dist_width>=param->app->restriction2_r_pk_width && r_pk_pow_degree>=param->app->restriction2_r_pk_pow ) {
								en = 1;
							}
						}
						else if( r_pk_dist_width>=param->app->restriction1_r_pk_width && r_pk_pow_degree>=param->app->restriction1_r_pk_pow ) {
							en = 1;
						}
					}
				}
				else {
					en = 1;
				}

				if( en ) {
					// found convex
					_res[pk_n] = a1;
					spc[pk_n] = d1;
					pk_n++;
				}
			}
		}
	}
	
	if(param->app->out_of_fov_check) {
		int max_ang_is_det = 0;
		for(int i=0;i<pk_n;i++) {
			if(_res[i]==max_ang) {
				max_ang_is_det = 1;
				break;
			}
		}
		if(max_ang_is_det==0) {
			pk_n = 0;
		}
	}
	
	if( pk_n>max_pk_n ) {
		for(int i=0; i<pk_n; i++) {
			for(int j=i+1; j<pk_n; j++) {
				if(spc[i] < spc[j]) {
					srsl::fp tmpf = spc[i];
					spc[i] = spc[j];
					spc[j] = tmpf;

					srsl::fp tmpi = _res[i];
					_res[i] = _res[j];
					_res[j] = tmpi;
				}
			}
		}
		pk_n = max_pk_n;
	}
	for(int i=0; i<pk_n; ++i) {

		if( (phase==HD && param->app->AngH_Cmp_en) || (phase==ED && param->app->AngE_Cmp_en) ) {
			//
			// Correct the angle
			//
			int a = _res[i]-1;
			if(a<0) a=0;
			if(a>178) a=178;
			int ang = 0;
			if( phase==ED ) {
				ang = AngE_comp_table_rxAVE[a];
			}
			else {
				ang = AngH_comp_table_rx12[a];
			}
			_res[i] += ang;
		}

		res[i] = _res[i];
	}
	


	return pk_n;
}
//------------------------------------------------------------------------------
int C_2D_detect::calc_xyz(rs_float r, rs_float ang_h, rs_float ang_e, rs_float *x, rs_float *y, rs_float *z)
{
	rs_float c = (rs_float)PI / 180;

	rs_float _x = -r*srsl::sin((ang_h)*c);
	rs_float _y = -r*srsl::sin((ang_e)*c);
	rs_float sum = r*r - _x*_x - _y*_y;
	if (sum<=0) {
		return -1;
	}
	rs_float _z = srsl::sqrt(sum);

	*x = _x;
	*y = _y;
	*z = _z;
	
	return 0;

}
//------------------------------------------------------------------------------
void C_2D_detect::get_xyz_data(rs_loc3d_algo_xyz_data_ex *data)
{
	rs_uint32_t num_e = pki2[ED].num;
	rs_uint32_t num_h = pki2[HD].num;
	rs_uint32_t xyz_n = 0;
	for (rs_uint32_t e = 0; e < num_e; ++e)
	{
		int ie = pki2[ED].r[e];
		for (rs_uint32_t h = 0; h < num_h; ++h)
		{
			int ih = pki2[HD].r[h];
			if (ie == ih)
			{
				rs_float r = (int)(ix2dist[ie] * 10.0);
				rs_float a_e = pki2[ED].a[e] - 90;
				rs_float a_h = pki2[HD].a[h] - 90;
				rs_float x, y, z;
				if( calc_xyz(r, a_h, a_e, &x, &y, &z)==0 ) {
					x = -x;
					y = -y;

					data->pos[xyz_n].x = (rs_int32_t)x;
					data->pos[xyz_n].y = (rs_int32_t)y;
					data->pos[xyz_n].z = (rs_int32_t)z;

					xyz_n++;
				}
				break;
			}
		}

	}
	data->num = xyz_n;

}
//------------------------------------------------------------------------------
void C_2D_detect::get_xyz_data_wpara(struct clst_param *para, rs_loc3d_algo_xyz_data_ex *data)
{
	rs_uint32_t num_e = pki2[ED].num;
	rs_uint32_t num_h = pki2[HD].num;
	rs_uint32_t xyz_n = 0;
	for (rs_uint32_t e = 0; e < num_e; ++e)
	{
		int ie = pki2[ED].r[e];
		for (rs_uint32_t h = 0; h < num_h; ++h)
		{
			int ih = pki2[HD].r[h];
			if (ie == ih)
			{
				rs_float r = (int)(ix2dist[ie] * 10.0);
				rs_float a_e = pki2[ED].a[e] - 90;
				rs_float a_h = pki2[HD].a[h] - 90;
				rs_float x, y, z;
				if( calc_xyz(r, a_h, a_e, &x, &y, &z)==0 ) {
					x = -x;
					y = -y;

					data->pos[xyz_n].x = (rs_int32_t)x;
					data->pos[xyz_n].y = (rs_int32_t)y;
					data->pos[xyz_n].z = (rs_int32_t)z;
					data->pos[xyz_n].pow = (rs_float)pki2[ED].pow[e];
					data->pos[xyz_n].r_index = (rs_int32_t)pki2[ED].r[e];
					data->pos[xyz_n].a_e = (rs_int32_t)pki2[ED].a[e];
					data->pos[xyz_n].a_h = (rs_int32_t)pki2[HD].a[h];
					data->pos[xyz_n].a_e_real = (rs_int32_t)pki2[ED].a_real[e];
					//printf("a_e %d\n", data->pos[xyz_n].a_e);

					if( para->grp_range_x_valid ) {
						float _x = x;
						if( _x<para->grp_range_x_min || _x>para->grp_range_x_max ) {
							continue;
						}
					}
					if( para->grp_range_y_valid ) {
						float _y = y;
						if( _y<para->grp_range_y_min || _y>para->grp_range_y_max ) {
							continue;
						}
					}
					if( para->grp_range_z_valid ) {
						float _z = z;
						if( _z<para->grp_range_z_min || _z>para->grp_range_z_max ) {
							continue;
						}
					}
					xyz_n++;
				}
				break;
			}
		}

	}
	data->num = xyz_n;

}
