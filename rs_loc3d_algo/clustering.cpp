/*
 * Copyright (c) 2016-2021 Socionext Inc.
 * All rights reserved.
 */
#include <cmath>
#include <string.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "clustering.h"

#define POW_MAX_N   (25)


void C_clustering::init(void)
{
	wp = 0;
	memset(clst_inf, 0, sizeof(clst_inf));

}

int C_clustering::run(struct clst_param *para, uint32_t fno, struct rs_loc3d_algo_xyz_data_ex *indata, struct rs_loc3d_algo_xyz_data_ex *res)
{
	int num = 0;
	for(int i=0;i<indata->num;i++) {
		int x = (int) indata->pos[i].x;
		int y = (int) indata->pos[i].y;
		int z = (int) indata->pos[i].z;
		if( para->grp_range_x_valid ) {
			float _x = x;
			if( _x<para->grp_range_x_min || _x>para->grp_range_x_max ) {
				continue;
			}
		}
		if( para->grp_range_y_valid ) {
			float _y = y;
			if( _y<para->grp_range_y_min || _y>para->grp_range_y_max ) {
				continue;
			}
		}
		if( para->grp_range_z_valid ) {
			float _z = z;
			if( _z<para->grp_range_z_min || _z>para->grp_range_z_max ) {
				continue;
			}
		}


		clst_inf[wp].x = x;
		clst_inf[wp].y = y;
		clst_inf[wp].z = z;
		clst_inf[wp].pow = indata->pos[i].pow;
		clst_inf[wp].r_index = indata->pos[i].r_index;
		clst_inf[wp].a_e = indata->pos[i].a_e;
		clst_inf[wp].a_h = indata->pos[i].a_h;
		clst_inf[wp].fno = fno;
		clst_inf[wp].no = wp;
		clst_inf[wp].valid = 1;
		clst_inf[wp].a_e_real = indata->pos[i].a_e_real;

		wp++;
		if(wp==MAX_CLST) wp = 0;
		num++;
	}
	if( num==0 ) {
		clst_inf[wp].valid = 0;
		clst_inf[wp].fno = fno;
		wp++;
		if(wp==MAX_CLST) wp = 0;
	}

	run_pairing(para, res);
	return 0;
}

void C_clustering::run_pairing(struct clst_param *para, struct rs_loc3d_algo_xyz_data_ex *res)
{
	int now_fno,old_fno;

	//
	// step1
	//
	int rp = wp-1;
	if(rp<0) rp = MAX_CLST-1;

	now_fno = clst_inf[rp].fno;
	old_fno = now_fno - para->frm_hist_n;
	if(old_fno<0) old_fno = 0;

	int plot_n = 0;

#if 1
	int end_rp;
	for(int i=0;i<MAX_CLST;i++) {
		clst_inf[rp].gno = -1;
		if(clst_inf[rp].fno>=old_fno) {
			plot_n++;
		}
		else {
			end_rp = rp;
			break;
		}

		rp--;
		if(rp<0) rp = MAX_CLST-1;
	}


	//
	// step 2
	//
	int gmax = 0;
	int gmem_no[MAX_PAIR];
	gmem_no[0] = 0;

	rp = wp-1;
	if(rp<0) rp = MAX_CLST-1;
	clst_inf[rp].gno = gmax;
	gmem_no[gmax]++;
	gmax++;

	int rp1,rp2;
	for(int i=1;i<plot_n;i++) {

		rp1 = rp-i;
		if(rp1<0) rp1 = MAX_CLST + rp1;

		if( clst_inf[rp1].valid==0 ) continue;

		int d_min = para->grp_range;
		uint32_t d;
		int grouping_done = 0;
		for(int j=0;j<i;j++) {
			rp2 = wp-j-1;
			if(rp2<0) rp2 = MAX_CLST + rp2;

			if( rp1==rp2 ) continue;
			if( clst_inf[rp2].valid==0 ) continue;

			d = (clst_inf[rp2].x - clst_inf[rp1].x)*(clst_inf[rp2].x - clst_inf[rp1].x)
				  +
				(clst_inf[rp2].y - clst_inf[rp1].y)*(clst_inf[rp2].y - clst_inf[rp1].y)
				  +
				(clst_inf[rp2].z - clst_inf[rp1].z)*(clst_inf[rp2].z - clst_inf[rp1].z);
			d = sqrt((float)d);

			if( d<d_min ) {
				d_min = d;
				int gno = clst_inf[rp2].gno;
				gmem_no[gno]++;
				clst_inf[rp1].gno = gno;
				grouping_done = 1;
				break;
			}
		}
		if(grouping_done==0) {
			gmem_no[gmax] = 1;
			clst_inf[rp1].gno = gmax;
			gmax++;

			if(gmax>=MAX_PAIR) {
				//printf("gmax over\n");
				break;
			}
		}
	}


	//
	// step 3
	//
	int p=0;
	for(int i=0;i<gmax;i++) {
		if( gmem_no[i]<para->grp_smp_n ) {
			continue;
		}
		{
			int x=0;
			int y=0;
			int z=0;
			rs_float pow=0;
			int r_index=0;
			int a_e=0;
			int a_h=0;
			int a_e_real=0;
			int n=0;
			int n_pow=0;
			for(int k=0;k<plot_n;k++) {
				rp = wp-k-1;
				if(rp<0) rp = MAX_CLST + rp;
				if( clst_inf[rp].gno!=i ) continue;

				x += clst_inf[rp].x;
				y += clst_inf[rp].y;
				z += clst_inf[rp].z;
				r_index += clst_inf[rp].r_index;
				a_e += clst_inf[rp].a_e;
				a_h += clst_inf[rp].a_h;
				a_e_real += clst_inf[rp].a_e_real;

				if(n_pow<POW_MAX_N) {
					pow += clst_inf[rp].pow;
					n_pow++;
				}
				n++;
			}
			if(n>0 && p<MAX_PAIR) {
				x = x / n;
				y = y / n;
				z = z / n;
				pow = pow / n_pow;
				r_index = r_index / n;
				a_e = a_e / n;
				a_h = a_h / n;

				res->pos[p].x = x;
				res->pos[p].y = y;
				res->pos[p].z = z;
				res->pos[p].pow = pow;
				res->pos[p].r_index = r_index;
				res->pos[p].a_e = a_e;
				res->pos[p].a_h = a_h;
				res->pos[p].a_e_real = a_e_real;
				res->pos[p].smp_n = n;
				res->pos[p].life = 0;

				p++;
			}
		}
	}
	res->num = p;

#else
	// simple moving average
	int x_ave=0;
	int y_ave=0;
	int z_ave=0;
	for(int i=0;i<MAX_CLST;i++) {
		if(clst_inf[rp].fno>=old_fno && clst_inf[rp].valid) {
			plot_n++;

			x_ave += clst_inf[rp].x;
			y_ave += clst_inf[rp].y;
			z_ave += clst_inf[rp].z;

		}

		rp--;
		if(rp<0) rp = MAX_CLST-1;
	}
	if( plot_n ) {
		x_ave=x_ave/plot_n;
		y_ave=y_ave/plot_n;
		z_ave=z_ave/plot_n;

		res->pos[0].x = x_ave;
		res->pos[0].y = y_ave;
		res->pos[0].z = z_ave;
		res->pos[0].r = 0;
		res->pos[0].pow = 0;
		res->num = 1;
	}
	else {
		res->num = 0;
	}
#endif

}

int C_clustering::run_2nd(struct clst_param *para, struct rs_loc3d_algo_xyz_data_ex *indata, struct rs_loc3d_algo_xyz_data_ex *res)
{
	if(para->grp_2_en==0) return 0;
	
	int gid[32];
	for(int i=0;i<32;i++) gid[i] = -1;
	
	for(int i=0;i<indata->num;i++) {
		if(gid[i]>=0) continue;
		gid[i] = i;
		for(int ii=0;ii<indata->num;ii++) {
			if(i==ii) continue;
			if(gid[ii]>=0) continue;
			rs_float r = std::pow(indata->pos[i].x-indata->pos[ii].x, 2) + 
						 std::pow(indata->pos[i].y-indata->pos[ii].y, 2) + 
						 std::pow(indata->pos[i].z-indata->pos[ii].z, 2);
			if(r>0) r = std::sqrt(r);
			int ang_diff = indata->pos[i].a_e - indata->pos[ii].a_e;
			ang_diff = std::abs(ang_diff);
			if(r<=para->grp_2_range) {
				if(ang_diff<=para->grp_2_ang) {
					gid[ii] = i;
					/*
					printf("### pair(%d&%d) (%.0f,%d)xyz %d %d %d : %d %d %d\n"
						, i, ii, r, ang_diff
						, indata->pos[i].x, indata->pos[i].y, indata->pos[i].z
						, indata->pos[ii].x, indata->pos[ii].y, indata->pos[ii].z
					);
					*/
				}
			}
			/*
			if(gid[ii]==-1) {
				printf("### Cant(%d&%d) (%.0f,%d)xyz %d %d %d : %d %d %d\n"
					, i, ii, r, ang_diff
					, indata->pos[i].x, indata->pos[i].y, indata->pos[i].z
					, indata->pos[ii].x, indata->pos[ii].y, indata->pos[ii].z
				);
			}
			*/
		}
		
	}
	
	int num=0;
	for(int i=0;i<indata->num;i++) {
		if(gid[i]==-1) continue;
		rs_float x = 0;
		rs_float y = 0;
		rs_float z = 0;
		rs_float r_index = 0;
		rs_float a_e = 0;
		rs_float a_h = 0;
		rs_float pow = 0;
		rs_int32_t smp_n = indata->pos[i].smp_n;
		int a_e_real = 0;
		
		x += (rs_float)indata->pos[i].x * indata->pos[i].smp_n;
		y += (rs_float)indata->pos[i].y * indata->pos[i].smp_n;
		z += (rs_float)indata->pos[i].z * indata->pos[i].smp_n;
		r_index += (rs_float)indata->pos[i].r_index * indata->pos[i].smp_n;
		a_e += (rs_float)indata->pos[i].a_e * indata->pos[i].smp_n;
		a_h += (rs_float)indata->pos[i].a_h * indata->pos[i].smp_n;
		pow += (rs_float)indata->pos[i].pow * indata->pos[i].smp_n;
		a_e_real += indata->pos[i].a_e_real;
		
		for(int ii=0;ii<indata->num;ii++) {
			if(i==ii) continue;
			if(gid[i]!=gid[ii]) continue;
			gid[ii] = -1;
			x += (rs_float)indata->pos[ii].x * indata->pos[ii].smp_n;
			y += (rs_float)indata->pos[ii].y * indata->pos[ii].smp_n;
			z += (rs_float)indata->pos[ii].z * indata->pos[ii].smp_n;
			r_index += (rs_float)indata->pos[ii].r_index * indata->pos[ii].smp_n;
			a_e += (rs_float)indata->pos[ii].a_e * indata->pos[ii].smp_n;
			a_h += (rs_float)indata->pos[ii].a_h * indata->pos[ii].smp_n;
			pow += (rs_float)indata->pos[ii].pow * indata->pos[ii].smp_n;
			smp_n += indata->pos[ii].smp_n;
			a_e_real += indata->pos[ii].a_e_real;
		}
		
		x = (rs_float)x/smp_n;
		y = (rs_float)y/smp_n;
		z = (rs_float)z/smp_n;
		r_index = (rs_float)r_index/smp_n;
		a_e = (rs_float)a_e/smp_n;
		a_h = (rs_float)a_h/smp_n;
		pow = (rs_float)pow/smp_n;
		
		
		res->pos[num].x        = x;
		res->pos[num].y        = y;
		res->pos[num].z        = z;
		res->pos[num].r_index  = r_index;
		res->pos[num].a_e      = a_e;
		res->pos[num].a_h      = a_h;
		res->pos[num].pow      = pow;
		res->pos[num].smp_n    = smp_n;
		res->pos[num].a_e_real = a_e_real;
		
		num++;
	}
	
	/*
	if(res->num != num) {
		printf("#### clst2nd num %d -> %d\n", res->num, num);
	}
	*/
	res->num = num;
	
	return 0;
}

int C_clustering::run_sort(struct clst_param *para, struct rs_loc3d_algo_xyz_data_ex *indata)
{
	if(para->sort_mode==0) return 0;
	
	struct rs_loc3d_algo_xyz_base_ex backup;
	
	for(int i=0;i<indata->num;i++) {
		rs_float r1 = std::pow(indata->pos[i].x, 2) + 
					  std::pow(indata->pos[i].y, 2) + 
					  std::pow(indata->pos[i].z, 2);
		for(int ii=i+1;ii<indata->num;ii++) {
			rs_float r2 = std::pow(indata->pos[ii].x, 2) + 
						  std::pow(indata->pos[ii].y, 2) + 
						  std::pow(indata->pos[ii].z, 2);
			if(r1>r2) {
				backup = indata->pos[i];
				indata->pos[i] = indata->pos[ii];
				indata->pos[ii] = backup;
				//printf("#### exchange %d->%d\n", i, ii);
			}
		}
	}
	
	//printf("#### SORT\n");

	return 0;
}

