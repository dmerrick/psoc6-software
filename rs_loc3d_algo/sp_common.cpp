/*
 * Copyright (c) 2016 Socionext Inc.
 * All rights reserved.
 */
#include <string>
#include <time.h>
#include <stdarg.h>
#include <string.h>
#include <srsl_type.h>
#include <srsl_math.h>
#include "sp_common.h"

//---------------------------------------------------------------------------
srsl::fp SP_LOG10(srsl::fp in)
{
	if( in>1 ) {
		return srsl::log10(in);
	}
	return 0;
}

//---------------------------------------------------------------------------
srsl::fp CalcNoiseLevel_of_fdata(int nfft, srsl::fp *fdata)
{

#	define div_n  (10)
	srsl::fp div_val;
	int ix;
	int ed_div;
	srsl::fp ave_div[div_n];
	srsl::fp min_ave_div;
	int min_ave_div_n;
	srsl::fp fdata_log;
	int ave_div_n;

	{
		int st = nfft/2-1;
		int ed = nfft;
		div_val = (srsl::fp)(ed-st)/div_n;
		ix = st;
		ed_div = st;
		min_ave_div = 0;
		min_ave_div_n = -1;

		for(int d=0;d<div_n;d++) {
			ed_div += div_val;
			if( ed_div>ed ) ed_div = ed;
			ave_div_n = 0;
			ave_div[d] = 0;
			for( ; ix<ed_div; ix++ ) {
				if( fdata[ix]>0 ) {
					fdata_log = DB_V * SP_LOG10(fdata[ix]);
					ave_div[d] += fdata_log;
					ave_div_n++;
				}
			}
			if( d<(div_n-1) && d>0 ) {
				if( ave_div_n ) {
					ave_div[d] = (srsl::fp) ave_div[d]/ave_div_n;
					if( min_ave_div_n==-1 || min_ave_div>ave_div[d] ) {
						min_ave_div = ave_div[d];
						min_ave_div_n = d;
					}
				}
			}
		}

	}
	return min_ave_div;
}

