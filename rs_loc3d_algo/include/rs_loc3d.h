/*
 * Copyright (c) 2018 Socionext Inc.
 * All rights reserved.
 */

#ifndef RS_LOC3D_H
#define RS_LOC3D_H

#include "rs_loc3d_platform.h"
#include "rs_loc3d_type.h"

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Type Definition
 */
typedef struct rs_loc3d_handle *rs_loc3d_handle_t;
typedef rs_int rs_loc3d_mode_t;
typedef rs_int rs_loc3d_state_t;

/*
 * Constant Definition
 */
#define RS_LOC_OK                      (0)
#define RS_LOC_EINV                   (-1)
#define RS_LOC_ETOUT                  (-2)
#define RS_LOC_EDEV                   (-3)
#define RS_LOC_ENOSUPPORT             (-4)
#define RS_LOC_ESTATE                 (-5)
#define RS_LOC_ECOM                   (-6)
#define RS_LOC_ENOMEM                 (-7)
#define RS_LOC_EFAULT                 (-8)

#define RS_LOC3D_MODE_0                (0)
#define RS_LOC3D_MODE_1                (1)
#define RS_LOC3D_MODE_2                (2)
#define RS_LOC3D_MODE_3                (3)
#define RS_LOC3D_MODE_4                (4)
#define RS_LOC3D_MODE_5                (5)
#define RS_LOC3D_MODE_6                (6)

#define RS_LOC3D_STATE_READY      (1)
#define RS_LOC3D_STATE_DEV_READY       (2)
#define RS_LOC3D_STATE_SENSING         (3)

/*
 * Structure Definition
 */
struct rs_loc3d_dev_exec_info {
	rs_uint32_t frame_counter;
	rs_uint32_t frame_time;
};

struct rs_hmp_data {
	rs_uint32_t pixel_size;
	rs_uint32_t axis_length;
	rs_uint8_t data_e[256][256];
	rs_uint8_t data_h[256][256];
};

struct rs_loc_base {
	rs_int32_t h;
	rs_int32_t w;
	rs_int32_t r;
	rs_int32_t a;
};

struct rs_loc_data {
	rs_uint32_t num_e;
	struct rs_loc_base loc_e[32];
	rs_uint32_t num_h;
	struct rs_loc_base loc_h[32];
};

struct rs_dist_data {
	rs_uint32_t num;
	rs_int32_t r[32];
};

struct rs_xyz_base {
	rs_int32_t x;
	rs_int32_t y;
	rs_int32_t z;
};

struct rs_xyz_data {
	rs_uint32_t num;
	struct rs_xyz_base pos[32];
};

struct rs_r_spc {
	rs_int fftsize;
	rs_float spc[2048];
	rs_float threshold;
};

struct rs_r_spc2_ex {
	rs_int fftsize;
	rs_float spc[256];
	rs_float threshold;
	rs_float cfar_th[256];
	rs_int   cfar_en;
};


/*
 * Function Definition
 */
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_get_api_ver(rs_uint16_t *p_api_major_ver, rs_uint16_t *p_api_minor_ver);
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_get_lib_ver(rs_uint16_t *p_lib_major_ver, rs_uint16_t *p_lib_minor_ver, rs_uint16_t *p_lib_revision, rs_uint16_t *p_lib_hotfix);

RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_initialize(const char *target_name, rs_loc3d_handle_t *p_handle);
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_free(rs_loc3d_handle_t handle);
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_start(rs_loc3d_handle_t handle, const char *dev_name, rs_loc3d_mode_t mode);
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_start_ex(rs_loc3d_handle_t handle, const char *dev_name, rs_loc3d_mode_t mode, rs_loc3d_mode_t submode);
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_stop(rs_loc3d_handle_t handle);
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_change_sns_mode(rs_loc3d_handle_t handle, rs_loc3d_mode_t mode);
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_change_sns_mode_ex(rs_loc3d_handle_t handle, rs_loc3d_mode_t mode, rs_loc3d_mode_t submode);

RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_set_init_param(rs_loc3d_handle_t handle);
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_set_hmp_pxl_size(rs_loc3d_handle_t handle, rs_loc3d_mode_t mode, rs_int apn, rs_uint32_t pixel_size);
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_set_hmp_axis_length(rs_loc3d_handle_t handle, rs_loc3d_mode_t mode, rs_int apn, rs_uint32_t length);
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_exec(rs_loc3d_handle_t handle, struct rs_loc3d_dev_exec_info *p_dev_exec_info, rs_uint32_t timeout);
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_get_state(rs_loc3d_handle_t handle, rs_loc3d_state_t *p_state);
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_get_dev_time(rs_loc3d_handle_t handle, rs_uint32_t *p_time);
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_get_sns_mode(rs_loc3d_handle_t handle, rs_loc3d_mode_t *p_mode);
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_get_sns_mode_ex(rs_loc3d_handle_t handle, rs_loc3d_mode_t *p_mode, rs_loc3d_mode_t *p_submode);
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_get_frame_rate(rs_loc3d_handle_t handle, rs_loc3d_mode_t mode, rs_uint32_t *p_frame_rate);
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_get_frame_rate_ex(rs_loc3d_handle_t handle, rs_loc3d_mode_t mode, rs_loc3d_mode_t submode, rs_uint32_t *p_frame_rate);
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_query_hmp_data(rs_loc3d_handle_t handle, rs_loc3d_mode_t mode, rs_int apn);
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_query_loc_data(rs_loc3d_handle_t handle, rs_loc3d_mode_t mode, rs_int apn);
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_query_dist_data(rs_loc3d_handle_t handle, rs_loc3d_mode_t mode, rs_int apn);
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_get_hmp_data(rs_loc3d_handle_t handle, rs_int apn, struct rs_hmp_data *p_hmp_data);
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_get_loc_data(rs_loc3d_handle_t handle, rs_int apn, struct rs_loc_data *p_loc_data);
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_get_dist_data(rs_loc3d_handle_t handle, rs_int apn, struct rs_dist_data *p_dist_data);
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_get_xyz_data(rs_loc3d_handle_t handle, rs_int apn, struct rs_xyz_data *p_xyz_data);
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_get_xyz_data_clst(rs_loc3d_handle_t handle, rs_int apn, struct rs_xyz_data *p_xyz_data);
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_set_hard_fft(rs_loc3d_handle_t handle, rs_int fft_en);
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_get_r_spc(rs_loc3d_handle_t handle, rs_int apn, struct rs_r_spc *p_r_spc);
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_set_clst_param(rs_loc3d_handle_t handle, rs_loc3d_mode_t mode, rs_int apn, struct clst_param *p_clst_param);

RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_set_range_smooth_factor(rs_loc3d_handle_t handle, rs_loc3d_mode_t mode, rs_int apn, rs_float alpha);
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_set_ang_smooth_factor(rs_loc3d_handle_t handle, rs_loc3d_mode_t mode, rs_int apn, rs_float alpha);
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_set_range_threshold(rs_loc3d_handle_t handle, rs_loc3d_mode_t mode, rs_int apn, rs_float th1, rs_float th2);
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_set_ang_threshold(rs_loc3d_handle_t handle, rs_loc3d_mode_t mode, rs_int apn, rs_float th);

// add v2.1
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_set_range_cfar(rs_loc3d_handle_t handle, rs_loc3d_mode_t mode, rs_int apn, rs_int cfar);
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_set_ang_min_max(rs_loc3d_handle_t handle, rs_loc3d_mode_t mode, rs_int apn, rs_int e_min, rs_int e_max, rs_int h_min, rs_int h_max);
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_set_range_abs_threshold(rs_loc3d_handle_t handle, rs_loc3d_mode_t mode, rs_int apn, rs_float abs_th);
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_set_range_min_max(rs_loc3d_handle_t handle, rs_loc3d_mode_t mode, rs_int apn, rs_float min, rs_float max);
RS_LOC3D_APICALL rs_int RS_LOC3D_APIENTRY rs_loc3d_get_r_spc2_ex(rs_loc3d_handle_t handle, rs_int apn, struct rs_r_spc2_ex *p_r_spc);
#ifdef __cplusplus
}
#endif

#endif /* RS_LOC3D_H */
