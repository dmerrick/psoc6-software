#ifndef RS_LOGGING_H
#define RS_LOGGING_H

struct rs_logging {
	int target_level;
	rs_logging();
	void print(int level, const char *format, ...);
	static const int INFO;
	static const int TRACE;
	static const int TRACE_FRAME;
	static const int TRACE_CHIRP;
	static const int DEBUG;
};

#endif /* RS_LOGGING_H */
