/*
 * Copyright (c) 2016-2021 Socionext Inc.
 * All rights reserved.
 */
#ifndef clusteringH
#define clusteringH
//---------------------------------------------------------------------------
#include "rs_loc3d_algo.h"
#include "sp_common.h"

#if TARGET_MCU==0
#define MAX_CLST      (128*10)
#else
#define MAX_CLST      (64*10)
#endif
#define MAX_PAIR      (10)
#define MAX_POS_FRM   (32)

struct rs_clst_inf
{
	uint32_t fno;
	short no;
	short gno;
	short valid;

	short x;
	short y;
	short z;
	short r;
	short r_index;
	short a_e;
	short a_h;
	short a_e_real;
	
	float pow;
};



class C_clustering
{
public:

	int run(struct clst_param *para, uint32_t fno, struct rs_loc3d_algo_xyz_data_ex *indata, struct rs_loc3d_algo_xyz_data_ex *res);
	void run_pairing(struct clst_param *para, struct rs_loc3d_algo_xyz_data_ex *res);
	void init(void);

	int run_2nd(struct clst_param *para, struct rs_loc3d_algo_xyz_data_ex *indata, struct rs_loc3d_algo_xyz_data_ex *res);
	int run_sort(struct clst_param *para, struct rs_loc3d_algo_xyz_data_ex *indata);
	
	rs_clst_inf clst_inf[MAX_CLST];
	int wp;

};

#endif
