/*
 * Copyright (c) 2018 Socionext Inc.
 * All rights reserved.
 */

#ifndef RS_LOC3D_PLATFORM_H
#define RS_LOC3D_PLATFORM_H

#if defined(_WIN32)
#  define RS_APICALL __declspec(dllimport)
#elif defined(__GNUC__)
#  define RS_APICALL __attribute__((visibility("default")))
#else
#  define RS_APICALL
#endif

#if defined(_WIN32)
//#  define RS_APIENTRY __stdcall
#  define RS_APIENTRY __cdecl
#else
#  define RS_APIENTRY
#endif

#ifndef RS_LOC3D_APICALL
#define RS_LOC3D_APICALL RS_APICALL
#endif

#ifndef RS_LOC3D_APIENTRY
#define RS_LOC3D_APIENTRY RS_APIENTRY
#endif

#if defined(_WIN32) && defined(_MSC_VER)

typedef signed __int8 rs_int8_t;
typedef unsigned __int8 rs_uint8_t;
typedef signed __int16 rs_int16_t;
typedef unsigned __int16 rs_uint16_t;
typedef signed __int32 rs_int32_t;
typedef unsigned __int32 rs_uint32_t;

#else /* defined(_WIN32) && defined(_MSC_VER) */

#include <stdint.h>

typedef int8_t rs_int8_t;
typedef uint8_t rs_uint8_t;
typedef int16_t rs_int16_t;
typedef uint16_t rs_uint16_t;
typedef int32_t rs_int32_t;
typedef uint32_t rs_uint32_t;

#endif /* defined(_WIN32) && defined(_MSC_VER) */

typedef rs_int32_t rs_int;
typedef float rs_float;

#endif /* RS_LOC3D_PLATFORM_H */
