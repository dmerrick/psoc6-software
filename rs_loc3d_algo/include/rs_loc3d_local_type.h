/*
 * Copyright (c) 2018 Socionext Inc.
 * All rights reserved.
 */

#ifndef RS_LOC3D_LOCAL_TYPE_H
#define RS_LOC3D_LOCAL_TYPE_H

#include <rs_loc3d_platform.h>

/*
 * Structure Definition
 */
struct sensor_param
{
	rs_int    apn[4 * 8]; // 4 chirp * 8 repeat

	rs_int    chirp_subs_en; // obsolete, has been merged to apn

	rs_int    lowp_mode;
	
	rs_int    mimo_en[2];
};

struct app_param
{
	rs_int    FFTNum;
	rs_int    FFTWin;
	rs_int    DcCutEn;
	rs_int    Epair;
	rs_int    Hpair;

	rs_float  MaxDist;
	rs_float  MinDist;
	rs_int    bg_elim_interval;
	rs_int    frame_rate_deci;
	rs_float  alpha_r;
	rs_float  alpha_a;

	rs_int    fMagMode;
	rs_float  fMagTh_r;
	rs_float  fMagTh_r2;
	rs_float  fMagTh_a;

	rs_float  fMagTh_TopDiff;
	rs_int    PkDetMode_r;
	rs_int    PkDetDist;
	rs_int    PkSortMode_r;
	rs_int    ObjNum_r;

	rs_int    HeatMapOp;
	rs_int    HeatMapCalMode;
	rs_float  AngValidDist;

	rs_float  RWeiVal;

	rs_float  Pk2PkMinDist;
	rs_float  Pk2PkMaxDist;

	rs_float  AngInPhaseRotH;
	rs_float  AngInPhaseRotE;
	rs_float  ant_space_e;
	rs_float  ant_space_h;
	rs_int    AngE_Cmp_en;
	rs_int    AngH_Cmp_en;

	// added in v1.5
	rs_int    RWeiLutEn;
	rs_int    adp_alpha_r_en;
	rs_float  adp_alpha_r_th;
	rs_float  adp_alpha_r_max;
	rs_float  adp_alpha_r_min;
	rs_float  adp_alpha_r_step;
	rs_int    PkDetAddBin;
	rs_int    ang_calc_en;
	rs_int    ang_Ng;
	rs_float  AWeiVal;
	rs_int    out_of_fov_check;
	rs_int    ang_r_e;
	rs_float  ang_r_dist_e;
	rs_int    ang_r_frm_e;
	rs_int    ang_r_h;
	rs_float  ang_r_dist_h;
	rs_int    ang_r_frm_h;
	rs_float  alpha_a_e;
	rs_float  alpha_a_h;
	rs_int    ang_min_e;
	rs_int    ang_max_e;
	rs_int    ang_min_h;
	rs_int    ang_max_h;
	rs_int    outof_min_ang_restriction;
	rs_int    outof_max_ang_restriction;
	rs_int    restriction1_r_pk_width;
	rs_float  restriction1_r_pk_pow;
	rs_int    restriction2_r_pk_width;
	rs_float  restriction2_r_pk_pow;
	rs_int    cfar;

	// added in v2.0
	rs_float  alpha_r2;
	// added in v2.1
	rs_float  tx2_scale_fct;
	rs_float  ang_r_pow_th;
	rs_float  ang_r_coeff;
	
	// reserve area
	rs_float  reserve_f0;
	rs_float  reserve_f1;
	rs_float  reserve_f2;
	rs_float  reserve_f3;
};

struct bitmap_param
{
	rs_int    PixelSize;
	rs_int    axis_len;
	rs_int    LRInverse;
	rs_int    center_pos;
	rs_int    modified_pos_en;

	rs_float  spc_log_min;
	rs_float  spc_log_max;
	rs_float  spc_min_th;
	rs_float  spc_max_th;
	rs_float  spc_log_valid_range;
	rs_float  angle_min_th;
	rs_float  angle_time_weight;
	rs_int    a_index_range;
	rs_int    a_filter_en;
	rs_int    a_filter_all_en;
	rs_int    r_filter_en;
	rs_int    chirp_window_max_sel;
	rs_int    chirp_window_ave_num;
	rs_float  ang_behind;
	rs_float  ang_behind_dist;
	rs_int    min_ang;
	rs_int    max_ang;
	rs_int    r_weight_en;
	rs_int    normalize_en;
	rs_float  r_wei_val;
	rs_float  a_wei_val;
	rs_int    r_decimation;
	rs_int    a_decimation;
	rs_int    r_valid_diff_en;
	rs_float  r_pk_diff_wei;
	rs_float  a_pk_diff_wei;

	rs_int    outof_min_ang_restriction;
	rs_int    outof_max_ang_restriction;

	rs_int    restriction1_r_pk_width;
	rs_float  restriction1_r_pk_pow;
	rs_int    restriction2_r_pk_width;
	rs_float  restriction2_r_pk_pow;
};


#endif /* RS_LOC3D_LOCAL_TYPE_H */
