/*
 * Copyright (c) 2018 Socionext Inc.
 * All rights reserved.
 */

#ifndef RS_LOC3D_TOP_H
#define RS_LOC3D_TOP_H

#include "rs_ctl.h"
#include "rs_ctl_local.h"
#include "rs_loc3d.h"
#include "rs_loc3d_algo_local.h"
#include "init_param.h"

#define MAX_APN     (1)
#define MAX_CHIRP   (2)

#define RS_GET_TIMEOUT   (0)
#define FNUM             (1)

rs_int algo_init(int n_chirp, snsr_param *sens_param);
rs_int algo_run(uint8_t *data, int rs_counter);
rs_int algo_get_xyz_data(rs_int apn, struct rs_xyz_data *data);
rs_int algo_get_xyz_data_clst(rs_int apn, struct rs_xyz_data *data);
rs_int algo_get_dist_data(rs_int apn, struct rs_dist_data *p_dist_data);
rs_int rs_loc3d_get_r_spc(rs_int apn, struct rs_r_spc *spc);

int algo_setup_seqcode(rs_handle_t handle, uint8_t *seqcode, int setup_en, int hwfft_en);
rs_int algo_test_write(uint16_t addr, uint8_t *data, size_t size);
rs_int algo_test_read(uint16_t addr, uint8_t *data, size_t *size);

extern uint8_t fdata[8192];
extern struct snsr_param sens_param[MAX_CHIRP];
#endif /* RS_LOC3D_TOP_H */
