
#ifndef INIT_PARAM_H
#define INIT_PARAM_H

int set_init_app_param(int mode, int apn, struct app_param *p);
int set_init_bmp_param(int mode, int apn, struct bitmap_param *p);
int set_init_clst_param(int mode, int apn, struct clst_param *p);
int load_seq_code(uint8_t *buf, int *size);

int set_init_app_param_hwfft(int mode, int apn, struct app_param *p);
int set_init_bmp_param_hwfft(int mode, int apn, struct bitmap_param *p);
int set_init_clst_param_hwfft(int mode, int apn, struct clst_param *p);
int load_seq_code_hwfft(uint8_t *buf, int *size);

#endif
