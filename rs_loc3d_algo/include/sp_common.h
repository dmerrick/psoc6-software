/*
 * Copyright (c) 2017 Socionext Inc.
 * All rights reserved.
 */

/*
 * common.h
 */

#ifndef __COMMON__
#define __COMMON__

#include "sp_param.h"

typedef unsigned long DWORD;

#ifndef INT_MAX
#define INT_MAX 0x7fffffff
#endif

#define Err_MSG 0
#define WAR_MSG 1
#define INF_MSG 2
#define DVL_MSG 3
#define DBG_MSG 4

#define DB_V  (20.0f)

void dprintf( const char *fmt, ... );
srsl::fp CalcNoiseLevel_of_fdata(int nfft, srsl::fp *fdata);
srsl::fp SP_LOG10(srsl::fp in);



#endif /* __COMMON__ */
