/*
 * Copyright (c) 2016-2021 Socionext Inc.
 * All rights reserved.
 */
#ifndef __SP_MAIN_H__
#define __SP_MAIN_H__

#include <stdint.h>
#include <srsl_type.h>
#include <srsl_matrix.h>
#include "rs_loc3d_algo.h"
#include "detect_distance.h"
#include "WindowFunction.h"
#include "ComplexFFT.h"
#include "Capon.h"
#include "sp_param.h"
#include "algo_measure.h"
#include "r_wei_lut.h"
#include "clustering.h"

#define FBGNR_EN (1)
#define DETECT_2D_POW(x,y) srsl::pow(x,y)

//------------------------------------------------------
#if SMALL_A_RSS==1
struct a_Rss_info
{
	bool            en;
	srsl::complexfp Rss[MAX_RCH_N * MAX_RCH_N];
	short           bin;
	uint8_t         frm;
};
#endif
//------------------------------------------------------
class C_2D_detect
{
public:
	C_2D_detect();

private:
	void angle_axis_init(void);
	srsl::fp GetPeakIndexFmcw(int pkdetmode,
	                          int pksortmode,
	                          srsl::fp* fdata,
	                          srsl::fp* cfar_th,
	                          int dataSize,
	                          int *iIndexNum,
	                          int *iIndexs,
	                          int iIndexMaxNum,
	                          int dThMode,
	                          srsl::fp dThreshold,
	                          srsl::fp dThreshold2,
	                          int iStIndex,
	                          int iEdIndex,
	                          int *ShortIndex,
	                          int *MaxpowIndex,
	                          int *MaxpowIndex_en,
	                          srsl::fp *DistWidth,
	                          srsl::fp *PowDegree,
	                          int *PkIndexNum);

public:
#if TARGET_MCU==0
	int sp_run(srsl::vector< srsl::complex<int> > in[MAX_RCH_N],
#else
	int sp_run(srsl::complex<short> in[MAX_RCH_N][MAX_IQIN_SIZE],
#endif
               int objnum,
               int *rxchmap,
               int snum,
               int fft_en,
               int txch
	           );

public:
	int SetParam(const proc_param *param_in);

private:
	void Init(void);

	int GetEdIndex(void);

	int GetDist2Index(srsl::fp dist);
	int GetDist2IndexNum(srsl::fp dist);

	int GetStIndex(void);

	int AspcCovexIndexSearch(int phase, srsl::fp *indat, int st, int ed, int deci, int max_pk_n, int *res);

	C_Capon Capon[D3D];       // angle estimation class
	
	int Epair;
	int Hpair;
	int dccuten;
	srsl::fp MaxDist;
	srsl::fp MinDist;
	int EdIndex;
	int StIndex;
	int AngR_Cnt;
	int frm;
	int frm_counter_256;
	int txch2_det;

	int pre_r_pk_num;
	srsl::fp a_A_spc_max;
	int max_idxnum;
	int min_idxnum;
	srsl::fp r_wei[MAX_SIZE_R];
	int r_pk_mindist_index;
	int pre_r_pk_indexs[PEAK_N_R];
	srsl::fp a_wei[MAX_SIZE_A];

	srsl::complexfp R_out_pre[MAX_FRM_N][MAX_RCH_N][MAX_HSIZE_R]; // complex data 1d fft
	bool R_out_pre_valid[MAX_FRM_N];
	srsl::complexfp BGNR_data[MAX_TCH_N][MAX_RCH_N][MAX_HSIZE_R];            // complex data after BGNR
	srsl::fp a_R_spc[MAX_SIZE_R];                                 // 1D fft spectrum
	srsl::fp distance[PEAK_N_R];
public:
	C_detect_distance det_r[1];                           // distance calc class

	const proc_param *param;
	int apn;
	int nfft;
	int nang;
	uint32_t frm_counter_i;
	int r_pk_num;
	int r_pk_num_for_ang;
	
#if SMALL_A_RSS==0
	srsl::complexfp a_Rss[D3D][MAX_HSIZE_R * MAX_RCH_N * MAX_RCH_N];
#else
	a_Rss_info a_Rss_inf[D3D][MAX_A_RSS_N];
#endif
	srsl::complexfp sa_Rss[MAX_RCH_N * MAX_RCH_N];
	srsl::fp A_spc[D3D][MAX_SIZE_A];

	srsl::fp ***a_A_spc_all = NULL;        // angle spectrum (all range)

	srsl::fp angle_axis_c[MAX_SIZE_A];
	srsl::fp a_R_spc_tmp[MAX_SIZE_R];
	srsl::fp R_spc_threshold;
	srsl::fp cfar_th[MAX_SIZE_R];

	int r_pk_indexs[PEAK_N_R];
	srsl::fp ix2dist[MAX_SIZE_R];

	bitmap_param *bmp_param;
	st_pk_2dpos_info pki2[D3D];

	int a_valid_r_index;

	int r_pk_maxpow_index;
	int r_pk_maxpow_index_en;

	srsl::fp r_pk_dist_width;
	srsl::fp r_pk_pow_degree;
	int r_pk_num_detmode2;

#if RS_LOC3D_ALGO_USE_STOPWATCH
private:
	algo_measure *measure;
public:
	void set_measure(algo_measure *p) { measure = p; }
#endif /* RS_LOC3D_ALGO_USE_STOPWATCH */
	
	srsl::fp pre_alpha_r;
	st_ang_r_info ang_r_inf[D3D][256];
	int ang_r_dist_e;
	int ang_r_dist_h;
	srsl::fp a_A_spc[D3D][PEAK_N_R][181];
	
	srsl::fp get_cfar(int mode, int gd, int m, srsl::fp* fdata, int index, int datasize);

	C_clustering clst;
	struct rs_loc3d_algo_xyz_data_ex xyz_data;
	struct rs_loc3d_algo_xyz_data_ex xyz_data_clst;
	struct rs_loc3d_algo_xyz_data_ex xyz_data_hold_req;
	int calc_xyz(rs_float r, rs_float ang_h, rs_float ang_e, rs_float *x, rs_float *y, rs_float *z);
	void get_xyz_data(rs_loc3d_algo_xyz_data_ex *data);
	void get_xyz_data_wpara(struct clst_param *para, rs_loc3d_algo_xyz_data_ex *data);
	
	srsl::fp r_wei_table[128];
	
	int hold_en;
	int hold_count;
	int hold_index;
};

#endif
