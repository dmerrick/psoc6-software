/*
 * Copyright (c) 2016-2021 Socionext Inc.
 * All rights reserved.
 */

#ifndef RS_LOC3D_ALGO_H
#define RS_LOC3D_ALGO_H

#include <stdlib.h>
#include "rs_loc3d_platform.h"
#include "rs_loc3d_local_type.h"
#include "rs_loc3d_algo_local.h"

#define RS_LOC3D_ALGO_APICALL
#define RS_LOC3D_ALGO_APIENTRY

#ifdef __cplusplus
extern "C" {
#endif

/*
 * Type Definition
 */
typedef struct rs_loc3d_algo_handle *rs_loc3d_algo_handle_t;

/*
 * Constant Definition
 */
#define RS_LOC_ALGO_OK                   0	// Success
#define RS_LOC_ALGO_EINV                -1	// Invalid argument 
#define RS_LOC_ALGO_ETOUT               -2	// Time out 
#define RS_LOC_ALGO_EDEV                -3	// Device error 
#define RS_LOC_ALGO_ENOSUPPORT          -4	// Not supported 
#define RS_LOC_ALGO_ESTATE              -5	// State error 
#define RS_LOC_ALGO_ECOM                -6	// UART/SPI open error
#define RS_LOC_ALGO_ENOMEM              -7	// not enough space
#define RS_LOC_ALGO_EFAULT              -8	// Bad address or internal error
#define RS_LOC_ALGO_reserved            -9	// reserved

/*
 * Struct Definition
 */
struct rs_loc3d_algo_hmp_data
{
	rs_uint32_t pixel_size;
	rs_uint8_t data_e[256][256];
	rs_uint8_t data_h[256][256];
};

struct rs_loc3d_algo_hmp_data_ref
{
	rs_uint32_t pixel_size;
	rs_uint8_t (*data_e)[256][256];
	rs_uint8_t (*data_h)[256][256];
};

struct rs_loc3d_algo_loc_base
{
	rs_int32_t h;
	rs_int32_t w;
	rs_int32_t r;
	rs_int32_t a;
};

struct rs_loc3d_algo_loc_data
{
	rs_uint32_t num_e;
	struct rs_loc3d_algo_loc_base loc_e[32];
	rs_uint32_t num_h;
	struct rs_loc3d_algo_loc_base loc_h[32];
};

struct rs_loc3d_algo_dist_data
{
	rs_uint32_t num;
	rs_int32_t r[32];
};

struct rs_loc3d_algo_r_spc
{
	rs_uint32_t fftsize;
	rs_float spc[2048];
	rs_float threshold;
};

struct rs_loc3d_algo_r_spc_ex
{
	rs_uint32_t fftsize;
	rs_float spc[512];
	rs_float threshold;
	rs_float cfar_th[512];
	rs_int   cfar_en;
};

struct rs_loc3d_algo_r_spc2
{
	rs_uint32_t fftsize;
	rs_float spc[256];
	rs_float threshold;
};

struct rs_loc3d_algo_r_spc2_ex
{
	rs_uint32_t fftsize;
	rs_float spc[256];
	rs_float threshold;
	rs_float cfar_th[256];
	rs_int   cfar_en;
};

struct rs_loc3d_algo_r_dat
{
	rs_uint32_t fftsize;
	rs_float real[2048];
	rs_float imag[2048];
	int      pk_n;
	int      pk_index[32];
};

struct rs_loc3d_algo_r_pk
{
	int      pk_n;
	int      pk_index[32];
};

struct rs_loc3d_algo_a_spc
{
	rs_int   valid;
	rs_int   r_index;
	rs_float spc[181];
};

struct rs_loc3d_algo_xyz_base {
	rs_int32_t x;
	rs_int32_t y;
	rs_int32_t z;
};
struct rs_loc3d_algo_xyz_base_ex {
	rs_int32_t x;
	rs_int32_t y;
	rs_int32_t z;
	rs_int32_t r_index;
	rs_int32_t rep_r_index;
	rs_int32_t r_index_dummy_en;
	rs_int32_t a_e;
	rs_int32_t a_h;
	rs_float   pow;
	rs_int32_t smp_n;
	rs_int32_t life;
	rs_int32_t a_e_real;
};

struct rs_loc3d_algo_xyz_data {
	rs_uint32_t num;
	struct rs_loc3d_algo_xyz_base pos[32];
};

struct rs_loc3d_algo_xyz_data_ex {
	rs_uint32_t num;
	struct rs_loc3d_algo_xyz_base_ex pos[32];
};

#define RS_LOC3D_ALGO_MAX_TIME_INFO 16

struct rs_loc3d_algo_time_info
{
	rs_uint32_t elapsed[RS_LOC3D_ALGO_MAX_TIME_INFO];
};

/*
 * Function Definition
 */
RS_LOC3D_ALGO_APICALL rs_loc3d_algo_handle_t RS_LOC3D_ALGO_APIENTRY rs_loc3d_algo_create(int apn);
RS_LOC3D_ALGO_APICALL int RS_LOC3D_ALGO_APIENTRY rs_loc3d_algo_release(rs_loc3d_algo_handle_t handle);

RS_LOC3D_ALGO_APICALL int RS_LOC3D_ALGO_APIENTRY rs_loc3d_algo_set_snsr_param(rs_loc3d_algo_handle_t handle, struct snsr_param *param);
RS_LOC3D_ALGO_APICALL int RS_LOC3D_ALGO_APIENTRY rs_loc3d_algo_set_app_param(rs_loc3d_algo_handle_t handle, struct app_param *param);
RS_LOC3D_ALGO_APICALL int RS_LOC3D_ALGO_APIENTRY rs_loc3d_algo_set_clst_param(rs_loc3d_algo_handle_t handle, struct clst_param *param);
RS_LOC3D_ALGO_APICALL int RS_LOC3D_ALGO_APIENTRY rs_loc3d_algo_set_bitmap_param(rs_loc3d_algo_handle_t handle, struct bitmap_param *param);
RS_LOC3D_ALGO_APICALL int RS_LOC3D_ALGO_APIENTRY rs_loc3d_algo_set_mimo_en(rs_loc3d_algo_handle_t handle, rs_int mimo_en);
RS_LOC3D_ALGO_APICALL int RS_LOC3D_ALGO_APIENTRY rs_loc3d_algo_set_r_wei_table(rs_loc3d_algo_handle_t handle, rs_float *buf);
RS_LOC3D_ALGO_APICALL int RS_LOC3D_ALGO_APIENTRY rs_loc3d_algo_setup(rs_loc3d_algo_handle_t handle);

RS_LOC3D_ALGO_APICALL int RS_LOC3D_ALGO_APIENTRY rs_loc3d_algo_exec(rs_loc3d_algo_handle_t handle, size_t framesize, unsigned char *framedata, uint8_t txch);

RS_LOC3D_ALGO_APICALL int RS_LOC3D_ALGO_APIENTRY rs_loc3d_algo_get_hmp_data(rs_loc3d_algo_handle_t handle, struct rs_loc3d_algo_hmp_data *data);
RS_LOC3D_ALGO_APICALL int RS_LOC3D_ALGO_APIENTRY rs_loc3d_algo_get_hmp_data_ref(rs_loc3d_algo_handle_t handle, struct rs_loc3d_algo_hmp_data_ref *data);
RS_LOC3D_ALGO_APICALL int RS_LOC3D_ALGO_APIENTRY rs_loc3d_algo_get_loc_data(rs_loc3d_algo_handle_t handle, struct rs_loc3d_algo_loc_data *data);
RS_LOC3D_ALGO_APICALL int RS_LOC3D_ALGO_APIENTRY rs_loc3d_algo_get_dist_data(rs_loc3d_algo_handle_t handle, struct rs_loc3d_algo_dist_data *data);
RS_LOC3D_ALGO_APICALL int RS_LOC3D_ALGO_APIENTRY rs_loc3d_algo_get_r_spc(rs_loc3d_algo_handle_t handle, struct rs_loc3d_algo_r_spc *spc);
RS_LOC3D_ALGO_APICALL int RS_LOC3D_ALGO_APIENTRY rs_loc3d_algo_get_r_spc_ex(rs_loc3d_algo_handle_t handle, struct rs_loc3d_algo_r_spc_ex *spc);
RS_LOC3D_ALGO_APICALL int RS_LOC3D_ALGO_APIENTRY rs_loc3d_algo_get_r_dat(rs_loc3d_algo_handle_t handle, struct rs_loc3d_algo_r_dat *dat);
RS_LOC3D_ALGO_APICALL int RS_LOC3D_ALGO_APIENTRY rs_loc3d_algo_get_a_spc(rs_loc3d_algo_handle_t handle, int eh, int pk_n, struct rs_loc3d_algo_a_spc *spc);
RS_LOC3D_ALGO_APICALL int RS_LOC3D_ALGO_APIENTRY rs_loc3d_algo_get_r_pk(rs_loc3d_algo_handle_t handle, struct rs_loc3d_algo_r_pk *dat);
RS_LOC3D_ALGO_APICALL int RS_LOC3D_ALGO_APIENTRY rs_loc3d_algo_get_xyz_data(rs_loc3d_algo_handle_t handle, struct rs_loc3d_algo_xyz_data *data);
RS_LOC3D_ALGO_APICALL int RS_LOC3D_ALGO_APIENTRY rs_loc3d_algo_get_xyz_data_clst_ex(rs_loc3d_algo_handle_t handle, struct rs_loc3d_algo_xyz_data_ex *data);
RS_LOC3D_ALGO_APICALL int RS_LOC3D_ALGO_APIENTRY rs_loc3d_algo_get_xyz_data_clst(rs_loc3d_algo_handle_t handle, struct rs_loc3d_algo_xyz_data *data);

RS_LOC3D_ALGO_APICALL int RS_LOC3D_ALGO_APIENTRY rs_loc3d_algo_get_r_spc2(rs_loc3d_algo_handle_t handle, struct rs_loc3d_algo_r_spc2 *spc);
RS_LOC3D_ALGO_APICALL int RS_LOC3D_ALGO_APIENTRY rs_loc3d_algo_get_r_spc2_ex(rs_loc3d_algo_handle_t handle, struct rs_loc3d_algo_r_spc2_ex *spc);

RS_LOC3D_ALGO_APICALL int RS_LOC3D_ALGO_APIENTRY rs_loc3d_algo_get_time_info(rs_loc3d_algo_handle_t handle, struct rs_loc3d_algo_time_info *p_time_info);

	RS_LOC3D_ALGO_APICALL int RS_LOC3D_ALGO_APIENTRY rs_loc3d_algo_set_xyz_hold_info(rs_loc3d_algo_handle_t handle, struct rs_loc3d_algo_xyz_data_ex *data);

#ifdef __cplusplus
}
#endif

#endif /* RS_LOC3D_ALGO_H */
