/*
 * Copyright (c) 2016 Socionext Inc.
 * All rights reserved.
 */
#ifndef __DETECT_DISTANCE_H__
#define __DETECT_DISTANCE_H__

#include <stdlib.h>
#include <string.h>
#include <srsl_type.h>
#include "sp_param.h"
#include "WindowFunction.h"
#include "ComplexFFT.h"

class C_detect_distance
{
public:
	// *****************************************************************************
	// * Function : Constructor
	// *****************************************************************************
	C_detect_distance();

	// *****************************************************************************
	// * Function : Common parameters setting
	// *****************************************************************************
	int SetParam(const proc_param *param_in);
	// *****************************************************************************
	// * Function : Initialization
	// *****************************************************************************
	void Init(void);
	// *****************************************************************************
	// * Function : FFT
	// *****************************************************************************
	void FFT_run(const int dataSize, const srsl::complex<short>* input);
	// *****************************************************************************
	// * Function : Conversion from FFT index to frequency
	// *****************************************************************************
	srsl::fp GetDopplerFreq(int iIndex, int dataSize, srsl::fp dFs);
	// *****************************************************************************
	// * Function : Conversion from distance to FFT index
	// *****************************************************************************
	int GetDist2Index(srsl::fp dist); // Distance -> Index conversion
	// *****************************************************************************
	// * Function : Conversion from FFT index to distance
	// *****************************************************************************
	srsl::fp GetIndex2Dist(int index); // Index -> Distance conversion
	// *****************************************************************************
	// * Function : Finding distance unit for FFT index
	// *****************************************************************************
	srsl::fp GetR(void) const;

private:
	srsl::complexfp intmp[MAX_SIZE_R]; // Intermediate buffer before FFT

	//
	// angle parameter
	//
	const proc_param *param;
	int nfft;
	int fftwin;
	int dccuten;

	//
	// Distance - Index conversion parameter
	//
	srsl::fp gf;
	srsl::fp gR;
	srsl::fp gR_resolution;
	int gR_resolution_en;

	C_WindowFunction<MAX_SIZE_R> WindowFunction;

public:
	C_ComplexFFT<MAX_SIZE_R> CFFT;
};

#endif
