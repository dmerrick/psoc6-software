/*
 * Copyright (c) 2016-2021 Socionext Inc.
 * All rights reserved.
 */
#ifndef RS_LOC3D_ALGO_LOCAL_H
#define RS_LOC3D_ALGO_LOCAL_H

#include <rs_attr.h>

struct snsr_param
{
	uint32_t modu_mode;
	enum rs_tx_power tx_power;
	enum rs_rx_gain rx_gain;
	uint32_t frame_rate;
	uint32_t center_freq;
	uint32_t chirp_rate_constant;
	enum rs_bit_width bit_width;
	enum rs_header_mode header_mode;
	enum rs_ex_status_header_mode ex_status_header_mode;

	struct rs_chirp_fmcw rs_para;
	struct rs_chirp_fmcw_m rs_para_m;
	struct rs_chirp_fft fft;
	rs_fft_en fft_en;
};

#endif /* RS_LOC3D_ALGO_LOCAL_H */
