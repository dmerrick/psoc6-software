/*
 * Copyright (c) 2018 Socionext Inc.
 * All rights reserved.
 */

#ifndef RS_LOC3D_TYPE_H
#define RS_LOC3D_TYPE_H

#include <rs_loc3d_platform.h>

struct clst_param
{
	int grp_range_x_valid = 0;
	int grp_range_y_valid = 0;
	int grp_range_z_valid = 0;

	int grp_range_x_min;
	int grp_range_x_max;
	int grp_range_y_min;
	int grp_range_y_max;
	int grp_range_z_min;
	int grp_range_z_max;

	int frm_hist_n = 0;
	int grp_range = 0;
	int grp_smp_n = 0;

	int grp_2_en = 0;
	int grp_2_range = 0;
	int grp_2_ang = 0;
	
	int sort_mode = 0;

};


#endif /* RS_LOC3D_TYPE_H */
