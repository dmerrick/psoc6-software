/*
 * Copyright (c) 2016-2021 Socionext Inc.
 * All rights reserved.
 */
#ifndef __SP_PARAM_H__
#define __SP_PARAM_H__

#include <rs_loc3d_type.h>
#include <rs_loc3d_local_type.h>
#include <srsl_type.h>

#define RS_LOC3D_ALGO_USE_BMPMAP (0)
#define TARGET_MCU               (1)
#define SMALL_A_RSS              (1)
//------------------------------------------------------------------------------------------------

#define I_BIT                    (1)
#define Q_BIT                    (2)

//------------------------------------------------------------------------------------------------
// Size definition for signal processing
//------------------------------------------------------------------------------------------------
#if TARGET_MCU==0
#define MAX_IQIN_SIZE            (2048)
#define                          PEAK_N_R     (32)
#define                          MAX_FRM_N    (10)
#define pk_max                   (32)
#else
#define MAX_IQIN_SIZE            (256)
#define                          PEAK_N_R     (8)
#define                          MAX_FRM_N    (6)
#define pk_max                   (16)
#endif

#define MAX_SIZE_R               (256)
#define MAX_SIZE_A               (181)
#define MAX_HSIZE_R              (MAX_SIZE_R/2)
#define                          MAX_RCH_N    (4)
#define                          MAX_TCH_N    (2)
#define                          MAX_RPT_N    (8)
#define                          MAX_FRM_CNT  (0xffff)
#define                          MAX_A_RSS_N  (32)

#define                          D3D          (2)
#define                          ED           (0)
#define                          HD           (1)

#define                          MAX_IDIFF    (0)  // unit is cm
#define                          MIN_IDIFF    (50) // unit is cm
#define                          MIN_POWDIFF  (7)

#define                          MAX_APP_NUM  (3)

#define                          ANG_ALGO_FFT (0)
#define                          ANG_ALGO_CPN (1)

#define                          PK_HIST_MAX  (24)

//------------------------------------------------------------------------------------------------
// processing parameter structure
//------------------------------------------------------------------------------------------------

struct proc_param {
	srsl::fp       Fs;                   // sampling rate
	srsl::fp       SweepTime;            // unit is us
	srsl::fp       SweepFreq;            // unit is MHz

	int            chirp_subs_en = 0;
	int            mimo_en = 0;
	app_param      *app;
	clst_param     *clst;
	proc_param() : Fs(0.0f), SweepTime(0.0f), SweepFreq(0.0f), chirp_subs_en(0), mimo_en(0), app(0) {}
};



struct st_ang_r_info {
	short num;
	short r[pk_max];
	short a[pk_max];
	short pow[pk_max];
};
struct st_pk_2dpos_info {
	short num;
	short h[pk_max];
	short w[pk_max];
	short r[pk_max];
	short a[pk_max];
	short pow[pk_max];
	short a_real[pk_max];
};

//---------------------------------------------------------------------------
// bitmap parameter
//---------------------------------------------------------------------------
#define invalid_num     (10)
#define invalid_color   (127)

#define hist_depth      (32)
#define PK_LIST_ALL_N   (MAX_SIZE_R/2*2)


#endif
