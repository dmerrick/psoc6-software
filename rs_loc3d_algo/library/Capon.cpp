/*
 * Copyright (c) 2016-2021 Socionext Inc.
 * All rights reserved.
 */
#ifdef _MATH_FUNCTION_CALL_DEBUG
#include <math.h>
#endif
#include <stdio.h>
#include <srsl_math.h>
#include "Capon.h"

#ifdef _MATH_FUNCTION_CALL_DEBUG
#define COMPLEX_CAPON_SQRT(x) sqrt(x)
#define COMPLEX_CAPON_LOG10(x) log10(x)
#else
#define COMPLEX_CAPON_SQRT(x) srsl::sqrt(x)
#define COMPLEX_CAPON_LOG10(x) srsl::log10(x)
#endif

#define DETZEROTH (1e-30)

static const int e11 =  0; // 0 * 4 + 0
static const int e12 =  1; // 0 * 4 + 1
static const int e13 =  2; // 0 * 4 + 2
static const int e14 =  3; // 0 * 4 + 3
static const int e21 =  4; // 1 * 4 + 0
static const int e22 =  5; // 1 * 4 + 1
static const int e23 =  6; // 1 * 4 + 2
static const int e24 =  7; // 1 * 4 + 3
static const int e31 =  8; // 2 * 4 + 0
static const int e32 =  9; // 2 * 4 + 1
static const int e33 = 10; // 2 * 4 + 2
static const int e34 = 11; // 2 * 4 + 3
static const int e41 = 12; // 3 * 4 + 0
static const int e42 = 13; // 3 * 4 + 1
static const int e43 = 14; // 3 * 4 + 2
static const int e44 = 15; // 3 * 4 + 3
//
static const int s11 = 0;
static const int s12 = 1;
static const int s21 = 2;
static const int s22 = 3;
//
static const int s1 = 0;
static const int s2 = 1;
static const int s3 = 2;
static const int s4 = 3;
//
static const int a11 = 0;
static const int a21 = 1;
static const int a22 = 2;
static const int a31 = 3;
static const int a32 = 4;
static const int a33 = 5;
static const int a41 = 6;
static const int a42 = 7;
static const int a43 = 8;
static const int a44 = 9;
//
static const int b11 = 0;
static const int b21 = 1;
static const int b22 = 2;
//
static const int d1 = 0;
static const int d2 = 1;
static const int d3 = 2;
static const int d4 = 3;
static const int d5 = 4;
static const int d6 = 5;
//
static const int c11 = 0;
static const int c12 = 1;
static const int c13 = 2;
static const int c21 = 3;
static const int c22 = 4;
static const int c23 = 5;
static const int c31 = 6;
static const int c32 = 7;
static const int c33 = 8;
//
static const int w1 = 0;
static const int w2 = 1;
static const int w3 = 2;

C_Capon::C_Capon()
	: CaponNumAngle(-1)
	, CaponNumElem(-1)
	, AntPitch(2.4776e-3)
	, AsW(181, 10)
	, B(16)
	, NonZeroCounter(0)
{
}

void C_Capon::Init(int NARRAY, srsl::fp ant_pitch)
{
	AntPitch = ant_pitch;
	if (NARRAY == 4) Capon_4Ainit();
	if (NARRAY == 2) Capon_2Ainit();
}

void C_Capon::Capon_init(int NRX, srsl::fp ant_pitch)
{
	AntPitch = ant_pitch;
	if (false) {}
	else if (NRX == 4) { Capon_4Ainit(); }
	else if (NRX == 2) { Capon_2Ainit(); }
	else if (NRX == 3) { Capon_3Ainit(); }
}

void C_Capon::Run(
                  int min_ang, int max_ang,
                  int Ng, int NRmax, int NARRAY,
                  int nfft,
                  srsl::fp ALFA_A,
                  int index,
                  int index_pair,
                  srsl::vector<srsl::complexfp> &pks,
                  int numRpks,
                  srsl::complexfp *a_Rss,
                  srsl::complexfp *sa_Rss,
                  srsl::fp *A_spc)
{
	{

		srsl::vector<srsl::complexfp> Rss(NARRAY * NARRAY);
		Rss.zeros();

		srsl::vector<srsl::complexfp> PKx(NARRAY);
		srsl::vector<srsl::complexfp> PKy(NARRAY);
		srsl::vector<srsl::complexfp> PK(NARRAY * NARRAY);
		PKx.zeros();
		PKy.zeros();
		PK.zeros();
		for (int k = 0; k < numRpks; k++)
		{
			for (int ii = 0; ii < NARRAY; ii++)
			{
				PKx(ii).real = pks(ii * numRpks + k).real;
				PKx(ii).imag = pks(ii * numRpks + k).imag;
				PKy(ii).real = pks(ii * numRpks + k).real;
				PKy(ii).imag = -pks(ii * numRpks + k).imag;
			}
			mtx_CpxMlt(NARRAY, 1, NARRAY, PKx, PKy, PK);
			for (int ii = 0; ii < NARRAY * NARRAY; ii++)
			{
				Rss(ii).real += PK(ii).real;
				Rss(ii).imag += PK(ii).imag;
			}
		}


		for (int ii = 0; ii < NARRAY * NARRAY; ii++)
		{
			Rss(ii).real /= numRpks;
			Rss(ii).imag /= numRpks;
		}

		srsl::fp diagor = 0;
		srsl::fp diagoi = 0;
		for (int ii = 0; ii < NARRAY; ii++)
		{
			diagor += Rss(ii * NARRAY + ii).real;
			diagoi += Rss(ii * NARRAY + ii).imag;
		}
		diagor *= 0.001 / NARRAY;
		diagoi *= 0.001 / NARRAY;
		for (int ii = 0; ii < NARRAY; ii++)
		{
			Rss(ii * NARRAY + ii).real += diagor;
			Rss(ii * NARRAY + ii).imag += diagoi;
		}

		if(a_Rss) {
			int inq1 = index * NARRAY * NARRAY;
			int inq2 = index_pair * NARRAY * NARRAY;
			for (int ii = 0; ii < NARRAY * NARRAY; ii++)
			{
				a_Rss[inq1 + ii].real = a_Rss[inq2 + ii].real * ALFA_A + Rss(ii).real * (1 - ALFA_A);
				a_Rss[inq1 + ii].imag = a_Rss[inq2 + ii].imag * ALFA_A + Rss(ii).imag * (1 - ALFA_A);
			}
			
			for (int ii = 0; ii < NARRAY * NARRAY; ii++)
			{
				sa_Rss[ii].real = a_Rss[inq1 + ii].real;
				sa_Rss[ii].imag = a_Rss[inq1 + ii].imag;
			}
		}
		else {
			for (int ii = 0; ii < NARRAY * NARRAY; ii++)
			{
				sa_Rss[ii].real = sa_Rss[ii].real * ALFA_A + Rss(ii).real * (1 - ALFA_A);
				sa_Rss[ii].imag = sa_Rss[ii].imag * ALFA_A + Rss(ii).imag * (1 - ALFA_A);
			}
		}

		for (int ii = 0; ii < nfft; ii++)
		{
			A_spc[ii] = 0;
		}

		srsl::vector<bool> ang_en(max_ang + 1);
		ang_en.zeros();

		{
			if (NARRAY == 4)
			{
				Capon_4A(sa_Rss, A_spc, min_ang, max_ang, ang_en);
			}
			if (NARRAY == 2)
			{
				Capon_2A(sa_Rss, A_spc, min_ang, max_ang, ang_en);
			}
		}

		for (int n = 0; n < min_ang; n++)
		{
			A_spc[n] = A_spc[min_ang];
		}
		for (int n = max_ang + 1; n <= 180; n++)
		{
			A_spc[n] = A_spc[max_ang];
		}
	}
}

int C_Capon::Capon(int NRX, srsl::vector<srsl::complexfp>& R, srsl::vector<srsl::fp>& Awn)
{
	return -1;
}

/*******************************************************
*******************************************************/
void C_Capon::mtx_CpxMlt(int N, int M, int K, srsl::vector<srsl::complexfp> &A, srsl::vector<srsl::complexfp> &B, srsl::vector<srsl::complexfp> &C)
{
	srsl::fp cr, ci;

	int iM = 0, iK = 0;
	for (int i = 0; i < N; i++, iM += M, iK += K)
	{
		for (int k = 0; k < K; k++)
		{
			cr = 0.0;
			ci = 0.0;
			int jK = 0;
			for (int j = 0; j < M; j++, jK += K)
			{
				cr += A(iM + j).real * B(jK + k).real - A(iM + j).imag * B(jK + k).imag;
				ci += A(iM + j).real * B(jK + k).imag + A(iM + j).imag * B(jK + k).real;
			}
			C(iK + k).real = cr;
			C(iK + k).imag = ci;
		}
	}
}

/*******************************************************
*******************************************************/
void C_Capon::Capon_4Ainit(void)
{
	srsl::fp Lambda = 299792458 / 60.5e9;
	srsl::fp Fc = 2 * PI * AntPitch / Lambda;
	srsl::vector<srsl::complexfp> AS(4); // 4:NRX

	if (CaponNumElem != a44 + 1)
	{
		CaponNumAngle = 180 + 1;
		CaponNumElem = a44 + 1;
		AsW.resize(CaponNumAngle, CaponNumElem);
	}

	for (int n = 0; n <= 180; n++)
	{
		srsl::fp Sn = srsl::sin((n - 90) * PI / 180);
		for (int an = 0; an < 4; an++)
		{
			AS(an).real = srsl::cos(an * Fc * Sn);
			AS(an).imag = srsl::sin(an * Fc * Sn);
		}
		AsW(n, a11).real = AS(s1).real * AS(s1).real + AS(s1).imag * AS(s1).imag;
		AsW(n, a11).imag = 0;
		AsW(n, a21).real = AS(s2).real * AS(s1).real + AS(s2).imag * AS(s1).imag;
		AsW(n, a21).imag = AS(s2).real * AS(s1).imag - AS(s2).imag * AS(s1).real;
		AsW(n, a31).real = AS(s3).real * AS(s1).real + AS(s3).imag * AS(s1).imag;
		AsW(n, a31).imag = AS(s3).real * AS(s1).imag - AS(s3).imag * AS(s1).real;
		AsW(n, a41).real = AS(s4).real * AS(s1).real + AS(s4).imag * AS(s1).imag;
		AsW(n, a41).imag = AS(s4).real * AS(s1).imag - AS(s4).imag * AS(s1).real;

		AsW(n, a22).real = AS(s2).real * AS(s2).real + AS(s2).imag * AS(s2).imag;
		AsW(n, a22).imag = 0;
		AsW(n, a32).real = AS(s3).real * AS(s2).real + AS(s3).imag * AS(s2).imag;
		AsW(n, a32).imag = AS(s3).real * AS(s2).imag - AS(s3).imag * AS(s2).real;
		AsW(n, a42).real = AS(s4).real * AS(s2).real + AS(s4).imag * AS(s2).imag;
		AsW(n, a42).imag = AS(s4).real * AS(s2).imag - AS(s4).imag * AS(s2).real;

		AsW(n, a33).real = AS(s3).real * AS(s3).real + AS(s3).imag * AS(s3).imag;
		AsW(n, a33).imag = 0;
		AsW(n, a43).real = AS(s4).real * AS(s3).real + AS(s4).imag * AS(s3).imag;
		AsW(n, a43).imag = AS(s4).real * AS(s3).imag - AS(s4).imag * AS(s3).real;

		AsW(n, a44).real = AS(s4).real * AS(s4).real + AS(s4).imag * AS(s4).imag;
		AsW(n, a44).imag = 0;
	}
}

int C_Capon::Capon_4A(srsl::complexfp *R, srsl::fp *Awn, int min_ang, int max_ang, srsl::vector<bool> &ang_en, int scale)
{
	srsl::vector<srsl::complexfp> D(16);
	srsl::vector<srsl::complexfp> sD(6);
	double DETre, DETim;
	double DETn2;

	sD(d1).real = (R[e33].real * R[e44].real - R[e33].imag * R[e44].imag ) - (R[e34].real * R[e43].real - R[e34].imag * R[e43].imag );
	sD(d1).imag = (R[e33].real * R[e44].imag + R[e33].imag * R[e44].real ) - (R[e34].real * R[e43].imag + R[e34].imag * R[e43].real );
	sD(d2).real = (R[e32].real * R[e44].real - R[e32].imag * R[e44].imag ) - (R[e34].real * R[e42].real - R[e34].imag * R[e42].imag );
	sD(d2).imag = (R[e32].real * R[e44].imag + R[e32].imag * R[e44].real ) - (R[e34].real * R[e42].imag + R[e34].imag * R[e42].real );
	sD(d3).real = (R[e34].real * R[e41].real - R[e34].imag * R[e41].imag ) - (R[e31].real * R[e44].real - R[e31].imag * R[e44].imag );
	sD(d3).imag = (R[e34].real * R[e41].imag + R[e34].imag * R[e41].real ) - (R[e31].real * R[e44].imag + R[e31].imag * R[e44].real );
	sD(d4).real = (R[e32].real * R[e43].real - R[e32].imag * R[e43].imag ) - (R[e33].real * R[e42].real - R[e33].imag * R[e42].imag );
	sD(d4).imag = (R[e32].real * R[e43].imag + R[e32].imag * R[e43].real ) - (R[e33].real * R[e42].imag + R[e33].imag * R[e42].real );
	sD(d5).real = (R[e31].real * R[e43].real - R[e31].imag * R[e43].imag ) - (R[e33].real * R[e41].real - R[e33].imag * R[e41].imag );
	sD(d5).imag = (R[e31].real * R[e43].imag + R[e31].imag * R[e43].real ) - (R[e33].real * R[e41].imag + R[e33].imag * R[e41].real );
	sD(d6).real = (R[e31].real * R[e42].real - R[e31].imag * R[e42].imag ) - (R[e32].real * R[e41].real - R[e32].imag * R[e41].imag );
	sD(d6).imag = (R[e31].real * R[e42].imag + R[e31].imag * R[e42].real ) - (R[e32].real * R[e41].imag + R[e32].imag * R[e41].real );

	D(e11).real = (R[e22].real * sD(d1).real - R[e22].imag * sD(d1).imag) - (R[e23].real * sD(d2).real - R[e23].imag * sD(d2).imag) + (R[e24].real * sD(d4).real - R[e24].imag * sD(d4).imag);
	D(e11).imag = (R[e22].real * sD(d1).imag + R[e22].imag * sD(d1).real) - (R[e23].real * sD(d2).imag + R[e23].imag * sD(d2).real) + (R[e24].real * sD(d4).imag + R[e24].imag * sD(d4).real);
	D(e12).real = (R[e21].real * sD(d1).real - R[e21].imag * sD(d1).imag) + (R[e23].real * sD(d3).real - R[e23].imag * sD(d3).imag) + (R[e24].real * sD(d5).real - R[e24].imag * sD(d5).imag);
	D(e12).imag = (R[e21].real * sD(d1).imag + R[e21].imag * sD(d1).real) + (R[e23].real * sD(d3).imag + R[e23].imag * sD(d3).real) + (R[e24].real * sD(d5).imag + R[e24].imag * sD(d5).real);
	D(e13).real = (R[e21].real * sD(d2).real - R[e21].imag * sD(d2).imag) + (R[e22].real * sD(d3).real - R[e22].imag * sD(d3).imag) + (R[e24].real * sD(d6).real - R[e24].imag * sD(d6).imag);
	D(e13).imag = (R[e21].real * sD(d2).imag + R[e21].imag * sD(d2).real) + (R[e22].real * sD(d3).imag + R[e22].imag * sD(d3).real) + (R[e24].real * sD(d6).imag + R[e24].imag * sD(d6).real);
	D(e14).real = (R[e21].real * sD(d4).real - R[e21].imag * sD(d4).imag) - (R[e22].real * sD(d5).real - R[e22].imag * sD(d5).imag) + (R[e23].real * sD(d6).real - R[e23].imag * sD(d6).imag);
	D(e14).imag = (R[e21].real * sD(d4).imag + R[e21].imag * sD(d4).real) - (R[e22].real * sD(d5).imag + R[e22].imag * sD(d5).real) + (R[e23].real * sD(d6).imag + R[e23].imag * sD(d6).real);

	D(e21).real = (R[e12].real * sD(d1).real - R[e12].imag * sD(d1).imag) - (R[e13].real * sD(d2).real - R[e13].imag * sD(d2).imag) + (R[e14].real * sD(d4).real - R[e14].imag * sD(d4).imag);
	D(e21).imag = (R[e12].real * sD(d1).imag + R[e12].imag * sD(d1).real) - (R[e13].real * sD(d2).imag + R[e13].imag * sD(d2).real) + (R[e14].real * sD(d4).imag + R[e14].imag * sD(d4).real);
	D(e22).real = (R[e11].real * sD(d1).real - R[e11].imag * sD(d1).imag) + (R[e13].real * sD(d3).real - R[e13].imag * sD(d3).imag) + (R[e14].real * sD(d5).real - R[e14].imag * sD(d5).imag);
	D(e22).imag = (R[e11].real * sD(d1).imag + R[e11].imag * sD(d1).real) + (R[e13].real * sD(d3).imag + R[e13].imag * sD(d3).real) + (R[e14].real * sD(d5).imag + R[e14].imag * sD(d5).real);
	D(e23).real = (R[e11].real * sD(d2).real - R[e11].imag * sD(d2).imag) + (R[e12].real * sD(d3).real - R[e12].imag * sD(d3).imag) + (R[e14].real * sD(d6).real - R[e14].imag * sD(d6).imag);
	D(e23).imag = (R[e11].real * sD(d2).imag + R[e11].imag * sD(d2).real) + (R[e12].real * sD(d3).imag + R[e12].imag * sD(d3).real) + (R[e14].real * sD(d6).imag + R[e14].imag * sD(d6).real);
	D(e24).real = (R[e11].real * sD(d4).real - R[e11].imag * sD(d4).imag) - (R[e12].real * sD(d5).real - R[e12].imag * sD(d5).imag) + (R[e13].real * sD(d6).real - R[e13].imag * sD(d6).imag);
	D(e24).imag = (R[e11].real * sD(d4).imag + R[e11].imag * sD(d4).real) - (R[e12].real * sD(d5).imag + R[e12].imag * sD(d5).real) + (R[e13].real * sD(d6).imag + R[e13].imag * sD(d6).real);

	sD(d1).real = (R[e11].real * R[e22].real - R[e11].imag * R[e22].imag ) - (R[e12].real * R[e21].real - R[e12].imag * R[e21].imag );
	sD(d1).imag = (R[e11].real * R[e22].imag + R[e11].imag * R[e22].real ) - (R[e12].real * R[e21].imag + R[e12].imag * R[e21].real );
	sD(d2).real = (R[e11].real * R[e23].real - R[e11].imag * R[e23].imag ) - (R[e13].real * R[e21].real - R[e13].imag * R[e21].imag );
	sD(d2).imag = (R[e11].real * R[e23].imag + R[e11].imag * R[e23].real ) - (R[e13].real * R[e21].imag + R[e13].imag * R[e21].real );
	sD(d3).real = (R[e14].real * R[e21].real - R[e14].imag * R[e21].imag ) - (R[e11].real * R[e24].real - R[e11].imag * R[e24].imag );
	sD(d3).imag = (R[e14].real * R[e21].imag + R[e14].imag * R[e21].real ) - (R[e11].real * R[e24].imag + R[e11].imag * R[e24].real );
	sD(d4).real = (R[e12].real * R[e23].real - R[e12].imag * R[e23].imag ) - (R[e13].real * R[e22].real - R[e13].imag * R[e22].imag );
	sD(d4).imag = (R[e12].real * R[e23].imag + R[e12].imag * R[e23].real ) - (R[e13].real * R[e22].imag + R[e13].imag * R[e22].real );
	sD(d5).real = (R[e12].real * R[e24].real - R[e12].imag * R[e24].imag ) - (R[e14].real * R[e22].real - R[e14].imag * R[e22].imag );
	sD(d5).imag = (R[e12].real * R[e24].imag + R[e12].imag * R[e24].real ) - (R[e14].real * R[e22].imag + R[e14].imag * R[e22].real );
	sD(d6).real = (R[e13].real * R[e24].real - R[e13].imag * R[e24].imag ) - (R[e14].real * R[e23].real - R[e14].imag * R[e23].imag );
	sD(d6).imag = (R[e13].real * R[e24].imag + R[e13].imag * R[e24].real ) - (R[e14].real * R[e23].imag + R[e14].imag * R[e23].real );

	D(e31).real = (R[e42].real * sD(d6).real - R[e42].imag * sD(d6).imag) - (R[e43].real * sD(d5).real - R[e43].imag * sD(d5).imag) + (R[e44].real * sD(d4).real - R[e44].imag * sD(d4).imag);
	D(e31).imag = (R[e42].real * sD(d6).imag + R[e42].imag * sD(d6).real) - (R[e43].real * sD(d5).imag + R[e43].imag * sD(d5).real) + (R[e44].real * sD(d4).imag + R[e44].imag * sD(d4).real);
	D(e32).real = (R[e41].real * sD(d6).real - R[e41].imag * sD(d6).imag) + (R[e43].real * sD(d3).real - R[e43].imag * sD(d3).imag) + (R[e44].real * sD(d2).real - R[e44].imag * sD(d2).imag);
	D(e32).imag = (R[e41].real * sD(d6).imag + R[e41].imag * sD(d6).real) + (R[e43].real * sD(d3).imag + R[e43].imag * sD(d3).real) + (R[e44].real * sD(d2).imag + R[e44].imag * sD(d2).real);
	D(e33).real = (R[e41].real * sD(d5).real - R[e41].imag * sD(d5).imag) + (R[e42].real * sD(d3).real - R[e42].imag * sD(d3).imag) + (R[e44].real * sD(d1).real - R[e44].imag * sD(d1).imag);
	D(e33).imag = (R[e41].real * sD(d5).imag + R[e41].imag * sD(d5).real) + (R[e42].real * sD(d3).imag + R[e42].imag * sD(d3).real) + (R[e44].real * sD(d1).imag + R[e44].imag * sD(d1).real);
	D(e34).real = (R[e41].real * sD(d4).real - R[e41].imag * sD(d4).imag) - (R[e42].real * sD(d2).real - R[e42].imag * sD(d2).imag) + (R[e43].real * sD(d1).real - R[e43].imag * sD(d1).imag);
	D(e34).imag = (R[e41].real * sD(d4).imag + R[e41].imag * sD(d4).real) - (R[e42].real * sD(d2).imag + R[e42].imag * sD(d2).real) + (R[e43].real * sD(d1).imag + R[e43].imag * sD(d1).real);

	D(e41).real = (R[e32].real * sD(d6).real - R[e32].imag * sD(d6).imag) - (R[e33].real * sD(d5).real - R[e33].imag * sD(d5).imag) + (R[e34].real * sD(d4).real - R[e34].imag * sD(d4).imag);
	D(e41).imag = (R[e32].real * sD(d6).imag + R[e32].imag * sD(d6).real) - (R[e33].real * sD(d5).imag + R[e33].imag * sD(d5).real) + (R[e34].real * sD(d4).imag + R[e34].imag * sD(d4).real);
	D(e42).real = (R[e31].real * sD(d6).real - R[e31].imag * sD(d6).imag) + (R[e33].real * sD(d3).real - R[e33].imag * sD(d3).imag) + (R[e34].real * sD(d2).real - R[e34].imag * sD(d2).imag);
	D(e42).imag = (R[e31].real * sD(d6).imag + R[e31].imag * sD(d6).real) + (R[e33].real * sD(d3).imag + R[e33].imag * sD(d3).real) + (R[e34].real * sD(d2).imag + R[e34].imag * sD(d2).real);
	D(e43).real = (R[e31].real * sD(d5).real - R[e31].imag * sD(d5).imag) + (R[e32].real * sD(d3).real - R[e32].imag * sD(d3).imag) + (R[e34].real * sD(d1).real - R[e34].imag * sD(d1).imag);
	D(e43).imag = (R[e31].real * sD(d5).imag + R[e31].imag * sD(d5).real) + (R[e32].real * sD(d3).imag + R[e32].imag * sD(d3).real) + (R[e34].real * sD(d1).imag + R[e34].imag * sD(d1).real);
	D(e44).real = (R[e31].real * sD(d4).real - R[e31].imag * sD(d4).imag) - (R[e32].real * sD(d2).real - R[e32].imag * sD(d2).imag) + (R[e33].real * sD(d1).real - R[e33].imag * sD(d1).imag);
	D(e44).imag = (R[e31].real * sD(d4).imag + R[e31].imag * sD(d4).real) - (R[e32].real * sD(d2).imag + R[e32].imag * sD(d2).real) + (R[e33].real * sD(d1).imag + R[e33].imag * sD(d1).real);

	DETre = (R[e11].real * D(e11).real - R[e11].imag * D(e11).imag) - (R[e21].real * D(e21).real - R[e21].imag * D(e21).imag) + (R[e31].real * D(e31).real - R[e31].imag * D(e31).imag) - (R[e41].real * D(e41).real - R[e41].imag * D(e41).imag);
	DETim = (R[e11].real * D(e11).imag + R[e11].imag * D(e11).real) - (R[e21].real * D(e21).imag + R[e21].imag * D(e21).real) + (R[e31].real * D(e31).imag + R[e31].imag * D(e31).real) - (R[e41].real * D(e41).imag + R[e41].imag * D(e41).real);
	DETn2 = DETre * DETre + DETim * DETim;
	if (DETn2 < DETZEROTH)
	{
		for (int n = 0; n <= 180; n++)
		{
			Awn[n] = DETZEROTH;
		}
		return -1;
	}
	DETre = +DETre / DETn2;
	DETim = -DETim / DETn2;

	B(e11).real = +(srsl::fp)(D(e11).real * DETre - D(e11).imag * DETim);
	B(e11).imag = +(srsl::fp)(D(e11).real * DETim + D(e11).imag * DETre);
	B(e12).real = -(srsl::fp)(D(e21).real * DETre - D(e21).imag * DETim);
	B(e12).imag = -(srsl::fp)(D(e21).real * DETim + D(e21).imag * DETre);
	B(e13).real = +(srsl::fp)(D(e31).real * DETre - D(e31).imag * DETim);
	B(e13).imag = +(srsl::fp)(D(e31).real * DETim + D(e31).imag * DETre);
	B(e14).real = -(srsl::fp)(D(e41).real * DETre - D(e41).imag * DETim);
	B(e14).imag = -(srsl::fp)(D(e41).real * DETim + D(e41).imag * DETre);

	B(e21).real = -(srsl::fp)(D(e12).real * DETre - D(e12).imag * DETim);
	B(e21).imag = -(srsl::fp)(D(e12).real * DETim + D(e12).imag * DETre);
	B(e22).real = +(srsl::fp)(D(e22).real * DETre - D(e22).imag * DETim);
	B(e22).imag = +(srsl::fp)(D(e22).real * DETim + D(e22).imag * DETre);
	B(e23).real = -(srsl::fp)(D(e32).real * DETre - D(e32).imag * DETim);
	B(e23).imag = -(srsl::fp)(D(e32).real * DETim + D(e32).imag * DETre);
	B(e24).real = +(srsl::fp)(D(e42).real * DETre - D(e42).imag * DETim);
	B(e24).imag = +(srsl::fp)(D(e42).real * DETim + D(e42).imag * DETre);

	B(e31).real = +(srsl::fp)(D(e13).real * DETre - D(e13).imag * DETim);
	B(e31).imag = +(srsl::fp)(D(e13).real * DETim + D(e13).imag * DETre);
	B(e32).real = -(srsl::fp)(D(e23).real * DETre - D(e23).imag * DETim);
	B(e32).imag = -(srsl::fp)(D(e23).real * DETim + D(e23).imag * DETre);
	B(e33).real = +(srsl::fp)(D(e33).real * DETre - D(e33).imag * DETim);
	B(e33).imag = +(srsl::fp)(D(e33).real * DETim + D(e33).imag * DETre);
	B(e34).real = -(srsl::fp)(D(e43).real * DETre - D(e43).imag * DETim);
	B(e34).imag = -(srsl::fp)(D(e43).real * DETim + D(e43).imag * DETre);

	B(e41).real = -(srsl::fp)(D(e14).real * DETre - D(e14).imag * DETim);
	B(e41).imag = -(srsl::fp)(D(e14).real * DETim + D(e14).imag * DETre);
	B(e42).real = +(srsl::fp)(D(e24).real * DETre - D(e24).imag * DETim);
	B(e42).imag = +(srsl::fp)(D(e24).real * DETim + D(e24).imag * DETre);
	B(e43).real = -(srsl::fp)(D(e34).real * DETre - D(e34).imag * DETim);
	B(e43).imag = -(srsl::fp)(D(e34).real * DETim + D(e34).imag * DETre);
	B(e44).real = +(srsl::fp)(D(e44).real * DETre - D(e44).imag * DETim);
	B(e44).imag = +(srsl::fp)(D(e44).real * DETim + D(e44).imag * DETre);

	srsl::fp CCre, CCim;
	srsl::fp CC;
	srsl::fp CCbuf = 1 / DETZEROTH;
	int DZcnt = 0;
	for (int n = min_ang; n <= max_ang; n += 1)
	{
		ang_en(n) = 1;
		CCre = (AsW(n, a11).real * (B(e11).real))
		     + (AsW(n, a21).real * (B(e21).real + B(e12).real) - AsW(n, a21).imag * (B(e21).imag - B(e12).imag))
		     + (AsW(n, a31).real * (B(e31).real + B(e13).real) - AsW(n, a31).imag * (B(e31).imag - B(e13).imag))
		     + (AsW(n, a41).real * (B(e41).real + B(e14).real) - AsW(n, a41).imag * (B(e41).imag - B(e14).imag))
		     + (AsW(n, a22).real * (B(e22).real))
		     + (AsW(n, a32).real * (B(e32).real + B(e23).real) - AsW(n, a32).imag * (B(e32).imag - B(e23).imag))
		     + (AsW(n, a42).real * (B(e42).real + B(e24).real) - AsW(n, a42).imag * (B(e42).imag - B(e24).imag))
		     + (AsW(n, a33).real * (B(e33).real))
		     + (AsW(n, a43).real * (B(e43).real + B(e34).real) - AsW(n, a43).imag * (B(e43).imag - B(e34).imag))
		     + (AsW(n, a44).real * (B(e44).real));

		CCim = (AsW(n, a11).real * (B(e11).imag))
		     + (AsW(n, a21).real * (B(e21).imag + B(e12).imag) + AsW(n, a21).imag * (B(e21).real - B(e12).real))
		     + (AsW(n, a31).real * (B(e31).imag + B(e13).imag) + AsW(n, a31).imag * (B(e31).real - B(e13).real))
		     + (AsW(n, a41).real * (B(e41).imag + B(e14).imag) + AsW(n, a41).imag * (B(e41).real - B(e14).real))
		     + (AsW(n, a22).real * (B(e22).imag))
		     + (AsW(n, a32).real * (B(e32).imag + B(e23).imag) + AsW(n, a32).imag * (B(e32).real - B(e23).real))
		     + (AsW(n, a42).real * (B(e42).imag + B(e24).imag) + AsW(n, a42).imag * (B(e42).real - B(e24).real))
		     + (AsW(n, a33).real * (B(e33).imag))
		     + (AsW(n, a43).real * (B(e43).imag + B(e34).imag) + AsW(n, a43).imag * (B(e43).real - B(e34).real))
		     + (AsW(n, a44).real * (B(e44).imag));

		CC = CCre * CCre + CCim * CCim;
		if (CC < DETZEROTH) { CC = CCbuf; DZcnt++; }
		else { CCbuf = CC; }
		if (scale == 0)
		{
			Awn[n] = COMPLEX_CAPON_SQRT(1 / CC);
		}
		else
		{
			Awn[n] = -20 * COMPLEX_CAPON_LOG10(COMPLEX_CAPON_SQRT(CC));
		}
	}
	return DZcnt;
}

void C_Capon::Capon_3Ainit(void)
{
	srsl::fp Lambda = 299792458.0F / 60.5e9F;
	srsl::fp Fc = (srsl::fp)(2 * PI * AntPitch / Lambda);
	srsl::vector<srsl::complexfp> AS(3);

	if (CaponNumElem != c33 + 1)
	{
		CaponNumAngle = 180 + 1;
		CaponNumElem = c33 + 1;
		AsW.resize(CaponNumAngle, CaponNumElem);
	}
	for (int n = 0; n <= 180; n++)
	{
		srsl::fp Sn = (srsl::fp)srsl::sin((n - 90) * PI / 180);
		for (int an = 0; an < 3; an++)
		{
			AS(an).real = srsl::cos(an * Fc * Sn);
			AS(an).imag = srsl::sin(an * Fc * Sn);
		}
		AsW(n, c11).real = AS(w1).real * AS(w1).real + AS(w1).imag * AS(w1).imag;
		AsW(n, c21).real = AS(w2).real * AS(w1).real + AS(w2).imag * AS(w1).imag;
		AsW(n, c31).real = AS(w3).real * AS(w1).real + AS(w3).imag * AS(w1).imag;
		AsW(n, c22).real = AS(w2).real * AS(w2).real + AS(w2).imag * AS(w2).imag;
		AsW(n, c32).real = AS(w3).real * AS(w2).real + AS(w3).imag * AS(w2).imag;
		AsW(n, c33).real = AS(w3).real * AS(w3).real + AS(w3).imag * AS(w3).imag;

		AsW(n, c11).imag = 0;
		AsW(n, c21).imag = AS(w2).real * AS(w1).imag - AS(w2).imag * AS(w1).real;
		AsW(n, c31).imag = AS(w3).real * AS(w1).imag - AS(w3).imag * AS(w1).real;
		AsW(n, c22).imag = 0;
		AsW(n, c32).imag = AS(w3).real * AS(w2).imag - AS(w3).imag * AS(w2).real;
		AsW(n, c33).imag = 0;
	}
}

int C_Capon::Capon_3A(srsl::complexfp *R, srsl::fp *Awn, int min_ang, int max_ang, srsl::vector<bool> &ang_en, int scale)
{
	srsl::vector<srsl::complexfp> A(9);
	double DETre, DETim;
	double DETn2;

	A(c11).real = +((R[c22].real * R[c33].real - R[c22].imag * R[c33].imag ) - (R[c23].real * R[c32].real - R[c23].imag * R[c32].imag ));
	A(c11).imag = +((R[c22].real * R[c33].imag + R[c22].imag * R[c33].real ) - (R[c23].real * R[c32].imag + R[c23].imag * R[c32].real ));
	A(c21).real = -((R[c21].real * R[c33].real - R[c21].imag * R[c33].imag ) - (R[c23].real * R[c31].real - R[c23].imag * R[c31].imag ));
	A(c21).imag = -((R[c21].real * R[c33].imag + R[c21].imag * R[c33].real ) - (R[c23].real * R[c31].imag + R[c23].imag * R[c31].real ));
	A(c31).real = +((R[c21].real * R[c32].real - R[c21].imag * R[c32].imag ) - (R[c22].real * R[c31].real - R[c22].imag * R[c31].imag ));
	A(c31).imag = +((R[c21].real * R[c32].imag + R[c21].imag * R[c32].real ) - (R[c22].real * R[c31].imag + R[c22].imag * R[c31].real ));

	A(c12).real = -((R[c12].real * R[c33].real - R[c12].imag * R[c33].imag ) - (R[c13].real * R[c32].real - R[c13].imag * R[c32].imag ));
	A(c12).imag = -((R[c12].real * R[c33].imag + R[c12].imag * R[c33].real ) - (R[c13].real * R[c32].imag + R[c13].imag * R[c32].real ));
	A(c22).real = +((R[c11].real * R[c33].real - R[c11].imag * R[c33].imag ) - (R[c13].real * R[c31].real - R[c13].imag * R[c31].imag ));
	A(c22).imag = +((R[c11].real * R[c33].imag + R[c11].imag * R[c33].real ) - (R[c13].real * R[c31].imag + R[c13].imag * R[c31].real ));
	A(c32).real = -((R[c11].real * R[c32].real - R[c11].imag * R[c32].imag ) - (R[c12].real * R[c31].real - R[c12].imag * R[c31].imag ));
	A(c32).imag = -((R[c11].real * R[c32].imag + R[c11].imag * R[c32].real ) - (R[c12].real * R[c31].imag + R[c12].imag * R[c31].real ));

	A(c13).real = +((R[c12].real * R[c23].real - R[c12].imag * R[c23].imag ) - (R[c13].real * R[c22].real - R[c13].imag * R[c22].imag ));
	A(c13).imag = +((R[c12].real * R[c23].imag + R[c12].imag * R[c23].real ) - (R[c13].real * R[c22].imag + R[c13].imag * R[c22].real ));
	A(c23).real = -((R[c11].real * R[c23].real - R[c11].imag * R[c23].imag ) - (R[c13].real * R[c21].real - R[c13].imag * R[c21].imag ));
	A(c23).imag = -((R[c11].real * R[c23].imag + R[c11].imag * R[c23].real ) - (R[c13].real * R[c21].imag + R[c13].imag * R[c21].real ));
	A(c33).real = +((R[c11].real * R[c22].real - R[c11].imag * R[c22].imag ) - (R[c12].real * R[c21].real - R[c12].imag * R[c21].imag ));
	A(c33).imag = +((R[c11].real * R[c22].imag + R[c11].imag * R[c22].real ) - (R[c12].real * R[c21].imag + R[c12].imag * R[c21].real ));

	DETre = (R[c11].real * A(c11).real - R[c11].imag * A(c11).imag) + (R[c12].real * A(c21).real - R[c12].imag * A(c21).imag) + (R[c13].real * A(c31).real - R[c13].imag * A(c31).imag);
	DETim = (R[c11].real * A(c11).imag + R[c11].imag * A(c11).real) + (R[c12].real * A(c21).imag + R[c12].imag * A(c21).real) + (R[c13].real * A(c31).imag + R[c13].imag * A(c31).real);
	DETn2 = DETre * DETre + DETim * DETim;
	if (DETn2 < DETZEROTH)
	{
		for (int n = 0; n <= 180; n++)
		{
			Awn[n] = DETZEROTH;
		}
		return -1;
	}
	DETre = +DETre / DETn2;
	DETim = -DETim / DETn2;

	B(c11).real = (srsl::fp)(A(c11).real * DETre - A(c11).imag * DETim);
	B(c11).imag = (srsl::fp)(A(c11).real * DETim + A(c11).imag * DETre);
	B(c12).real = (srsl::fp)(A(c12).real * DETre - A(c12).imag * DETim);
	B(c12).imag = (srsl::fp)(A(c12).real * DETim + A(c12).imag * DETre);
	B(c13).real = (srsl::fp)(A(c13).real * DETre - A(c13).imag * DETim);
	B(c13).imag = (srsl::fp)(A(c13).real * DETim + A(c13).imag * DETre);

	B(c21).real = (srsl::fp)(A(c21).real * DETre - A(c21).imag * DETim);
	B(c21).imag = (srsl::fp)(A(c21).real * DETim + A(c21).imag * DETre);
	B(c22).real = (srsl::fp)(A(c22).real * DETre - A(c22).imag * DETim);
	B(c22).imag = (srsl::fp)(A(c22).real * DETim + A(c22).imag * DETre);
	B(c23).real = (srsl::fp)(A(c23).real * DETre - A(c23).imag * DETim);
	B(c23).imag = (srsl::fp)(A(c23).real * DETim + A(c23).imag * DETre);

	B(c31).real = (srsl::fp)(A(c31).real * DETre - A(c31).imag * DETim);
	B(c31).imag = (srsl::fp)(A(c31).real * DETim + A(c31).imag * DETre);
	B(c32).real = (srsl::fp)(A(c32).real * DETre - A(c32).imag * DETim);
	B(c32).imag = (srsl::fp)(A(c32).real * DETim + A(c32).imag * DETre);
	B(c33).real = (srsl::fp)(A(c33).real * DETre - A(c33).imag * DETim);
	B(c33).imag = (srsl::fp)(A(c33).real * DETim + A(c33).imag * DETre);

	srsl::fp CCre, CCim, CC;
	srsl::fp CCbuf = DETZEROTH;
	for (int n = min_ang; n <= max_ang; n += 1)
	{
		ang_en(n) = 1;
		CCre = AsW(n, c11).real *  B(c11).real
		     + AsW(n, c21).real * (B(c21).real + B(c12).real) - AsW(n, c21).imag * (B(c21).imag - B(c12).imag)
		     + AsW(n, c31).real * (B(c31).real + B(c13).real) - AsW(n, c31).imag * (B(c31).imag - B(c13).imag)
		     + AsW(n, c22).real *  B(c22).real
		     + AsW(n, c32).real * (B(c32).real + B(c23).real) - AsW(n, c32).imag * (B(c32).imag - B(c23).imag)
		     + AsW(n, c33).real *  B(c33).real;

		CCim = AsW(n, c11).real *  B(c11).imag
		     + AsW(n, c21).real * (B(c21).imag + B(c12).imag) + AsW(n, c21).imag * (B(c21).real - B(c12).real)
		     + AsW(n, c31).real * (B(c31).imag + B(c13).imag) + AsW(n, c31).imag * (B(c31).real - B(c13).real)
		     + AsW(n, c22).real *  B(c22).imag
		     + AsW(n, c32).real * (B(c32).imag + B(c23).imag) + AsW(n, c32).imag * (B(c32).real - B(c23).real)
		     + AsW(n, c33).real *  B(c33).imag;

		CC = CCre * CCre + CCim * CCim;
		if (CC < DETZEROTH) CC = CCbuf;
		else CCbuf = CC;
		if (scale == 0)
		{
			Awn[n] = COMPLEX_CAPON_SQRT(1 / CC);
		}
		else
		{
			Awn[n] = -20 * COMPLEX_CAPON_LOG10(COMPLEX_CAPON_SQRT(CC));
		}
	}
	return 0;
}

void C_Capon::Capon_2Ainit(void)
{
	srsl::fp Lambda = 299792458 / 60.5e9;
	srsl::fp Fc = 2 * PI * AntPitch / Lambda;
	srsl::vector<srsl::complexfp> AS(2); // 2:NRX

	if (CaponNumElem != b22 + 1)
	{
		CaponNumAngle = 180 + 1;
		CaponNumElem = b22 + 1;
		AsW.resize(CaponNumAngle, CaponNumElem);
	}

	for (int n = 0; n <= 180; n++)
	{
		srsl::fp Sn = srsl::sin((n - 90) * PI / 180);
		for (int an = 0; an < 2; an++)
		{
			AS(an).real = srsl::cos(an * Fc * Sn);
			AS(an).imag = srsl::sin(an * Fc * Sn);
		}
		AsW(n, b11).real = 1;
		AsW(n, b11).imag = 0;
		AsW(n, b21).real = +srsl::cos(Fc * Sn);
		AsW(n, b21).imag = -srsl::sin(Fc * Sn);

		AsW(n, b22).real = 1;
		AsW(n, b22).imag = 0;
	}
}

int C_Capon::Capon_2A(srsl::complexfp *R, srsl::fp *Awn, int min_ang, int max_ang, srsl::vector<bool> &ang_en, int scale)
{
	double DETre, DETim;
	double DETn2;

	//
	DETre = (R[s11].real * R[s22].real - R[s11].imag * R[s22].imag ) - (R[s12].real * R[s21].real - R[s12].imag * R[s21].imag );
	DETim = (R[s11].real * R[s22].imag + R[s11].imag * R[s22].real ) - (R[s12].real * R[s21].imag + R[s12].imag * R[s21].real );
	DETn2 = DETre * DETre + DETim * DETim;
	if (DETn2 < DETZEROTH)
	{
		for (int n = 0; n <= 180; n++)
		{
			Awn[n] = DETZEROTH;
		}
		return -1;
	}
	DETre = +DETre / DETn2;
	DETim = -DETim / DETn2;

	B(s11).real = +(srsl::fp)(R[s22].real * DETre - R[s22].imag * DETim);
	B(s11).imag = +(srsl::fp)(R[s22].real * DETim + R[s22].imag * DETre);
	B(s12).real = -(srsl::fp)(R[s12].real * DETre - R[s12].imag * DETim);
	B(s12).imag = -(srsl::fp)(R[s12].real * DETim + R[s12].imag * DETre);
	B(s21).real = -(srsl::fp)(R[s21].real * DETre - R[s21].imag * DETim);
	B(s21).imag = -(srsl::fp)(R[s21].real * DETim + R[s21].imag * DETre);
	B(s22).real = +(srsl::fp)(R[s11].real * DETre - R[s11].imag * DETim);
	B(s22).imag = +(srsl::fp)(R[s11].real * DETim + R[s11].imag * DETre);

	//

	srsl::fp CCre, CCim, CC;
	srsl::fp CCbuf = DETZEROTH;
	for (int n = min_ang; n <= max_ang; n += 1)
	{
		ang_en(n) = 1;
		CCre = B(s11).real
		     + AsW(n, b21).real * (B(s21).real + B(s12).real) - AsW(n, b21).imag * (B(s21).imag - B(s12).imag)
		     + B(s22).real;

		CCim = B(s11).imag
		     + AsW(n, b21).real * (B(s21).imag + B(s12).imag) + AsW(n, b21).imag * (B(s21).real - B(s12).real)
		     + B(s22).imag;

		CC = CCre * CCre + CCim * CCim;
		if (CC < DETZEROTH) CC = CCbuf;
		else CCbuf = CC;
		if (scale == 0)
		{
			Awn[n] = COMPLEX_CAPON_SQRT(1 / CC);
		}
		else
		{
			Awn[n] = -20 * COMPLEX_CAPON_LOG10(COMPLEX_CAPON_SQRT(CC));
		}
	}
	return 0;
}

int C_Capon::Capon_1A(srsl::complexfp *R, srsl::fp *Awn, int min_ang, int max_ang, srsl::vector<bool> &ang_en, int scale)
{
	srsl::fp CC;
	srsl::fp CCbuf = DETZEROTH;
	for (int n = min_ang; n <= max_ang; n += 1)
	{
		ang_en(n) = 1;

		CC = R[0].real * R[0].real + R[0].imag * R[0].imag;
		if (CC < DETZEROTH) CC = CCbuf;
		else CCbuf = CC;
		if (scale == 0)
		{
			Awn[n] = COMPLEX_CAPON_SQRT(CC);
		}
		else
		{
			Awn[n] = 20 * COMPLEX_CAPON_LOG10(COMPLEX_CAPON_SQRT(CC));
		}
	}
	return 0;
}
