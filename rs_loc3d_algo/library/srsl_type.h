/*
 * Copyright (c) 2016-2021 Socionext Inc.
 * All rights reserved.
 */
#ifndef SRSL_TYPE_H
#define SRSL_TYPE_H

#include "srsl_platform.h"

namespace srsl
{

	template<typename T> struct complex
	{
		T real;
		T imag;
		complex() {}
		complex(T r, T i) : real(r), imag(i) {}
		complex(const complex<T> &obj) : real(obj.real), imag(obj.imag) {}
		void assign(const complex<T> &obj) { real = obj.real; imag = obj.imag; }
		template<typename T2> void assign(const complex<T2> &obj)
		{
			real = (T)obj.real;
			imag = (T)obj.imag;
		}
		void add(const complex<T> &obj) { real += obj.real; imag += obj.imag; }
		void sub(const complex<T> &obj) { real -= obj.real; imag -= obj.imag; }
		void mul(const T &obj) { real *= obj; imag *= obj; }
		void div(const T &obj) { real /= obj; imag /= obj; }
		complex<T> &operator=(const complex<T> &obj) { assign(obj); return *this; }
		template<typename T2> complex<T> &operator=(const complex<T2> &obj) { assign(obj); return *this; }
		complex<T> &operator+=(const complex<T> &obj) { add(obj); return *this; }
		complex<T> &operator-=(const complex<T> &obj) { sub(obj); return *this; }
		complex<T> &operator*=(const T &obj) { mul(obj); return *this; }
		complex<T> &operator/=(const T &obj) { div(obj); return *this; }
	};

	template<typename T> complex<T> operator+(const complex<T> &a, const complex<T> &b)
	{
		complex<T> c(a);
		c.add(b);
		return c;
	}
	template<typename T> complex<T> operator-(const complex<T> &a, const complex<T> &b)
	{
		complex<T> c(a);
		c.sub(b);
		return c;
	}
	template<typename T> complex<T> operator*(const complex<T> &a, const T &b)
	{
		complex<T> c(a);
		c.mul(b);
		return c;
	}
	template<typename T> complex<T> operator*(const T &a, const complex<T> &b)
	{
		return b * a;
	}
	template<typename T> complex<T> operator/(const complex<T> &a, const T &b)
	{
		complex<T> c(a);
		c.div(b);
		return c;
	}

	// square of absolute value of a complex number
	template<typename T> T sqabs(const complex<T> &x)
	{
		return x.real * x.real + x.imag * x.imag;
	}

	typedef complex<fp> complexfp;

}

#endif /* SRSL_TYPE_H */
