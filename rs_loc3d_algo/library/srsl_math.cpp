/*
 * Copyright (c) 2016-2021 Socionext Inc.
 * All rights reserved.
 */
#include <cmath>
#include "srsl_type.h"
#include "srsl_math_int.h"

namespace srsl
{

	fp fabs(fp x) { return std::fabs(x); }
#if __cplusplus >= 201103L
	fp cabs(complexfp x) { return std::hypot(x.real, x.imag); }
#else
	fp cabs(complexfp x) { return srsl_math_int_hypot(x.real, x.imag); }
#endif

	fp pow(fp x, fp y) { return std::pow(x, y); }
	fp sqrt(fp x) { return std::sqrt(x); }

	fp exp(fp x) { return std::exp(x); }
	fp log(fp x) { return std::log(x); }
	fp log10(fp x) { return std::log10(x); }

	fp cos(fp x) { return std::cos(x); }
	fp sin(fp x) { return std::sin(x); }
	fp tan(fp x) { return std::tan(x); }

	fp acos(fp x) { return std::acos(x); }
	fp asin(fp x) { return std::asin(x); }
	fp atan(fp x) { return std::atan(x); }

	fp ceil(fp x) { return std::ceil(x); }
	fp floor(fp x) { return std::floor(x); }
#if __cplusplus >= 201103L
	fp nearbyint(fp x) { return std::nearbyint(x); }
	fp rint(fp x) { return std::rint(x); }
	fp round(fp x) { return std::round(x); }
	fp trunc(fp x) { return std::trunc(x); }
#else
	fp nearbyint(fp x) { return srsl_math_int_nearbyint(x); }
	fp rint(fp x) { return srsl_math_int_rint(x); }
	fp round(fp x) { return srsl_math_int_round(x); }
	fp trunc(fp x) { return srsl_math_int_trunc(x); }
#endif

	fp copysign(fp x, fp y) { return std::copysign(x, y); }

}
