/*
 * Copyright (c) 2016-2021 Socionext Inc.
 * All rights reserved.
 */
#ifndef __WINDOW_FUNCTION_H__
#define __WINDOW_FUNCTION_H__

#include <srsl_math.h>
#include <srsl_type.h>

#define WINDOW_FUNC_COS(x) srsl::cos(x)

template<size_t FFT_N>
class C_WindowFunction
{
public:
	static const int RECTANGULAR_WINDOW = 0;
	static const int HANN_WINDOW = 1;
	static const int HAMMING_WINDOW = 2;
	static const int BLACKMAN_WINDOW = 3;
	static const int GAUSSIAN_WINDOW = 4;

public:
	C_WindowFunction()
	{
		coeff_size = -1;
		WindowMode = -1;
	}

public:
	int window1(const int mode,
	                             const int numofaclrdata,
	                             const srsl::complex<short> *indata,
	                             srsl::complexfp *outdata)
	{
		srsl::complexfp indatafp[MAX_SIZE_R];

		for (int ct = 0; ct < numofaclrdata; ++ct)
		{
			indatafp[ct] = *indata;
			++indata;
		}

		return window2(mode, numofaclrdata, indatafp, outdata);
	}
	int window2(const int mode,
	                             const int numofaclrdata,
	                             const srsl::complexfp *indata,
	                             srsl::complexfp *outdata)
	{
		int ct;

		if (mode != WindowMode || coeff_size != numofaclrdata)
		{
			coeff_size = numofaclrdata;
			//coeff.resize(numofaclrdata);
			WindowMode = mode;

			switch (mode)
			{
			case HANN_WINDOW:
				for (ct = 0; ct < numofaclrdata; ct++)
				{
					coeff[ct] = 0.5 - 0.5 * WINDOW_FUNC_COS(2 * PI * ct / (numofaclrdata-1));
				}
				break;
			case HAMMING_WINDOW:
				for (ct = 0; ct < numofaclrdata; ct++)
				{
					coeff[ct] = 0.54 - 0.46 * WINDOW_FUNC_COS(2 * PI * ct / (numofaclrdata-1));
				}
				break;
			case BLACKMAN_WINDOW:
				for (ct = 0; ct < numofaclrdata; ct++)
				{
					coeff[ct] = 0.42 - 0.5 * WINDOW_FUNC_COS(2 * PI * ct / (numofaclrdata-1))
					          + 0.08 * WINDOW_FUNC_COS(4 * PI * ct / (numofaclrdata-1));
				}
				break;
			case GAUSSIAN_WINDOW:
				for (ct = 0; ct < numofaclrdata; ct++)
				{
					coeff[ct] = srsl::exp(-4.5 * (2.0 * ct - numofaclrdata) / numofaclrdata
					                           * (2.0 * ct - numofaclrdata) / numofaclrdata);
				}
				break;
			case RECTANGULAR_WINDOW:
			default:
				for (ct = 0; ct < numofaclrdata; ct++)
				{
					coeff[ct] = 1.0;
				}
				break;
			}
		}

		//!!if (indata.size() >= numofaclrdata && outdata.size() >= numofaclrdata)
		{
			for (ct = 0; ct < numofaclrdata; ct++)
			{
				outdata[ct] = indata[ct] * coeff[ct];
			}
		}

		return 0;
	}

private:
	int WindowMode;
	int coeff_size;
	srsl::fp coeff[FFT_N];

};

#endif
