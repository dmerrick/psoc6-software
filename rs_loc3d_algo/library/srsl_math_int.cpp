/*
 * Copyright (c) 2016-2021 Socionext Inc.
 * All rights reserved.
 */
#include <cmath>

#if __cplusplus < 201103L
extern "C" double srsl_math_int_hypot(double x, double y) { return hypot(x, y); }

extern "C" double srsl_math_int_nearbyint(double x) { return nearbyint(x); }
extern "C" double srsl_math_int_rint(double x) { return rint(x); }
extern "C" double srsl_math_int_round(double x) { return round(x); }
extern "C" double srsl_math_int_trunc(double x) { return trunc(x); }
#endif
