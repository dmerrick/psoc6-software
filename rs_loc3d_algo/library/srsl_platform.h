/*
 * Copyright (c) 2016-2021 Socionext Inc.
 * All rights reserved.
 */
#include <float.h>

namespace srsl
{

	typedef float fp;

	static const fp FP_MIN = FLT_MIN;
	static const fp FP_MAX = FLT_MAX;

}
