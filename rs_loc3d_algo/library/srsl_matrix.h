/*
 * Copyright (c) 2016-2021 Socionext Inc.
 * All rights reserved.
 */
#ifndef SRSL_MATRIX_H
#define SRSL_MATRIX_H

#ifndef SRSL_DEBUG_MATRIX
#define SRSL_DEBUG_MATRIX 0
#endif

#if SRSL_DEBUG_MATRIX
#include <iostream>
#endif /* SRSL_DEBUG_MATRIX */
#if defined(SRSL_USE_MATRIX_ID)
#include <string>
#endif /* defined(SRSL_USE_MATRIX_ID) */
#include "srsl_osl.h"

namespace srsl
{

	template<typename T> class storage
	{
	public:
		storage(const char *id = "") : m_id(id), m_size(0), m_data(0) {}
		virtual ~storage() {}
	private:
		storage(const storage<T> &obj);

	public:
		void allocate(int s)
		{
			m_size = s;
			m_data = (m_size > 0) ? allocate_array<T>(m_size) : 0;
#if SRSL_DEBUG_MATRIX
			std::cout << "allocate " << m_id;
			std::cout << " with " << m_size;
			std::cout << " at " << m_data << std::endl;
#endif /* SRSL_DEBUG_MATRIX */
		}
		void deallocate(void)
		{
#if SRSL_DEBUG_MATRIX
			std::cout << "deallocate " << m_id;
			std::cout << " with " << m_size;
			std::cout << " at " << m_data << std::endl;
#endif /* SRSL_DEBUG_MATRIX */
			deallocate_array<T>(m_data);
			m_size = 0;
			m_data = 0;
		}
		void assign(const storage<T> &obj)
		{
			if (m_size > 0) copy_array<T>(m_data, obj.m_data, m_size);
		}
		void assign(const T *src)
		{
			if (m_size > 0) copy_array<T>(m_data, src, m_size);
		}
		int size(void) const { return m_size; }
		T *data(void) { return m_data; }
		const T *data(void) const { return m_data; }
		T *ref(int idx) { return &m_data[idx]; }
		const T *ref(int idx) const { return &m_data[idx]; }

	public:
		void clear(int start = 0) { if (m_size > 0) clear_array<T>(m_data, m_size, start); }
		void fill(const T &val) { if (m_size > 0) fill_array<T>(m_data, m_size, val); }

	private:
#if defined(SRSL_USE_MATRIX_ID)
		std::string m_id;
#else /* defined(SRSL_USE_MATRIX_ID) */
		const char *m_id;
#endif /* defined(SRSL_USE_MATRIX_ID) */
		int m_size;
		T *m_data;
	};

	template<typename ScalarType> class matrix
	{
	public:
		class refvector
		{
		public:
			refvector(int size, ScalarType *ptr) : m_size(size), m_ptr(ptr) {}
			refvector(const refvector &obj) : m_size(obj.m_size), m_ptr(obj.m_ptr) {}
			~refvector() {}
		private:
			refvector();

		public:
			int size(void) const { return m_size; }
			ScalarType *get(void) { return m_ptr; }
			const ScalarType *get(void) const { return m_ptr; }
			ScalarType &at(int idx)
			{
				return m_ptr[idx];
			}
			const ScalarType &at(int idx) const
			{
				return m_ptr[idx];
			}
			ScalarType &operator()(int idx) { return at(idx); }
			const ScalarType &operator()(int idx) const { return at(idx); }

		private:
			int m_size;
			ScalarType *m_ptr;
		};

	public:
		matrix(const char *id = "")
			: m_rows(0)
			, m_cols(0)
			, m_storage(id)
		{
		}
		matrix(int rows, int cols, const char *id = "")
			: m_rows(rows)
			, m_cols(cols)
			, m_storage(id)
		{
			m_storage.allocate(m_rows * m_cols);
		}
		matrix(int rows, int cols, const ScalarType &initial, const char *id = "")
			: m_rows(rows)
			, m_cols(cols)
			, m_storage(id)
		{
			m_storage.allocate(m_rows * m_cols);
			m_storage.fill(initial);
		}
		~matrix()
		{
			m_storage.deallocate();
		}
	private:
		matrix(const matrix<ScalarType> &obj);

	public:
		int rows(void) const { return m_rows; }
		int cols(void) const { return m_cols; }
		int size(void) const { return m_storage.size(); }
		void resize(int rows, int cols)
		{
			m_rows = rows;
			m_cols = cols;
			m_storage.deallocate();
			m_storage.allocate(m_rows * m_cols);
		}
		void zeros(void) { m_storage.clear(); }
		void fill_zero(int start_row = 0)
		{
			m_storage.clear(start_row * m_cols);
		}
		void assign(const matrix<ScalarType> &obj)
		{
			m_storage.assign(obj.m_storage);
		}
		void assign(const ScalarType *src)
		{
			m_storage.assign(src);
		}
		ScalarType *get(void) { return m_storage.data(); }
		const ScalarType *get(void) const { return m_storage.data(); }
		refvector row(int idx)
		{
			return refvector(m_cols, m_storage.ref(idx * m_cols));
		}
		const refvector row(int idx) const
		{
			return refvector(m_cols, m_storage.ref(idx * m_cols));
		}
		ScalarType &at(int row, int col)
		{
			return m_storage.data()[row * m_cols + col];
		}
		const ScalarType &at(int row, int col) const
		{
			return m_storage.data()[row * m_cols + col];
		}
		ScalarType &operator()(int row, int col) { return at(row, col); }
		const ScalarType &operator()(int row, int col) const { return at(row, col); }

	private:
		int m_rows;
		int m_cols;
		storage<ScalarType> m_storage;
	};

	template<typename ScalarType> class vector
	{
	public:
		vector(const char *id = "")
			: m_storage(id)
		{
		}
		vector(int size, const char *id = "")
			: m_storage(id)
		{
			m_storage.allocate(size);
		}
		vector(int size, const ScalarType &initial, const char *id = "")
			: m_storage(id)
		{
			m_storage.allocate(size);
			m_storage.fill(initial);
		}
		vector(const typename matrix<ScalarType>::refvector &obj)
			: m_storage()
		{
			m_storage.allocate(obj.size());
			m_storage.assign(obj.get());
		}
		~vector()
		{
			m_storage.deallocate();
		}
	private:
		vector(const vector<ScalarType> &obj);

	public:
		int size(void) const { return m_storage.size(); }
		void resize(int size)
		{
			m_storage.deallocate();
			m_storage.allocate(size);
		}
		void zeros(void) { m_storage.clear(); }
		void fill_zero(int start = 0)
		{
			m_storage.clear(start);
		}
		void assign(const vector<ScalarType> &obj)
		{
			m_storage.assign(obj.m_storage);
		}
		void assign(const ScalarType *src)
		{
			m_storage.assign(src);
		}
		void assign(const typename matrix<ScalarType>::refvector &obj)
		{
			m_storage.assign(obj.get());
		}
		ScalarType *get(void) { return m_storage.data(); }
		const ScalarType *get(void) const { return m_storage.data(); }
		ScalarType &at(int idx)
		{
			return m_storage.data()[idx];
		}
		const ScalarType &at(int idx) const
		{
			return m_storage.data()[idx];
		}
		ScalarType &operator()(int idx) { return at(idx); }
		const ScalarType &operator()(int idx) const { return at(idx); }

	private:
		storage<ScalarType> m_storage;
	};

}

#endif /* SRSL_MATRIX_H */
