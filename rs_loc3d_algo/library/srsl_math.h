/*
 * Copyright (c) 2016-2021 Socionext Inc.
 * All rights reserved.
 */
#ifndef SRSL_MATH_H
#define SRSL_MATH_H

#include "srsl_type.h"

#ifdef _PI_PRECISION_DEBUG
#define PI 3.1415926535897932384626433832795f
#else
#define PI 3.1415926535897932384626433832795
#endif

namespace srsl
{

	fp fabs(fp x);
	fp cabs(complexfp x);

	fp pow(fp x, fp y);
	fp sqrt(fp x);

	fp exp(fp x);
	fp log(fp x);
	fp log10(fp x);

	fp cos(fp x);
	fp sin(fp x);
	fp tan(fp x);

	fp acos(fp x);
	fp asin(fp x);
	fp atan(fp x);

	fp ceil(fp x);
	fp floor(fp x);
	fp nearbyint(fp x);
	fp rint(fp x);
	fp round(fp x);
	fp trunc(fp x);

	fp copysign(fp x, fp y);

}

#endif /* SRSL_MATH_H */
