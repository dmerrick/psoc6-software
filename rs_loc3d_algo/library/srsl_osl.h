/*
 * Copyright (c) 2016-2021 Socionext Inc.
 * All rights reserved.
 */
#ifndef SRSL_OSL_H
#define SRSL_OSL_H

#include <cstring>
#include <cassert>

namespace srsl
{

	template<typename T> T *allocate_array(int n)
	{
		assert(n > 0);
		return new T[n];
	}
	template<typename T> void deallocate_array(T *p)
	{
		delete[] p;
	}
	template<typename T> void clear_array(T *s, int n, int from = 0)
	{
		assert(s != 0);
		assert(n > 0);
		assert(from < n);
		(void) std::memset(&s[from], 0, sizeof(T) * (n - from));
	}
	template<typename T> void fill_array(T *s, int n, const T &val, int from = 0)
	{
		assert(s != 0);
		assert(n > 0);
		for (int i = from; i < n; ++i) s[i] = val;
	}
	template<typename T> void copy_array(T *dst, const T *src, int n)
	{
		assert(dst != 0);
		assert(src != 0);
		assert(n > 0);
#if defined(_MSC_VER)
		size_t sz = sizeof(T) * n;
		(void) std::memcpy_s(dst, sz, src, sz);
#else /* defined(_MSC_VER) */
		(void) std::memcpy(dst, src, sizeof(T) * n);
#endif /* defined(_MSC_VER) */
	}

}

#endif /* SRSL_OSL_H */
