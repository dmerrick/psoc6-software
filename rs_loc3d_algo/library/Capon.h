/*
 * Copyright (c) 2016-2021 Socionext Inc.
 * All rights reserved.
 */
#ifndef __CAPON_H__
#define __CAPON_H__

#include <stdlib.h>
#include <srsl_type.h>
#include <srsl_matrix.h>

class C_Capon
{
public:
	//
	//
	//
	C_Capon();

	void Init(int NARRAY, srsl::fp ant_pitch);
	void Capon_init(int NRX, srsl::fp ant_pitch);

	void Run(
	         int min_ang, int max_ang,
	         int Ng, int NRmax, int NARRAY,
	         int nfft,
	         srsl::fp ALFA_A,
	         int index,
             int index_pair,
	         srsl::vector<srsl::complexfp> &_pks,
	         int numRpks,
	         srsl::complexfp *a_Rss,
	         srsl::complexfp *sa_Rss,
	         srsl::fp *A_spc);

	int Capon(int NRX, srsl::vector<srsl::complexfp>& R, srsl::vector<srsl::fp>& Awn);

private:
	void mtx_CpxMlt(int N, int M, int K, srsl::vector<srsl::complexfp> &A, srsl::vector<srsl::complexfp> &B, srsl::vector<srsl::complexfp> &C);

	int inverse1(const srsl::vector<srsl::complexfp> &A, srsl::vector<srsl::complexfp> &B);

	void Capon_4Ainit(void);
	int Capon_4A(srsl::complexfp *R, srsl::fp *Awn, int min_ang, int max_ang, srsl::vector<bool> &ang_en, int scale = 0);

	void Capon_3Ainit(void);
	int Capon_3A(srsl::complexfp *R, srsl::fp *Awn, int min_ang, int max_ang, srsl::vector<bool> &ang_en, int scale = 0);

	void Capon_2Ainit(void);
	int Capon_2A(srsl::complexfp *R, srsl::fp *Awn, int min_ang, int max_ang, srsl::vector<bool> &ang_en, int scale = 0);

	int Capon_1A(srsl::complexfp *R, srsl::fp *Awn, int min_ang, int max_ang, srsl::vector<bool> &ang_en, int scale = 0);

private:
	int CaponNumAngle;
	int CaponNumElem;
	srsl::fp AntPitch;
	srsl::matrix<srsl::complexfp> AsW;
	srsl::vector<srsl::complexfp> B;
	int NonZeroCounter;

public:
	int apn;
	int eh;
};

#endif
