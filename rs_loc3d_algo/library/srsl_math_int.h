/*
 * Copyright (c) 2016-2021 Socionext Inc.
 * All rights reserved.
 */
#if __cplusplus < 201103L
extern "C" double srsl_math_int_hypot(double x, double y);

extern "C" double srsl_math_int_nearbyint(double x);
extern "C" double srsl_math_int_rint(double x);
extern "C" double srsl_math_int_round(double x);
extern "C" double srsl_math_int_trunc(double x);
#endif
