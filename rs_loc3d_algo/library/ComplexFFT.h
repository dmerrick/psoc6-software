/*
 * Copyright (c) 2016-2021 Socionext Inc.
 * All rights reserved.
 */
#ifndef __COMPLEX_FFT_H__
#define __COMPLEX_FFT_H__

#include <srsl_type.h>
#include <srsl_math.h>

template<size_t FFT_N>
class C_ComplexFFT
{
public:
	//
	//
	//
	C_ComplexFFT()
		: FFTNum(-1)
		, gBRMap()
		, SinTable()
		, out()
	{
	}
	~C_ComplexFFT()
	{
	}

	// *****************************************************************************
	// * Function   :
	// * Argument   :
	// * Argument   :
	// * Argument   :
	// * return     :
	// *****************************************************************************
	void run(const int nfft, const srsl::complexfp *input)
	{
		int i;
		
		//
		if (FFTNum != nfft)
		{
			MakeBitReverseMap(nfft);
			SetSinTable(nfft);
			FFTNum = nfft;
		}

		//
		//!!out.resize(nfft);
		for (i = 0; i < nfft; i++)
		{
			out[i].real = input[gBRMap[i]].real;
			out[i].imag = input[gBRMap[i]].imag;
		}

		//
		ComplexButterfly(out, nfft);
	}

	void run(const int nfft, const srsl::complexfp *input, srsl::complexfp *out)
	{
		int i;

		if (nfft <= 0) return;

		if (FFTNum != nfft)
		{
			MakeBitReverseMap(nfft);
			SetSinTable(nfft);
			FFTNum = nfft;
		}

		//!!if (input.size() > 0)
		{
			//!!if (out.size() != nfft) out.resize(nfft);
			for (i = 0; i < nfft; i++)
			{
				out[i].real = input[gBRMap[i]].real;
				out[i].imag = input[gBRMap[i]].imag;
			}

			ComplexButterfly(out, nfft);
		}
	}

private:
	// *****************************************************************************
	// * Function   :
	// * Argument   :
	// * return     :
	// *****************************************************************************
	void MakeBitReverseMap(int nfft)
	{
		int i, j;
		int nexp = 0;

		//
		for (i = 0; i <= nfft; i++)
		{
			if (nexp == 0)
			{
				if ((nfft >> i) & 1)
				{
					nexp = i;
					break;
				}
			}
		}
		//
		//!!gBRMap.resize(nfft);
		gBRMap[0] = 0;
		//
		for (i = 1; i < nfft; i++)
		{
			int br = 0;
			int div = 1;

			for (j = 0; j <= nexp; j++)
			{
				if (i & (1 << j))
				{
					br += nfft >> div;
				}
				div++;
			}
			gBRMap[i] = br;
		}
	}

	// *****************************************************************************
	// * Function   :
	// * Argument   :
	// * return     :
	// *****************************************************************************
	void SetSinTable(int nfft)
	{
		//!!SinTable.resize(nfft / 4 + 1);
		for (int K = 0; K <= nfft / 4; K++)
		{
			SinTable[K] = srsl::sin((srsl::fp)(2 * PI * K / nfft));
		}
	}

	//****************************************************************
	// * Function   :
	// * Argument   :
	// * Argument   :
	// * Argument   :
	// * return     :
	//****************************************************************
	void ComplexButterfly(srsl::complexfp *X, int nfft)
	{
		srsl::fp WRe, WIm;
		srsl::fp bRe, bIm;
		int N, M, K;

		int NF4 = nfft / 4;
		int NF2 = nfft / 2;

		for (N = 2; N <= nfft; N *= 2)
		{
			int N2 = N / 2;
			int NFN = nfft / N;
			int kNFN = 0;

			for (K = 0; K < N2; K++, kNFN += NFN)
			{
				int KN2 = K + N2;

				if (4 * K < N)
				{
					WRe =  SinTable[ NF4 - kNFN];
					WIm =  SinTable[       kNFN];
				}
				else
				{
					WRe = -SinTable[-NF4 + kNFN];
					WIm =  SinTable[ NF2 - kNFN];
				}
				for (M = 0; M < nfft; M += N)
				{
					bRe = WRe * X[M + KN2].real + WIm * X[M + KN2].imag;
					bIm = WRe * X[M + KN2].imag - WIm * X[M + KN2].real;
					X[M + KN2].real = X[M + K].real - bRe;
					X[M + KN2].imag = X[M + K].imag - bIm;
					X[M + K  ].real = X[M + K].real + bRe;
					X[M + K  ].imag = X[M + K].imag + bIm;
				}
			}
		}
	}

private:
	//
	//
	//
	int FFTNum;
	int gBRMap[FFT_N];
	srsl::fp SinTable[FFT_N];

public:
	srsl::complexfp out[FFT_N];

};

#endif
