/*
 * Copyright (c) 2016-2021 Socionext Inc.
 * All rights reserved.
 */
#ifndef ALGO_MEASURE_H
#define ALGO_MEASURE_H

#if defined(_MSC_VER)
/* avoid to conflict bcWidth in wingdi.h */
#define RS_LOC3D_ALGO_USE_STOPWATCH 0
#else
#define RS_LOC3D_ALGO_USE_STOPWATCH 0
#endif

#if RS_LOC3D_ALGO_USE_STOPWATCH
#include <vector>
#include <rs_stopwatch.h>

class algo_measure
{
public:
	std::vector<rs_stopwatch_t> sw;

public:
	algo_measure(size_t n) { sw.resize(n); }
};
#endif /* RS_LOC3D_ALGO_USE_STOPWATCH */

#endif /* ALGO_MEASURE_H */
