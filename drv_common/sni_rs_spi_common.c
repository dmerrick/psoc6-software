/*
 * sni_rs_spi_common.c
 *
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

#ifdef TARGET_KERNEL
#include <linux/types.h>
#include <linux/uaccess.h>
#else
#include <stddef.h>
#include <stdint.h>
#endif

#include "sni_rs_spi.h"


enum SNI_RET sni_rs_spi_resetctl(void *_spi, const uint8_t op)
{
	enum SNI_RET ret;
	uint8_t tx_buf[1] = {op};

	ret = sni_rs_spi_data_transmit_only(_spi, tx_buf, sizeof(tx_buf));

	return ret;
}

enum SNI_RET sni_rs_spi_modectl(void *_spi, const uint8_t op)
{
	enum SNI_RET ret;
	uint8_t tx_buf[2] = {OP_MODECTL, op};

	ret = sni_rs_spi_data_transmit_only(_spi, tx_buf, sizeof(tx_buf));

	return ret;
}

enum SNI_RET sni_rs_spi_reg_write32(void *_spi, const uint32_t addr, const uint32_t data)
{
	enum SNI_RET ret;
	uint8_t buf[4];

	buf[0] = (data >> 24) & 0xff;
	buf[1] = (data >> 16) & 0xff;
	buf[2] = (data >>  8) & 0xff;
	buf[3] = (data >>  0) & 0xff;

	ret = sni_rs_spi_reg_write8(_spi, addr, buf, sizeof(buf));

	return ret;
}

enum SNI_RET sni_rs_spi_reg_read32(void *_spi, const uint32_t addr, uint32_t *data)
{
	enum SNI_RET ret;
	uint8_t buf[4];

	ret = sni_rs_spi_reg_read8(_spi, addr, buf, sizeof(buf));

	*data = buf[0];
	*data <<= 8;
	*data |= buf[1];
	*data <<= 8;
	*data |= buf[2];
	*data <<= 8;
	*data |= buf[3];

	return ret;
}

enum SNI_RET sni_rs_spi_read_stat(void *_spi, uint8_t *data)
{
	enum SNI_RET ret;
	uint8_t tx_buf[3] = { 0 };
	uint8_t rx_buf[3] = { 0 };

	tx_buf[0] = OP_READ_STATUS_REG;
	ret = sni_rs_spi_data_transmit(_spi, tx_buf, rx_buf, sizeof(rx_buf));
	*data = rx_buf[2];

	return ret;
}

enum SNI_RET sni_rs_spi_isbusy(void *_spi)
{
	enum SNI_RET ret;
	uint8_t data;

	ret = sni_rs_spi_read_stat(_spi, &data);
	if(ret) {
		return ret;
	}

	if ((data & MASK_STATUS_BUSY) == 0x00) {
		return SNI_RET_OK;
	}
	return SNI_RET_EDEV;
}
