/*
 * sni_rs_txpower.c
 *
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

#ifdef TARGET_KERNEL
#include <linux/types.h>
#include <linux/uaccess.h>
#else
#include <string.h>
#include <stddef.h>
#include <stdint.h>
#endif

#include "sni_rs_txpower.h"


void sni_rs_get_index_table(int val, int *idx, int index_table[], int num)
{
	int i;
	
	for(i = 0; i < num; i++) {
		*idx = i;
		if(val == index_table[i]) {
			break;
		}
	}
}

/* extern function */
void sni_rs_txpowerinfo_init(struct sni_rs_txpowerinfo *info)
{
	info->txpower_value = -10;				// TxPower = -10dBm
	info->txpower_reg_num = 0;
}

enum SNI_BOOL sni_rs_txpowerinfo_set_txpower(struct sni_rs_txpowerinfo *info, uint8_t value, uint8_t sign)
{
	int txpower;
	if (sign == 0x00) {
		txpower = value;
	} else if (sign == 0x01 && value != 0x00) {
		txpower = -value;
	} else {
		return SNI_FALSE;
	}
	
	if (txpower != 0 && txpower != -10) {
		return SNI_FALSE;
	}
	
	info->txpower_value = txpower;
	
	return SNI_TRUE;
}

void sni_rs_txpowerinfo_calc_reg(struct sni_rs_txpowerinfo *info, struct sni_rs_hw_info *hw_info)
{
	struct sni_rs_reg_table tx_power_reg_tbl;
	int i;
	
	/* calculate tx_power register value */
	uint32_t addr30_value = (0x08400005) 
							| ((hw_info->hw_efuse36 & 0xFF00) << 6)
							| ((hw_info->hw_efuse36 & 0x00F0) << 4);

	uint32_t addr24_value, addr27_value;

	if (hw_info->hw_efuse36 & 0x0002) {
		addr24_value = 0x02010777;
	} else {
		addr24_value = 0x01010777;
	}
	if (hw_info->hw_efuse36 & 0x0001) {
		addr27_value = 0x02010777;
	} else {
		addr27_value = 0x01010777;
	}
	
	// select Tx Power register table
	{
		// Tx Power register table
		struct sni_rs_hw_reg tx_power_0dbm[] = {
			// addr,         value
			{0x0024,	0x01030777},
			{0x0027,	0x01030777},
			{0x006B,	0x00A00A00},
			{0x0030,	addr30_value},
		};
		struct sni_rs_hw_reg tx_power_10dbm[] = {
			// addr,         value
			{0x0024,	addr24_value},
			{0x0027,	addr27_value},
			{0x006B,	0x00000000},
			{0x0030,	addr30_value},
		};
		
		int tx_power_val[] = {
			0, -10,
		};
		
		struct sni_rs_reg_table tx_power_reg_tbl_s[][2] = {
			{	// ver 1
				{ARRAY_SIZE(tx_power_0dbm),  tx_power_0dbm},
				{ARRAY_SIZE(tx_power_10dbm), tx_power_10dbm},
			},
		};

		// select Tx Power register table
		int power_idx;
		
		sni_rs_get_index_table(info->txpower_value, &power_idx, tx_power_val, ARRAY_SIZE(tx_power_val));
		tx_power_reg_tbl = tx_power_reg_tbl_s[0][power_idx];
		
		// setting Tx Power registers
		info->txpower_reg_num = tx_power_reg_tbl.num;
		for (i = 0; i < tx_power_reg_tbl.num; i++) {
			info->txpower_reg[i].addr = tx_power_reg_tbl.regs[i].addr;
			info->txpower_reg[i].value = tx_power_reg_tbl.regs[i].value;
		}
	}
}
