/*
 * sni_rs_version.h
 *
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

#ifndef __sni_rs_version_h__
#define __sni_rs_version_h__

#ifdef TARGET_KERNEL
#include <linux/types.h>
#include <linux/uaccess.h>
#else
#include <stddef.h>
#include <stdint.h>
#endif

/*
 * User Header Version
 */
static const uint32_t sni_rs_version_user_header = 0;

/*
 * Driver Version
 */
static const uint16_t sni_rs_version_major = 2;
static const uint16_t sni_rs_version_minor = 6;
static const uint16_t sni_rs_version_variation = 0;
static const uint16_t sni_rs_version_update = 1;

#endif	/* __sni_rs_version_h__ */
