/*
 * sni_rs_spi.h
 *
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

#ifndef _sni_rs_spi_H
#define _sni_rs_spi_H

#ifdef TARGET_KERNEL
#include <linux/types.h>
#include <linux/uaccess.h>
#else
#include <stddef.h>
#include <stdint.h>
#endif

#include "sni_rs_common.h"

// opcode
#define OP_HARD_RESET				0xC7
#define OP_SOFT_RESET				0xAB
#define OP_MODECTL					0x01
#define OP_SINGLE_SHOT				0x08
#define OP_CONT_START				0x0C
#define OP_CONT_STOP				0x04
#define OP_READ_DATA				0x0B
#define OP_WRITE_DATA				0x02
#define OP_DEEP_SLEEP				0xB9
#define OP_READ_STATUS_REG			0x05

// addr
#define REG_LDOSET5					0x000060
#define REG_SELSET13				0x000071
#define REG_WATERMARK				0x000083
#define REG_DIG_05	 				0x000085			//en_seq
#define REG_DIG_06	 				0x000086			//seq start and end address
#define REG_DIG_18	 				0x000098			//eFuse read trigger
#define REG_DIG_1F	 				0x00009F
#define REG_BBSET40	 				0x0000A8
#define REG_DIG_2F	 				0x0000AF
#define REG_EFUSE16					0x0000E0
#define REG_EFUSE36					0x0000F5
#define REG_EFUSE37					0x0000F6
#define REG_SRAM_IQ_FIFO			0x010000
#define REG_SRAM_SEQ				0x020000
#define REG_SRAM_FFTWIN				0x030000

// bit mask
#define MASK_STATUS_BUSY			0x01

#ifdef __cplusplus
extern "C" {
#endif	/* __cplusplus */

	extern int sni_rs_spi_init(void *_spi);
	extern int sni_rs_spi_destroy(void *_spi);
	extern enum SNI_RET sni_rs_spi_data_transmit_only(void *_spi, uint8_t *data, const size_t size);
	extern enum SNI_RET sni_rs_spi_data_transmit(void *_spi, uint8_t *wdata, uint8_t *rdata, const size_t size);
	extern enum SNI_RET sni_rs_spi_reg_write8(void *_spi, const uint32_t addr, uint8_t *data, const size_t size);
	extern enum SNI_RET sni_rs_spi_reg_read8(void *_spi, const uint32_t addr, uint8_t *data, const size_t size);
	extern enum SNI_RET sni_rs_spi_reg_read8_iq(void *_spi, uint8_t *data, const size_t size, const size_t ssize);
	extern enum SNI_RET sni_rs_spi_reg_read8_iq_dma(void *_spi, uint8_t *data, const size_t size, const size_t ssize);

	extern enum SNI_RET sni_rs_spi_resetctl(void *_spi, const uint8_t op);
	extern enum SNI_RET sni_rs_spi_modectl(void *_spi, const uint8_t op);
	extern enum SNI_RET sni_rs_spi_reg_write32(void *_spi, const uint32_t addr, const uint32_t data);
	extern enum SNI_RET sni_rs_spi_reg_read32(void *_spi, const uint32_t addr, uint32_t *data);
	extern enum SNI_RET sni_rs_spi_read_stat(void *_spi, uint8_t *data);
	extern enum SNI_RET sni_rs_spi_isbusy(void *_spi);

#ifdef __cplusplus
}
#endif	/* __cplusplus */

#endif /* _sni_rs_spi_H */
