/*
 * sni_rs_ctrl.h
 *
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

#ifndef _sni_rs_ctrl_H
#define _sni_rs_ctrl_H

#ifdef TARGET_KERNEL
#include <linux/types.h>
#include <linux/uaccess.h>
#include <linux/list.h>
#include <linux/mutex.h>
#include <linux/slab.h>
#else
#include <string.h>
#include <stddef.h>
#include <stdint.h>
#endif

#include "sni_rs_setupinfo.h"
#include "sni_rs_ctrl_statistics.h"
#include "sni_rs_fifo.h"
#include "sni_rs_pos.h"
#include "sni_rs_spi.h"
#include "sni_rs_tools.h"
#include "sni_rs_termctl.h"
#include "sni_rs_algo.h"


struct sni_rs_ctrl_attr
{
	void *spi;
	struct sni_rs_termctl_t *termctl;
	const struct sni_rs_fifo_attr *fifo_attr;
	const struct sni_rs_pos_attr *pos_attr;
	const struct sni_rs_setupinfo_attr *setupinfo_attr;
	const struct sni_rs_startinfo_attr *startinfo_attr;
};

struct sni_rs_ctrl
{
	void *_rf_spi1;										// SPI with sensor device
	struct sni_rs_termctl_t *termctl;					// terminal
	struct sni_rs_fifo fifo;							// FIFO management information
	struct sni_rs_stat_info stat;						// debug information including statistics
	struct sni_rs_pos pos;								// frame position information for reading IQ data
	enum SNI_STATE cur_state;							// current state

	struct sni_rs_setupinfo setupinfo;					// setup info
	struct sni_rs_startinfo startinfo;					// start info
	
	struct sni_rs_hw_info hw_info;
	enum SNI_BOOL hif_dma_flag;							// Host IF DMA enable/disable flag

	struct sni_rs_algo_ctrl algo_ctrl;
};

#ifdef __cplusplus
extern "C" {
#endif	/* __cplusplus */

	extern void sni_rs_ctrl_init(struct sni_rs_ctrl *ctrl, const struct sni_rs_ctrl_attr *attr);
	extern void sni_rs_ctrl_free(struct sni_rs_ctrl *ctrl);

	extern void sni_rs_ctrl_get_data(struct sni_rs_ctrl *ctrl);

	static SNI_INLINE enum SNI_STATE sni_rs_ctrl_get_state(const struct sni_rs_ctrl *ctrl) {
		return ctrl->cur_state;
	}
	static SNI_INLINE enum SNI_BOOL sni_rs_ctrl_check_state(const struct sni_rs_ctrl *ctrl, enum SNI_STATE state) {
		return SNI_BOOL_CONV(sni_rs_ctrl_get_state(ctrl) == state);
	}
	extern enum SNI_RET sni_rs_ctrl_trans_state(struct sni_rs_ctrl *ctrl, enum SNI_STATE new_state);

	extern enum SNI_RET sni_rs_ctrl_state_set(struct sni_rs_ctrl *ctrl, enum SNI_STATE new_state);

	extern enum SNI_RET sni_rs_ctrl_term_boot(struct sni_rs_ctrl *ctrl);
	extern void sni_rs_ctrl_term_reset(struct sni_rs_ctrl *ctrl);
	extern void sni_rs_ctrl_term_set(struct sni_rs_ctrl *ctrl);
	extern void sni_rs_ctrl_term_shutdown(struct sni_rs_ctrl *ctrl);
	extern enum SNI_RET sni_rs_ctrl_term_setup(struct sni_rs_ctrl *ctrl);
	extern enum SNI_RET sni_rs_ctrl_term_cal(struct sni_rs_ctrl *ctrl);
	extern void sni_rs_ctrl_set_hw_info(struct sni_rs_ctrl *ctrl, const struct sni_rs_hw_info *hw_info);
	extern void sni_rs_ctrl_get_hw_info(const struct sni_rs_ctrl *ctrl, struct sni_rs_hw_info *hw_info);

	static SNI_INLINE void sni_rs_ctrl_disable_irq(struct sni_rs_termctl_t *termctl)
	{
		sni_rs_intr_disable(&(termctl->m_or));
	}
	static SNI_INLINE void sni_rs_ctrl_enable_irq(struct sni_rs_termctl_t *termctl)
	{
		sni_rs_intr_enable(&(termctl->m_or));
	}

	static SNI_INLINE void sni_rs_ctrl_get_spi1_statistics(struct sni_rs_ctrl *ctrl, struct sni_rs_statistics *statistics) {
		statistics->frm_cnt = ctrl->stat._frm_cnt;
		statistics->of_cnt = ctrl->stat._of_cnt;
		statistics->err_cnt = ctrl->stat._err_cnt;
		statistics->stored_size = sni_rs_startinfo_get_framenum(&(ctrl->startinfo))*sni_rs_fifo_get_pagenum(&ctrl->fifo) + ctrl->stat._frm_no;
		statistics->queue_size = sni_rs_fifo_get_maxframenum(&(ctrl->fifo));
	}

#ifdef __cplusplus
}
#endif	/* __cplusplus */

#endif /* _sni_rs_ctrl_H */
