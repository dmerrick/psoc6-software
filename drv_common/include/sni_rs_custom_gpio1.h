/*
 * sni_rs_custom_gpio1.h
 *
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

#ifndef __sni_rs_custom_gpio1_H__
#define __sni_rs_custom_gpio1_H__

#ifdef __ENABLE_CUSTOM_GPIO1__

#ifdef __cplusplus
extern "C" {
#endif	/* __cplusplus */

	extern int sni_rs_custom_gpio1_init(void);
	extern int sni_rs_custom_gpio1_set(void);
	extern int sni_rs_custom_gpio1_clear(void);

#ifdef __cplusplus
}
#endif	/* __cplusplus */

#else	/* __ENABLE_CUSTOM_GPIO1__ */

#define sni_rs_custom_gpio1_init()
#define sni_rs_custom_gpio1_set()
#define sni_rs_custom_gpio1_clear()

#endif	/* __ENABLE_CUSTOM_GPIO1__ */

#endif	/* __sni_rs_custom_gpio1_H__ */
