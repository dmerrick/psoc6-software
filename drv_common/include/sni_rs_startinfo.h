/*
 * sni_rs_startinfo.h
 *
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

#ifndef __sni_rs_startinfo_h__
#define __sni_rs_startinfo_h__

#ifdef TARGET_KERNEL
#include <linux/types.h>
#include <linux/uaccess.h>
#else
#include <string.h>
#include <stddef.h>
#include <stdint.h>
#endif

#include "sni_rs_mem.h"
#include "sni_rs_common.h"
#include "sni_rs_def.h"
#include "sni_rs_setupinfo.h"


struct sni_rs_startinfo_attr
{
	struct sni_rs_setupinfo_attr setupinfo_attr;
};

struct sni_rs_frameinfo
{
	struct sni_rs_framewordsize word;
	struct sni_rs_framebytesize byte;

	struct sni_rs_frametype type;
};

struct sni_rs_startinfo
{
	struct sni_rs_setupinfo setupinfo;
	struct sni_rs_frameinfo frameinfo;

	uint8_t frames_per_page;				// number of frames in a page
	size_t watermark_word;					// watermark (word)
};


#ifdef __cplusplus
extern "C" {
#endif	/* __cplusplus */

	extern enum SNI_BOOL sni_rs_startinfo_init(struct sni_rs_startinfo *info, const struct sni_rs_startinfo_attr *attr);
	extern enum SNI_RET sni_rs_startinfo_setup(struct sni_rs_startinfo *info, const struct sni_rs_setupinfo *setupinfo);
	extern enum SNI_BOOL sni_rs_startinfo_clear(struct sni_rs_startinfo *info);
	extern void sni_rs_startinfo_free(struct sni_rs_startinfo *info);

	static SNI_INLINE size_t sni_rs_startinfo_word2byte(const struct sni_rs_startinfo *info, size_t size) {
		return size * ((info->frameinfo.type.bitwidth != 0) ? 3 : 4);
	}

	extern enum SNI_BOOL sni_rs_startinfo_set_watermark(struct sni_rs_startinfo *info, size_t wordsize);
	extern enum SNI_BOOL sni_rs_startinfo_set_framenum(struct sni_rs_startinfo *info, uint8_t num);

	static SNI_INLINE size_t sni_rs_startinfo_get_watermark_word(const struct sni_rs_startinfo *info) {
		return info->watermark_word;
	}
	static SNI_INLINE size_t sni_rs_startinfo_get_watermark_byte(const struct sni_rs_startinfo *info) {
		return sni_rs_startinfo_word2byte(info, sni_rs_startinfo_get_watermark_word(info));
	}
	static SNI_INLINE size_t sni_rs_startinfo_get_framenum(const struct sni_rs_startinfo *info) {
		return info->frames_per_page;
	}
	static SNI_INLINE uint8_t sni_rs_startinfo_get_statheadmode(const struct sni_rs_startinfo *info) {
		return info->frameinfo.type.status_mode;
	}
	static SNI_INLINE uint8_t sni_rs_startinfo_get_headheadmode(const struct sni_rs_startinfo *info) {
		return info->frameinfo.type.header_mode;
	}
	static SNI_INLINE uint8_t sni_rs_startinfo_get_bitwidth(const struct sni_rs_startinfo *info) {
		return info->frameinfo.type.bitwidth;
	}

#ifdef __cplusplus
}
#endif	/* __cplusplus */


#endif /* __sni_rs_startinfo_h__ */
