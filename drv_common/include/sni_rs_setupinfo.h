/*
 * sni_rs_setupinfo.h
 *
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

#ifndef __sni_rs_setupinfo_h__
#define __sni_rs_setupinfo_h__

#ifdef TARGET_KERNEL
#include <linux/types.h>
#include <linux/uaccess.h>
#else
#include <string.h>
#include <stddef.h>
#include <stdint.h>
#endif

#include "sni_rs_mem.h"
#include "sni_rs_common.h"
#include "sni_rs_def.h"
#include "sni_rs_txpower.h"


struct sni_rs_setupinfo_attr
{
	const struct sni_rs_mem_attr *buf_seqcode;
	const struct sni_rs_mem_attr *buf_regaddr;
	const struct sni_rs_mem_attr *buf_regdata;
	const struct sni_rs_mem_attr *buf_fftwin;
};

struct sni_rs_frametype
{
	uint8_t status_mode;					// 1: exist device header (status)
	uint8_t header_mode;					// 1: exist device header (header)
	uint8_t bitwidth;						// bitwidth of device's output-data
};

struct sni_rs_setupinfo
{
	// data of "SET_REG_S"
	struct {
		int flag;
		size_t num;							// number of SETUP registers (32 bit word) 
		struct sni_rs_mem addr;				// (uint16_t) buffer for address of SETUP registers
		struct sni_rs_mem data;				// (uint32_t) buffer for data of SETUP registers
	} reg_s;

	// data of "SET_SEQ"
	struct {
		int flag;
		size_t size;						// byte number of sequencer code
		struct sni_rs_mem code;				// (uint8_t) buffer for sequencer code
	} seqcode;

	// data of "SET_FFTWIN"
	struct {
		int flag;
		size_t size;						// byte number of fftwin data table
		struct sni_rs_mem data;				// (uint8_t) buffer for fftwin data
	} fftwin;

	// data of "SET_SEQ_ADDR"
	struct {
		int flag;
		uint32_t sensing;					// address of sequencer code for Sensing
		uint32_t cal;						// address of sequencer code for CAL
	} seqaddr;

	struct sni_rs_frametype type;
	uint32_t iqdata_wordsize;
	
	// data of "SET_TX_POWER"
	struct {
		int flag;
		struct sni_rs_txpowerinfo txpowerinfo;		// txpower info
	} txpower;
};


#ifdef __cplusplus
extern "C" {
#endif	/* __cplusplus */

	/* setupinfo */
	extern enum SNI_BOOL sni_rs_setupinfo_init(struct sni_rs_setupinfo *info, const struct sni_rs_setupinfo_attr *attr);
	extern enum SNI_BOOL sni_rs_setupinfo_clear(struct sni_rs_setupinfo *info);
	extern void sni_rs_setupinfo_free(struct sni_rs_setupinfo *info);
	extern enum SNI_BOOL sni_rs_setupinfo_copy(struct sni_rs_setupinfo *dst, const struct sni_rs_setupinfo *src);
	extern enum SNI_BOOL sni_rs_setupinfo_isenable(const struct sni_rs_setupinfo *info);

	extern enum SNI_BOOL sni_rs_setupinfo_set_framesize(struct sni_rs_setupinfo *info, uint32_t wordsize);
	extern enum SNI_RET sni_rs_setupinfo_set_seqcode(struct sni_rs_setupinfo *info, const uint8_t *d, size_t num);
	extern enum SNI_RET sni_rs_setupinfo_set_reg(struct sni_rs_setupinfo *info, const uint8_t *d, size_t num);
	extern enum SNI_BOOL sni_rs_setupinfo_set_addr(struct sni_rs_setupinfo *info, const uint8_t *d, size_t num);
	extern enum SNI_BOOL sni_rs_setupinfo_init_fftwin(struct sni_rs_setupinfo *info);
	extern enum SNI_RET sni_rs_setupinfo_set_fftwin(struct sni_rs_setupinfo *info, const uint8_t *d, size_t num);
	extern enum SNI_BOOL sni_rs_setupinfo_set_headheadmode(struct sni_rs_setupinfo *info, uint8_t hmode);
	extern enum SNI_BOOL sni_rs_setupinfo_set_statheadmode(struct sni_rs_setupinfo *info, uint8_t smode);
	extern enum SNI_BOOL sni_rs_setupinfo_set_bitwidth(struct sni_rs_setupinfo *info, uint8_t bwidth);
	extern enum SNI_BOOL sni_rs_setupinfo_set_txpower(struct sni_rs_setupinfo *info, uint8_t value, uint8_t sign, struct sni_rs_hw_info *hw_info);

	static SNI_INLINE uint8_t *sni_rs_setupinfo_get_seqcodedata(const struct sni_rs_setupinfo *setupinfo) {
		return ((uint8_t *) sni_rs_mem_get(&(setupinfo->seqcode.code)));
	}
	static SNI_INLINE size_t sni_rs_setupinfo_get_seqcodenum(const struct sni_rs_setupinfo *setupinfo) {
		return setupinfo->seqcode.size;
	}
	static SNI_INLINE size_t sni_rs_setupinfo_get_regnum(const struct sni_rs_setupinfo *setupinfo) {
		return setupinfo->reg_s.num;
	}
	static SNI_INLINE uint16_t *sni_rs_setupinfo_get_regaddr(const struct sni_rs_setupinfo *setupinfo) {
		return ((uint16_t *) (sni_rs_mem_get(&(setupinfo->reg_s.addr))));
	}
	static SNI_INLINE uint32_t *sni_rs_setupinfo_get_regdata(const struct sni_rs_setupinfo *setupinfo) {
		return ((uint32_t *) (sni_rs_mem_get(&(setupinfo->reg_s.data))));
	}

	static SNI_INLINE uint8_t *sni_rs_setupinfo_get_fftwindata(const struct sni_rs_setupinfo *setupinfo) {
		return ((uint8_t *) sni_rs_mem_get(&(setupinfo->fftwin.data)));
	}
	static SNI_INLINE size_t sni_rs_setupinfo_get_fftwinnum(const struct sni_rs_setupinfo *setupinfo) {
		return setupinfo->fftwin.size;
	}

	static SNI_INLINE uint32_t sni_rs_setupinfo_get_caladdr(const struct sni_rs_setupinfo *setupinfo) {
		return setupinfo->seqaddr.cal;
	}
	static SNI_INLINE uint32_t sni_rs_setupinfo_get_sensingaddr(const struct sni_rs_setupinfo *setupinfo) {
		return setupinfo->seqaddr.sensing;
	}

#ifdef __cplusplus
}
#endif	/* __cplusplus */

#endif /* __sni_rs_setupinfo_h__ */
