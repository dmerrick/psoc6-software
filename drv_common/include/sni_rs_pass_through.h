/*
 * sni_rs_pass_through.h
 *
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

#ifndef _sni_rs_pass_through_H
#define _sni_rs_pass_through_H

#ifdef TARGET_KERNEL
#include <linux/types.h>
#include <linux/uaccess.h>
#else
#include <stddef.h>
#include <stdint.h>
#endif

#include "sni_rs_ctrl.h"

#define PASS_TH_MAXSIZE	(MAX_FRAME_BODY * 4 + 64)

#ifdef __cplusplus
extern "C" {
#endif	/* __cplusplus */

	extern enum SNI_RET sni_rs_pass_through(struct sni_rs_ctrl *ctrl, uint8_t *wdata, size_t wsize, uint8_t *rdata, size_t *rsize);

#ifdef __cplusplus
}
#endif	/* __cplusplus */

#endif /* _sni_rs_pass_through_H */
