/*
 * sni_rs_target.h
 *
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

#ifndef __sni_rs_target_h__
#define __sni_rs_target_h__

#ifdef TARGET_STM32

#ifdef __cplusplus
extern "C" {
#endif	/* __cplusplus */

	extern void sni_rs_target_reboot(void);
	extern void sni_rs_target_cache_dclean(void);

#ifdef __cplusplus
}
#endif	/* __cplusplus */

#else	/* TARGET_KERNEL */
#define sni_rs_target_reboot()
#define sni_rs_target_cache_dclean()
#endif	/* TARGET_KERNEL */

#endif	/* __sni_rs_target_h__ */
