/*
 * sni_rs_txpower.h
 *
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

#ifndef __sni_rs_txpower_h__
#define __sni_rs_txpower_h__

#ifdef TARGET_KERNEL
#include <linux/types.h>
#include <linux/uaccess.h>
#else
#include <string.h>
#include <stddef.h>
#include <stdint.h>
#endif

#include "sni_rs_mem.h"
#include "sni_rs_common.h"
#include "sni_rs_def.h"


struct sni_rs_hw_reg
{
	uint16_t addr;											// register address
	uint32_t value;											// register value
};

struct sni_rs_hw_info
{
	uint16_t hw_efuse36;									// HW EFUSE36[31:16] value
	uint32_t hw_efuse16;									// HW EFUSE16[31:0] value
};

struct sni_rs_txpowerinfo
{
	int txpower_value;										// Tx power value
	int txpower_reg_num;									// number of registers for setting Tx power
	struct sni_rs_hw_reg txpower_reg[MAX_TP_REG_SIZE];		// address and value of registers for setting Tx power
};

struct sni_rs_reg_table {
	int num;												// number of register table
	struct sni_rs_hw_reg *regs;
};

#ifdef __cplusplus
extern "C" {
#endif	/* __cplusplus */

	extern void sni_rs_txpowerinfo_init(struct sni_rs_txpowerinfo *info);
	extern enum SNI_BOOL sni_rs_txpowerinfo_set_txpower(struct sni_rs_txpowerinfo *info, uint8_t value, uint8_t sign);
	extern void sni_rs_txpowerinfo_calc_reg(struct sni_rs_txpowerinfo *info, struct sni_rs_hw_info *hw_info);
	static SNI_INLINE size_t sni_rs_txpowerinfo_get_reg_num(const struct sni_rs_txpowerinfo *info) {
		return info->txpower_reg_num;
	}
	static SNI_INLINE uint32_t sni_rs_txpowerinfo_get_regaddr(const struct sni_rs_txpowerinfo *info, size_t idx) {
		return (info->txpower_reg[idx].addr | (0x0000 << 16));
	}
	static SNI_INLINE uint32_t sni_rs_txpowerinfo_get_regdata(const struct sni_rs_txpowerinfo *info, size_t idx) {
		return info->txpower_reg[idx].value;
	}

#ifdef __cplusplus
}
#endif	/* __cplusplus */


#endif /* __sni_rs_txpower_h__ */
