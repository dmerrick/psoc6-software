/*
 * sni_rs_algo.h
 *
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

#ifndef __sni_rs_algo_H__
#define __sni_rs_algo_H__

/*
 * Enumerated Type Definition
 */

enum rs_algo_err {
	RS_NONE_ERR =  0,
	RS_OPEN_ERR =  1,
	RS_BOOT_ERR =  2,
	RS_PARAM_ERR =  3,
	RS_START_ERR =  4,
	RS_FRAMSIZE_ERR =  5,
	RS_GET_RAWDATA_ERR =  5,
	RS_ALGO_RUN_ERR = 6,
	RS_CHANGE_SENSEMODE_ERR = 7,
};


struct sni_rs_algo_ctrl
{
	int start_req;
	int start_ack;
	int stop_req;
	int stop_ack;
	int start_en;
	int hwfft_en;
	enum rs_algo_err err;
	int line;
	
};

#endif	/* __sni_rs_algo_H__ */
