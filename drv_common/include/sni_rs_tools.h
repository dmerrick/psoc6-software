/*
 * sni_rs_tools.h
 *
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

#ifndef __sni_rs_tools_h__
#define __sni_rs_tools_h__

#ifdef TARGET_KERNEL
#include <linux/types.h>
#include <linux/uaccess.h>
#else
#include <stddef.h>
#include <stdint.h>
#endif

#include "sni_rs_common.h"

#ifdef __cplusplus
extern "C" {
#endif	/* __cplusplus */


	// swap uint8_t array
	//   (ex) swap_array(data, 2, 2)
	//           {0, 1} => {1, 0}
	//   (ex) swap_array(data, 8, 4)
	//           {0, 1, 2, 3, 4, 5, 6, 7} => {3, 2, 1, 0, 7, 6, 5, 4}
	static SNI_INLINE int swap_array(uint8_t *data, size_t size, size_t unit)
	{
		size_t i;
		size_t j;

		if(unit < 2){
			return -1;
		}
		if(size % unit != 0){
			return -1;
		}

		for(i=0; i<size/unit; i++){
			for(j=0; j<unit/2; j++){
				uint8_t temp = data[j];
				data[j] = data[unit-1-j];
				data[unit-1-j] = temp;
			}
			data += unit;
		}

		return 0;
	}

	extern int sni_rs_systimer_init(void);
	extern int sni_rs_systimer_destroy(void);
	extern uint32_t sni_rs_systimer_get_usec(void);

	extern void sni_rs_sleep_msec(uint32_t msec);
	extern void sni_rs_sleep_usec(uint32_t usec);

#ifdef __cplusplus
}
#endif	/* __cplusplus */

#endif	/* __sni_rs_tools_h__ */
