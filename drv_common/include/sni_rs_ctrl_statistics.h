/*
 * sni_rs_ctrl_statistics.h
 *
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

#ifndef _sni_rs_ctrl_statistics_H
#define _sni_rs_ctrl_statistics_H

#include "sni_rs_setupinfo.h"

#define STATISTICS_SIZE			18

struct sni_rs_stat_info									// debug information including statistics
{
	int _err_cnt;										// error frame count after the start
	int _end_code;										// latest frame end code when _err_cnt was incremented
	int _header;										// latest header when _err_cnt was incremented
	int _frm_cnt;										// total frame count after the start
	int _of_cnt;										// this FIFO overflow frame count after the start
	int _rcv_cnt;										// received frame count after the start (_frm_cnt = _of_cnt + _rcv_cnt)
	int _sn_skip_cnt;									// sensor device FIFO overflow frame count
	uint8_t _pre_sn;									// serial number in the header of latest received frame for calculating _sn_skip_cnt
	uint8_t _frm_no;									// frame number in FIFO page
	int _start_flag;
	int _frm_cnt_sum;
	int _rcv_cnt_sum;
	int _del_cnt_sum;
	int _err_cnt_sum;
};

struct sni_rs_counters									// debug information for dmesg(0)
{
	int frm_cnt;										// total frame count after the start
	int rcv_cnt;										// received frame count after the start (frm_cnt = of_cnt + rcv_cnt)
	int of_cnt;											// this FIFO overflow frame count after the start
	int err_cnt;										// error frame count after the start
	int end_code;										// latest frame end code when _err_cnt was incremented
	int header;											// latest header when _err_cnt was incremented
	int sn_skip_cnt;									// sensor device FIFO overflow frame count
	int frm_cnt_sum;
	int rcv_cnt_sum;
	int del_cnt_sum;
	int err_cnt_sum;
};

struct sni_rs_statistics								// statistics for GET_STATISTICS command
{
	int frm_cnt;										// total frame count after the start
	int of_cnt;											// this FIFO overflow frame count after the start
	int err_cnt;										// error frame count after the start
	uint8_t stored_size;								// number of frames stored in FIFO
	uint8_t queue_size;									// FIFO queue size (number of frames)
};

#ifdef __cplusplus
extern "C" {
#endif	/* __cplusplus */

	static SNI_INLINE uint8_t sni_rs_ctrl_get_last_frm_cnt(struct sni_rs_stat_info *stat) {
		return stat->_pre_sn;
	}

	static SNI_INLINE void sni_rs_ctrl_get_spi1_counters(struct sni_rs_stat_info *stat, struct sni_rs_counters *cnt) {
		cnt->frm_cnt = stat->_frm_cnt;
		cnt->rcv_cnt = stat->_rcv_cnt;
		cnt->of_cnt = stat->_of_cnt;
		cnt->err_cnt = stat->_err_cnt;
		cnt->end_code = stat->_end_code;
		cnt->header = stat->_header;
		cnt->sn_skip_cnt = stat->_sn_skip_cnt;
		cnt->frm_cnt_sum = stat->_frm_cnt_sum;
		cnt->rcv_cnt_sum = stat->_rcv_cnt_sum;
		cnt->del_cnt_sum = stat->_del_cnt_sum;
		cnt->err_cnt_sum = stat->_err_cnt_sum;
	}

	static SNI_INLINE void sni_rs_ctrl_stat_clear(struct sni_rs_stat_info *stat)
	{
		stat->_err_cnt = 0;
		stat->_frm_cnt = 0;
		stat->_header = 0;
		stat->_end_code = 0;
		stat->_of_cnt = 0;
		stat->_rcv_cnt = 0;
		stat->_sn_skip_cnt = 0;
		stat->_pre_sn = 255;
		stat->_frm_no = 0;
		stat->_start_flag = 1;
	}

	static SNI_INLINE void sni_rs_ctrl_stat_sum_clear(struct sni_rs_stat_info *stat)
	{
		stat->_err_cnt_sum = 0;
		stat->_frm_cnt_sum = 0;
		stat->_del_cnt_sum = 0;
		stat->_rcv_cnt_sum = 0;
	}

	static SNI_INLINE void sni_rs_ctrl_stat_inc_del_cnt(struct sni_rs_stat_info *stat, int num)
	{
		stat->_del_cnt_sum = stat->_del_cnt_sum + num;
	}

#ifdef __cplusplus
}
#endif	/* __cplusplus */

#endif /* _sni_rs_ctrl_statistics_H */
