/*
 * sni_rs_fifo.h
 *
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

#ifndef _sni_rs_fifo_H
#define _sni_rs_fifo_H

#ifdef TARGET_KERNEL
#include <linux/types.h>
#include <linux/uaccess.h>
#else
#include <string.h>
#include <stddef.h>
#include <stdint.h>
#endif

#include "sni_rs_startinfo.h"
#include "sni_rs_ctrl_statistics.h"
#include "sni_rs_common.h"

#include "sni_rs_fifo_target.h"


struct sni_rs_fifo_attr
{
	size_t pagenum;
	struct sni_rs_fifo_attr_target target;
};

struct sni_rs_fifo										// FIFO management information
{
	size_t _byte_frame_size_wh;							// frame size including header, status, frame end code and EVK header (byte)
	size_t _frm_num;									// number of frames in a page
	size_t max_pagenum;									// max pages
	struct sni_rs_fifo_target target;
};

#ifdef __cplusplus
extern "C" {
#endif	/* __cplusplus */

	/* sni_rs_fifo functions */
	extern enum SNI_BOOL sni_rs_fifo_init(struct sni_rs_fifo *fifo, const struct sni_rs_fifo_attr *attr);
	extern void sni_rs_fifo_free(struct sni_rs_fifo *fifo);

	extern void sni_rs_fifo_setup(struct sni_rs_fifo *fifo, const struct sni_rs_startinfo *info);
	extern void sni_rs_fifo_clear(struct sni_rs_fifo *fifo);
	extern void sni_rs_fifo_clear_of(struct sni_rs_fifo *fifo, int *overflow);
	extern void sni_rs_fifo_reset(struct sni_rs_fifo *fifo);

	extern uint8_t *sni_rs_fifo_get_rp(struct sni_rs_fifo *fifo);
	extern void sni_rs_fifo_inc_rp(struct sni_rs_fifo *fifo);

	extern void sni_rs_fifo_set_framenum(struct sni_rs_fifo *fifo, const struct sni_rs_startinfo *info);
	extern size_t sni_rs_fifo_get_framenum(const struct sni_rs_fifo *fifo);
	extern int sni_rs_fifo_get_pagenum(struct sni_rs_fifo *fifo);

	extern enum SNI_BOOL sni_rs_fifo_put_framedata(struct sni_rs_fifo *fifo, struct sni_rs_stat_info *stat, const uint8_t *frame_data, size_t frame_size);

	extern void sni_rs_fifo_get_pagedata(struct sni_rs_fifo *fifo, void **buf, size_t *size);
	extern void sni_rs_fifo_purge_pagedata(struct sni_rs_fifo *fifo);

	extern uint8_t sni_rs_fifo_get_maxframenum(const struct sni_rs_fifo *fifo);

	/* sni_rs_fifo functions */
	extern enum SNI_BOOL sni_rs_fifo_init_target(struct sni_rs_fifo *fifo, const struct sni_rs_fifo_attr *attr);
	extern void sni_rs_fifo_free_target(struct sni_rs_fifo *fifo);

#ifdef __cplusplus
}
#endif	/* __cplusplus */

#endif /* _sni_rs_fifo_H */
