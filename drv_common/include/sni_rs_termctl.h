/*
 * sni_rs_termctl.h
 *
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

#ifndef __sni_rs_termctl_h__
#define __sni_rs_termctl_h__

#ifdef TARGET_KERNEL
#include <linux/types.h>
#include <linux/uaccess.h>
#include <linux/interrupt.h>
#include <linux/mutex.h>
#else
#include <stddef.h>
#include <stdint.h>
#endif

//#ifdef TARGET_STM32
typedef void (*sni_rs_intr_isr_t)(void *);
typedef void *sni_rs_term_t;
typedef void *sni_rs_intr_t;
//#endif
//#ifdef TARGET_KERNEL
//typedef irq_handler_t sni_rs_intr_isr_t;
//typedef struct _sni_rs_term_t {
//	int type;
//	int term;
//} sni_rs_term_t;
//typedef struct _sni_rs_intr_t {
//	sni_rs_term_t term;
//	int irq;
//	int isenable;
//	struct mutex irq_mutex;
//} sni_rs_intr_t;
//#endif
//#ifdef TARGET_FT4222H
//typedef void (*sni_rs_intr_isr_t)(void *);
//typedef struct _sni_rs_term_t
//{
//	void *lib;
//	uint32_t port;
//} sni_rs_term_t;
//typedef void *sni_rs_intr_t;
//#endif


struct sni_rs_termctl_t
{
	sni_rs_term_t nrst;
	sni_rs_term_t ce;
	sni_rs_term_t ld1;
	sni_rs_term_t ld3;
	sni_rs_term_t led;
	sni_rs_term_t digtest0;
	sni_rs_term_t digtest1;
	sni_rs_term_t sens;

	sni_rs_intr_t m_or;
};


#ifdef __cplusplus
extern "C" {
#endif	/* __cplusplus */

	extern int sni_rs_termctl_init(struct sni_rs_termctl_t *term, void *dev);
	extern int sni_rs_termctl_free(struct sni_rs_termctl_t *term);
	extern int sni_rs_term_write(sni_rs_term_t *term, int val);
	extern int sni_rs_intr_enable(sni_rs_intr_t *intr);
	extern int sni_rs_intr_disable(sni_rs_intr_t *intr);
	extern int sni_rs_intr_setisr(sni_rs_intr_t *intr, sni_rs_intr_isr_t func, void *arg);
	extern int sni_rs_intr_clearisr(sni_rs_intr_t *intr, void *arg);

#ifdef __cplusplus
}
#endif	/* __cplusplus */

#endif	/* __sni_rs_termctl_h__ */
