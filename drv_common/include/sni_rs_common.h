/*
 * sni_rs_common.h
 *
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

#ifndef __sni_rs_common_H__
#define __sni_rs_common_H__

enum SNI_BOOL
{
	SNI_FALSE			= 0,
	SNI_TRUE			= !SNI_FALSE,
};

enum SNI_RET
{
	SNI_RET_OK			= 0,
	SNI_RET_EINV		= 1,
	SNI_RET_EDEV		= 3,
	SNI_RET_ECMD		= 4,
	SNI_RET_ESTATE		= 5,
	SNI_RET_ENOMEM		= 7,
	SNI_RET_EBUSY		= 9,
	SNI_RET_EDMA12		= 12,
	SNI_RET_EDMA13		= 13,
	SNI_RET_ESPI		= 3,
};

enum SNI_STATE
{
	SNI_STATE_INVALID	= -1,
	SNI_STATE_INIT		= 0,
	SNI_STATE_BOOT		= 1,
	SNI_STATE_READY		= 2,
	SNI_STATE_SENSING	= 3,
	SNI_STATE_INIT2		= 4,
	SNI_STATE_READY_SD	= 5,
	SNI_STATE_READY_DS	= 6,
};


#define SNI_BOOL_CONV(a)		((a) ? SNI_TRUE : SNI_FALSE)

#if defined(_MSC_VER) && !defined(__cplusplus)
#  define SNI_INLINE __inline
#else
#  define SNI_INLINE inline
#endif

#ifdef _MSC_VER
#  define snprintf(x,y,...) _snprintf_s((x), (y), ((y) - 1), __VA_ARGS__)
#endif

/*
 * [frame data size information]
 *
 * +-------------+-----------------+-------------------+---------+
 * |             |  device header  |                   |         |
 * | user header +--------+--------+ iq data (1 frame) | endcode |
 * |             | status | header |                   |         |
 * +-------------+--------+--------+-------------------+---------+
 *
 *     frame data			: total
 *       user header		: user_header
 *       device output		: device_total
 *         device header	: device_header
 *           status			: device_header_status
 *           header			: device_header_header
 *         iq data			: device_iqdata
 *         device endcode	: device_endcode
 *
 */

struct sni_rs_framebytesize
{
	size_t user_header;

	size_t device_header_status;
	size_t device_header_header;
	size_t device_iqdata;
	size_t device_endcode;

	size_t device_header;
	size_t device_total;
	size_t total;
};

struct sni_rs_framewordsize
{
	size_t device_header_status;
	size_t device_header_header;
	size_t device_iqdata;
	size_t device_endcode;

	size_t device_header;
	size_t device_total;
};

#endif	/* __sni_rs_common_H__ */
