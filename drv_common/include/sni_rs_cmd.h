/*
 * sni_rs_cmd.h
 *
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

#ifndef __sni_rs_cmd_h__
#define __sni_rs_cmd_h__

#ifdef TARGET_KERNEL
#include <linux/types.h>
#include <linux/uaccess.h>
#else
#include <stddef.h>
#include <stdint.h>
#endif

#include "sni_rs_ctrl.h"
#include "sni_rs_mem.h"

typedef void (*func_putc_t)(void *, uint8_t);

#define CMD_NOP						0x00
#define CMD_REBOOT					0x01
#define CMD_ERR_MSG					0x02
#define CMD_DEBUG_MSG				0x03
#define CMD_TEST_MSG				0x04
#define CMD_BOOT					0x10
#define CMD_SHUTDOWN				0x11
#define CMD_SETUP					0x12
#define CMD_START					0x13
#define CMD_STOP					0x14
#define CMD_CAL						0x15
#define CMD_RESET					0x16
#define CMD_CE						0x17
#define CMD_NRST					0x18
#define CMD_CHANGE_SENSEMODE		0x19
#define CMD_SOFT_RESET				0x20
#define CMD_HARD_RESET				0x21
#define CMD_DEEP_SLEEP				0x22
#define CMD_READ_STATUS				0x30
#define CMD_SPI_W					0x32
#define CMD_SPI_R					0x33
#define CMD_GET_STATUS				0x34
#define CMD_GET_FRAME_SIZE			0x35
#define CMD_GET_HEADER_MODE			0x36
#define CMD_GET_STATUS_MODE			0x37
#define CMD_GET_BITWIDTH			0x38
#define CMD_GET_INFO				0x3A
#define CMD_GET_STATISTICS			0x3B
#define CMD_GET_WATERMARK			0x3C
#define CMD_TX_SET_FRAME_NUM		0x42
#define CMD_TX_GET_FRAME_NUM		0x43
#define CMD_TX_GET_FIFO_NUM			0x44
#define CMD_TX_GET_FRAME_COUNT		0x45
#define CMD_TX_RAW					0x50
#define CMD_REG_W					0x60
#define CMD_REG_R					0x61
#define CMD_SET_REG_S				0x62
#define CMD_SET_SEQ					0x63
#define CMD_SET_FFTWIN				0x64
#define CMD_SET_SEQ_ADDR			0x65
#define CMD_SET_FRAME_SIZE			0x66
#define CMD_SET_HEADER_MODE			0x67
#define CMD_SET_STATUS_MODE			0x68
#define CMD_SET_BITWIDTH			0x69
#define CMD_SET_WATERMARK			0x6B
#define CMD_SET_TX_POWER			0x6C
#define CMD_WRITE_SRAM				0x70
#define CMD_SPI_OPECODE				0x71
#define CMD_PASS_THROUGH 			0xF0

#ifdef __cplusplus
extern "C" {
#endif	/* __cplusplus */

	extern int sni_rs_cmd_execute_init(struct sni_rs_ctrl *ctrl, const struct sni_rs_mem_attr *buf);
	extern int sni_rs_cmd_execute_destroy(struct sni_rs_ctrl *ctrl);

	extern int sni_rs_cmd_execute(struct sni_rs_ctrl *ctrl, struct sni_rs_termctl_t *termctl, uint8_t *inbuf, uint32_t inbufsize, uint8_t *outbuf, uint32_t outbufsize, uint32_t *outsize);
	extern int sni_rs_cmd_execute_directout(struct sni_rs_ctrl *ctrl, struct sni_rs_termctl_t *termctl, uint8_t *inbuf, uint32_t inbufsize, func_putc_t func, void *func_arg);

	extern enum SNI_RET sni_rs_cmd_boot(struct sni_rs_ctrl *ctrl);
	extern enum SNI_RET sni_rs_cmd_shutdown(struct sni_rs_ctrl *ctrl);
	extern enum SNI_RET sni_rs_cmd_set_reg_s(struct sni_rs_ctrl *ctrl, uint8_t *data, size_t size);
	extern enum SNI_RET sni_rs_cmd_set_seq(struct sni_rs_ctrl *ctrl, uint8_t *data, size_t size);
	extern enum SNI_RET sni_rs_cmd_set_fftwin(struct sni_rs_ctrl *ctrl, uint8_t *data, size_t size);
	extern enum SNI_RET sni_rs_cmd_set_seq_addr(struct sni_rs_ctrl *ctrl, uint8_t *data, size_t size);
	extern enum SNI_RET sni_rs_cmd_setup(struct sni_rs_ctrl *ctrl);
	extern enum SNI_RET sni_rs_cmd_start(struct sni_rs_ctrl *ctrl, uint8_t type);
	extern enum SNI_RET sni_rs_cmd_stop(struct sni_rs_ctrl *ctrl);
	extern enum SNI_RET sni_rs_cmd_tx_raw(struct sni_rs_ctrl *ctrl);
	extern enum SNI_RET sni_rs_cmd_cal(struct sni_rs_ctrl *ctrl);
	extern enum SNI_RET sni_rs_cmd_reset(struct sni_rs_ctrl *ctrl);
	extern enum SNI_RET sni_rs_cmd_reg_w(struct sni_rs_ctrl *ctrl, uint32_t addr, uint32_t data);
	extern enum SNI_RET sni_rs_cmd_reg_r(struct sni_rs_ctrl *ctrl, uint32_t addr, uint32_t *data);
	extern enum SNI_RET sni_rs_cmd_set_watermark(struct sni_rs_ctrl *ctrl, size_t size);
	extern enum SNI_RET sni_rs_cmd_set_txpower(struct sni_rs_ctrl *ctrl, uint8_t value, uint8_t sign);
	extern enum SNI_RET sni_rs_cmd_change_sensemode(struct sni_rs_ctrl *ctrl, uint8_t value);

	extern enum SNI_RET sni_rs_cmd_soft_reset(struct sni_rs_ctrl *ctrl);
	extern enum SNI_RET sni_rs_cmd_hard_reset(struct sni_rs_ctrl *ctrl);
	extern enum SNI_RET sni_rs_cmd_deep_sleep(struct sni_rs_ctrl *ctrl);

	extern enum SNI_RET sni_rs_cmd_read_status(struct sni_rs_ctrl *ctrl, uint8_t *data);
	extern enum SNI_RET sni_rs_cmd_get_statistics(struct sni_rs_ctrl *ctrl, const uint8_t **data, size_t *size);
	extern enum SNI_RET sni_rs_cmd_spi_w(struct sni_rs_ctrl *ctrl, uint8_t *data, size_t size);
	extern enum SNI_RET sni_rs_cmd_spi_r(struct sni_rs_ctrl *ctrl, uint8_t *wdata, uint8_t *rdata, size_t size);

	extern enum SNI_RET sni_rs_cmd_write_sram(struct sni_rs_ctrl *ctrl, uint8_t *data, size_t size);
	extern enum SNI_RET sni_rs_cmd_spi_opecode(struct sni_rs_ctrl *ctrl, uint8_t op);

	extern enum SNI_RET sni_rs_cmd_pass_through(struct sni_rs_ctrl *ctrl, uint8_t *wdata, size_t wsize, uint8_t *rdata, size_t *rsize);

#ifdef __cplusplus
}
#endif	/* __cplusplus */

#endif	/* __sni_rs_cmd_h__ */
