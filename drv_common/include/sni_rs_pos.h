/*
 * sni_rs_pos.h
 *
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

#ifndef _sni_rs_pos_H
#define _sni_rs_pos_H

#ifdef TARGET_KERNEL
#include <linux/types.h>
#include <linux/uaccess.h>
#else
#include <string.h>
#include <stddef.h>
#include <stdint.h>
#endif

#include "sni_rs_startinfo.h"
#include "sni_rs_mem.h"


struct sni_rs_pos_attr
{
	const struct sni_rs_mem_attr *frame;
};

struct sni_rs_pos										// frame position information for reading IQ data
{
	struct sni_rs_mem buf;								// memory of framedata
	size_t wp_idx;										// write pointer (index)

	struct sni_rs_framebytesize size;					// framesize info (byte)
	size_t watermark;									// size of watermark (byte)
	uint32_t counter;									// spi read counter
};

#ifdef __cplusplus
extern "C" {
#endif	/* __cplusplus */

	extern void sni_rs_pos_init(struct sni_rs_pos *pos, const struct sni_rs_pos_attr *attr);
	extern void sni_rs_pos_free(struct sni_rs_pos *pos);

	extern enum SNI_BOOL sni_rs_pos_setup(struct sni_rs_pos *pos, const struct sni_rs_startinfo *info);
	extern void sni_rs_pos_clear(struct sni_rs_pos *pos);
	extern void sni_rs_pos_reset(struct sni_rs_pos *pos);

	static SNI_INLINE void sni_rs_pos_set_watermark(struct sni_rs_pos *pos, const struct sni_rs_startinfo *info) {
		pos->watermark = sni_rs_startinfo_get_watermark_byte(info);
	}

	static SNI_INLINE enum SNI_BOOL sni_rs_pos_isempty(const struct sni_rs_pos *pos) {
		return SNI_BOOL_CONV(pos->wp_idx == 0);
	}
	static SNI_INLINE enum SNI_BOOL sni_rs_pos_isfull(const struct sni_rs_pos *pos) {
		return SNI_BOOL_CONV(pos->wp_idx == pos->size.total);
	}

	extern enum SNI_BOOL sni_rs_pos_inc_wp(struct sni_rs_pos *pos, size_t size);
	extern enum SNI_BOOL sni_rs_pos_get_bufofuserheader(struct sni_rs_pos *pos, void **buf, size_t *size);
	extern enum SNI_BOOL sni_rs_pos_get_buftoread(struct sni_rs_pos *pos, void **buf, size_t *size, size_t *offset);

	extern enum SNI_BOOL sni_rs_pos_get_framedata(const struct sni_rs_pos *pos, void **data, size_t *size);

#ifdef __cplusplus
}
#endif	/* __cplusplus */

#endif /* _sni_rs_pos_H */
