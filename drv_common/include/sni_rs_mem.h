/*
 * sni_rs_mem.h
 *
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

#ifndef __sni_rs_mem_h__
#define __sni_rs_mem_h__

#include "sni_rs_common.h"
//#include "sni_rs_dprint.h"

struct sni_rs_mem
{
	void *alloc_buf;
	size_t alloc_size;

	struct {
		void *buf;
		size_t size;
	} static_mem;
};

struct sni_rs_mem_attr
{
	void *buf;
	size_t size;
};

#ifdef __cplusplus
extern "C" {
#endif	/* __cplusplus */

	static SNI_INLINE void sni_rs_mem_attr_set(struct sni_rs_mem_attr *attr, void *buf, size_t size) {
		attr->buf = buf;
		attr->size = size;
	}

	extern void sni_rs_mem_dmesg_print(void);
	extern void sni_rs_mem_dmesg_cnt_alloc(size_t size);
	extern void sni_rs_mem_dmesg_cnt_alloc_error(void);
	extern void sni_rs_mem_dmesg_cnt_free(size_t);
	extern void sni_rs_mem_dmesg_cnt_free_error(void);
	extern void sni_rs_mem_dmesg_cnt_mem_get_null(void);

	/* set attr = NULL, if use dynamic memory allocation */
	extern enum SNI_BOOL sni_rs_mem_init(struct sni_rs_mem *mem, const struct sni_rs_mem_attr *attr);
	extern enum SNI_BOOL sni_rs_mem_alloc(struct sni_rs_mem *mem, size_t size);
	extern void sni_rs_mem_free(struct sni_rs_mem *mem);
	static SNI_INLINE void *sni_rs_mem_get(const struct sni_rs_mem *mem) {
		if(mem->alloc_buf == NULL) {
//			sni_rs_dprint(SNI_RS_DPRINT_ERR, "[sni_rs] %s: get nullptr\n", __FUNCTION__);
			sni_rs_mem_dmesg_cnt_mem_get_null();
		}
		return mem->alloc_buf;
	}
	static SNI_INLINE size_t sni_rs_mem_getsize(const struct sni_rs_mem *mem) {
		return mem->alloc_size;
	}

	extern enum SNI_BOOL sni_rs_mem_calloc(struct sni_rs_mem *mem, size_t size);
	extern enum SNI_BOOL sni_rs_mem_copy(struct sni_rs_mem *dst, const struct sni_rs_mem *src);

#ifdef __cplusplus
}
#endif	/* __cplusplus */


#endif	/* __sni_rs_mem_h__ */
