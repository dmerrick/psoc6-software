/*
 * sni_rs_def.h
 *
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

#ifndef __sni_rs_def_h__
#define __sni_rs_def_h__

#define SNI_RS_DEFAULT_FRAMES_PER_PAGE			(1)
#define SNI_RS_DEFAULT_HEADER_STATUS_MODE		(0)
#define SNI_RS_DEFAULT_HEADER_HEADER_MODE		(0)
#define SNI_RS_DEFAULT_BITWIDTH					(0)

#define SNI_RS_DEVICE_ENDCODE_WORDSIZE			(1)
#define SNI_RS_USERHEADER_SIZE					(20)

#define DEVICE_HEADER				3													// 32 bit word(status,header,frame endcode)
//#define MAX_FRAME_BODY				8189												// 32 bit word(8191-3(DEVICE_HEADER))
//#define MAX_FRAME_BODY				1536												// 32 bit word(8191-3(DEVICE_HEADER))
#define MAX_FRAME_BODY				2048												// 32 bit word(8191-3(DEVICE_HEADER))
#define MAX_FRAME_SIZE				(4*((MAX_FRAME_BODY)+(DEVICE_HEADER))+SNI_RS_USERHEADER_SIZE)
//#define BASE_FRAME_BODY				4096												// 32 bit word
//#define BASE_FRAME_BODY				1536												// 32 bit word
#define BASE_FRAME_BODY				2048												// 32 bit word
#define BASE_FRAME_SIZE				(4*((BASE_FRAME_BODY)+(DEVICE_HEADER))+SNI_RS_USERHEADER_SIZE)
#define MAX_REG_SIZE				256													// 32 bit word
//#define MAX_SEQ_SIZE				(16*1024)											// byte
//#define MAX_SEQ_SIZE				(8*1024)											// byte
#define MAX_SEQ_SIZE				(5*1024)											// byte
#define MAX_FFT_SIZE				512													// 16 bit word
//#define PAGE_CAPACITY				((BASE_FRAME_SIZE)*3)								// max 3 frame if frame size = 16KB+alpha
#define PAGE_CAPACITY				((BASE_FRAME_SIZE)*1)								// for STM memory delete
#define MAX_TP_REG_SIZE				8

#define XTAL_WAIT					5000

#define CAL_STAT_COUNT				100
#define CAL_WAIT_US					100													// CAL_STAT_COUNT * CAL_WAIT_US = 10ms

#define SNI_RS_SETUP_SEQCODE_MAXSIZE			(MAX_SEQ_SIZE)
#define SNI_RS_SETUP_REGADDR_MAXSIZE			((MAX_REG_SIZE) * sizeof(uint16_t))
#define SNI_RS_SETUP_REGDATA_MAXSIZE			((MAX_REG_SIZE) * sizeof(uint32_t))
#define SNI_RS_SETUP_FFTWIN_MAXSIZE				((MAX_FFT_SIZE) * sizeof(uint16_t))

#ifdef TARGET_STM32
#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0])) /* Array size */
#endif	/* TARGET_STM32 */
#ifdef TARGET_FT4222H
#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0])) /* Array size */
#endif /* TARGET_FT4222H */

#endif /* __sni_rs_def_h__ */
