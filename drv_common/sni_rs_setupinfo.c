/*
 * sni_rs_setupinfo.c
 *
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

#ifdef TARGET_KERNEL
#include <linux/types.h>
#include <linux/uaccess.h>
#else
#include <string.h>
#include <stddef.h>
#include <stdint.h>
#endif

#include "sni_rs_setupinfo.h"
#include "sni_rs_common.h"


/* extern function */
enum SNI_BOOL sni_rs_setupinfo_init(struct sni_rs_setupinfo *info, const struct sni_rs_setupinfo_attr *attr)
{
	memset(info, 0, sizeof(struct sni_rs_setupinfo));

	sni_rs_mem_init(&(info->seqcode.code), attr->buf_seqcode);
	sni_rs_mem_init(&(info->reg_s.addr), attr->buf_regaddr);
	sni_rs_mem_init(&(info->reg_s.data), attr->buf_regdata);
	sni_rs_mem_init(&(info->fftwin.data), attr->buf_fftwin);

	sni_rs_setupinfo_clear(info);

	sni_rs_txpowerinfo_init(&(info->txpower.txpowerinfo));

	return SNI_TRUE;
}

enum SNI_BOOL sni_rs_setupinfo_clear(struct sni_rs_setupinfo *info)
{
	info->type.status_mode = SNI_RS_DEFAULT_HEADER_STATUS_MODE;
	info->type.header_mode = SNI_RS_DEFAULT_HEADER_HEADER_MODE;
	info->type.bitwidth = SNI_RS_DEFAULT_BITWIDTH;
	info->reg_s.flag = SNI_FALSE;
	info->seqcode.flag = SNI_FALSE;
	info->seqaddr.flag = SNI_FALSE;
	info->txpower.flag = SNI_FALSE;
	info->reg_s.num = 0;
	info->seqcode.size = 0;
	info->seqaddr.sensing = 0;
	info->seqaddr.cal = 0;
	sni_rs_setupinfo_init_fftwin(info);
	sni_rs_txpowerinfo_init(&(info->txpower.txpowerinfo));
	return SNI_TRUE;
}

enum SNI_BOOL sni_rs_setupinfo_copy(struct sni_rs_setupinfo *dst, const struct sni_rs_setupinfo *src)
{
	{
		dst->reg_s.num = src->reg_s.num;
		if(!sni_rs_mem_copy(&(dst->reg_s.addr), &(src->reg_s.addr))) {
			return SNI_FALSE;
		}
		if(!sni_rs_mem_copy(&(dst->reg_s.data), &(src->reg_s.data))) {
			return SNI_FALSE;
		}
	}
	{
		dst->seqcode.size = src->seqcode.size;
		if(!sni_rs_mem_copy(&(dst->seqcode.code), &(src->seqcode.code))) {
			return SNI_FALSE;
		}
	}
	{
		dst->fftwin.size = src->fftwin.size;
		if(!sni_rs_mem_copy(&(dst->fftwin.data), &(src->fftwin.data))) {
			return SNI_FALSE;
		}
	}
	{
		dst->seqaddr.sensing = src->seqaddr.sensing;
		dst->seqaddr.cal = src->seqaddr.cal;
	}

	memcpy(&(dst->type), &(src->type), sizeof(dst->type));
	dst->iqdata_wordsize = src->iqdata_wordsize;

	memcpy(&(dst->txpower.txpowerinfo), &(src->txpower.txpowerinfo), sizeof(dst->txpower.txpowerinfo));

	return SNI_TRUE;
}

void sni_rs_setupinfo_free(struct sni_rs_setupinfo *info)
{
	sni_rs_mem_free(&(info->reg_s.addr));
	sni_rs_mem_free(&(info->reg_s.data));
	sni_rs_mem_free(&(info->seqcode.code));
	sni_rs_mem_free(&(info->fftwin.data));
}

enum SNI_BOOL sni_rs_setupinfo_isenable(const struct sni_rs_setupinfo *info)
{
	if((info->reg_s.flag) && (info->seqcode.flag) && (info->seqaddr.flag) && (info->fftwin.flag) && (info->iqdata_wordsize != 0) && (info->txpower.flag)) {
		return SNI_TRUE;
	}

	return SNI_FALSE;
}

enum SNI_BOOL sni_rs_setupinfo_set_framesize(struct sni_rs_setupinfo *info, uint32_t wordsize)
{
	if (wordsize > MAX_FRAME_BODY) {
		return SNI_FALSE;
	}
	if (wordsize == 0) {
		return SNI_FALSE;
	}

	info->iqdata_wordsize = wordsize;

	return SNI_TRUE;
}

enum SNI_RET sni_rs_setupinfo_set_seqcode(struct sni_rs_setupinfo *info, const uint8_t *d, size_t num)
{
	if (num % 16 != 0){
		return SNI_RET_EINV;
	}
	info->seqcode.flag = SNI_TRUE;

	info->seqcode.size = num;
	sni_rs_mem_free(&(info->seqcode.code));
	if(!sni_rs_mem_calloc(&(info->seqcode.code), info->seqcode.size)) {
		return SNI_RET_ENOMEM;
	}
	memcpy(sni_rs_mem_get(&(info->seqcode.code)), d, num);

	return SNI_RET_OK;
}

enum SNI_RET sni_rs_setupinfo_set_reg(struct sni_rs_setupinfo *info, const uint8_t *d, size_t num) {
	if (num % 6 != 0){
		return SNI_RET_EINV;
	}
	info->reg_s.flag = SNI_TRUE;

	info->reg_s.num = num / 6;
	sni_rs_mem_free(&(info->reg_s.addr));
	sni_rs_mem_free(&(info->reg_s.data));
	if(!sni_rs_mem_calloc(&(info->reg_s.addr), info->reg_s.num * sizeof(uint16_t))) {
		return SNI_RET_ENOMEM;
	}
	if(!sni_rs_mem_calloc(&(info->reg_s.data), info->reg_s.num * sizeof(uint32_t))) {
		sni_rs_mem_free(&(info->reg_s.addr));
		return SNI_RET_ENOMEM;
	}
	{
		size_t i;
		for (i = 0; i < info->reg_s.num; i++) {
			((uint16_t *) (sni_rs_mem_get(&(info->reg_s.addr))))[i] = (d[0] << 0) | (d[1] << 8);
			((uint32_t *) (sni_rs_mem_get(&(info->reg_s.data))))[i] = (d[2] << 0) | (d[3] << 8) | (d[4] << 16) | (d[5] << 24);
			d += 6;
		}
	}

	return SNI_RET_OK;
}

enum SNI_BOOL sni_rs_setupinfo_init_fftwin(struct sni_rs_setupinfo *info) {
	const uint8_t data[4] = {0x3f, 0xff, 0x3f, 0xff};
	size_t size = 4;

	if(sni_rs_setupinfo_set_fftwin(info, data, size) != SNI_RET_OK) {
		return SNI_FALSE;
	}

	return SNI_TRUE;
}

enum SNI_RET sni_rs_setupinfo_set_fftwin(struct sni_rs_setupinfo *info, const uint8_t *d, size_t num) {
	if (num % 2 != 0){
		return SNI_RET_EINV;
	}
	info->fftwin.flag = SNI_TRUE;

	info->fftwin.size = num;
	sni_rs_mem_free(&(info->fftwin.data));
	if(!sni_rs_mem_calloc(&(info->fftwin.data), info->fftwin.size * sizeof(uint8_t))) {
		return SNI_RET_ENOMEM;
	}
	memcpy(sni_rs_mem_get(&(info->fftwin.data)), d, num);

	return SNI_RET_OK;
}

enum SNI_BOOL sni_rs_setupinfo_set_addr(struct sni_rs_setupinfo *info, const uint8_t *d, size_t num)
{
	if (num != 8){
		return SNI_FALSE;
	}
	info->seqaddr.flag = SNI_TRUE;

	info->seqaddr.sensing = (d[0] << 0) | (d[1] << 8) | (d[2] << 16) | (d[3] << 24);
	info->seqaddr.cal     = (d[4] << 0) | (d[5] << 8) | (d[6] << 16) | (d[7] << 24);

	return SNI_TRUE;
}

enum SNI_BOOL sni_rs_setupinfo_set_headheadmode(struct sni_rs_setupinfo *info, uint8_t hmode)
{
	if (hmode >= 2){
		return SNI_FALSE;
	}
	info->type.header_mode = hmode;

	return SNI_TRUE;
}

enum SNI_BOOL sni_rs_setupinfo_set_statheadmode(struct sni_rs_setupinfo *info, uint8_t smode)
{
	if (smode >= 2){
		return SNI_FALSE;
	}
	info->type.status_mode = smode;

	return SNI_TRUE;
}

enum SNI_BOOL sni_rs_setupinfo_set_bitwidth(struct sni_rs_setupinfo *info, uint8_t bwidth)
{
	if (bwidth >= 4){
		return SNI_FALSE;
	}
	info->type.bitwidth = bwidth;

	return SNI_TRUE;
}

enum SNI_BOOL sni_rs_setupinfo_set_txpower(struct sni_rs_setupinfo *info, uint8_t value, uint8_t sign, struct sni_rs_hw_info *hw_info)
{
	if (!sni_rs_txpowerinfo_set_txpower(&(info->txpower.txpowerinfo), value, sign)){
		return SNI_FALSE;
	}
	info->txpower.flag = SNI_TRUE;

	sni_rs_txpowerinfo_calc_reg(&info->txpower.txpowerinfo, hw_info);

	return SNI_TRUE;
}
