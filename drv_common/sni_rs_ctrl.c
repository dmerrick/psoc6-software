/*
 * sni_rs_ctrl.c
 *
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

#ifdef TARGET_KERNEL
#include <linux/types.h>
#include <linux/uaccess.h>
#else
#include <string.h>
#include <stddef.h>
#include <stdint.h>
#endif

#include "sni_rs_setupinfo.h"
#include "sni_rs_spi.h"
#include "sni_rs_tools.h"
#include "sni_rs_fifo.h"
#include "sni_rs_pos.h"
#include "sni_rs_ctrl.h"
#include "sni_rs_version.h"
#include "sni_rs_common.h"
#include "sni_rs_txpower.h"
//#include "sni_rs_dmesg.h"

static SNI_INLINE void sni_rs_ctrl_term_init(struct sni_rs_ctrl *ctrl)
{
	sni_rs_term_write(&(ctrl->termctl->nrst), 0);
	sni_rs_term_write(&(ctrl->termctl->ce), 0);
	// sni_rs_term_write(&(ctrl->termctl->digtest1), 0);
	// sni_rs_term_write(&(ctrl->termctl->digtest0), 0);
}

enum SNI_RET sni_rs_ctrl_term_read_hw_info(struct sni_rs_ctrl *ctrl, struct sni_rs_hw_info *hw_info)
{
	enum SNI_RET ret;
	uint32_t rdata;
	
	ret = sni_rs_spi_reg_read32(ctrl->_rf_spi1, REG_EFUSE36, &rdata);
	if(ret) {
		return ret;
	}
	hw_info->hw_efuse36 = (rdata & 0xffff0000) >> 16;

	ret = sni_rs_spi_reg_read32(ctrl->_rf_spi1, REG_EFUSE16, &rdata);
	if(ret) {
		return ret;
	}
	hw_info->hw_efuse16 = rdata;

	return SNI_RET_OK;
}

enum SNI_RET sni_rs_ctrl_term_efuse_set(struct sni_rs_ctrl *ctrl)
{
	enum SNI_RET ret;

	ret = sni_rs_spi_reg_write32(ctrl->_rf_spi1, REG_DIG_18, 0x00000001);
	if(ret) {
		return ret;
	}

	// sni_rs_sleep_usec(100);
    thread_sleep_for(1);

	ret = sni_rs_spi_reg_write32(ctrl->_rf_spi1, REG_DIG_2F, 0x00000F0F);

	return ret;
}

enum SNI_RET sni_rs_ctrl_term_set_selset13(struct sni_rs_ctrl *ctrl)
{
	enum SNI_RET ret = SNI_RET_OK;

	// efuse setting mode
	ret = sni_rs_spi_reg_write32(ctrl->_rf_spi1, REG_SELSET13, 0x00000000);

	return ret;
}

enum SNI_RET sni_rs_ctrl_term_boot(struct sni_rs_ctrl *ctrl)
{
	enum SNI_RET ret;
	struct sni_rs_hw_info hw_info;

	ret = sni_rs_spi_resetctl(ctrl->_rf_spi1, OP_SOFT_RESET);
	if(ret) {
		return ret;
	}
	//sni_rs_sleep_usec(XTAL_WAIT);
    thread_sleep_for(5); //ms
    
	//Initial register setting
	ret = sni_rs_spi_reg_write32(ctrl->_rf_spi1, REG_WATERMARK,  0x00001FFF);
	if(ret) {
		return ret;
	}

	//read e-fuse
	ret = sni_rs_ctrl_term_efuse_set(ctrl);
	if(ret) {
		return ret;
	}
	//read version
	ret = sni_rs_ctrl_term_read_hw_info(ctrl, &hw_info);
	if(ret) {
		return ret;
	}
	sni_rs_ctrl_set_hw_info(ctrl, &hw_info);
	ret = sni_rs_ctrl_term_set_selset13(ctrl);
	if(ret) {
		return ret;
	}

	ret = sni_rs_spi_resetctl(ctrl->_rf_spi1, OP_DEEP_SLEEP);
	if(ret) {
		return ret;
	}

	return SNI_RET_OK;
}

void sni_rs_ctrl_term_reset(struct sni_rs_ctrl *ctrl)
{
	sni_rs_ctrl_term_shutdown(ctrl);
	//sni_rs_sleep_usec(500);
    thread_sleep_for(5); //ms
	sni_rs_ctrl_term_set(ctrl);
	//sni_rs_sleep_usec(100);
    thread_sleep_for(5); //ms
}

void sni_rs_ctrl_term_set(struct sni_rs_ctrl *ctrl)
{
	sni_rs_term_write(&(ctrl->termctl->ce), 1);
	// sni_rs_sleep_msec(3);
    thread_sleep_for(5); //ms
	sni_rs_term_write(&(ctrl->termctl->nrst), 1);
}

void sni_rs_ctrl_term_shutdown(struct sni_rs_ctrl *ctrl)
{
	sni_rs_term_write(&(ctrl->termctl->nrst), 0);
	// sni_rs_sleep_usec(500);
    thread_sleep_for(5); //ms
	sni_rs_term_write(&(ctrl->termctl->ce), 0);
}

enum SNI_RET sni_rs_ctrl_term_write_regs(struct sni_rs_ctrl *ctrl)
{
	enum SNI_RET ret;
	size_t i;
	uint16_t *buf_addr = sni_rs_setupinfo_get_regaddr(&(ctrl->startinfo.setupinfo));
	uint32_t *buf_data = sni_rs_setupinfo_get_regdata(&(ctrl->startinfo.setupinfo));
	uint32_t tmp_data;

	// write initial registers for below sequencer code
	if(!(buf_addr && buf_data)) {
		return SNI_RET_ENOMEM;
	}
	for (i = 0; i < sni_rs_setupinfo_get_regnum(&(ctrl->startinfo.setupinfo)); i++) {
		ret = sni_rs_spi_reg_write32(ctrl->_rf_spi1, (uint32_t) buf_addr[i], buf_data[i]);
		if(ret) {
			return ret;
		}
	}

	// wrte Tx Power registers
	for (i = 0; i < sni_rs_txpowerinfo_get_reg_num(&ctrl->startinfo.setupinfo.txpower.txpowerinfo); i++) {
		ret = sni_rs_spi_reg_write32(ctrl->_rf_spi1, sni_rs_txpowerinfo_get_regaddr(&(ctrl->startinfo.setupinfo.txpower.txpowerinfo), i), sni_rs_txpowerinfo_get_regdata(&(ctrl->startinfo.setupinfo.txpower.txpowerinfo), i));
		if(ret) {
			return ret;
		}
	}

	// write watermark
	ret = sni_rs_spi_reg_write32(ctrl->_rf_spi1, REG_WATERMARK,  sni_rs_startinfo_get_watermark_word(&(ctrl->startinfo)));

	// write LDOSET5
	tmp_data = 0x03000103 | ((ctrl->hw_info.hw_efuse16 & 0x000FFF00) << 4);
	ret = sni_rs_spi_reg_write32(ctrl->_rf_spi1, REG_LDOSET5, tmp_data);

	return ret;
}

enum SNI_RET sni_rs_ctrl_term_write_seqcode(struct sni_rs_ctrl *ctrl)
{
	enum SNI_RET ret;
	uint8_t *buf = sni_rs_setupinfo_get_seqcodedata(&(ctrl->startinfo.setupinfo));
	if(!buf) {
		return SNI_RET_ENOMEM;
	}

	ret = sni_rs_spi_reg_write32(ctrl->_rf_spi1, REG_DIG_05, 0x00000000);
	if(ret) {
		return ret;
	}
	ret = sni_rs_spi_reg_write32(ctrl->_rf_spi1, REG_DIG_1F, 0x00000000);
	if(ret) {
		return ret;
	}
	ret = sni_rs_spi_reg_write8(ctrl->_rf_spi1, REG_SRAM_SEQ, buf, sni_rs_setupinfo_get_seqcodenum(&(ctrl->startinfo.setupinfo)));
	if(ret) {
		return ret;
	}
	ret = sni_rs_spi_reg_write32(ctrl->_rf_spi1, REG_DIG_1F, 0x00000001);         // cksel_mseq = penb
	if(ret) {
		return ret;
	}
	ret = sni_rs_spi_resetctl(ctrl->_rf_spi1, OP_SOFT_RESET);
	if(ret) {
		return ret;
	}
	ret = sni_rs_spi_reg_write32(ctrl->_rf_spi1, REG_DIG_05, 0x00000001);         // en_seq
	if(ret) {
		return ret;
	}

	ret = sni_rs_spi_resetctl(ctrl->_rf_spi1, OP_SOFT_RESET);
	
	return ret;
}

enum SNI_RET sni_rs_ctrl_term_write_fftwin(struct sni_rs_ctrl *ctrl)
{
	enum SNI_RET ret;
	uint8_t *buf = sni_rs_setupinfo_get_fftwindata(&(ctrl->startinfo.setupinfo));
	uint32_t rdata;
	
	ret = sni_rs_spi_reg_read32(ctrl->_rf_spi1, REG_BBSET40, &rdata);
	if(ret) {
		return ret;
	}
	rdata |= 0x00000004;

	ret = sni_rs_spi_reg_write32(ctrl->_rf_spi1, REG_DIG_1F, 0x00000001);
	if(ret) {
		return ret;
	}
	ret = sni_rs_spi_reg_write32(ctrl->_rf_spi1, REG_BBSET40, 0x00000000);
	if(ret) {
		return ret;
	}
	ret = sni_rs_spi_reg_write8(ctrl->_rf_spi1, REG_SRAM_FFTWIN, buf, sni_rs_setupinfo_get_fftwinnum(&(ctrl->startinfo.setupinfo)));
	if(ret) {
		return ret;
	}
	ret = sni_rs_spi_reg_write32(ctrl->_rf_spi1, REG_BBSET40, rdata);
	if(ret) {
		return ret;
	}
	ret = sni_rs_spi_reg_write32(ctrl->_rf_spi1, REG_DIG_1F, 0x00000003);
	if(ret) {
		return ret;
	}

	return SNI_RET_OK;
}

enum SNI_RET sni_rs_ctrl_term_setup(struct sni_rs_ctrl *ctrl)
{
	uint32_t start_time = 0;
	uint32_t elapsed_time = 0;
	enum SNI_RET ret;
	
	sni_rs_ctrl_disable_irq(ctrl->termctl);
	ret = sni_rs_spi_resetctl(ctrl->_rf_spi1, OP_SOFT_RESET);
	if(ret) {
		return ret;
	}
	
	sni_rs_sleep_usec(120);
	start_time = sni_rs_systimer_get_usec();

	// write registers
	ret = sni_rs_ctrl_term_write_regs(ctrl);
	if(ret) {
		return ret;
	}

	// wait for x'tal stabilized
	elapsed_time = sni_rs_systimer_get_usec() - start_time;
	if (elapsed_time < XTAL_WAIT) {
		sni_rs_sleep_usec(XTAL_WAIT - elapsed_time);
	}

	if (sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_SD)){
		struct sni_rs_hw_info hw_info;
		
		// Trimming based on eFuse
		ret = sni_rs_ctrl_term_efuse_set(ctrl);
		if(ret) {
			return ret;
		}
		sni_rs_ctrl_get_hw_info(ctrl, &hw_info);
		ret = sni_rs_ctrl_term_set_selset13(ctrl);
		if(ret) {
			return ret;
		}
		sni_rs_sleep_usec(100);
	}

	// write sequencer code
	ret = sni_rs_ctrl_term_write_seqcode(ctrl);
	if(ret) {
		return ret;
	}

	// write fftwin 
	ret = sni_rs_ctrl_term_write_fftwin(ctrl);
	if(ret) {
		return ret;
	}

	return SNI_RET_OK;
}

enum SNI_RET sni_rs_ctrl_term_cal(struct sni_rs_ctrl *ctrl)
{
	enum SNI_RET ret;
	int i;

	ret = sni_rs_spi_reg_write32(ctrl->_rf_spi1, REG_DIG_06, sni_rs_setupinfo_get_caladdr(&(ctrl->startinfo.setupinfo)));
	if(ret) {
		return ret;
	}
	
	//start CAL
	ret = sni_rs_spi_modectl(ctrl->_rf_spi1, OP_SINGLE_SHOT);
	if(ret) {
		return ret;
	}
	
	for (i = 0; i < CAL_STAT_COUNT; i++) {
		if(!sni_rs_spi_isbusy(ctrl->_rf_spi1)) {
			return SNI_RET_OK;
		}
		sni_rs_sleep_usec(CAL_WAIT_US);
	}

	return SNI_RET_EDEV;
}

enum SNI_RET sni_rs_ctrl_state_set(struct sni_rs_ctrl *ctrl, enum SNI_STATE new_state)
{
	switch(new_state){
	case SNI_STATE_INIT:
	case SNI_STATE_INIT2:
		sni_rs_term_write(&(ctrl->termctl->ld1), 0);
		sni_rs_term_write(&(ctrl->termctl->ld3), 0);
		sni_rs_term_write(&(ctrl->termctl->led), 1);
		break;
	case SNI_STATE_BOOT:
		sni_rs_term_write(&(ctrl->termctl->ld1), 1);
		sni_rs_term_write(&(ctrl->termctl->ld3), 0);
		sni_rs_term_write(&(ctrl->termctl->led), 1);
		break;
	case SNI_STATE_READY:
		sni_rs_term_write(&(ctrl->termctl->ld1), 0);
		sni_rs_term_write(&(ctrl->termctl->ld3), 1);
		sni_rs_term_write(&(ctrl->termctl->led), 1);
		break;
	case SNI_STATE_SENSING:
		sni_rs_term_write(&(ctrl->termctl->ld1), 1);
		sni_rs_term_write(&(ctrl->termctl->ld3), 1);
		sni_rs_term_write(&(ctrl->termctl->led), 1);
		break;
	case SNI_STATE_READY_SD:
		sni_rs_term_write(&(ctrl->termctl->ld1), 0);
		sni_rs_term_write(&(ctrl->termctl->ld3), 1);
		sni_rs_term_write(&(ctrl->termctl->led), 0);
		break;
	case SNI_STATE_READY_DS:
		sni_rs_term_write(&(ctrl->termctl->ld1), 1);
		sni_rs_term_write(&(ctrl->termctl->ld3), 1);
		sni_rs_term_write(&(ctrl->termctl->led), 0);
		break;
	case SNI_STATE_INVALID:
	default:
		sni_rs_term_write(&(ctrl->termctl->ld1), 0);
		sni_rs_term_write(&(ctrl->termctl->ld3), 0);
		sni_rs_term_write(&(ctrl->termctl->led), 1);
		return SNI_RET_EDEV;
	}

	ctrl->cur_state = new_state;

	return SNI_RET_OK;
}


void sni_rs_ctrl_init(struct sni_rs_ctrl *ctrl, const struct sni_rs_ctrl_attr *attr)
{
	memset(ctrl, 0, sizeof(struct sni_rs_ctrl));

	ctrl->_rf_spi1 = attr->spi;
	ctrl->termctl = attr->termctl;

	sni_rs_ctrl_term_init(ctrl);
	sni_rs_setupinfo_init(&(ctrl->setupinfo), attr->setupinfo_attr);
	sni_rs_startinfo_init(&(ctrl->startinfo), attr->startinfo_attr);
	sni_rs_fifo_init(&(ctrl->fifo), attr->fifo_attr);
	sni_rs_pos_init(&(ctrl->pos), attr->pos_attr);

	sni_rs_ctrl_state_set(ctrl, SNI_STATE_INIT);
	ctrl->hif_dma_flag = SNI_TRUE; 
}

void sni_rs_ctrl_free(struct sni_rs_ctrl *ctrl)
{
	sni_rs_pos_free(&(ctrl->pos));
	sni_rs_fifo_free(&(ctrl->fifo));
	sni_rs_setupinfo_free(&(ctrl->setupinfo));
	sni_rs_startinfo_free(&(ctrl->startinfo));
}

static SNI_INLINE enum SNI_BOOL sni_rs_ctrl_create_user_header(uint8_t *frame_data, size_t size, const struct sni_rs_stat_info *stat, const struct sni_rs_startinfo *info)
{
	if(size != info->frameinfo.byte.user_header) {
		return SNI_FALSE;
	}

	{
		const uint32_t timeinfo = sni_rs_systimer_get_usec();
		const uint32_t hmode = sni_rs_startinfo_get_headheadmode(info);
		const uint32_t smode = sni_rs_startinfo_get_statheadmode(info);
		const uint32_t bwidth = sni_rs_startinfo_get_bitwidth(info);
		const uint32_t frame_wordsize = info->frameinfo.word.device_iqdata;
		const uint32_t frm_num = sni_rs_startinfo_get_framenum(info);
		const uint32_t frm_cnt = stat->_frm_cnt;
		const uint32_t frm_no = stat->_frm_no;
		const uint32_t header_size = info->frameinfo.byte.user_header;

		frame_data[ 0] = 0x55;												//syncword
		frame_data[ 1] = 0xaa;												//syncword
		frame_data[ 2] = sni_rs_version_user_header;						//user header version
		frame_data[ 3] = header_size;										//user header size
		frame_data[ 4] = (timeinfo & 0xFF000000) >> 24;						//time_info
		frame_data[ 5] = (timeinfo & 0x00FF0000) >> 16;						//time_info
		frame_data[ 6] = (timeinfo & 0x0000FF00) >>  8;						//time_info
		frame_data[ 7] = (timeinfo & 0x000000FF) >>  0;						//time_info
		frame_data[ 8] = (frm_cnt & 0xFF000000) >> 24;						//counter
		frame_data[ 9] = (frm_cnt & 0x00FF0000) >> 16;						//counter
		frame_data[10] = (frm_cnt & 0x0000FF00) >>  8;						//counter
		frame_data[11] = (frm_cnt & 0x000000FF) >>  0;						//counter
		frame_data[12] = (frame_wordsize & 0x0000FF00) >> 8;				//framesize (word)
		frame_data[13] = (frame_wordsize & 0x000000FF) >> 0;				//framesize (word)
		frame_data[14] = ((hmode & 0x00000001) << 7) | ((smode & 0x00000001) << 6) | ((bwidth & 0x00000003) << 4); //h_mode, s_mode, bit
		frame_data[15] = 0;													//reserved
		frame_data[16] = frm_num & 0x000000FF;								//frame_num
		frame_data[17] = frm_no;											//frame_no
		frame_data[18] = 0;													//reserved
		frame_data[19] = 0;													//reserved
	}

	return SNI_TRUE;
}

static SNI_INLINE void sni_rs_ctrl_check_framedata(struct sni_rs_stat_info *stat, uint8_t *frame_data, size_t framesize, const struct sni_rs_startinfo *info)
{
	uint32_t end_code2;
	uint32_t header2;
	const uint8_t hmode = sni_rs_startinfo_get_headheadmode(info);
	const uint8_t smode = sni_rs_startinfo_get_statheadmode(info);
	const uint8_t bwidth = sni_rs_startinfo_get_bitwidth(info);
	const uint32_t frame_wordsize = info->frameinfo.word.device_iqdata;

	// check for input frame
	end_code2 = (frame_data[framesize-4] << 24) | (frame_data[framesize-3] << 16) | (frame_data[framesize-2] << 8) | (frame_data[framesize-1]);
	header2 = 0;
	if(bwidth == 0 && hmode == 1 && smode == 1) {
		header2 = (frame_data[SNI_RS_USERHEADER_SIZE+4] << 24) | (frame_data[SNI_RS_USERHEADER_SIZE+5] << 16) | (frame_data[SNI_RS_USERHEADER_SIZE+6] << 8) | (frame_data[SNI_RS_USERHEADER_SIZE+7]);
	} else if (bwidth == 0 && hmode == 1) {
		header2 = (frame_data[SNI_RS_USERHEADER_SIZE+0] << 24) | (frame_data[SNI_RS_USERHEADER_SIZE+1] << 16) | (frame_data[SNI_RS_USERHEADER_SIZE+2] << 8) | (frame_data[SNI_RS_USERHEADER_SIZE+3]);
	} else if (hmode == 1 && smode == 1) {
		header2 = (0x00 << 24) | (frame_data[SNI_RS_USERHEADER_SIZE+3] << 16) | (frame_data[SNI_RS_USERHEADER_SIZE+4] << 8) | (frame_data[SNI_RS_USERHEADER_SIZE+5]);
	} else if (hmode == 1) {
		header2 = (0x00 << 24) | (frame_data[SNI_RS_USERHEADER_SIZE+0] << 16) | (frame_data[SNI_RS_USERHEADER_SIZE+1] << 8) | (frame_data[SNI_RS_USERHEADER_SIZE+2]);
	} else {
		header2 = 0x40000000 | (frame_wordsize & 0x0000ffff);
	}

	// check header and frame end code
	if(bwidth == 0) {
		if(end_code2 != 0x80018001  || (header2 & 0xff000000) != 0x40000000 || (header2 & 0x0000ffff) != (frame_wordsize & 0x0000ffff)) {
			stat->_end_code = end_code2;
			stat->_header = header2;
			stat->_err_cnt++;
			stat->_err_cnt_sum++;
		}
	} else {
		if((end_code2 & 0x00ffffff) != 0x00801801  || (header2 & 0x0000ffff) != (frame_wordsize & 0x0000ffff)) {
			stat->_end_code = end_code2;
			stat->_header = header2;
			stat->_err_cnt++;
			stat->_err_cnt_sum++;
		}
	}
#ifdef HSPI_DEBUG
	// replace input frame data with debug data
	uint8_t tmp = 0;
	int start_addr = 0;
	if (bwidth == 0) {
		frame_data[framesize-4] = 0x80;
		frame_data[framesize-3] = 0x01;
		frame_data[framesize-2] = 0x80;
		frame_data[framesize-1] = 0x01;
		if (smode == 1) {
			start_addr += 4;
		}
		if (hmode == 1) {
			start_addr += 4;
			frame_data[SNI_RS_USERHEADER_SIZE + start_addr - 4] = 0x40;
			frame_data[SNI_RS_USERHEADER_SIZE + start_addr - 3] = (stat->_frm_cnt & 0x000000ff);
			frame_data[SNI_RS_USERHEADER_SIZE + start_addr - 2] = (frame_wordsize & 0x0000FF00) >> 8;
			frame_data[SNI_RS_USERHEADER_SIZE + start_addr - 1] = (frame_wordsize & 0x000000FF);
		}
	} else {
		frame_data[framesize-3] = 0x80;
		frame_data[framesize-2] = 0x18;
		frame_data[framesize-1] = 0x01;
		if (smode == 1) {
			start_addr += 3;
		}
		if (hmode == 1) {
			start_addr += 3;
			frame_data[SNI_RS_USERHEADER_SIZE + start_addr - 3] = (stat->_frm_cnt & 0x000000ff);
			frame_data[SNI_RS_USERHEADER_SIZE + start_addr - 2] = (frame_wordsize & 0x0000FF00) >> 8;
			frame_data[SNI_RS_USERHEADER_SIZE + start_addr - 1] = (frame_wordsize & 0x000000FF);
		}
	}
	for (int i=SNI_RS_USERHEADER_SIZE + start_addr; i < (int) (framesize - 4); i++) {
		frame_data[i] = tmp;
		tmp++;
	}
#endif /* HSPI_DEBUG */
	// check to skip serial frame number (SN)
	if (hmode == 1) {
		uint8_t sn, sn_sa, pre_sn_one;
		sn = (header2 & 0x00ff0000) >> 16;
		if(stat->_pre_sn == 255) {
			pre_sn_one = 0;
		} else {
			pre_sn_one = stat->_pre_sn + 1;
		}
		if ((sn != pre_sn_one) && !(stat->_start_flag)) {
			if(sn > stat->_pre_sn) {
				sn_sa = sn - stat->_pre_sn - 1;
			} else {
				sn_sa = stat->_pre_sn - sn;
				sn_sa = 256 - sn_sa - 1;
			}
			stat->_sn_skip_cnt = stat->_sn_skip_cnt + sn_sa;
		}
		stat->_pre_sn = sn;
	}
	stat->_start_flag = 0;
}

void sni_rs_ctrl_get_data(struct sni_rs_ctrl *ctrl)
{
	// create user header at top of frame
	if (sni_rs_pos_isempty(&(ctrl->pos))) {
        
		void *buf = NULL;
		size_t size = 0;
		if(!sni_rs_pos_get_bufofuserheader(&(ctrl->pos), &buf, &size)) {
			return;
		}
		sni_rs_ctrl_create_user_header(buf, size, &(ctrl->stat), &(ctrl->startinfo));
		sni_rs_pos_inc_wp(&(ctrl->pos), size);
	}
	// read from device
	{
		void *buf = NULL;
		size_t size = 0;
		size_t offset = 0;
		if(!sni_rs_pos_get_buftoread(&(ctrl->pos), &buf, &size, &offset)) {
			return;
		}
        uint32_t t1 = sni_rs_systimer_get_usec();
		sni_rs_spi_reg_read8_iq_dma(ctrl->_rf_spi1, buf, size, offset);
        uint32_t t2 = sni_rs_systimer_get_usec();
        // printf("SPI read8 iq data %d\n", t2-t1);

		sni_rs_pos_inc_wp(&(ctrl->pos), size);
	}
	// write framedata into fifo
	if (sni_rs_pos_isfull(&(ctrl->pos))) {
		void *data = NULL;
		size_t size = 0;

		if(sni_rs_pos_get_framedata(&(ctrl->pos), &data, &size)){
			// check frame data
			sni_rs_ctrl_check_framedata(&ctrl->stat, data, size, &(ctrl->startinfo));

			// write frame data to FIFO
			sni_rs_fifo_put_framedata(&ctrl->fifo, &ctrl->stat, data, size);
		}

		if (sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_SD)) {
			sni_rs_ctrl_term_shutdown(ctrl);
			sni_rs_dmesg_lp(ctrl);
		}
		if (sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_DS)) {
			sni_rs_spi_resetctl(ctrl->_rf_spi1, OP_DEEP_SLEEP);
			sni_rs_dmesg_lp(ctrl);
		}
		if (sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY)) {
			sni_rs_dmesg_stop_before(ctrl);
			sni_rs_dmesg_stop_after(ctrl);
		}

		sni_rs_pos_clear(&(ctrl->pos));
	}
}

void sni_rs_ctrl_set_hw_info(struct sni_rs_ctrl *ctrl, const struct sni_rs_hw_info *hw_info)
{
	ctrl->hw_info.hw_efuse36 = hw_info->hw_efuse36;
	ctrl->hw_info.hw_efuse16 = hw_info->hw_efuse16;
}

void sni_rs_ctrl_get_hw_info(const struct sni_rs_ctrl *ctrl, struct sni_rs_hw_info *hw_info)
{
	hw_info->hw_efuse36 = ctrl->hw_info.hw_efuse36;
	hw_info->hw_efuse16 = ctrl->hw_info.hw_efuse16;
}
