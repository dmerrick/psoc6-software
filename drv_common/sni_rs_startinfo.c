/*
 * sni_rs_startinfo.c
 *
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

#ifdef TARGET_KERNEL
#include <linux/types.h>
#include <linux/uaccess.h>
#else
#include <string.h>
#include <stddef.h>
#include <stdint.h>
#endif

#include "sni_rs_setupinfo.h"
#include "sni_rs_common.h"
#include "sni_rs_startinfo.h"


/* extern function */
enum SNI_RET sni_rs_startinfo_setup(struct sni_rs_startinfo *info, const struct sni_rs_setupinfo *setupinfo)
{
	struct sni_rs_framewordsize *word = &(info->frameinfo.word);
	struct sni_rs_framebytesize *byte = &(info->frameinfo.byte);

	if(!sni_rs_setupinfo_isenable(setupinfo)) {
		return SNI_RET_ESTATE;
	}

	// update frameinfo
	{
		memcpy(&(info->frameinfo.type), &(setupinfo->type), sizeof(info->frameinfo.type));

		word->device_header_status = setupinfo->type.status_mode;
		word->device_header_header = setupinfo->type.header_mode;
		word->device_iqdata = setupinfo->iqdata_wordsize;
		word->device_endcode = SNI_RS_DEVICE_ENDCODE_WORDSIZE;
		word->device_header = word->device_header_status + word->device_header_header;
		word->device_total = word->device_header + word->device_iqdata + word->device_endcode;

		byte->user_header = SNI_RS_USERHEADER_SIZE;
		byte->device_header_status = sni_rs_startinfo_word2byte(info, word->device_header_status);
		byte->device_header_header = sni_rs_startinfo_word2byte(info, word->device_header_header);
		byte->device_iqdata = sni_rs_startinfo_word2byte(info, word->device_iqdata);
		byte->device_endcode = sni_rs_startinfo_word2byte(info, word->device_endcode);
		byte->device_header = sni_rs_startinfo_word2byte(info, word->device_header);
		byte->device_total = sni_rs_startinfo_word2byte(info, word->device_total);

		byte->total = byte->user_header + byte->device_total;
	}

	// update startinfo
	{
		if(!sni_rs_setupinfo_copy(&(info->setupinfo), setupinfo)) {
			return SNI_RET_ENOMEM;
		}

		if(!sni_rs_startinfo_set_framenum(info, SNI_RS_DEFAULT_FRAMES_PER_PAGE)) {
			return SNI_RET_ESTATE;
		}

		// set default watermark (same as framesize)
		if(!sni_rs_startinfo_set_watermark(info, word->device_iqdata)) {
			return SNI_RET_ESTATE;
		}
	}

	return SNI_RET_OK;
}

enum SNI_BOOL sni_rs_startinfo_init(struct sni_rs_startinfo *info, const struct sni_rs_startinfo_attr *attr)
{
	sni_rs_setupinfo_init(&(info->setupinfo), &(attr->setupinfo_attr));

	return SNI_TRUE;
}

enum SNI_BOOL sni_rs_startinfo_clear(struct sni_rs_startinfo *info)
{
	memset(&(info->frameinfo), 0, sizeof(info->frameinfo));

	return SNI_TRUE;
}

void sni_rs_startinfo_free(struct sni_rs_startinfo *info)
{
	sni_rs_setupinfo_free(&(info->setupinfo));
}

enum SNI_BOOL sni_rs_startinfo_set_watermark(struct sni_rs_startinfo *info, size_t wordsize)
{
	if (wordsize == 0) {
		return SNI_FALSE;
	}
	if (info->frameinfo.word.device_iqdata == 0) {
		return SNI_FALSE;
	}
	if (wordsize > info->frameinfo.word.device_iqdata) {
		return SNI_FALSE;
	}
	if (info->frameinfo.word.device_iqdata % wordsize != 0) {
		return SNI_FALSE;
	}

	info->watermark_word = wordsize;

	return SNI_TRUE;
}

enum SNI_BOOL sni_rs_startinfo_set_framenum(struct sni_rs_startinfo *info, uint8_t num)
{
	if (num == 0) {
		return SNI_FALSE;
	}
	if (info->frameinfo.byte.total * num > PAGE_CAPACITY){
		return SNI_FALSE;
	}
	info->frames_per_page = num;

	return SNI_TRUE;
}
