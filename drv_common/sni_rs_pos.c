/*
 * sni_rs_pos.c
 *
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

#ifdef TARGET_KERNEL
#include <linux/types.h>
#include <linux/uaccess.h>
#else
#include <string.h>
#include <stddef.h>
#include <stdint.h>
#endif

#include "sni_rs_startinfo.h"
#include "sni_rs_pos.h"
#include "sni_rs_mem.h"
#include "sni_rs_common.h"

/*
 * static functions
 */
static SNI_INLINE uint8_t *sni_rs_pos_get_wp(const struct sni_rs_pos *pos)
{
#ifdef TARGET_FT4222H
	void *p = sni_rs_mem_get(&(pos->buf));
	if (!p) return NULL;
	return ((uint8_t *) p) + pos->wp_idx;
#else
	return ((uint8_t *) sni_rs_mem_get(&(pos->buf))) + pos->wp_idx;
#endif
}


/*
 * extern functions
 */
void sni_rs_pos_init(struct sni_rs_pos *pos, const struct sni_rs_pos_attr *attr)
{
	memset(pos, 0, sizeof(struct sni_rs_pos));

	sni_rs_mem_init(&(pos->buf), attr->frame);

	sni_rs_pos_clear(pos);
}

void sni_rs_pos_free(struct sni_rs_pos *pos)
{
	sni_rs_pos_reset(pos);
}


enum SNI_BOOL sni_rs_pos_setup(struct sni_rs_pos *pos, const struct sni_rs_startinfo *info)
{
	sni_rs_pos_clear(pos);

	memcpy(&(pos->size), &(info->frameinfo.byte), sizeof(pos->size));

	sni_rs_pos_set_watermark(pos, info);
	sni_rs_mem_free(&(pos->buf));
	if (!sni_rs_mem_calloc(&(pos->buf), pos->size.total)) {
		return SNI_FALSE;
	}

	return SNI_TRUE;
}

void sni_rs_pos_clear(struct sni_rs_pos *pos)
{
	pos->wp_idx = 0;
	pos->counter = 0;
}

void sni_rs_pos_reset(struct sni_rs_pos *pos)
{
	sni_rs_pos_clear(pos);
	sni_rs_mem_free(&(pos->buf));
}


enum SNI_BOOL sni_rs_pos_inc_wp(struct sni_rs_pos *pos, size_t size)
{
	if (pos->wp_idx + size > pos->size.total) {
		return SNI_FALSE;
	}
	pos->wp_idx += size;

	return SNI_TRUE;
}

enum SNI_BOOL sni_rs_pos_get_bufofuserheader(struct sni_rs_pos *pos, void **buf, size_t *size)
{
	if (!sni_rs_pos_isempty(pos)) {
		*buf = NULL;
		*size = 0;
		return SNI_FALSE;
	}

	*buf = sni_rs_mem_get(&(pos->buf));
#ifdef TARGET_FT4222H
	if(!*buf) {
		//printf("sni_rs_pos_get_bufofuserheader: *buf==NULL\n");
#else
	if(!buf) {
#endif
		return SNI_FALSE;
	}
	*size = pos->size.user_header;

	return SNI_TRUE;
}

enum SNI_BOOL sni_rs_pos_get_buftoread(struct sni_rs_pos *pos, void **buf, size_t *size, size_t *offset)
{
	*buf = sni_rs_pos_get_wp(pos);
	*size = 0;
	*offset = 0;

#ifdef TARGET_FT4222H
	if(!*buf) {
		//printf("sni_rs_pos_get_buftoread: *buf==NULL\n");
#else
	if(!buf) {
#endif
//		sni_rs_dprint(SNI_RS_DPRINT_ERR, "[sni_rs] %s: get nullptr\n", __FUNCTION__);
		return SNI_FALSE;
	}

	if(pos->counter == 0) {
		*size += pos->size.device_header;
		if (pos->size.device_header_header == 0) {
			*size += pos->watermark;
		}
	} else {
		*offset += pos->size.device_header_status;
		*size += pos->watermark;
	}

	if (pos->wp_idx + *size + pos->size.device_endcode == pos->size.total) {
		*size += pos->size.device_endcode;
	}

	if (pos->wp_idx + *size > pos->size.total) {
		return SNI_FALSE;
	}

	pos->counter++;

	return SNI_TRUE;
}

enum SNI_BOOL sni_rs_pos_get_framedata(const struct sni_rs_pos *pos, void **data, size_t *size)
{
	if(!sni_rs_pos_isfull(pos)) {
		return SNI_FALSE;
	}
	*data = sni_rs_mem_get(&(pos->buf));
#ifdef TARGET_FT4222H
	if(!*data) {
#else
	if(!data) {
#endif
		return SNI_FALSE;
	}
	*size = pos->size.total;

	return SNI_TRUE;
}
