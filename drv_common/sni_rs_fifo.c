/*
 * sni_rs_fifo.c
 *
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

#ifdef TARGET_KERNEL
#include <linux/types.h>
#include <linux/uaccess.h>
#else
#include <string.h>
#include <stddef.h>
#include <stdint.h>
#endif

#include "sni_rs_startinfo.h"
#include "sni_rs_ctrl_statistics.h"
#include "sni_rs_fifo.h"
#include "sni_rs_mem.h"
#include "sni_rs_common.h"


/*
 * sni_rs_fifo functions
 */
enum SNI_BOOL sni_rs_fifo_init(struct sni_rs_fifo *fifo, const struct sni_rs_fifo_attr *attr)
{
	if(fifo == NULL) {
		return SNI_FALSE;
	}

	if(attr == NULL) {
		return SNI_FALSE;
	}

	memset(fifo, 0, sizeof(struct sni_rs_fifo));

	fifo->max_pagenum = attr->pagenum;

	return sni_rs_fifo_init_target(fifo, attr);
}

void sni_rs_fifo_free(struct sni_rs_fifo *fifo)
{
	sni_rs_fifo_clear(fifo);

	sni_rs_fifo_free_target(fifo);
}

void sni_rs_fifo_reset(struct sni_rs_fifo *fifo)
{
	sni_rs_fifo_clear(fifo);
}

void sni_rs_fifo_set_framenum(struct sni_rs_fifo *fifo, const struct sni_rs_startinfo *info)
{
	fifo->_frm_num = sni_rs_startinfo_get_framenum(info);
}

size_t sni_rs_fifo_get_framenum(const struct sni_rs_fifo *fifo)
{
	return fifo->_frm_num;
}

void sni_rs_fifo_setup(struct sni_rs_fifo *fifo, const struct sni_rs_startinfo *info)
{
	fifo->_byte_frame_size_wh = info->frameinfo.byte.total;
	sni_rs_fifo_set_framenum(fifo, info);
}

void sni_rs_fifo_get_pagedata(struct sni_rs_fifo *fifo, void **buf, size_t *size)
{
	*buf = sni_rs_fifo_get_rp(fifo);
	*size = fifo->_byte_frame_size_wh * fifo->_frm_num;
}

void sni_rs_fifo_purge_pagedata(struct sni_rs_fifo *fifo)
{
	sni_rs_fifo_inc_rp(fifo);
}
