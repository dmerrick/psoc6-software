/*
 * sni_rs_mem.c
 *
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

#ifdef TARGET_KERNEL
#include <linux/types.h>
#include <linux/uaccess.h>
#else
#include <sys/types.h>
#include <string.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#endif

#include "sni_rs_mem.h"
#include "sni_rs_common.h"
//#include "sni_rs_dmesg.h"

static const int dmesg_id = 3;
static size_t alloc_count = 0;
static size_t alloc_totalsize = 0;
static size_t alloc_errcount = 0;
static size_t free_count = 0;
static size_t free_totalsize = 0;
static size_t free_errcount = 0;
static size_t mem_get_null_count = 0;

//void sni_rs_mem_dmesg_print(void)
//{
//#ifdef __ENABLE_DMESG__
//	snprintf(
//		sni_rs_dmesg_get_ptr(dmesg_id), sni_rs_dmesg_get_bufsize(dmesg_id),
//		"[mem] %d(b) <alloc:%zu> size:%zu ecnt:%zu <free:%zu> size:%zu ecnt:%zu <get> ecnt:%zu",
//		(int) (alloc_totalsize - free_totalsize),
//		alloc_count,
//		alloc_totalsize,
//		alloc_errcount,
//		free_count,
//		free_totalsize,
//		free_errcount,
//		mem_get_null_count
//	);
//#endif
//}

void sni_rs_mem_dmesg_cnt_alloc(size_t size)
{
	alloc_count++;
	alloc_totalsize += size;

//	sni_rs_mem_dmesg_print();
}

void sni_rs_mem_dmesg_cnt_alloc_error(void)
{
	alloc_errcount++;

//	sni_rs_mem_dmesg_print();
}

void sni_rs_mem_dmesg_cnt_free(size_t size)
{
	free_count++;
	free_totalsize += size;

//	sni_rs_mem_dmesg_print();
}

void sni_rs_mem_dmesg_cnt_free_error(void)
{
	free_errcount++;

//	sni_rs_mem_dmesg_print();
}

void sni_rs_mem_dmesg_cnt_mem_get_null(void)
{
	mem_get_null_count++;

//	sni_rs_mem_dmesg_print();
}

/*
 * sni_rs_mem functions
 */
enum SNI_BOOL sni_rs_mem_calloc(struct sni_rs_mem *mem, size_t size)
{
	if(!sni_rs_mem_alloc(mem, size)) {
		return SNI_FALSE;
	}

	memset(sni_rs_mem_get(mem), 0, sni_rs_mem_getsize(mem));

	return SNI_TRUE;
}

enum SNI_BOOL sni_rs_mem_copy(struct sni_rs_mem *dst, const struct sni_rs_mem *src)
{
	size_t size_src = 0;
	size_t size_dst = 0;

	size_src = sni_rs_mem_getsize(src);
	size_dst = sni_rs_mem_getsize(dst);

	if(size_dst < size_src) {
		sni_rs_mem_free(dst);
		if(!sni_rs_mem_alloc(dst, size_src)){
			return SNI_FALSE;
		}
	}

	if(size_src > 0) {
		void *buf_src = sni_rs_mem_get(src);
		if(!buf_src) {
			return SNI_FALSE;
		}
		memcpy(sni_rs_mem_get(dst), buf_src, size_src);
	}

	return SNI_TRUE;
}
