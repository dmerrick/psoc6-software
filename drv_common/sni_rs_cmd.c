/*
 * sni_rs_cmd.c
 *
 * Copyright (c) 2017-2018 Socionext Inc.
 * All rights reserved.
 */

#ifdef TARGET_KERNEL
#include <linux/types.h>
#include <linux/uaccess.h>
#else
#include <stddef.h>
#include <stdint.h>
#endif

#include "sni_rs_ctrl.h"
#include "sni_rs_setupinfo.h"
#include "sni_rs_spi.h"
#ifdef TARGET_STM32
#include "MCU_SPI4_HostIF_if.h"
#endif	/* TARGET_STM32 */

#include "sni_rs_target.h"
#include "sni_rs_tools.h"
#include "sni_rs_cmd.h"
#include "sni_rs_txpower.h"
#include "sni_rs_pass_through.h"

enum SNI_RET sni_rs_cmd_boot(struct sni_rs_ctrl *ctrl)
{
	if (!(sni_rs_ctrl_check_state(ctrl, SNI_STATE_INIT) || sni_rs_ctrl_check_state(ctrl, SNI_STATE_INIT2))) {
        //printf("SNI_RET_ESTATE in sni_rs_cmd_boot\n");
		return SNI_RET_ESTATE;
	}

	sni_rs_setupinfo_init_fftwin(&(ctrl->setupinfo));

	sni_rs_ctrl_term_reset(ctrl);
	sni_rs_fifo_clear(&ctrl->fifo);
	sni_rs_ctrl_stat_clear(&ctrl->stat);
	sni_rs_ctrl_stat_sum_clear(&ctrl->stat);
	sni_rs_pos_clear(&ctrl->pos);
	sni_rs_ctrl_term_boot(ctrl);

	sni_rs_ctrl_state_set(ctrl, SNI_STATE_BOOT);

	return SNI_RET_OK;
}

enum SNI_RET sni_rs_cmd_shutdown(struct sni_rs_ctrl *ctrl)
{
	sni_rs_ctrl_term_shutdown(ctrl);
	sni_rs_ctrl_disable_irq(ctrl->termctl);
	sni_rs_fifo_clear(&ctrl->fifo);
	sni_rs_ctrl_stat_clear(&ctrl->stat);
	sni_rs_ctrl_stat_sum_clear(&ctrl->stat);
	sni_rs_pos_reset(&ctrl->pos);
	sni_rs_setupinfo_clear(&(ctrl->setupinfo));
	sni_rs_setupinfo_free(&(ctrl->setupinfo));
	sni_rs_startinfo_clear(&(ctrl->startinfo));
	sni_rs_startinfo_free(&(ctrl->startinfo));

	sni_rs_ctrl_state_set(ctrl, SNI_STATE_INIT);

	return SNI_RET_OK;
}

enum SNI_RET sni_rs_cmd_set_reg_s(struct sni_rs_ctrl *ctrl, uint8_t *data, size_t size)
{
	enum SNI_RET ret;

	if (!(sni_rs_ctrl_check_state(ctrl, SNI_STATE_BOOT) || sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY))) {
		return SNI_RET_ESTATE;
	}

	ret = sni_rs_setupinfo_set_reg(&(ctrl->setupinfo), data, size);

	return ret;
}


enum SNI_RET sni_rs_cmd_set_seq(struct sni_rs_ctrl *ctrl, uint8_t *data, size_t size)
{
	enum SNI_RET ret;

	if (!(sni_rs_ctrl_check_state(ctrl, SNI_STATE_BOOT) || sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY))) {
		return SNI_RET_ESTATE;
	}

	ret = sni_rs_setupinfo_set_seqcode(&(ctrl->setupinfo), data, size);

	return ret;
}

enum SNI_RET sni_rs_cmd_set_fftwin(struct sni_rs_ctrl *ctrl, uint8_t *data, size_t size)
{
	enum SNI_RET ret;

	if (!(sni_rs_ctrl_check_state(ctrl, SNI_STATE_BOOT) || sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY))) {
		return SNI_RET_ESTATE;
	}
	ret = sni_rs_setupinfo_set_fftwin(&(ctrl->setupinfo), data, size);

	return ret;
}

enum SNI_RET sni_rs_cmd_set_seq_addr(struct sni_rs_ctrl *ctrl, uint8_t *data, size_t size)
{
	if (!(sni_rs_ctrl_check_state(ctrl, SNI_STATE_BOOT) || sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY))) {
		return SNI_RET_ESTATE;
	}

	if(!sni_rs_setupinfo_set_addr(&(ctrl->setupinfo), data, size)) {
		return SNI_RET_EINV;
	}

	return SNI_RET_OK;
}

enum SNI_RET sni_rs_cmd_setup(struct sni_rs_ctrl *ctrl)
{
	enum SNI_RET ret;
	
	ret = sni_rs_startinfo_setup(&(ctrl->startinfo), &(ctrl->setupinfo));
	if (ret){
		return ret;
	}
	sni_rs_setupinfo_clear(&(ctrl->setupinfo));
	sni_rs_setupinfo_free(&(ctrl->setupinfo));

	if (!(sni_rs_ctrl_check_state(ctrl, SNI_STATE_BOOT) || sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY))) {
		return SNI_RET_ESTATE;
	}

	ret = sni_rs_ctrl_term_setup(ctrl);
	if(ret) {
		return ret;
	}

	ret = sni_rs_ctrl_term_cal(ctrl);
	if(ret) {
		return ret;
	}

	if(!sni_rs_pos_setup(&(ctrl->pos), &(ctrl->startinfo))) {
		return SNI_RET_ENOMEM;
	}
	sni_rs_fifo_setup(&(ctrl->fifo), &(ctrl->startinfo));
	sni_rs_ctrl_stat_clear(&ctrl->stat);
	sni_rs_ctrl_stat_sum_clear(&ctrl->stat);

	sni_rs_ctrl_state_set(ctrl, SNI_STATE_READY);

	return SNI_RET_OK;
}

enum SNI_RET sni_rs_cmd_start(struct sni_rs_ctrl *ctrl, uint8_t type)
{
	enum SNI_RET ret;
	
	if (!(sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY) || sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_SD) || sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_DS))) {
		return SNI_RET_ESTATE;
	}

	if (sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY)){
		if (!((type == 0) || (type == 1))){
			return SNI_RET_EINV;
		}
		ret = sni_rs_spi_isbusy(ctrl->_rf_spi1);
		if (ret) {
			return ret;
		}
	}

	if (sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_SD)){
		if (!(type == 0)){
			return SNI_RET_EINV;
		}
		sni_rs_ctrl_disable_irq(ctrl->termctl);
		sni_rs_ctrl_term_set(ctrl);
		ret = sni_rs_ctrl_term_setup(ctrl);
		if (ret) {
			return ret;
		}
		sni_rs_ctrl_stat_inc_del_cnt(&ctrl->stat, sni_rs_fifo_get_pagenum(&ctrl->fifo));
	}

	if (sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_DS)){
		if (!(type == 0)){
			return SNI_RET_EINV;
		}
		ret = sni_rs_spi_resetctl(ctrl->_rf_spi1, OP_SOFT_RESET);
		if (ret) {
			return ret;
		}
		sni_rs_sleep_usec(XTAL_WAIT);
		sni_rs_ctrl_stat_inc_del_cnt(&ctrl->stat, sni_rs_fifo_get_pagenum(&ctrl->fifo));
	}

	sni_rs_fifo_clear(&ctrl->fifo);
	sni_rs_ctrl_stat_clear(&ctrl->stat);
	sni_rs_pos_clear(&ctrl->pos);
// #ifdef TARGET_STM32
// 	sni_rs_hif_dma_cnt_clr(sni_rs_hif);
// 	sni_rs_hif_clr_transmitted(sni_rs_hif);
// #endif	/* TARGET_STM32 */

	ret = sni_rs_spi_reg_write32(ctrl->_rf_spi1, REG_DIG_06, sni_rs_setupinfo_get_sensingaddr(&(ctrl->startinfo.setupinfo)));
	if(ret) {
		return ret;
	}
	sni_rs_ctrl_enable_irq(ctrl->termctl);

	// SINGLE
	if (type == 0) {
		ret = sni_rs_spi_modectl(ctrl->_rf_spi1, OP_SINGLE_SHOT);
		if(ret) {
			return ret;
		}
	}

	// CONTINUOUS
	if (type == 1) {
		ret = sni_rs_spi_modectl(ctrl->_rf_spi1, OP_CONT_START);
		if(ret) {
			return ret;
		}
		sni_rs_ctrl_state_set(ctrl, SNI_STATE_SENSING);
	}

	return SNI_RET_OK;
}

enum SNI_RET sni_rs_cmd_stop(struct sni_rs_ctrl *ctrl)
{
	enum SNI_RET ret;

	if (!sni_rs_ctrl_check_state(ctrl, SNI_STATE_SENSING)) {
		return SNI_RET_ESTATE;
	}

	ret = sni_rs_spi_modectl(ctrl->_rf_spi1, OP_CONT_STOP);
	if(ret) {
		return ret;
	}

	sni_rs_ctrl_state_set(ctrl, SNI_STATE_READY);

	return SNI_RET_OK;
}

enum SNI_RET sni_rs_cmd_tx_raw(struct sni_rs_ctrl *ctrl)
{
#ifdef TARGET_STM32
	size_t size;
	void *rawdata;
	int ret;
#endif	/* TARGET_STM32 */

	if (!(sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY) || sni_rs_ctrl_check_state(ctrl, SNI_STATE_SENSING) || sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_SD) || sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_DS))) {
		return SNI_RET_ESTATE;
	}

	if (sni_rs_fifo_get_pagenum(&ctrl->fifo) == 0) {
		// TX_RAW no IQ data
		return SNI_RET_EDMA13;
	}

#ifdef TARGET_STM32
	// if (ctrl->hif_dma_flag == SNI_TRUE) {
	// 	if (sni_rs_hif_transmitted(sni_rs_hif) == sni_rs_hif_false) {
	// 		// TX_RAW in DMA transmit
	// 		return SNI_RET_EDMA13;
	// 	}
    
		sni_rs_fifo_get_pagedata(&ctrl->fifo, &rawdata, &size);
    
		// ret = sni_rs_hif_error_code(sni_rs_hif);
		// sni_rs_target_cache_dclean(); /* Dan */
		// sni_rs_hif_standby(sni_rs_hif, size, rawdata);
    
		// if ((ret == 8) || (ret == 16)) {
		// 	return SNI_RET_EDMA12;
		// }
		// if (ret != 0) {
		// 	// TX_RAW DMA other error
		// 	return SNI_RET_EDEV;
		// }
	// }
#endif	/* TARGET_STM32 */


	return SNI_RET_OK;
}

enum SNI_RET sni_rs_cmd_cal(struct sni_rs_ctrl *ctrl)
{
	enum SNI_RET ret;

	if (!sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY)) {
		return SNI_RET_ESTATE;
	}

	ret = sni_rs_spi_isbusy(ctrl->_rf_spi1);
	if (ret) {
		return ret;
	}

	sni_rs_ctrl_disable_irq(ctrl->termctl);

	ret = sni_rs_ctrl_term_cal(ctrl);

	return ret;
}

enum SNI_RET sni_rs_cmd_reset(struct sni_rs_ctrl *ctrl)
{
	enum SNI_RET ret;

	if (!(sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY) || sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_SD) || sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_DS))) {
		return SNI_RET_ESTATE;
	}

	sni_rs_ctrl_disable_irq(ctrl->termctl);

	if (sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY) || sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_DS)) {
		ret = sni_rs_spi_resetctl(ctrl->_rf_spi1, OP_HARD_RESET);
		if(ret) {
			return ret;
		}
	}

	if (sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_SD)) {
		sni_rs_ctrl_term_reset(ctrl);
	}

	sni_rs_ctrl_term_boot(ctrl);
	sni_rs_fifo_clear(&ctrl->fifo);
	sni_rs_ctrl_stat_clear(&ctrl->stat);
	sni_rs_ctrl_stat_sum_clear(&ctrl->stat);
	sni_rs_pos_reset(&ctrl->pos);
	sni_rs_setupinfo_clear(&(ctrl->setupinfo));
	sni_rs_startinfo_clear(&(ctrl->startinfo));
	sni_rs_startinfo_free(&(ctrl->startinfo));

	sni_rs_ctrl_state_set(ctrl, SNI_STATE_BOOT);

	return SNI_RET_OK;
}

enum SNI_RET sni_rs_cmd_reg_w(struct sni_rs_ctrl *ctrl, uint32_t addr, uint32_t data)
{
	enum SNI_RET ret;

	if (sni_rs_ctrl_check_state(ctrl, SNI_STATE_INIT) || sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_SD)) {
		return SNI_RET_ESTATE;
	}

	ret = sni_rs_spi_reg_write32(ctrl->_rf_spi1, addr, data);

	return ret;
}

enum SNI_RET sni_rs_cmd_reg_r(struct sni_rs_ctrl *ctrl, uint32_t addr, uint32_t *data)
{
	enum SNI_RET ret;

	if (sni_rs_ctrl_check_state(ctrl, SNI_STATE_INIT) || sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_SD)) {
		return SNI_RET_ESTATE;
	}
    // {
    //     uint8_t data;
    //     sni_rs_spi_read_stat(ctrl->_rf_spi1, &data);
    //     //printf("data: %d\n", data);
    // }
	ret = sni_rs_spi_reg_read32(ctrl->_rf_spi1, addr, data);
    // {
    //     uint8_t data;
    //     sni_rs_spi_read_stat(ctrl->_rf_spi1, &data);
    //     //printf("data: %d\n", data);
    // }
	return ret;
}

enum SNI_RET sni_rs_cmd_set_watermark(struct sni_rs_ctrl *ctrl, size_t size)
{
	enum SNI_RET ret;

	if (!sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY)) {
		return SNI_RET_ESTATE;
	}

	if (!sni_rs_startinfo_set_watermark(&(ctrl->startinfo), size)){
		return SNI_RET_EINV;
	}

	ret = sni_rs_spi_reg_write32(ctrl->_rf_spi1, REG_WATERMARK, size);
	if(ret) {
		return ret;
	}

	sni_rs_pos_set_watermark(&(ctrl->pos), &(ctrl->startinfo));

	return SNI_RET_OK;
}

enum SNI_RET sni_rs_cmd_set_txpower(struct sni_rs_ctrl *ctrl, uint8_t value, uint8_t sign)
{
	if (!(sni_rs_ctrl_check_state(ctrl, SNI_STATE_BOOT) || sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY))) {
		return SNI_RET_ESTATE;
	}

	if (!sni_rs_setupinfo_set_txpower(&(ctrl->setupinfo), value, sign, &(ctrl->hw_info))){
		return SNI_RET_EINV;
	}

	return SNI_RET_OK;
}

enum SNI_RET sni_rs_cmd_change_sensemode(struct sni_rs_ctrl *ctrl, uint8_t value)
{
	enum SNI_RET ret;

	if (!(sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY) || sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_SD) || sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_DS))) {
		return SNI_RET_ESTATE;
	}

	if ((sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY) && (value == SNI_STATE_READY)) || (sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_SD) && (value == SNI_STATE_READY_SD)) || (sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_DS) && (value == SNI_STATE_READY_DS))) {
		return SNI_RET_ESTATE;
	}

	if (!((sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY) && ((value == SNI_STATE_READY_SD) || (value == SNI_STATE_READY_DS))) || (sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_SD) && ((value == SNI_STATE_READY) || (value == SNI_STATE_READY_DS))) || (sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_DS) && ((value == SNI_STATE_READY) || (value == SNI_STATE_READY_SD))))) {
		return SNI_RET_EINV;
	}

	sni_rs_fifo_clear(&ctrl->fifo);
	sni_rs_ctrl_stat_sum_clear(&ctrl->stat);
	sni_rs_pos_clear(&ctrl->pos);

	if (sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY)){
		if (value == SNI_STATE_READY_SD) {
			sni_rs_ctrl_term_shutdown(ctrl);
			sni_rs_ctrl_disable_irq(ctrl->termctl);
			sni_rs_ctrl_state_set(ctrl, SNI_STATE_READY_SD);
		} else if (value == SNI_STATE_READY_DS) {
			ret = sni_rs_spi_resetctl(ctrl->_rf_spi1, OP_DEEP_SLEEP);
			if(ret) {
				return ret;
			}
			sni_rs_ctrl_disable_irq(ctrl->termctl);
			sni_rs_ctrl_state_set(ctrl, SNI_STATE_READY_DS);
		}
	} else if (sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_SD)){
		sni_rs_ctrl_term_set(ctrl);
		ret = sni_rs_ctrl_term_setup(ctrl);
		if(ret) {
			return ret;
		}
		if (value == SNI_STATE_READY) {
			ret = sni_rs_ctrl_term_cal(ctrl);
			if(ret) {
				return ret;
			}
			sni_rs_ctrl_state_set(ctrl, SNI_STATE_READY);
		} else if (value == SNI_STATE_READY_DS) {
			ret = sni_rs_spi_resetctl(ctrl->_rf_spi1, OP_DEEP_SLEEP);
			if(ret) {
				return ret;
			}
			sni_rs_ctrl_disable_irq(ctrl->termctl);
			sni_rs_ctrl_state_set(ctrl, SNI_STATE_READY_DS);
		}
	} else if (sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_DS)){
		if (value == SNI_STATE_READY) {
			ret = sni_rs_spi_resetctl(ctrl->_rf_spi1, OP_SOFT_RESET);
			if(ret) {
				return ret;
			}
			sni_rs_sleep_usec(XTAL_WAIT);
			ret = sni_rs_ctrl_term_cal(ctrl);
			if(ret) {
				return ret;
			}
			sni_rs_ctrl_state_set(ctrl, SNI_STATE_READY);
		} else if (value == SNI_STATE_READY_SD) {
			sni_rs_ctrl_term_shutdown(ctrl);
			sni_rs_ctrl_disable_irq(ctrl->termctl);
			sni_rs_ctrl_state_set(ctrl, SNI_STATE_READY_SD);
		}
	}

	return SNI_RET_OK;
}

enum SNI_RET sni_rs_cmd_soft_reset(struct sni_rs_ctrl *ctrl)
{
	enum SNI_RET ret;

	if (sni_rs_ctrl_check_state(ctrl, SNI_STATE_INIT) || sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_SD)) {
		return SNI_RET_ESTATE;
	}

	ret = sni_rs_spi_resetctl(ctrl->_rf_spi1, OP_SOFT_RESET);
	if(ret) {
		return ret;
	}

	if (sni_rs_ctrl_check_state(ctrl, SNI_STATE_SENSING)) {
		sni_rs_ctrl_state_set(ctrl, SNI_STATE_READY);
	}

	return SNI_RET_OK;
}

enum SNI_RET sni_rs_cmd_hard_reset(struct sni_rs_ctrl *ctrl)
{
	enum SNI_RET ret;

	if (sni_rs_ctrl_check_state(ctrl, SNI_STATE_INIT) || sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_SD)) {
		return SNI_RET_ESTATE;
	}

	sni_rs_ctrl_disable_irq(ctrl->termctl);
	ret = sni_rs_spi_resetctl(ctrl->_rf_spi1, OP_HARD_RESET);
	if(ret) {
		return ret;
	}

	sni_rs_fifo_clear(&ctrl->fifo);
	sni_rs_ctrl_stat_clear(&ctrl->stat);
	sni_rs_ctrl_stat_sum_clear(&ctrl->stat);
	sni_rs_pos_reset(&ctrl->pos);
	sni_rs_setupinfo_clear(&(ctrl->setupinfo));
	sni_rs_setupinfo_free(&(ctrl->setupinfo));
	sni_rs_startinfo_clear(&(ctrl->startinfo));
	sni_rs_startinfo_free(&(ctrl->startinfo));

	sni_rs_ctrl_state_set(ctrl, SNI_STATE_INIT2);

	return SNI_RET_OK;
}

enum SNI_RET sni_rs_cmd_deep_sleep(struct sni_rs_ctrl *ctrl)
{
	enum SNI_RET ret;

	if (sni_rs_ctrl_check_state(ctrl, SNI_STATE_INIT) || sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_SD)) {
		return SNI_RET_ESTATE;
	}

	ret = sni_rs_spi_resetctl(ctrl->_rf_spi1, OP_DEEP_SLEEP);
	if(ret) {
		return ret;
	}
	if (sni_rs_ctrl_check_state(ctrl, SNI_STATE_SENSING)) {
		sni_rs_ctrl_state_set(ctrl, SNI_STATE_READY);
	}

	return SNI_RET_OK;
}

enum SNI_RET sni_rs_cmd_read_status(struct sni_rs_ctrl *ctrl, uint8_t *data)
{
	enum SNI_RET ret;

	if (sni_rs_ctrl_check_state(ctrl, SNI_STATE_INIT) || sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_SD)) {
		return SNI_RET_ESTATE;
	}

	ret = sni_rs_spi_read_stat(ctrl->_rf_spi1, data);

	return ret;
}

enum SNI_RET sni_rs_cmd_get_statistics(struct sni_rs_ctrl *ctrl, const uint8_t **data, size_t *size)
{
	static uint8_t _statictics[STATISTICS_SIZE];
	struct sni_rs_statistics statistics;
	uint32_t m_timer = sni_rs_systimer_get_usec();

	if (!(sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY) || sni_rs_ctrl_check_state(ctrl, SNI_STATE_SENSING) || sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_SD) || sni_rs_ctrl_check_state(ctrl, SNI_STATE_READY_DS))) {
		return SNI_RET_ESTATE;
	}

	sni_rs_ctrl_get_spi1_statistics(ctrl, &statistics);

	_statictics[ 0] = (m_timer   & 0x000000ff);
	_statictics[ 1] = (m_timer   & 0x0000ff00) >> 8;
	_statictics[ 2] = (m_timer   & 0x00ff0000) >> 16;
	_statictics[ 3] = (m_timer   & 0xff000000) >> 24;
	_statictics[ 4] = (statistics.frm_cnt & 0x000000ff);
	_statictics[ 5] = (statistics.frm_cnt & 0x0000ff00) >> 8;
	_statictics[ 6] = (statistics.frm_cnt & 0x00ff0000) >> 16;
	_statictics[ 7] = (statistics.frm_cnt & 0xff000000) >> 24;
	_statictics[ 8] = (statistics.err_cnt & 0x000000ff);
	_statictics[ 9] = (statistics.err_cnt & 0x0000ff00) >> 8;
	_statictics[10] = (statistics.err_cnt & 0x00ff0000) >> 16;
	_statictics[11] = (statistics.err_cnt & 0xff000000) >> 24;
	_statictics[12] = (statistics.of_cnt  & 0x000000ff);
	_statictics[13] = (statistics.of_cnt  & 0x0000ff00) >> 8;
	_statictics[14] = (statistics.of_cnt  & 0x00ff0000) >> 16;
	_statictics[15] = (statistics.of_cnt  & 0xff000000) >> 24;
	_statictics[16] = statistics.stored_size;
	_statictics[17] = statistics.queue_size;

	*data = _statictics;
	*size = STATISTICS_SIZE;

	return SNI_RET_OK;
}

enum SNI_RET sni_rs_cmd_spi_w(struct sni_rs_ctrl *ctrl, uint8_t *data, size_t size)
{
	enum SNI_RET ret;

	if (sni_rs_ctrl_check_state(ctrl, SNI_STATE_INIT)) {
		return SNI_RET_ESTATE;
	}

	ret = sni_rs_spi_data_transmit_only(ctrl->_rf_spi1, data, size);

	return ret;
}

enum SNI_RET sni_rs_cmd_spi_r(struct sni_rs_ctrl *ctrl, uint8_t *wdata, uint8_t *rdata, size_t size)
{
	enum SNI_RET ret;

	if (sni_rs_ctrl_check_state(ctrl, SNI_STATE_INIT)) {
		return SNI_RET_ESTATE;
	}

	ret = sni_rs_spi_data_transmit(ctrl->_rf_spi1, wdata, rdata, size);

	return ret;
}

enum SNI_RET sni_rs_cmd_write_sram(struct sni_rs_ctrl *ctrl, uint8_t *data, size_t size)
{
	enum SNI_RET ret;
	const uint32_t addr = REG_SRAM_SEQ;
	ret = sni_rs_spi_reg_write8(ctrl->_rf_spi1, addr, data, size);

	return ret;
}

enum SNI_RET sni_rs_cmd_spi_opecode(struct sni_rs_ctrl *ctrl, uint8_t op)
{
	enum SNI_RET ret;

	ret = sni_rs_spi_resetctl(ctrl->_rf_spi1, op);
	if(ret) {
		return ret;
	}

	return SNI_RET_OK;
}

enum SNI_RET sni_rs_cmd_pass_through(struct sni_rs_ctrl *ctrl, uint8_t *wdata, size_t wsize, uint8_t *rdata, size_t *rsize)
{
	enum SNI_RET ret;

	ret = sni_rs_pass_through(ctrl, wdata, wsize, rdata, rsize);
	if(ret) {
		return ret;
	}

	return SNI_RET_OK;
}
